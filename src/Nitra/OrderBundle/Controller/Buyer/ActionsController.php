<?php
namespace Nitra\OrderBundle\Controller\Buyer;

use Admingenerated\NitraOrderBundle\BaseBuyerController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ActionsController extends BaseActionsController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * автокомплит покупателя по имени
     * @Route("/autocomplete-name", name="Nitra_OrderBundle_Buyer_Name_Autocomplete")
     * @return JsonResponse
     */
    public function nameAutocompleteAction()
    {
        // получить значение для поиска
        $searchValue = trim($this->getRequest()->get('term'));
        if (!$searchValue) {
            return new JsonResponse();
        }
        
        // запрос получения покупателей 
        $buyers = $this->em->getRepository('NitraOrderBundle:Buyer')
            ->createQueryBuilder('b');
        
        // получить массив слов поиска
        $words = preg_split('/\s[\s]*|\'+/', $searchValue);
        foreach($words as $word) {
            // поиск покупателя по имени
            $buyers->orWhere('b.name LIKE :name')->setParameter('name', '%'.$word.'%');
        }
        
        // получить покупателей
        $buyers = $buyers
            ->setMaxResults(10)
            ->orderBy('b.name', 'asc')
            ->getQuery()
            ->getArrayResult();
        
        // результирующий массив
        $result = array();
        foreach($buyers as $buyer) {
            $result[] = array(
                'value' => $buyer['id'],
                'label' => $buyer['name'],
            );
        }
        
        // вернуть результирующий массив
        return new JsonResponse($result);
    }
    
    /**
     * автокомплит покупателя по телефону
     * @Route("/autocomplete-phone", name="Nitra_OrderBundle_Buyer_Phone_Autocomplete")
     * @return JsonResponse
     */
    public function phoneAutocompleteAction()
    {
        // получить значение для поиска
        $searchValue = trim($this->getRequest()->get('term'));
        if (!$searchValue) {
            return new JsonResponse();
        }
        
        // получить покупателей 
        $buyers = $this->em->getRepository('NitraOrderBundle:Buyer')
            ->createQueryBuilder('b')
            ->where('b.phone LIKE :phone')->setParameter('phone', '%'.$searchValue.'%')
            ->setMaxResults(10)
            ->orderBy('b.name', 'asc')
            ->getQuery()
            ->getArrayResult();
        
        // результирующий массив
        $result = array();
        foreach($buyers as $buyer) {
            $result[] = array(
                'value' => $buyer['id'],
                'label' => $buyer['phone'],
            );
        }
        
        // вернуть результирующий массив
        return new JsonResponse($result);
    }
    
}

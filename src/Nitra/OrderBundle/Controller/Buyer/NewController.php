<?php

namespace Nitra\OrderBundle\Controller\Buyer;

use Admingenerated\NitraOrderBundle\BaseBuyerController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    public function indexAction() 
    {
        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
    }
}

<?php

namespace Nitra\OrderBundle\Controller\Buyer;

use Admingenerated\NitraOrderBundle\BaseBuyerController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;
use JMS\DiExtraBundle\Annotation as DI;

class ListController extends BaseListController
{

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**
     * @var Pagerfanta\Pagerfanta 
     */
    private $paginator;

    protected function getPager()
    {

        // проверить инициализацию $this->paginator
        if ($this->paginator !== null) {
            return $this->paginator;
        }

        $paginator = parent::getPager();

        // инициализировать $paginator для $this
        // для предотвращения выполнения повторныз запросов 
        // при вызове getAdditionalRenderParameters
        $this->paginator = $paginator;

        // вернуть $paginator
        return $this->paginator;
    }

    protected function getAdditionalRenderParameters()
    {

        // массив orderEntries
        $orderEntries = array();
        //бежим по результатам pager
        foreach ($this->getPager() as $buyers) {
            $results[$buyers->getId()] = $this->getOeBuyer($buyers->getId(), $max = 4);
        }
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // массив позиций заказов
            'orderEntries' => $results,
        );
    }

    /**            buyerOrdersAction
     * заказы покупателя
     * @Route("/buyer-orders", name="Nitra_OrderBundle_Buyer_Orders")
     * @return JsonResponse
     */
    public function buyerOrdersAction()
    {
        //Получаем айди покупателя
        $buyer_id = $this->getRequest()->get('buyer');

        //Если нет id покупателя или не ajax запрос отправляем на buyer list
        if (!$buyer_id || !$this->getRequest()->isXmlHttpRequest()) {
            return null;
        }

        //Получаем обект покупателя          
        $orderEntries = $this->getOeBuyer($buyer_id);


        return $this->render('NitraOrderBundle:BuyerList:row.ajax.column.orderEntries.html.twig', array(
                    'orderEntries' => $orderEntries,
        ));
    }

    public function getOeBuyer($buyer_id, $max = null)
    {

        $query = $this->em->createQueryBuilder();
        $query->select('oe')
                ->from('NitraOrderBundle:OrderEntry', 'oe')
                ->innerJoin('oe.order', 'o')
                ->innerJoin('o.buyer', 'b')
                ->where('b.id =:buyer_id')
                ->setParameter('buyer_id', $buyer_id)
                ->orderBy('oe.id', 'desc'); //От новых к старым

        if ($max) {
            $query->setMaxResults($max);
        }

            $orderEntries = $query->getQuery()->getResult();
        return $orderEntries;
    }

}

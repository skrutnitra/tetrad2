<?php

namespace Nitra\OrderBundle\Controller\Order;

use Admingenerator\GeneratorBundle\Controller\Doctrine\BaseController as BaseController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query;
use Nitra\OrderBundle\Entity\Buyer;

/**
 * SiteController
 * Контроллер получения данных сайтом
 */
class SiteController extends BaseController
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @DI\Inject("doctrine_mongodb.odm.document_manager")
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;
    
    /**
     * получить первый склад на котором есть продукт
     * @Route("/get-closest-warehouse-for-product", name="get_closest_warehouse_for_product")
     */
    public function getClosestWarehouseForProduct(Request $request)
    {
        
        // получить продукт
        $product = $this->dm
            ->getRepository('NitraMainBundle:Product')
            ->find($request->get('productId', false));
        
        // Проверить продукт
        if (!$product) {
            // вернуть пустой результат
            return new JsonResponse();
        }
        
        // получаем id своих складов
        $ownWarehouseIds = $this->em
            ->getRepository('NitraMainBundle:Warehouse')
            ->buildQueryOwnWarehouses()
            ->select('q.id, q.id')
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // если есть свои склады 
        if ($ownWarehouseIds) {
            // получаем стоки складов
            $ownStocks = $this->dm
                ->createQueryBuilder('NitraMainBundle:Stock')
                ->field('productId')->equals($product->getId())
                // проверяем склад по array_keys потмоу что 
                // в проитвном случае монго воспринимает 
                // key как integer value как string
                // и отдает не праивльный результат
                ->field('warehouseId')->in(array_keys($ownWarehouseIds))
                // получаем стоки свободные для заказа 
                ->where("function() { return (this.quantity-this.reserved) > 0; }")
                ->getQuery()
                ->execute();
            
            // проверить стоки 
            if ($ownStocks->count()) {
                
                // получить склад первого стока
                $ownWarehouse = $this->em
                    ->getRepository('NitraMainBundle:Warehouse')
                    ->find($ownStocks->getSingleResult()->getWarehouseId());
                
                // вернуть склад
                return new JsonResponse(array(
                    'id' => $ownWarehouse->getId(),
                    'businessKey' => $ownWarehouse->getBusinessKey(),
                    'deliveryId' => ($ownWarehouse->getDelivery()) ? $ownWarehouse->getDelivery()->getId() : false,
                    'supplierId' => ($ownWarehouse->getSupplier()) ? $ownWarehouse->getSupplier()->getId() : false,
                    'address' => $ownWarehouse->getAddress(),
                    'cityId' => ($ownWarehouse->getCity()) ? $ownWarehouse->getCity()->getId() : false,
                    'cityBusinessKey' => ($ownWarehouse->getCity()) ? $ownWarehouse->getCity()->getBusinessKey() : false,
                ));
            }
        }
        
        
        // получаем id складов ТК
        $tkWarehouseIds = $this->em
            ->getRepository('NitraMainBundle:Warehouse')
            ->buildQueryDeliveryWarehouses()
            ->select('q.id, q.id')
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // если есть склады 
        if ($tkWarehouseIds) {
            // получаем стоки складов
            $tkStocks = $this->dm
                ->createQueryBuilder('NitraMainBundle:Stock')
                ->field('productId')->equals($product->getId())
                // проверяем склад по array_keys потмоу что 
                // в проитвном случае монго воспринимает 
                // key как integer value как string
                // и отдает не праивльный результат
                ->field('warehouseId')->in(array_keys($tkWarehouseIds))
                // получаем стоки свободные для заказа 
                ->where("function() { return (this.quantity-this.reserved) > 0; }")
                ->getQuery()
                ->execute();
            
            // проверить стоки 
            if ($tkStocks->count()) {
                
                // получить склад первого стока
                $tkWarehouse = $this->em
                    ->getRepository('NitraMainBundle:Warehouse')
                    ->find($tkStocks->getSingleResult()->getWarehouseId());
                
                // вернуть склад
                return new JsonResponse(array(
                    'id' => $tkWarehouse->getId(),
                    'businessKey' => $tkWarehouse->getBusinessKey(),
                    'deliveryId' => ($tkWarehouse->getDelivery()) ? $tkWarehouse->getDelivery()->getId() : false,
                    'supplierId' => ($tkWarehouse->getSupplier()) ? $tkWarehouse->getSupplier()->getId() : false,
                    'address' => $tkWarehouse->getAddress(),
                    'cityId' => ($tkWarehouse->getCity()) ? $tkWarehouse->getCity()->getId() : false,
                    'cityBusinessKey' => ($tkWarehouse->getCity()) ? $tkWarehouse->getCity()->getBusinessKey() : false,
                ));
            }
        }
        
        
        // получаем id складов поставщиков
        $supWarehouseIds = $this->em
            ->getRepository('NitraMainBundle:Warehouse')
            ->buildQuerySupplierWarehouses()
            ->select('q.id, q.id')
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // если есть склады 
        if ($supWarehouseIds) {
            // получаем стоки складов
            $supStocks = $this->dm
                ->createQueryBuilder('NitraMainBundle:Stock')
                ->field('productId')->equals($product->getId())
                // проверяем склад по array_keys потмоу что 
                // в проитвном случае монго воспринимает 
                // key как integer value как string
                // и отдает не праивльный результат
                ->field('warehouseId')->in(array_keys($supWarehouseIds))
                // получаем стоки свободные для заказа 
                ->where("function() { return (this.quantity-this.reserved) > 0; }")
                ->getQuery()
                ->execute();
            
            // проверить стоки 
            if ($supStocks->count()) {
                
                // получить склад первого стока
                $supWarehouse = $this->em
                    ->getRepository('NitraMainBundle:Warehouse')
                    ->find($supStocks->getSingleResult()->getWarehouseId());
                
                // вернуть склад
                return new JsonResponse(array(
                    'id' => $supWarehouse->getId(),
                    'businessKey' => $supWarehouse->getBusinessKey(),
                    'deliveryId' => ($supWarehouse->getDelivery()) ? $supWarehouse->getDelivery()->getId() : false,
                    'supplierId' => ($supWarehouse->getSupplier()) ? $supWarehouse->getSupplier()->getId() : false,
                    'address' => $supWarehouse->getAddress(),
                    'cityId' => ($supWarehouse->getCity()) ? $supWarehouse->getCity()->getId() : false,
                    'cityBusinessKey' => ($supWarehouse->getCity()) ? $supWarehouse->getCity()->getBusinessKey() : false,
                ));
            }
        }
        
        
        // продукт не найден ни на одном из складов
        return new JsonResponse();
    }
    
    /**
     * @Route("/buyers-synchronizer", name="buyers_synchronizer")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function buyersSynchronizerAction(Request $request)
    {
        // если пришли пользователи - обновляем
        if ($request->request->has('buyers')) {
            $inserted = 0;
            $updated  = 0;
            $failed   = 0;
            
            // обойти всех покупателей
            foreach ($request->request->get('buyers') as $id => $buyer) {
                
                // получить покупателя по подвязке
                if (key_exists('tetradkaId', $buyer)) {
                    $Buyer = $this->em->find('NitraOrderBundle:Buyer', $buyer['tetradkaId']);
                }
                
                // получить покупателя по телефону
                if (!$Buyer && key_exists('phone', $buyer)) {
                    $Buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->findOneByPhone($buyer['phone']);
                }
                
                // покупатель не найден создать нового
                if (!$Buyer) {
                    $Buyer = new Buyer();
                    $this->em->persist($Buyer);
                }
                
                // установить данные покупателя
                $Buyer->setName($buyer['name']);
                
                // установить телефон
                if (key_exists('phone', $buyer)) {
                    $Buyer->setPhone($buyer['phone']);
                }
                
                // установить email
                if (key_exists('email', $buyer) && method_exists('\Nitra\OrderBundle\Entity\Buyer', 'setEmail')) {
                    $Buyer->setEmail($buyer['email']);
                }
                
                // обновить счетчики 
                if ($Buyer->getId()) {
                    // обновить счетчик обновленных покупателей
                    ++$updated;
                } else {
                    // обновить счетчик добавленных покупателей
                    ++$inserted;
                }

                // сохранить покупателя
                try {
                    $this->em->flush($Buyer);
                } catch (\Exception $ex) {
                    // обновить счетчик не обновленных покупателей
                    $failed ++;
                }
            }
            
            // результирующий массив
            $json = array(
                'inserted'  => $inserted,
                'updated'   => $updated,
                'failed'    => $failed,
            );
            
            // вернуть результирующий массив 
            return new JsonResponse($json);
        }
        
        // получить покупателей
        $email = method_exists('\Nitra\OrderBundle\Entity\Buyer', 'getEmail') ? ' b.email,' : null;
        $Buyers = $this->em->createQueryBuilder()
            ->from('NitraOrderBundle:Buyer', 'b')
            ->select("b.id, b.name, b.phone,$email o.address, c.id as city_id, c.name as city_name")
            ->leftJoin("NitraOrderBundle:Order", "o", "WITH", "b.id = o.buyer")
            ->leftJoin("NitraTetradkaGeoBundle:City", "c", "WITH", "c.id = o.city")
            ->orWhere('o.createdAt = (SELECT MAX(so.createdAt) FROM NitraOrderBundle:Order so WHERE so.buyer = b.id)')
            ->orWhere('o.createdAt IS NULL')
            ->getQuery()->execute(null, Query::HYDRATE_ARRAY);
        
        // результирующий массив покупателей
        $json   = array();
        foreach ($Buyers as $buyer) {
            $id = $buyer['id'];
            $json[$id] = $buyer;
        }
        
        // вернуть результирующий массив 
        return new JsonResponse($json);
    }

    /**
     * обновление пользователя, при редактировании им данных из личного кабинета
     * @Route("/update-buyer", name="update_buyer")
     */
    public function updateBuyer(Request $request)
    {
        $data   = $request->request->all();
        $Buyer  = $this->em->find('NitraOrderBundle:Buyer', $data['id']);
        $return = array();
        
        if ($Buyer) {
            $Buyer->setName($data['name']);
            $Buyer->setPhone($data['phone']);
            if (method_exists('\Nitra\OrderBundle\Entity\Buyer', 'setEmail')) {
                $Buyer->setEmail($data['email']);
            }
            $this->em->flush($Buyer);
            $return['status'] = 'success';
        } else {
            $return['status'] = 'error';
        }
        
        return new JsonResponse($return);
    }
}
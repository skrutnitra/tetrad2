<?php

namespace Nitra\OrderBundle\Controller\Order;

use Admingenerated\NitraOrderBundle\BaseOrderController\ListController as BaseListController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\OrderBundle\Entity\Order;
use Nitra\MainBundle\Document\Product;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\OrderBundle\Form\Type\Order\OrderCommentType;
use Nitra\OrderBundle\Form\Type\Order\FiltersType;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ListController extends BaseListController
{

    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @DI\Inject("doctrine_mongodb.odm.document_manager")
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;
    
    public function getMovementFormFilter()
    {
        $CityChoices = array();

        $CityMovement = $this->em->getRepository('NitraTetradkaGeoBundle:City')->findBy(array(), array('name' => 'ASC'));

        // Формируем  массив с городов
        foreach ($CityMovement as $city) {
            $CityChoices[$city->getId()] = (string) $city;
        }
        /**
         * @var $from , $to - стандартные значения для виджета Даты (сегдняшний день)
         */
        $from = new \DateTime;
        $to = new \DateTime;
        $form = $this->createFormBuilder()
                ->add('movementAt', 'date_range', array(
                    'required' => true,
                    'format' => 'd MMM y',
                    'label' => 'Дата')
                )
                ->add('city', 'choice', array(
                    'attr' => array(
                        'class' => 'select2',
                    ),
                    'multiple' => false,
                    'required' => true,
                    'choices' => $CityChoices,
                    'preferred_choices' => array('5'),
                    'label' => 'Город', 'translation_domain' => 'Admin',
                ))
                ->add('send', 'submit', array('label' => 'Cкачать'))
                ->setData(array('movementAt' => array('from' => $from, 'to' => $to)))
                ->getForm();

        return $form;
    }

    /**
     * Шаблон выбора даты и города (в клорбоксе) для формирования маршрутного листа 
     * @Route("/movement-list", name="Nitra_OrderBundle_Order_movement_list")
     */
    public function movementList()
    {

        $form = $this->getMovementFormFilter();

        return $this->render('NitraOrderBundle:OrderList:movement.list.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * Формирование Excel маршрутного листа 
     * @Route("/movement-excel", name="Nitra_OrderBundle_Order_save_movement_xls")
     */
    public function saveMovementExcelList()
    {

        $form = $this->getMovementFormFilter();

        $form->bind($this->get('request'));
        //Обьект даты
        $date = $form->get('movementAt')->getData();
        //Id города
        $city_id = $form->get('city')->getData();

        //Если нет даты или id города
        if (!isset($date) || !isset($city_id)) {
            return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_list"));
        }
        //Получаем формат даты
        $dateFrom = $date['from']->format('Y-m-d');
        $dateTo = $date['to']->format('Y-m-d');

        //Запрос на выборку Order по дате доставки и городу
        $repository = $this->em->getRepository('NitraOrderBundle:Order');
        $query = $repository->createQueryBuilder('o')
                ->select('o')
                ->innerJoin('o.warehouse', 'w')
                ->where('o.deliveryDate >=:dateFrom AND o.deliveryDate <=:dateTo')
                ->andWhere('o.city =:city')
                //только для адрессной доставки
                ->andWhere('o.isDirect = 1')
                //отгрузка со склада магазина
                ->andWhere('w.delivery IS NULL AND w.supplier IS NULL')
                ->setParameters(array(
                    'dateFrom' => $dateFrom,
                    'dateTo' => $dateTo,
                    'city' => $city_id
                ))
                ->getQuery();

        $results = $query->getResult();

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();



        // Set document properties
        $objPHPExcel->getProperties()->setCreator("tetradka")
                ->setLastModifiedBy("tetradka")
                ->setTitle("Movement List(" . $dateFrom . ' - ' . $dateTo . ')')
                ->setSubject("Movement");


//              номер страницы
        $sheetIndex = 0;

//      Если результат не пустой
        if ($results) {

//                Бежим по заказам
            foreach ($results as $result) {

//                    Создаем лист
                if ($sheetIndex != 0) {
                    $objPHPExcel->createSheet($sheetIndex);
                }
//                    Название страницы
                $objPHPExcel->getSheet($sheetIndex)->setTitle('Маршрутный лист_' . $sheetIndex);

//                  Border  Style 
                $styleHeirBorders = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_HAIR
                        )
                    )
                );

                $styleBorders = array(
                    'borders' => array(
                        'top' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THICK,
                        ),
                        'right' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THICK,
                        ),
                        'bottom' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THICK,
                        ),
                        'left' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THICK,
                        ),
                    )
                );



                //                  Рабочая страница
                $sheet = $objPHPExcel->setActiveSheetIndex($sheetIndex);


                //                    Вырвниваем Номер Заказа
                $sheet->getStyle('A2')
                        ->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('A2')->getFont()->setBold(true);

                //                    Выравниваем цену по правому краю
                $sheet->getStyle('E8:F256')
                        ->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                //                    Выравниваем дату доставки
                $sheet->getStyle('E3:E5')
                        ->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                //                    Вырвниваем шапки товаров
                $sheet->getStyle('A7:F7')
                        ->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



                //                  ширина клонок                    
                $sheet->getColumnDimension('A')->setWidth(15);
                $sheet->getColumnDimension('D')->setWidth(20);
                $sheet->getColumnDimension('E')->setWidth(15);
                $sheet->getColumnDimension('F')->setWidth(15);


//                    merge data
                $sheet->mergeCells('B3:C3') // Фио
                        ->mergeCells('B4:C4') // Телефон
                        ->mergeCells('B5:D5') // Адрес
                        ->mergeCells('E3:F3') // текст( дата дотсавки )
                        ->mergeCells('E4:F4') // дата доставки
                        ->mergeCells('A2:B2') // дата доставки
                        ->mergeCells('A7:C7') // Навзание товара
                        ->mergeCells('A6:F6') // пустая сторока
                        ->mergeCells('A2:F2'); // Лист доставки

                $sheet->setCellValue('A2', 'Лист доставки № ' . $result->getId()) // Номер заказа
//                            ФИО
                        ->setCellValue('A3', 'Фио:')
                        ->setCellValue('B3', $result->getBuyer()->getName())
//                            Дата доставки
                        ->setCellValue('E3', 'Дата доставки:')
                        ->setCellValue('E4', $result->getDeliveryDate()->format('Y-m-d'))
//                            Телефон
                        ->setCellValue('A4', 'Телефон:')
                        ->setCellValue('B4', $result->getBuyer()->getPhone())
//                            Адрес доставки
                        ->setCellValue('A5', 'Адрес:')
                        ->setCellValue('B5', $result->getAddress())
//                            товар
                        ->setCellValue('A7', 'Название товара')
                        ->setCellValue('D7', 'Количество, шт.')
                        ->setCellValue('E7', 'Цена, грн.')
                        ->setCellValue('F7', 'Сумма, грн.');


//                    Всего к оплате
                $total = 0;
//                    Номер строки для товаров
                $rows = 8;

//                    Бежим по заказными товарам
                foreach ($result->getOrderEntries() as $oe) {
//                            сумма = цена * количество
                    $summ = $oe->getPriceOut() * $oe->getQuantity();
//                            Формируем "Всего к оплате"
                    $total += $summ;
//                            Информация о товаре
                    $sheet
//                                    Для названия товара
                            ->mergeCells('A' . $rows . ':C' . $rows)
//                                    название товара
                            ->setCellValue('A' . $rows, (string) $oe->getProduct())
//                                    количество
                            ->setCellValue('D' . $rows, $oe->getQuantity())
//                                    цена за шт
                            ->setCellValue('E' . $rows, number_format($oe->getPriceOut(), 2, ',', ' '))
//                                    сумма
                            ->setCellValue('F' . $rows, number_format($summ, 2, ',', ' '));
//                            Переходим к следующей строке
                    $rows++;
                }


//                    Вырвниваем количество
                $sheet->getStyle('D8:D' . $rows)
                        ->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//                  Выравниваем стоимость доставки и цену
                $sheet->getStyle('A' . $rows . ':A' . ($rows + 1))
                        ->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


//                    стоимость доставки
                $sheet->mergeCells('A' . $rows . ':' . 'E' . $rows)
                        ->setCellValue('A' . $rows, 'Стоимость доставки:')
                        ->setCellValue('F' . $rows, number_format($result->getDeliveryCost(), 2, ',', ' '));



//                   Плюсуем стоимость доставки
                $total += $result->getDeliveryCost();



//                  Переносим на следующую строку
                $rows = $rows + 1;
//                    Выводим конечную стоимость заказа                    
                $sheet->mergeCells('A' . $rows . ':' . 'E' . $rows)
                        ->setCellValue('A' . $rows, 'Всего:')
                        ->setCellValue('F' . $rows, number_format($total, 2, ',', ' '));

                $sheet->getStyle('A' . $rows . ':F' . $rows)->getFont()->setBold(true);

                //                    Borders
                //                    product border
                $sheet->getStyle('A7:F' . $rows)->applyFromArray($styleHeirBorders);

                //                    outline border
                //                  Добавляем еще строку для отсутпа внизу
                $rows = $rows + 1;
                $sheet->getStyle('A1:F' . $rows)->applyFromArray($styleBorders);
                //                    
                //                    Устанавлеваем текстовый формат ячеек
                $sheet->getStyle('A1:Z26')
                        ->getNumberFormat()
                        ->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);

                $sheetIndex++;
            }
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:F1')
                    ->setCellValue('A1', 'Нет данных за период с ' . $dateFrom . '. По - ' . $dateTo);
        }

        // Set active sheet index to the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="MovementList(' . $dateFrom . ' - ' . $dateTo . ').xls"');
        header('Cache-Control: max-age=0');
        //create the response
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    /**
     * запрос получения списка позиций
     * @return Doctrine\ORM\QueryBuilder
     */
    protected function buildQuery()
    {

        // получить запрос родителем
        $query = parent::buildQuery();

        // обновить запрос получения списка 
        $query
                ->addSelect('q.id AS orderId') // алиас orderId необходим для контроллера $this->orderRenderTrAction
        ;

        // вернуть запрос получения списка
        return $query;
    }

    /**
     * получить массив дополнительных параметров передаваемых в шаблон
     * @return array
     */
    public function getAdditionalRenderParameters()
    {

        // объект формы добавления комментариев
        $commentForm = $this->createForm(new OrderCommentType($this->em));

        // вернуть массив параметров передаваемых в шаблон
        return array(
            // форма добавления коментариев
            'commentForm' => $commentForm->createView(),
        );
    }

    /**
     * Отображение строки заказа
     * @Route("/{pk}-render-tr", name="Nitra_OrderBundle_Order_Render_Tr", options={"expose"=true})
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderList:row.html.twig")
     */
    public function orderRenderTrAction(Order $order)
    {

        // массив доступных цепочек по каждой позиции заказа
        $orderEntryAllowedStatuses = array();
        foreach ($order->getOrderEntries() as $orderEntry) {
            // получить доступные цепочки для позиции заказа
            $orderEntryAllowedStatuses[$orderEntry->getId()] = $this->em->getRepository('NitraOrderBundle:OrderEntry')->getAllowedStatus($orderEntry);
        }

        // массив доступных цепочек для заказа
        $orderAllowedStatuses = $this->em->getRepository('NitraOrderBundle:Order')->getAllowedStatus($order);

////        // расходы по заказу
//        $orderExpense = array();
//        foreach ($this->getPager() as $order) {
//            $orderExpense = $this->em->createQueryBuilder()
//                ->select('e')
//                ->from('NlExpenseBundle:Expense', 'e')
//                ->where('e.order = :order_id')
//                ->setParameter('order_id', $order->getId())
//                ->getQuery()
//                ->getResult();
//            $orderExpense[$order->getId()] = 0;
//            foreach ($orderExpense as $expense) {
//                $orderExpense[$order->getId()] += $expense->getAmount();
//            }
//        }
        // вернуть массив данных передаваемых в шаблон
        return array(
            // массив доступных цепочек для заказа
            'orderAllowedStatuses' => $orderAllowedStatuses,
            // массив доступных цепочек по каждой позиции заказа
            'orderEntryAllowedStatuses' => $orderEntryAllowedStatuses,
            // объект заказа
            'Order' => $order,
        );
    }

    /**
     * getFiltersType
     */
    protected function getFiltersType()
    {
        $type = new FiltersType($this->em);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }

    /**
     * getFilters
     * Get filters from session
     * @return array
     */
    protected function getFilters()
    {

        // получить фильтры родителем класса
        $filters = parent::getFilters();

        // проверить фильр buyerName в EntityManager
        if (isset($filters['buyerName']) && $this->em->contains($filters['buyerName']) !== true) {
            $this->em->persist($filters['buyerName']);
        }

        // вернуть фильтры
        return $filters;
    }

    /**
     * processFilters
     * выполнить фильтр 
     * @param Doctrine\ORM\QueryBuilder $query объект запроса
     */
    protected function processFilters($query)
    {

        // выполнить фильтр родителя
        parent::processFilters($query);

        // получить объект запроса
        $queryFilter = $this->get('admingenerator.queryfilter.doctrine');
        $queryFilter->setQuery($query);

        // получить объект сессий
        $session = $this->get('session');
        // проверить глобальный фильтр отображать филиалы пользователя
        if ($session->has('myFilialsDisplay') && $session->get('myFilialsDisplay')) {
            $queryFilter->addCollectionFilter('filial', $session->get('myFilialsDisplay'));
        }

        // получить объект фильтров
        $filterObject = $this->getFilters();

        // фильтр по имени покупателя
        if (isset($filterObject['buyerName']) && $filterObject['buyerName']) {
            $query->andWhere('q.buyer = :buyerId');
            $query->setParameter('buyerId', $filterObject['buyerName']->getId());
        }
        
        // фильтр по телефону покупателя
        if (isset($filterObject['buyerPhone']) && $filterObject['buyerPhone']) {
            $query
                    ->innerJoin('q.buyer', 'b2')
                    ->andWhere('b2.phone LIKE :buyerPhone')
                    ->setParameter('buyerPhone', '%' . $filterObject['buyerPhone'] . '%');
        }

        // фильтр оплачен
        if (isset($filterObject['paid']) && $filterObject['paid']) {

            // проверить выбранный фильтр оплаты
            switch ($filterObject['paid']) {

                // оплачен полностью
                case 'full':
                    // подзапрос оплачено по заказу
                    $selectIncomes = "(SELECT SUM(i.amount) FROM NitraIncomeBundle:Income i WHERE q.id = i.order AND i.deletedAt IS NULL)";
                    $query->addSelect($selectIncomes . ' AS sumIncomes');

                    // подзапрос сумма оплаты по заказу
                    $selectPayTotal = "(SELECT SUM(oePay.priceOut*oePay.quantity) FROM NitraOrderBundle:OrderEntry oePay WHERE q.id = oePay.order AND oePay.deletedAt IS NULL)";
                    $query->addSelect($selectPayTotal . ' AS payTotal');

                    // оплачено равно к оплате
                    $query->having('sumIncomes = payTotal');
                    // break;
                    break;

                // оплачен частично
                case 'part':
                    // подзапрос оплачено по заказу
                    $selectIncomes = "(SELECT SUM(i.amount) FROM NitraIncomeBundle:Income i WHERE q.id = i.order)";
                    $query->addSelect($selectIncomes . ' AS sumIncomes');

                    // подзапрос сумма оплаты по заказу
                    $selectPayTotal = "(SELECT SUM(oePay.priceOut*oePay.quantity) FROM NitraOrderBundle:OrderEntry oePay WHERE q.id = oePay.order AND oePay.deletedAt IS NULL)";
                    $query->addSelect($selectPayTotal . ' AS payTotal');

                    // оплачено меньше чем всего к оплате
                    $query->having('sumIncomes < payTotal');
                    // break;
                    break;

                // не оплачен
                case 'no':
                    // добавить в основной запрос проверку сущестования оплаты
                    // проверяем нет ни одной оплаты
                    $query->andWhere('(NOT EXISTS (SELECT i.id FROM NitraIncomeBundle:Income i WHERE i.order = q.id AND i.deletedAt IS NULL))');
                    // break;
                    break;
            }
        }

        // фильтр название продукта
        if (isset($filterObject['productName']) && $filterObject['productName']) {
            // Исключаем лишние пробелы
            $productName = preg_replace("/[[:blank:]]+/", ' ', $filterObject['productName']);

            // Если введено несколко фраз - разбиваем по пробелу
            // получить продукты
            $qb = $this->dm->createQueryBuilder('NitraMainBundle:Product')
                            ->hydrate(false)->select('_id');
            foreach (explode(' ', $productName) as $w) {
                $qb->addAnd(
                        $qb->expr()->field('fullNameForSearch')->equals(new \MongoRegex('/' . $w . '/i'))
                );
            }
            $productsIds = $qb->getQuery()->execute()->toArray();
            $ids = array_keys($productsIds);
            // добавить в основной запрос поиск позиций по продуктам
            if ($ids) {
                $query->andWhere('(EXISTS (
                    SELECT oeProduct.id 
                    FROM NitraOrderBundle:OrderEntry oeProduct 
                    WHERE oeProduct.order = q.id 
                        AND oeProduct.productId IN (:productIds) 
                        AND oeProduct.deletedAt IS NULL
                    ))');
                $query->setParameter('productIds', $ids);
            }
        }

        // фильтр статус позиции заказа
        if (isset($filterObject['orderEntryStatus']) && $filterObject['orderEntryStatus']) {
            // добавить в основной запрос проверку статуса позиции
            $query->andWhere('(EXISTS (
                SELECT oeStatus.id 
                FROM NitraOrderBundle:OrderEntry oeStatus 
                WHERE oeStatus.order = q.id 
                    AND oeStatus.orderEntryStatus = :orderEntryStatus 
                    AND oeStatus.deletedAt IS NULL
                ))');
            $query->setParameter('orderEntryStatus', $filterObject['orderEntryStatus']);
        }

//        // фильтр дата завершения заказа 
//        if (
//            // проверить фильтр дата завершения заказа from 
//            (
//            isset($filterObject['completedAt']) && 
//            isset($filterObject['completedAt']['from']) && 
//            $filterObject['completedAt']['from'] && 
//            $filterObject['completedAt']['from'] instanceof \DateTime
//            )
//            // или
//            || 
//            // проверить фильтр дата завершения заказа to 
//            (
//            isset($filterObject['completedAt']) && 
//            isset($filterObject['completedAt']['to']) && 
//            $filterObject['completedAt']['to'] && 
//            $filterObject['completedAt']['to'] instanceof \DateTime
//            )
//        ) {
//            // проверка фильтра прошла успешно 
//            
//        }
        // проверить фильтр дата завершения заказа from 
        // создать флаг наличия фильтра
        if (isset($filterObject['completedAt']) &&
                isset($filterObject['completedAt']['from']) &&
                $filterObject['completedAt']['from'] &&
                $filterObject['completedAt']['from'] instanceof \DateTime
        ) {
            // есть фильтр по дате завершения заказа from 
            $hasCmpletedAtFrom = true;
        } else {
            // нет фильтра по дате завершения заказа from 
            $hasCmpletedAtFrom = false;
        }

        // проверить фильтр дата завершения заказа to 
        // создать флаг наличия фильтра
        if (isset($filterObject['completedAt']) &&
                isset($filterObject['completedAt']['to']) &&
                $filterObject['completedAt']['to'] &&
                $filterObject['completedAt']['to'] instanceof \DateTime
        ) {
            // есть фильтр по дате завершения заказа to
            $hasCmpletedAtTo = true;
        } else {
            // нет фильтра по дате завершения заказа to
            $hasCmpletedAtTo = false;
        }

        // проверить наличие фильтра 
        // по дате завершения заказа 
        // если есть фильтр даты завершения заказа from или фильтр даты завершения заказа TO
        if ($hasCmpletedAtFrom || $hasCmpletedAtTo) {

            // добавить в основной запрос статусы заказов по каждому заказу
            $query->innerJoin('q.orderStatus', 'oStatus');

            // добавить в основной запрос условие заказ завершен
            $query->andWhere('q.orderStatus = oStatus.id');
            $query->andWhere('oStatus.methodName = :oStatusMethodName');
            $query->setParameter('oStatusMethodName', 'completed');

            // добавить фильтр дате завершения заказа from 
            if ($hasCmpletedAtFrom) {

                // получить объект DateTime
                $dt = $filterObject['completedAt']['from'];

                // добавить подзапрос проверку дата перевода в статус
                $query->andWhere('(EXISTS (
                    SELECT oStHstrFr.id 
                    FROM NitraOrderBundle:OrderStatusHistory oStHstrFr
                    WHERE oStHstrFr.order = q.id 
                        AND oStHstrFr.createdAt >= :oStHstrFrCreatedAt
                        AND oStHstrFr.toStatus = q.orderStatus
                        AND oStHstrFr.deletedAt IS NULL
                    ))');
                $query->setParameter('oStHstrFrCreatedAt', $dt->format('Y-m-d') . ' 00:00:00');
            }

            // добавить фильтр дате завершения заказа to
            if ($hasCmpletedAtTo) {

                // получить объект DateTime
                $dt = $filterObject['completedAt']['to'];

                // добавить подзапрос проверку дата перевода в статус
                $query->andWhere('(EXISTS (
                    SELECT oStHstrTo.id 
                    FROM NitraOrderBundle:OrderStatusHistory oStHstrTo
                    WHERE oStHstrTo.order = q.id 
                        AND oStHstrTo.createdAt <= :oStHstrToCreatedAt
                        AND oStHstrTo.toStatus = q.orderStatus
                        AND oStHstrTo.deletedAt IS NULL
                    ))');
                $query->setParameter('oStHstrToCreatedAt', $dt->format('Y-m-d') . ' 23:59:59');
            }
        }

        // фильтр склад с которого заказали позицию 
        if (isset($filterObject['warehouseIds']) && $filterObject['warehouseIds']) {
            // добавить в основной запрос поиск позиций по указанному складу
            $query->andWhere('(EXISTS (
                SELECT oeWarehouse.id 
                    FROM NitraOrderBundle:OrderEntry oeWarehouse
                    WHERE oeWarehouse.order = q.id 
                        AND oeWarehouse.warehouse IN (:oeWarehouseIds)
                        AND oeWarehouse.deletedAt IS NULL
                    ))');
            $query->setParameter('oeWarehouseIds', $filterObject['warehouseIds']);
        }
    }
}

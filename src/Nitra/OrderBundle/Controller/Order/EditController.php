<?php
namespace Nitra\OrderBundle\Controller\Order;

use Admingenerated\NitraOrderBundle\BaseOrderController\EditController as BaseEditController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\OrderBundle\Form\Type\Order\SearchProductFiltersType;
use Nitra\OrderBundle\Entity\Buyer;
use Nitra\OrderBundle\Form\Type\Order\EditType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Form;


class EditController extends BaseEditController
{
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * массив объектов позиций заказов для удаления 
     * @var array \Nitra\OrderBundle\Entity\OrderEntry
     */
    private $removeOrderEntries;
    
    /**
     * конструкток
     */
    public function __construct()
    {
        // обнулить массив объектов позиций заказов для удаления 
        $this->removeOrderEntries = array();
    }
     
    public function updateAction($pk)
    {
        $Order = $this->getObject($pk);

        if (!$Order) {
            throw new NotFoundHttpException("The Nitra\OrderBundle\Entity\Order with id $pk can't be found");
        }

        $this->preBindRequest($Order);
        $form = $this->createForm($this->getEditType(), $Order);
        $form->bind($this->get('request'));
        
        // дополнительная проверка вложеных форм
        $childrenIsValid = $this->isFormChildrenValid($form);

        if ($form->isValid() && $childrenIsValid) {
            try {
                
                $this->preSave($form, $Order);
                $this->saveObject($Order);
                $this->postSave($form, $Order);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_edit", array('pk' => $pk) ));

                        } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $Order);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('NitraOrderBundle:OrderEdit:index.html.twig', $this->getAdditionalRenderParameters($Order) + array(
            "Order" => $Order,
            "form" => $form->createView(),
        ));
    }
    
    protected function getEditType()
    {
        $type = new EditType($this->em);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    /**
     * Get additional parameters for rendering.
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param \Nitra\OrderBundle\Entity\Order $Order your \Nitra\OrderBundle\Entity\Order object
     * return array
     **/
    protected function getAdditionalRenderParameters(\Nitra\OrderBundle\Entity\Order $Order)
    {
        
        // объект формы поиска продуктов
        $filterSearchType = new SearchProductFiltersType($this->dm);
        $filterSearch = $this->createForm($filterSearchType);
        
        // получить тип формы
        $formType = $this->getEditType();
        
        // получть данные формы 
        $formData = $this->getRequest()->get($formType->getName());
        
        // массив названий продуктов
        $productNames = array();
        
        // массив ID продуктов 
        $productIds = array();
        
        // наполнить массив ID продуктов из форомы заказов
        if (isset($formData['orderEntries']) && $formData['orderEntries']) {
            foreach($formData['orderEntries'] as $orderEntry) {
                $productIds[] = $orderEntry['productId'];
            }
        }
        
        // наполнить массив ID продуктов из объекта заказа
        foreach($Order->getOrderEntries() as $entry) {
            $productIds[] = $entry->getProductId();
        }
        
        // получить список продуктов по массиву ID 
        $productIds = array_unique($productIds);
        if ($productIds) {
            
            // объект запроса список проудуктов
            $mongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
            $mongoQuery->field('id')->in($productIds);
            
            // получить продукты
            $products = $mongoQuery
                ->getQuery()
                ->execute();
            
            // наполнить массив имен продуктов
            if ($products) {
                foreach($products as $product ) {
                    $productNames[$product->getId()] = (string)$product;
                }
            }
        }
        
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // форма фильтрации
            'filterSearch' => $filterSearch->createView(),
            // массив ID продуктов 
            'orderEntriesProductIds' => $productIds,
            // массив названий продуктов
            'orderEntriesProductNames' => $productNames,
        );
    }
    
    /**
     * preBindRequest
     * @param \Nitra\OrderBundle\Entity\Order $Order your \Nitra\OrderBundle\Entity\Order object
     */
    public function preBindRequest(\Nitra\OrderBundle\Entity\Order $Order)
    {
        
        // наполнить массив позиций заказов до сохранения 
        $this->removeOrderEntries = array();
        foreach ($Order->getOrderEntries() as $orderEntry) {
            $this->removeOrderEntries[$orderEntry->getId()] = $orderEntry;
        }
        
        // получить тип формы
        $formType = $this->getEditType();
        
        // получть данные формы 
        $formData = $this->getRequest()->get($formType->getName());
        
        // получить пользователя 
        $buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->processBuyerOderForm($formData);
        
        // добавить покупателья в заказ
        $Order->setBuyer($buyer);
        
    }
    
    /**
     * preSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\OrderBundle\Entity\Order $Order your \Nitra\OrderBundle\Entity\Order object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\OrderBundle\Entity\Order $Order)
    {
        // массив данных формы
        $formData = $this->getRequest()->get($form->getName());
        
        // добавить комментарий к заказу
        if (trim($formData['commentAdd'])) {
            $Order->addComment($formData['commentAdd'], $this->getUser()->getUsername());
        }
        
        // обойти все позиции заказов которые пришли в форме
        foreach ($Order->getOrderEntries() as $orderEntry) {
            
            // проверить если новая позиция для заказа
            if (!$this->em->contains($orderEntry)) {
                // установить статус позиции по умолчанию
                $this->em->getRepository('NitraOrderBundle:OrderEntry')->setDefaultStatus($orderEntry);
                // persist $orderEntry
                $this->em->persist($orderEntry);
            }
            
            // Отсеиваем позиции, которые пришли в посте
            if (isset($this->removeOrderEntries[$orderEntry->getId()])) {
                // позиция которая сохраниться в заказе удаляется из массива позиций на удаление
                unset($this->removeOrderEntries[$orderEntry->getId()]);
            }
            
        }
        
        // получить статус заказа по умолчанию
        $orderDefaultStatus = $this->em->getRepository('NitraOrderBundle:Order')->getDefaultStatus();
        
        // проверить статус заказа
        if ($Order->getOrderStatus()->getId() == $orderDefaultStatus->getId()) {
            // Удаляем позиции из заказа
            foreach ($this->removeOrderEntries as $orderEntry) {
                $this->em->remove($orderEntry);
            }
        } 
        
        // установить город исходя из выбранного склада
        $city = $Order->getWarehouse()->getCity();
        $Order->setCity($city);
    }
    
    /**
     * выполняет валидацию вложеных форм, добавляет для отображения ошибки в формы
     * @param object Symfony\Component\Form\Form $form
     * @return true | false 
    **/
    private function isFormChildrenValid(Form $form)
    {
        // статическое кол-во ошибок формы, так как функция выполняется рекурсивно
        static $form_errors = 0;

        $validator = $this->container->get('validator');
        
        // рекурсивный вызов функции если форма содержит свои вложенные формы
        foreach($form->all() as $child) {
            if($child->all()) {
                $this->isFormChildrenValid($child);
            }
        }
        
        // валидация обьекта из формы формы
        $errors = $validator->validate($form->getData());
        
        // добавление в форму ошибок для отображения (только по полям указаным в форме)
        foreach($errors as $error) {
            foreach($form->all() as $formField) {
                if($error->getPropertyPath() === (string) $formField->getPropertyPath()) {
                    $formField->addError(new \Symfony\Component\Form\FormError($error->getMessage()));
                    $form_errors += 1;
                }
            }
        }

        return ($form_errors > 0) ? false : true;
        
    }
    
    /**
     * saveObject
     */
    protected function saveObject(\Nitra\OrderBundle\Entity\Order $order)
    {
        // сохранить родителем
        parent::saveObject($order);
        
        // валидировать заказ для перевода в статус ready
        $errorMessageOrderToReady = $this->em
            ->getRepository('NitraOrderBundle:Order')
            ->validCommonToReady($order);
            
        // валидация пройдена успешно 
        if ($errorMessageOrderToReady === false) {
            
            // установить статус ready
            $errorMessage = $this->em
                ->getRepository('NitraOrderBundle:Order')
                ->setStatusReady($order);
            
            // проверить рузельтат обновления статуса заказа
            if ($errorMessage) {
                // ошибка обновления статуса заказа
                throw new \Exception($errorMessage);
            }
            
            // сохранить 
            $this->em->flush();
        }
    }
    
}

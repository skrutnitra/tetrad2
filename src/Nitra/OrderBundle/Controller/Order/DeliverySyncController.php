<?php
namespace Nitra\OrderBundle\Controller\Order;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * DeliverySyncController
 */
class DeliverySyncController extends Controller
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * Ajax получение информации по доставке
     * @Route("/estimate-delivery", name="Nitra_OrderBundle_DeliverySync_EstimateDelivery", options={"expose"=true})
     */
    public function estimateDeliveryAction(Request $request)
    {
        
        // получить город получатель
        $toCity = $this->em->getRepository('NitraTetradkaGeoBundle:City')->find($request->get('toCityId', false));
        if (!$toCity) {
            // валидация не пройдена
            return new JsonResponse(array('type'=> 'error', 'message'=> 'Не найден город получатель.'));
        }
        
        // получить склад получатель
        $toWarehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($request->get('toWarehouseId', false));
        
        // проверить массив продуктов
        if (!$request->get('products', false) || !is_array($request->get('products', false))) {
            // валидация не пройдена
            return new JsonResponse(array('type'=> 'error', 'message'=> 'Не указан массив доставляемых продуктов.'));
        }
        
        // массив ID продуктов
        $productIds = array();
        foreach($request->get('products') as $product) {
            $productIds[] = $product['productId'];
        }
        
        // получить доставляемые продукты
        $productsMongo = $this->dm->createQueryBuilder('NitraMainBundle:Product')
            ->field('id')->in($productIds)
            ->getQuery()
            ->execute();
        
        // массив продуктов для быстрого обращения
        $products = array();
        foreach($productsMongo as $product) {
            $products[$product->getId()] = $product;
        }
        
        // массив достаялемых продуктов 
        // подготовить массив ля передачи в DS
        $estimateProducts = $request->get('products');
        foreach($estimateProducts as $key => $pr) {
            
            // получить продукт
            $product = $products[$pr['productId']];
            
            // наполнить массив доставляемых продуктов
            $estimateProducts[$key]['name'] = (string)$product;
            $estimateProducts[$key]['width'] = $product->getWidth();
            $estimateProducts[$key]['height'] = $product->getHeight();
            $estimateProducts[$key]['length'] = $product->getLength();
            $estimateProducts[$key]['weight'] = $product->getWeight();
        }
        
        // расчет стоимости доставки
        $estimateDeliveryCost = $this->apiSendRequest('estimateDeliveryCost', array(
            // ID города получателя
            'toCityId' => $toCity->getBusinessKey(),
            // если выбран склад ТК передаем его ID, расчет будет производится именно на указанный склад
            'toWarehouseId' => (($toWarehouse && $toWarehouse->isDelivery()) ? $toWarehouse->getBusinessKey() : false),
            // маасив доставляемых продуктов
            'products' => $estimateProducts,
        ));
        
        // проверить ответ расчет стоимости доставки
        if (!$estimateDeliveryCost instanceof \stdClass) {
            // валидация не пройдена
            return new JsonResponse(array('type'=> 'error', 'message'=> "Ответ не был получен от сервера."));
        }
        
        // ответ сервера ошибка
        if (isset($estimateDeliveryCost->type) && $estimateDeliveryCost->type == 'error') {
            // валидация не пройдена
            return new JsonResponse(array('type'=> 'error', 'message'=> $estimateDeliveryCost->message));
        }
        
        // результирующий массив 
        return new JsonResponse(array(
            'type' => 'success', 
            'estimateDelivery' => $estimateDeliveryCost,
            // html отображение результатов расчетов
            'html' => $this->renderView('NitraOrderBundle:OrderEdit:estimateDeliveryResult.html.twig', array(
                // результат расчетов преобразовать из stdСlass в array
                'estimateDelivery' => json_decode(json_encode($estimateDeliveryCost), true),
            )),
        ));
        
        // вернуть результирующий массив
        return new JsonResponse($result);
    }
    
    
    /**
     * Отправить запрос на сервер
     * @param string $command - название команды синхронизации
     * @param array  $options - массив параметров передаваемых на сервер DeliverySync
     * @return stdClass $apiResponse - ответ сервера
     */
    protected function apiSendRequest($command, array $options=null)
    {
        
        // команда синхронизации отправляемая на сервер
        $sendParams = array(
            'command' => $command,
            'options' => $options,
        );
        
//        // ссыка команды передаваемая в ds.nitralabs.com
//        print '<pre>'; print_r($sendParams); print '</pre>';
//        echo "<a href=\"".$this->container->getParameter('delivery_sync_api_url').'?'.http_build_query($sendParams)."\">SD</a>";
//        die;
        
        // отправить запрос на сервер 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->container->getParameter('delivery_sync_api_url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sendParams));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $apiResponse = curl_exec($ch);
        curl_close($ch);
        
        // вернуть ответ сервера
        return json_decode($apiResponse);
    }
    
    
}

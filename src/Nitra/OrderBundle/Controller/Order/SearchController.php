<?php
namespace Nitra\OrderBundle\Controller\Order;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nitra\OrderBundle\Form\Type\Order\SearchProductFiltersType;

/**
 * SearchController
 */
class SearchController extends Controller
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /** количество позиций в результате поиска */
    private $countOnPage = 20;
    
    /** значение имени продукта для поиска */
    private $productName;
    
    /** номер искомой страницы основного поиска (по продуктам) */
    private $numberPageMain;

    /** номер искомой страницы дополнительного поиска (по стокам) */
    private $numberPageStocks;

    /** номер искомой страницы дополнительного поиска (по стокам, не подвязанным к продуктами) */
    private $numberPageStocksNotBinded;
    
    /** id магазина */
    private $storeId;
    
    /** массив id уже найденных стоков для запроса */
    private $foundStocksId;

    /** 
     * массив данных найденных стоков 
     * array(stockId => \Nitra\MainBundle\Document\Stock)
    */
    private $stockData = array();
    
    /** 
     * массив данных найденных неподвязанных стоков (stock.productId = null)
     * array(stockId => \Nitra\MainBundle\Document\Stock)
    */
    private $stockDataNotBinded = array();
    
    /** 
     * массив данных найденных складов по стокам 
     * array(warehouseId => \Nitra\MainBundle\Entityt\Warehouse)
     */
    private $warehouseData = array();
    
    /** 
     * массив данных по найденным продуктам  
     * array(productId => \Nitra\MainBundle\Document\Product) 
     */
    private $productData = array();
    
    
    /**
     * поиск товаров 
     * @Route("/search-product-autocomplete", name="Nitra_OrderBundle_Order_Search_Product_Autocomplete")
     * @return mixed - если есть результаты для отображения 
     *                 то вернуть шаблон который будет отображен
     *                 иначе вернуть пустой контент
     */
    public function searchProductAutocompleteAction()
    {
        // получить тип формы
        $filterType = new SearchProductFiltersType($this->dm);
        
        // данные формы поиска
        $postData = $this->getRequest()->get($filterType->getName());
        
        // получить id магазина через филиал пользователя по умолчанию 
        $this->storeId = $this->getUser()->getDefaultFilial()->getStoreId();
            
        // Фраза поиска по продуктам
        $this->productName = trim($postData['name']);
        
        if ($this->productName) {
            // Исключаем лишние пробелы
            $this->productName = preg_replace("/[[:blank:]]+/", ' ', $this->productName);
            // Если введено несколко фраз - разбиваем по пробелу
            if (strstr($this->productName, ' ')) {
                // Поиск с учетом регистра всех вхождений
                $this->productName = '/(?=.*' . str_replace(' ', ')(?=.*', $this->productName) . ')/i';
            } else {
                $this->productName = '/' . $this->productName . '/i';
            }
        }
        
        $this->numberPageMain = isset($postData['numberPageMain']) && $postData['numberPageMain'] ? $postData['numberPageMain'] : 0;

        $this->numberPageStocks = isset($postData['numberPageStocks']) && $postData['numberPageStocks'] ? $postData['numberPageStocks'] : 0;

        $this->numberPageStocksNotBinded = isset($postData['numberPageStocksNotBinded']) && $postData['numberPageStocksNotBinded'] ? $postData['numberPageStocksNotBinded'] : 0;
        
        $this->foundStocksId = isset($postData['foundStocksId']) && $postData['foundStocksId'] ? explode(',', $postData['foundStocksId']) : array();
        
        // продолжить дополнительный поиск по стокам с неподвязанными продуктами
        if($this->numberPageStocksNotBinded) {
            $this->executeAdditionalSearchInNotBindedStocks();
        }
        // продолжить дополнительный поиск по стокам
        else if ($this->numberPageStocks) {
            $this->executeAdditionalSearchInStocks();
        }
        // начать/продолжить поиск по продуктам
        else {
            // объект QueryBuilder для поиска продуктов
            $productsMongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
            
            // фильтр по имени продукта
            if ($this->productName) {
                $productsMongoQuery->field('fullNameForSearch')->equals(new \MongoRegex($this->productName));
            }
            
            // фильтр по категории
            if (isset($postData['categoryId']) && $postData['categoryId']) {
                $objCat = new \MongoId($postData['categoryId']);
                $productsMongoQuery->field('category.$id')->equals($objCat);
            }
            
            // первый запрос на получение продуктов, по которым есть хотя бы один сток (isActive = true)
            // получить список товаров удовлетворяющих условиям поиска
            $productsMongo = $productsMongoQuery
                ->skip($this->numberPageMain * $this->countOnPage) // Смещение
                ->limit($this->countOnPage) // Отображаем только 20 строк
                ->field('isActive')->equals(true)
                ->sort('name', 'asc')
                ->getQuery()
                ->execute();
            
            $this->numberPageMain ++;
            
            // наполняем массив данных найденными продуктами
            foreach($productsMongo as $product) {
                $this->productData[$product->getId()] = $product;
            }

            // проверить найденные продукты 
            // наполнить массив стоков данными
            // наполнить массив складов
            if ($this->productData) {

                // массив ID складов стоков
                $warehouseIds = array();

                // объект QueryBuilder поиск стоков 
                $stockMongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Stock');

                // получить список стоков для найженных товаров
                $stockMongo = $stockMongoQuery
                    ->field('productId')->in(array_keys($this->productData))
                    ->sort('storePrice.' . $this->storeId, 'asc')
                    ->where("function() { return (this.quantity-this.reserved) > 0; }") // отображаем стоки свободные для заказа 
                    ->getQuery()
                    ->execute();

                // обойти стоки продуктов
                foreach($stockMongo as $stock) {
                    // наполнить массив стоков данными
                    $this->stockData[$stock->getId()] = $stock;
                    // наполнить массив ID складов
                    $warehouseIds[] = $stock->getWarehouseId();
                }

                // проверить массив складов стоков
                if ($warehouseIds) {
                    // получить склады стоков 
                    $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array('id' => $warehouseIds));
                    foreach($warehouses as $warehouse) {
                        $this->warehouseData[$warehouse->getId()] = $warehouse;
                    }
                }
            }
            
            $this->foundStocksId = array_merge($this->foundStocksId, array_keys($this->stockData));
            
            // если найденных продуктов меньше чем количество позиций в результате поиска (достигнута последняя страница), то делаем дополнительный поиск по стокам 
            if(count($this->productData) < $this->countOnPage) {
                $this->executeAdditionalSearchInStocks();
            }
        }
        
        // результирующий массив продуктов 
        $products = array();

        // проверить стоки продуктов
        // наполнить результируюший массив
        foreach ($this->stockData as $stockId => $stock) {
            
            // проверить продукт
            if (!isset($this->productData[$stock->getProductId()])) {
                // продукт не найден перейти к след. итерации цикла
                continue;
            }
            
            // получить объект продукта из массива данных продуктов
            $product = $this->productData[$stock->getProductId()];
            
            // проверить продукт в результирующем массиве
            if (!isset($products[$product->getId()])) {
                // создать даныне в результирующем массиве 
                $products[$product->getId()] = array(
                    'id' => $product->getId(),
                    'name' => (string)$product,
                    'model' => $product->getModel(),
                    'article' => $product->getArticle(),
                    'isActive' => $product->getIsActive(),
                    'isSpecial' => $product->getIsSpecial(),
                    'stocks' => array(),
                );
            }
            
            // проверить склад
            if (!isset($this->warehouseData[$stock->getWarehouseId()])) {
                // склад не найден перейти к след. итерации цикла
                continue;
            }
            
            // получить объект склада из массива данных складов стоков
            $warehouse = $this->warehouseData[$stock->getWarehouseId()];
            
            // наполнить стоками результирующий массив
            $products[$product->getId()]['stocks'][$stockId] = array(
                'id' => $stockId,
                'supplierId'   => ($warehouse->isSupplier()) ? $warehouse->getSupplier()->getId() : '',
                'supplierName' => ($warehouse->isSupplier()) ? (string) $warehouse->getSupplier() : '',
                'deliveryId'   => ($warehouse->isDelivery()) ? $warehouse->getDelivery()->getId() : '',
                'deliveryName' => ($warehouse->isDelivery()) ? (string) $warehouse->getDelivery() : '',
                'currency' => $stock->getCurrency(),
                'currencyPrice' => $stock->getCurrencyPrice(),
                'priceIn' => $stock->getPriceIn(),
                'storePrice' => $stock->getStorePriceOut($this->storeId),
                'priceName' => $stock->getPriceName(),
                'productId' => $stock->getProductId(),
                'warehouseId' => $warehouse->getId(),
                'warehouseName' => (string) $warehouse,
                'warehouseBusinessKey' => (string) $warehouse->getBusinessKey(),
                'cityId' => $warehouse->getCity()->getId(),
                'cityName' => (string)$warehouse->getCity(),
                'cityBusinessKey' => (string)$warehouse->getCity()->getBusinessKey(),
                'quantity' => $stock->getQuantityFree(),
                'stockParams' => $stock->getStockParams(),
            );
        }
        
        // результирующий массив неподвязанных стоков 
        $stocksNotBinded = array();

        // проверить неподвязанные стоки (stock.productId = null)
        // наполнить результируюший массив
        foreach ($this->stockDataNotBinded as $stockId => $stock) {

            // получить объект склада из массива данных складов стоков
            if (isset($this->warehouseData[$stock->getWarehouseId()])) {
                // полчить склад
                $warehouse = $this->warehouseData[$stock->getWarehouseId()];
                
            } else {
                // склад не найден
                $warehouse = false;
            }

            // наполнить стоками результирующий массив
            $stocksNotBinded[$stockId] = array(
                'supplierName' => ($warehouse && $warehouse->isSupplier()) ? (string) $warehouse->getSupplier() : '',
                'deliveryName' => ($warehouse && $warehouse->isDelivery()) ? (string) $warehouse->getDelivery() : '',
                'currency' => $stock->getCurrency(),
                'priceIn' => $stock->getPriceIn(),
                'storePrice' => $stock->getStorePriceOut($this->storeId),
                'priceName' => $stock->getPriceName(),
                'warehouseName' => ($warehouse) ? (string)$warehouse : "",
                'warehouseBusinessKey' => ($warehouse) ? (string)$warehouse->getBusinessKey() : "",
                'cityId' => ($warehouse) ? $warehouse->getCity()->getId() : "",
                'cityName' => ($warehouse) ? (string)$warehouse->getCity() : "",
                'cityBusinessKey' => ($warehouse) ? (string)$warehouse->getCity()->getBusinessKey() : "",
                'quantity' => $stock->getQuantityFree(),
                'stockParams' => $stock->getStockParams(),
            );
        }
        
        // проверить
        // если есть результаты для отображения 
        if ($products || $stocksNotBinded) {
            // вернуть шаблон отображения
            return $this->render('NitraOrderBundle:OrderEdit:searchProduct.html.twig', array(
                'products'                  => $products,
                'stocksNotBinded'           => $stocksNotBinded,
                'foundStocksId'             => implode(',', $this->foundStocksId),
                'numberPageMain'            => $this->numberPageMain,
                'numberPageStocks'          => $this->numberPageStocks,
                'numberPageStocksNotBinded' => $this->numberPageStocksNotBinded,
            ));
        }
        
        // иначе вернуть пустой контент
        return new Response();
    }
    
    /**
     * дополнительный поиск по стокам
     */
    private function executeAdditionalSearchInStocks() 
    {
        // количество найденных стоков
        $stocksCount = 0;
        
        // массив ID складов стоков
        $warehouseIds = array();

        // массив ID продуктов стоков
        $productIds = array();

        // объект QueryBuilder поиск стоков 
        $stockMongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Stock');

        // поиск по имени
        if($this->productName) {
            $stockMongoQuery->field('priceName')->equals(new \MongoRegex($this->productName));
        }

        $stockMongo = $stockMongoQuery
            ->skip($this->numberPageStocks * $this->countOnPage) // Смещение
            ->limit($this->numberPageStocks) // Отображаем только 20 строк
            ->field('productId')->notEqual(null)
            ->field('stockId')->notIn($this->foundStocksId)
            ->sort('storePrice.' . $this->storeId, 'asc')
            ->where("function() { return (this.quantity-this.reserved) > 0; }") // отображаем стоки свободные для заказа 
            ->getQuery()
            ->execute();

        // дополнить стоки продуктов
        foreach($stockMongo as $stock) {
            // увеличиваем кол-во найденных стоков
            $stocksCount++;
            // дополнить массив стоков данными
            $this->stockData[$stock->getId()] = $stock;
            // дополнить массив ID складов
            if(!isset($this->warehouseData[$stock->getWarehouseId()])) {
                $warehouseIds[] = $stock->getWarehouseId();
            }
            // дополнить массив ID продуктов
            if(!isset($this->productData[$stock->getProductId()])) {
                $productIds[] = $stock->getProductId();
            }
        }

        // проверить массив складов стоков
        if ($warehouseIds) {
            // получить склады стоков 
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array('id' => $warehouseIds));
            // дополнить массив складов данными
            foreach($warehouses as $warehouse) {
                $this->warehouseData[$warehouse->getId()] = $warehouse;
            }
        }

        // проверить массив продуктов стоков
        if ($productIds) {
            // получить продукты стоков 
            $products = $this->dm
                ->createQueryBuilder('NitraMainBundle:Product')
                ->field('_id')->in($productIds)
                ->getQuery()
                ->execute();
            // дополнить массив продуктов данными
            foreach($products as $product) {
                $this->productData[$product->getId()] = $product;
            }
        }  
        
        // если найденных стоков меньше чем количество для отображения в результате поиска, то делаем дополнительный поиск 
        // по стокам с не подвязанными товарами (достигнута последняя страница)
        if($stocksCount < $this->countOnPage) {
            $this->executeAdditionalSearchInNotBindedStocks();
        }
        
        $this->numberPageStocks ++;
    }
    
    /**
     * дополнительный поиск по стокам c неподвязанными товарами
     */
    private function executeAdditionalSearchInNotBindedStocks() 
    {
        // массив ID складов стоков
        $warehouseIds = array();

        // объект QueryBuilder поиск стоков 
        $stockMongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Stock');

        if($this->productName) {
            $stockMongoQuery->field('priceName')->equals(new \MongoRegex($this->productName));
        }

        $stockMongo = $stockMongoQuery
            ->skip($this->numberPageStocksNotBinded * $this->countOnPage) // Смещение
            ->limit($this->countOnPage) // Отображаем только 20 строк
            ->field('productId')->equals(null)
            ->sort('storePrice.' . $this->storeId, 'asc')
            ->field('quantity')->gt(0)
            ->getQuery()
            ->execute();

        // дополнить стоки продуктов
        foreach($stockMongo as $stock) {
            // дополнить массив стоков данными
            $this->stockDataNotBinded[$stock->getId()] = $stock;
            // дополнить массив ID складов
            if(!isset($this->warehouseData[$stock->getWarehouseId()])) {
                $warehouseIds[] = $stock->getWarehouseId();
            }
        }

        // проверить массив складов стоков
        if ($warehouseIds) {
            // получить склады стоков 
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array('id' => $warehouseIds));
            // дополнить массив складов данными
            foreach($warehouses as $warehouse) {
                $this->warehouseData[$warehouse->getId()] = $warehouse;
            }
        }
        
        $this->numberPageStocksNotBinded ++;
    }
}


<?php

namespace Nitra\OrderBundle\Controller\Order;

use Admingenerated\NitraOrderBundle\BaseOrderController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;
use Nitra\IncomeBundle\Entity\Income;
//use Nitra\OrderBundle\Common\nlMoney;
use Nitra\OrderBundle\Entity\Order;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\OrderBundle\Form\Type\Order\OrderCanceledType;
use Nitra\OrderBundle\Form\Type\Order\OrderIncomeType;
use Nitra\OrderBundle\Form\Type\Order\OrderReportedType;
use Nitra\OrderBundle\Form\Type\Order\OrderCommentType;
use Nitra\OrderBundle\Form\Type\Order\SearchProductFiltersType;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntrySendedType;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntrySendedConfirmType;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntryArrivedType;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntryTransportType;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntryReturnedToSupplierType;
use Nitra\OrderBundle\Form\Type\Order\OrderLogisticsType;
use Nitra\OrderBundle\Form\Type\Order\OrderWarrantyType;
use Nitra\OrderBundle\Form\Type\Order\OrderSmsType;

class ActionsController extends BaseActionsController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
     /**
     * Метод для отрисовки и создания СМС
     * @param integer $pk ID NitraOrderBundle:Order
     */
    public function attemptObjectSms($pk)
    {
        // получить объект заказа
        $order = $this->getObject($pk);
        
        $history = array();

        // получить историю сообщений по заказу
        $orderSmsHistory = $this->em->getRepository('NitraOrderBundle:Sms')->findByOrder($order);

        foreach($orderSmsHistory as $smsHistory) {
            $arr = array();
            $arr['date'] = $smsHistory->getCreatedAt(); 
            $arr['text'] = $smsHistory->getText(); 
            $arr['status'] = $smsHistory->getStatus(); 
            $arr['createdBy'] = $smsHistory->getCreatedBy()->getUserName(); 
            $history[] = $arr;
        }
        // получаем позиции заказа по ордеру
        $product = $this->em->getRepository('NitraOrderBundle:Order')->getOrderEntiesNameByOrder($order);
      
        $form = $this->createForm(new OrderSmsType());
        
        // флаг успешной обработки формы
        $formIsValid = false;
        // обработка формы
        if ($this->getRequest()->getMethod() == 'POST') {
            
            // заполнить форму 
            $form->bind($this->getRequest());
           
            // еси форма валидна
            if ($form->isValid()) {
                try {
                    // TO DO
                    // вызов функции, отправляющей СМС 
                    $formData = $form->getData();
                    
                    // Дополнительные параметры для сообщения
                    $options = array();
                    $options['sms_phone'] = $order->getBuyer()->getPhone();
                    $options['sms_text'] = $formData['text'];
                    $options['manager'] = $this->getUser();
                    $options['sms_type'] = $formData['type'];
               
                    $smsParam = $this->container->hasParameter('sms') ? $this->container->getParameter('sms') : null;
                    
                    if (!$smsParam || !isset($smsParam['login']) || !isset($smsParam['password']) || !isset($smsParam['sender']) || !isset($smsParam['gate']) || !isset($smsParam['gateStatus'])
                    ) {
                        throw new \Exception('Параметры сервиса сообщений указаны не правильно. Пожалуйста обратитесь к администратору.');
                    }
                    
                    $options['sms_parameters'] = $smsParam;
                    
                    // отправка сообщения и формирование лога
                    $this->em->getRepository('NitraOrderBundle:Sms')->smsOrderLog($order, $options);
                    
                    $this->em->flush();
                    
                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new \Symfony\Component\Form\FormError($e->getMessage()));
                }
            }
        }

        return $this->render('NitraOrderBundle:OrderList:orderSms.html.twig', array(
            'form' => $form->createView(),
            'order' => $order,
            'products' => $product,
            'formIsValid' => $formIsValid,
            'history' => $history,
        ));
    }
    
    /**
     * Метод для отрисовки счет-фактуры
     * @param integer $pk ID NitraOrderBundle:Order
     */
    public function attemptObjectInvoice($pk)
    {
        // отобразить счет-фактуру
        return $this->render('NitraOrderBundle:OrderActions:orderInvoice.html.twig', array(
            // получить объект заказа
            'Order' => $this->getObject($pk),
        ));
    }
    
    /**
     * Метод для отрисовки гарантийного талона
     * @param integer $pk ID NitraOrderBundle:Order
     */
    public function attemptObjectWarranty($pk)
    {
        // получить объект заказа
        $order = $this->getObject($pk);
        
        // создание формы гарантии
        $form = $this->createForm(new OrderWarrantyType(), $order);
        
        // отобразить историю
        return $this->render('NitraOrderBundle:OrderActions:orderWarranty.html.twig', array(
            'Order' => $order,
            'form' => $form->createView(),
        ));
    }
    
     /**
     * Метод для отрисовки логистики
     * @param integer $pk ID NitraOrderBundle:Order
     */
    public function attemptObjectLogistics($pk)
    {
        // получить объект заказа
        $order = $this->getObject($pk);
        
        // массив всей логистики по позициям в заказе
        $logistics = array();
        
        // формирование логистики по всем позициям в заказе
        foreach($order->getOrderEntries() as $orderEntry) {
            
            // начальный склад для отображения (склад с которого была заказана позиция)
            $logistics[$orderEntry->getId()]['movements'][] = $orderEntry->getWarehouse()->getAddress();
            $logistics[$orderEntry->getId()]['delivery_date'][] = '';
            
            // получение имени продукта + параметр стока для отображения
            $orderEntryProduct = $this->dm->getRepository('NitraMainBundle:Product')->findOneById($orderEntry->getProductId());
            $logistics[$orderEntry->getId()]['product_name'] = $orderEntryProduct->getName() . ' ' . $orderEntry->getStockParams();
            
            // получение всех перемещений и деклараций по данной позиции в заказе  
            foreach($orderEntry->getMovementEntry() as $movementEntry) {
                // если нет склада-получателя для перемещения то должен быть указан контракт (товар был перемещен на склад поставщика)
                if($movementEntry->getMovement()->getToWarehouse()) {
                    $logistics[$orderEntry->getId()]['movements'][] =  $movementEntry->getMovement()->getToWarehouse()->getAddress();
                }
                else {
                    $logistics[$orderEntry->getId()]['movements'][] =  $movementEntry->getMovement()->getContract();
                }
                // сохранение даты прибытия текщего перемещения
                $logistics[$orderEntry->getId()]['delivery_date'][] = $movementEntry->getMovement()->getDate();
            }
        }

        // создание формы для отображения редактирования склада доставки и даты доставки заказа
        $form = $this->createForm(new OrderLogisticsType($this->em), $order);

        return $this->render('NitraOrderBundle:OrderActions:orderLogistics.html.twig', array(
            'logistics' => $logistics,
            'form' => $form->createView(),
            'order' => $order,
        ));
    }
    
    /**
     * colorbox форма приход по заказу
     * @Route("/{pk}-income-add", name="Nitra_OrderBundle_Order_Income_Add")
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderIncome.html.twig")
     */
    public function orderIncomeAddAction(Order $order, Request $request)
    {
        
        // получить объект пользователя
        $user = $this->getUser();
        
        // создать объект прихода
        $income = new Income();
        $income->setOrder($order);
        $income->setAmount($order->getPayedLeft());
        $income->setAccount($this->em->getRepository('NitraAccountBundle:Account')->findOneBy(array()));
            
        // объект формы
        $form = $this->createForm(new OrderIncomeType(), $income, array(
            'em' => $this->em,
            'session' => $this->get('session'),
            'manager' => $user,
        ));
        
        // флаг успешной обработки формы
        $formIsValid = false;
        
        // обработка формы
        if ($request->getMethod() == 'POST') {
            // заполнить форму 
            $form->submit($request);
            
            // валидация формы
            if ($form->isValid()) {
                
                try {
                   
                    // сохранить 
                    $this->em->persist($income);
                    $this->em->flush();
                    
                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator')); 
            }
        }
        
        // вернуть массив данных передаваемых в шаблон 
        return array(
            "Order" => $order,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }
    
    /**
     * Ajax Метод для сохранения значений по гарантии
     * @Route("/warranty-save-{pk}", name="Nitra_OrderBundle_Order_Warranty_Save", options={"expose"=true})
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     */
    public function warrantySaveAction(Order $order)
    {
        
        // создание формы гарантии
        $form = $this->createForm(new OrderWarrantyType(), $order);
        $form->submit($this->get('request'));
        
        // проверить правильность заполнения формы
        if ($form->isValid()) {
            
            // сохранить
            try {
                $this->em->flush();
                
            } catch (\Exception $e) {
                // вернуть ошибку
                return new JsonResponse(array(
                    'type' => 'error',
                    'message' => $e->getMessage()
                ));
            }

            // гарантийный талон сохранен успешно
            return new JsonResponse(array(
                'type' => 'success',
                'message' => '',
            ));
            
        } else {

            // обойти все позиции заказа
            foreach ($form->get('orderEntries') as $orderEntry) {
                // проверить заполнение каждой позиции заказа
                if (!$orderEntry->isValid()) {
                    // обойти все виджеты позиции заказа
                    foreach ($orderEntry->all() as $child) {
                        // виждет позиции заказа заполнен не правильно
                        if (!$child->isValid()) {
                            $errors = $child->getErrors();
                            // вернуть ошибку валидации
                            return new JsonResponse(array(
                                'type' => 'error',
                                'message' => $orderEntry->getData() . " " . $errors[0]->getMessage(),
                            ));
                        }
                    }
                }
            }

            // ошибка валидации
            return new JsonResponse(array(
                'type' => 'error',
                'message' => $this->get('translator')->trans("Неправильно заполнен гарантийный талон.", array(), 'Admingenerator')
            ));
        }
    }

    /**
     * Метод для сохранения даты доставки и склада-ролучателя по заказу в логистике
     * @Route("/logistics-save-{pk}", name="Nitra_OrderBundle_Order_Logistics_Save")
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     */
    public function logisticsSaveAction(Order $order)
    {
        if (!$order) {
            throw new NotFoundHttpException("The Nitra\OrderBundle\Entity\Order with id $pk can't be found");
        }

        $form = $this->createForm(new OrderLogisticsType(), $order);

        $form->submit($this->get('request'));

        if ($form->isValid()) {
            try {
                $this->em->persist($order);
                $this->em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator'));
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
        }

        return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_list"));
    }

    /**
     * добавление комментария
     * @Route("/comment-add", name="Nitra_OrderBundle_Order_Comment_Add")
     * @return JsonResponse
     */
    public function orderCommentAddAction()
    {

        // обработка формы
        if ($this->getRequest()->getMethod() == 'POST') {

            // объект формы 
            $form = $this->createForm(new OrderCommentType($this->em));

            // заполнить форму 
            $form->submit($this->getRequest());

            // проверить валидность формы
            if ($form->isValid()) {

                // получить обект данных форм
                $formData = $form->getData();
                // получить объект заказа
                $order = $form['id']->getData();

                // сохранить 
                try {

                    // автоинкримент комментария
                    $order->addComment($formData['comment'], $this->getUser()->getUsername());

                    // сохранить 
                    $this->em->flush();
                    
                } catch (\Exception $e) {
                    // вернуть ошибку
                    return new JsonResponse(array('type' => 'error', 'message' => $e->getMessage()));
                }

                // комментарий успешно добавлен, вернуть текст комментария
                return new JsonResponse(array('type' => 'success', 'orderId' => $order->getId(), 'comment' => $order->getComment()));
                
            } else {

                // Текст ошибки валидации
                $errorsText = '';

                // ошибка валидации по форме
                foreach ($form->getErrors() as $error) {
                    $errorsText .= " " . $error->getMessage();
                }

                // ошибка валидации по каждому виджету
                foreach ($form->all() as $child) {
                    $errors = $child->getErrors();
                    if ($errors) {
                        $errorsText .= " " . $errors[0]->getMessage();
                    }
                }

                // отображение ошибки валидации формы
                return new JsonResponse(array(
                    'type' => 'error',
                    'message' => $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') . $errorsText
                ));
            }
        }

        // вернуть пустой ответ
        return new JsonResponse();
    }

    /**
     * карточка товара
     * @Route("/get-product-info-{id}", name="Nitra_OrderBundle_Order_Product_Get_Info")
     * @Template("NitraOrderBundle:OrderActions:productGetInfo.html.twig")
     */
    public function productGetInfoAction($id)
    {
        $product = $this->dm->getRepository('NitraMainBundle:Product')->find($id);

        // получить id магазина через филиал пользователя по умолчанию 
        $storeId = $this->getUser()->getDefaultFilial()->getStoreId();

        return array(
            'product' => $product,
            'storeId' => $storeId,
        );
    }

    /**
     * Добавление продукта
     * из левого списка в правый
     * @Route("/product-add-to-cart", name="Nitra_OrderBundle_Order_Product_Add_To_Cart", options={"expose"=true})
     */
    public function productAddToCartAction()
    {

        // получить склад 
        $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($this->getRequest()->get('warehouseId', false));
        if (!$warehouse) {
            // склад не найден, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Склад не найден."));
        }

        // получить данные продукта
        $product = $this->dm->getRepository('NitraMainBundle:Product')->find($this->getRequest()->get('productId', false));
        if (!$product) {
            // продукт не найден, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Продукт не найден."));
        }

        // получить сток
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $product->getId());
        if (!$stock) {
            // сток не найден, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Продукт \"" . (string) $product . "\" не найден на складе \"" . (string) $warehouse . "\"."));
        }

        // результирующий массив 
        $result = array(
            'stockId' => $stock->getId(),
            'currency' => $stock->getCurrency(),
            'currencyPrice' => ($stock->getCurrencyPrice()) ? $stock->getCurrencyPrice() : 0,
            'priceIn' => ($stock->getPriceIn()) ? $stock->getPriceIn() : 0,
            'storePrice' => ($stock->getStorePrice()) ? $stock->getStorePrice() : 0,
            'productId' => $product->getId(),
            'productName' => (string) $product,
            'supplierId' => '', // ID поставщика, если заказ по складу поставщика
            'supplierName' => '', // Название поставщика, если заказ по складу поставщика
            'deliveryId' => '', // ID ТК, если заказ по складу ТК
            'deliveryName' => '', // Название ТК, если заказ по складу ТК
            'warehouseId' => $warehouse->getId(),
            'warehouseName' => (string) $warehouse,
            'warehouses' => array(),
        );

        // заказ с поставщика получить данные поставщика
        $supplier = $warehouse->getSupplier();
        if ($supplier) {
            $result['supplierId'] = $supplier->getId();
            $result['supplierName'] = (string) $supplier;
            $result['warehouses'] = array();
            // получить список складов для поставщика
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array('supplier' => $supplier->getId()));
            foreach ($warehouses as $warehouse) {
                $result['warehouses'][] = array(
                    'id' => $warehouse->getId(),
                    'name' => (string) $warehouse,
                );
            }
        }

        // заказ с ТК получить данные по ТК 
        $delivery = $warehouse->getDelivery();
        if ($delivery) {
            $result['deliveryId'] = $delivery->getId();
            $result['deliveryName'] = (string) $delivery;
            $result['warehouses'] = array();
            // получить список складов для ТК 
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array('delivery' => $delivery->getId()));
            foreach ($warehouses as $warehouse) {
                $result['warehouses'][] = array(
                    'id' => $warehouse->getId(),
                    'name' => (string) $warehouse,
                );
            }
        }

        // заказ позиции со своего склада
        if (!$supplier && !$delivery) {
            // массив складов
            $result['warehouses'] = array();
            // получить свои склады
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->getOwnWarehouses();
            foreach($warehouses as $warehouse) {
                $result['warehouses'][] = array(
                    'id' => $warehouse->getId(), 
                    'name' => (string)$warehouse,
                );
            }
        }

        // вернуть результирующий массив 
        return new JsonResponse($result);
    }

    /**
     * получить список складов
     * @Route("/get-warehouses-for", name="Nitra_OrderBundle_Get_Warehouses_For", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getWarehousesForAction()
    {

        // получить склады для поставщика
        if ($this->getRequest()->get('supplierId', false)) {

            // получить поставщика
            $supplier = $this->em->getRepository('NitraMainBundle:Supplier')->find($this->getRequest()->get('supplierId', false));
            if (!$supplier) {
                // поставщик не найден вернуть массив ошибок
                return new JsonResponse(array('type' => 'error', 'message' => "Поставщик не найден."));
            }

            // получить список складов для поставщика
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array(
                'supplier' => $supplier->getId(),
            ));

            // результирующий массив
            $result = array('warehouses' => array());

            // наполнить результирующий массив
            if (isset($warehouses) && $warehouses) {
                foreach($warehouses as $warehouse) {
                    $result['warehouses'][] = array('id' => $warehouse->getId(), 'name' => (string)$warehouse);
                }
            }

            // вернуть результируюший массив 
            return new JsonResponse($result);
        }

        // получить склады для ТК 
        if ($this->getRequest()->get('deliveryId', false)) {

            // получить ТК
            $delivery = $this->em->getRepository('NitraMainBundle:Delivery')->find($this->getRequest()->get('deliveryId', false));
            if (!$delivery) {
                // ТК не найдена вернуть массив ошибок
                return new JsonResponse(array('type' => 'error', 'message' => "Транспортная компания не найдена."));
            }

            // получить список складов для ТК
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array(
                'delivery' => $delivery->getId(),
            ));

            // результирующий массив
            $result = array('warehouses' => array());

            // наполнить результирующий массив
            if (isset($warehouses) && $warehouses) {
                foreach($warehouses as $warehouse) {
                    $result['warehouses'][] = array('id' => $warehouse->getId(), 'name' => (string)$warehouse);
                }
            }

            // вернуть результируюший массив 
            return new JsonResponse($result);
        }


        // результрующий массив
        $result = array();

        // получить данные по массиву поставщиков
        $supplierIds = $this->getRequest()->get('supplierIds', false);
        if ($supplierIds) {

            // массив из уникальных ID 
            $supplierIds = array_unique($supplierIds);

            // получить список поставщиков
            $suppliers = $this->em->getRepository('NitraMainBundle:Supplier')->findBy(array(
                'id' => $supplierIds
            ));

            // наполнить результирующий массив поставщиками
            if ($suppliers) {

                // массив поставщиков
                $result['suppliers'] = array();

                // наполнить массив поставщиков
                foreach ($suppliers as $supplier) {

                    // поулчить массив складов поставщиков
                    $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array(
                        'supplier' => $supplier->getId()
                    ));

                    // массив складов поставщика
                    $warehousesArr = array();
                    if ($warehouses) {
                        foreach ($warehouses as $warehouse) {
                            $warehousesArr[$warehouse->getId()] = array(
                                'id' => $warehouse->getId(),
                                'name' => (string) $warehouse,
                            );
                        }
                    }

                    // запомнить поставщика
                    $result['suppliers'][$supplier->getId()] = array(
                        'id' => $supplier->getId(),
                        'name' => (string) $supplier,
                        'warehouses' => $warehousesArr,
                    );
                }
            }
        }

        // получить данные по массиву ТК 
        $deliveryIds = $this->getRequest()->get('deliveryIds', false);
        if ($deliveryIds) {

            // массив из уникальных ID 
            $deliveryIds = array_unique($deliveryIds);

            // получить список ТК 
            $deliveries = $this->em->getRepository('NitraMainBundle:Delivery')->findBy(array(
                'id' => $deliveryIds
            ));

            // наполнить результирующий массив ТК
            if ($deliveries) {

                // массив ТК 
                $result['deliveries'] = array();

                // наполнить массив ТК 
                foreach ($deliveries as $delivery) {

                    // поулчить массив складов ТК
                    $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array(
                        'delivery' => $delivery->getId()
                    ));

                    // массив складов ТК
                    $warehousesArr = array();
                    if ($warehouses) {
                        foreach ($warehouses as $warehouse) {
                            $warehousesArr[$warehouse->getId()] = array(
                                'id' => $warehouse->getId(),
                                'name' => (string) $warehouse,
                            );
                        }
                    }

                    // запомнить ТК
                    $result['deliveries'][$delivery->getId()] = array(
                        'id' => $delivery->getId(),
                        'name' => (string) $delivery,
                        'warehouses' => $warehousesArr,
                    );
                }
            }
        }

        // получить свои склады
        if ($this->getRequest()->get('ownWarehouse', false)) {
            // массив складов
            $result['ownWarehouse'] = array();
            // получить свои склады
            $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->getOwnWarehouses();
            foreach ($warehouses as $warehouse) {
                $result['ownWarehouse'][] = array(
                    'id' => $warehouse->getId(), 
                    'name' => (string)$warehouse,
                );
            }
        }

        // вернуть результируюший массив 
        return new JsonResponse($result);
    }

    /**
     * выбор городов для автоподсказки
     * @Route("/get-city-order-autocomplete", name="Nitra_OrderBundle_Get_City_Autocomplete")
     */
    public function cityAutocompleteAction()
    {

        // То что приходит из поля ввода (строка поиска)
        $search_str = $this->getRequest()->get('q');

        $names = preg_split('/\s+/', trim($search_str));

        $where = array();
        foreach ($names AS $name) {
            $where [] = " c.name LIKE '%" . $name . "%' ";
        }

        $citys = $this->em->createQuery("SELECT c FROM NitraTetradkaGeoBundle:City c WHERE (" . implode(" OR ", $where) . " )")->getResult();

        $autocomplete_return = array();

        foreach ($citys as $city) {
            $autocomplete_return[] = $city->getName() . "|" . $city->getName() . "|" . $city->getId() . '|' . json_encode(array('filial_id' => $city->getRegion()->getFilial()->getId()));
        }

        return new Response(implode("\n", $autocomplete_return));
    }

    /**
     * выбор покупателей для автоподсказки
     * @Route("/get-buyer-order-autocomplete", name="Nitra_OrderBundle_Get_Buyer_Autocomplete")
     */
    public function buyerAutocompleteAction()
    {

        // То что приходит из поля ввода (строка поиска)
        $search_str = $this->getRequest()->get('q');
        $type_search = $this->getRequest()->get('search_by', 'name');

        $words = preg_split('/\s+/', trim($search_str));

        $where = array();
        foreach ($words AS $word) {

            switch ($type_search) {
                case 'name':
                    $where [] = " b.name LIKE '%" . $word . "%' ";
                    break;

                case 'phone':
                    $where [] = " b.phone LIKE '%" . $word . "%' ";
                    break;

                default :
                    $where [] = " b.id IS NULL ";
            }
        }

        // получить покупателей
        $buyers = $this->em->createQuery("SELECT b FROM NitraOrderBundle:Buyer b WHERE (" . implode(" OR ", $where) . " )")->getResult();

        $autocomplete_return = array();

        foreach ($buyers as $buyer) {

            switch ($type_search) {
                case 'name':
                    $current_buyer_str = $buyer->getName() . "|" . $buyer->getName() . "|" . $buyer->getId() . '|' . json_encode(array('phone' => $buyer->getPhone(), 'name' => $buyer->getName(), 'id' => $buyer->getId()));
                    break;

                case 'phone':
                    $current_buyer_str = $buyer->getPhone() . "|" . $buyer->getPhone() . "|" . $buyer->getId() . '|' . json_encode(array('phone' => $buyer->getPhone(), 'name' => $buyer->getName(), 'id' => $buyer->getId()));
                    break;

                default :
                    break;
            }

            $autocomplete_return[] = $current_buyer_str;
        }

        if ($type_search == 'name') {
            $upName = mb_convert_case($search_str, MB_CASE_TITLE, "UTF-8");
            $autocomplete_return[] = $upName . "\\\\новый покупатель|" . $upName . "|" . "new" . '|' . json_encode(array());
        }

        return new Response(implode("\n", $autocomplete_return));
    }

    /**
     * Изменение статуса заказа
     * @Route("/set-status", name="Nitra_OrderBundle_Order_Set_Status", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\JsonResponse or application/javascript
     */
    public function orderSetStatusAction()
    {

        // получить заказ
        $order = $this->em->getRepository('NitraOrderBundle:Order')->find($this->getRequest()->get('orderId', false));
        if (!$order) {
            // позиция заказа не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Заказ не найден."));
        }

        // новый статус заказа
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneBy(array('methodName' => $this->getRequest()->get('toStatus', false)));
        if (!$newStatus) {
            // устанавливаемый новый статус закза не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Новый статус заказа не найден."));
        }

        // старыйй статус (отображенный пользователю) заказа
        $oldStatus = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneBy(array('methodName' => $this->getRequest()->get('fromStatus', false)));
        if (!$oldStatus) {
            // отображенный пользователю статус закза не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Отображенный пользователю статус заказа не найден."));
        }

        // валидировать обработчик цепочки
        $errorMessage = $this->em
            ->getRepository('NitraOrderBundle:Order')
            ->validToStatus($order, $newStatus, $oldStatus);
        
        // проверить вализацию 
        if ($errorMessage) {
            // ошибка валидации
            return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
        }

        // Направление изменения статуса заказа
        $direction = $order->getOrderStatus()->getMethodName() . '_to_' . $newStatus->getMethodName();

        // выполнить цепочку
        switch ($direction) {

            // с будильника
            case 'waiting_to_ordered':
            case 'waiting_to_ready':

            // с корзинки 
            case 'ordered_to_waiting':
            case 'ordered_to_ready':

            // с ready
            case 'ready_to_waiting':
            case 'ready_to_ordered':
            case 'ready_to_completed':

            // с reporter
            case 'reported_to_ready':
            case 'reported_to_completed':

            // с завершен 
            case 'completed_to_reported':
            case 'completed_to_ready':

            // с отмены
            case 'canceled_to_waiting':
            case 'canceled_to_ordered':
            case 'canceled_to_ready':
            case 'canceled_to_reported':

                // параметры валидации/выполнения цепочки
                $options = array(
                    'manager' => $this->getUser(),
                );

                // обновить статус заказа
                $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:Order')
                        ->setStatus($newStatus, $order, $options);

                // цепочка не выполнена, вернуть массив ошибок
                if ($errorMessage) {
                    return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                }

                // break
                break;


            // отмена заказа
            case 'waiting_to_canceled':
            case 'ordered_to_canceled':
            case 'ready_to_canceled':
            case 'reported_to_canceled':

                // валидировать отмену 
                $errorMessage = $this->em->getRepository('NitraOrderBundle:Order')->validCommonToCanceled($order);
                if ($errorMessage) {
                    return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                }
                // валидация пройдена 
                // colorbox форма отмены заказа
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Status_Canceled', array('pk' => $order->getId()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                // break
                break;

            // отчет покупателю
            case 'ready_to_reported':
                // colorbox форма отмены заказа
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Status_Reported', array('pk' => $order->getId()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                break;

            // действия по цепочке не определены
            default:
                // ошибка валидации
                return new JsonResponse(array('type' => 'error', 'message' => "Не указан порядок изменения статуса для заказа. " . $direction));
                break;
        }

        // сохранить
        $this->em->flush();
        $this->dm->flush();
        
        // результирующий массив 
        $result = array('type' => 'success');
        
        // если заказ переведен в ready
        if(!in_array($order->getOrderStatus()->getMethodName(), array('completed','canceled')) && $newStatus->getMethodName() === 'ready') {
            //отправка сообщения
            $sendSms = $this->sendSmsAfterChangeStatusToReady($order, $options);
            // если error устанавливаем новое значение
            if(isset($sendSms['error'])) {
                $result['sms_error'] = $sendSms['error'];
            }
        }

        // цепочка выполнена успешно
        return new JsonResponse($result);
    }

    /**
     * colorbox форма отмена заказа
     * @Route("/{pk}-status-canceled", name="Nitra_OrderBundle_Order_Status_Canceled")
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderStatusCanceled.html.twig")
     */
    public function orderStatusCanceledAction(Order $order, Request $request)
    {

        // объект формы
        $form = $this->createForm(new OrderCanceledType($order));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                // получить обект данных форм
                $formData = $form->getData();

                // получить устанавливаемый статус позиции
                $newStatus = $this->em
                    ->getRepository('NitraOrderBundle:OrderStatus')
                    ->findOneBy(array('methodName' => 'canceled'));

                // параметры валидации/выполнения цепочки
                $options = array(
                    'cancelComment' => $formData['cancelComment'],
                    'username' => $this->getUser()->getUsername(),
                    'manager' => $this->getUser(),
                );

                try {

                    // выполнить цепочку 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:Order')
                        ->setStatus($newStatus, $order, $options);
                    
                    // проверить выполнение цепочки
                    if ($errorMessage) {
                        // цепочка не выполнена, throw
                        throw new \Exception($errorMessage);
                    }

                    // сохранить 
                    $this->em->flush();
                    $this->dm->flush();

                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "Order" => $order,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    /**
     * colorbox отчет покупателю
     * @Route("/{pk}-status-reported", name="Nitra_OrderBundle_Order_Status_Reported")
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderStatusReported.html.twig")
     */
    public function orderStatusReportedAction(Order $order, Request $request)
    {

        // объект формы 
        $form = $this->createForm(new OrderReportedType($this->em, $order));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                // получить обект данных форм
                $formData = $form->getData();

                // добавить комментарий 
                if ($formData['comment']) {
                    $order->addComment($formData['comment'], $this->getUser()->getUsername());
                }

                // нажата кнопка формы нет ответа
                if (!$formData['is_reported']) {
                    // добавить комментарий нет ответа
                    $order->addComment($this->get('translator')->trans("Нет ответа", array(), 'Admingenerator'), $this->getUser()->getUsername());
                }

                try {

                    // обработка события перевод в новый статус
                    // нажата кнопка Обновить статус
                    if ($formData['is_reported']) {

                        // новый статус заказа
                        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderStatus')
                                ->findOneBy(array('methodName' => 'reported'));

                        // параметры валидации/выполнения цепочки
                        $options = array(
                            'manager' => $this->getUser(),
                        );

                        // обновить статус позиции 
                        $errorMessage = $this->em
                            ->getRepository('NitraOrderBundle:Order')
                            ->setStatus($newStatus, $order, $options);

                        // проверить выполнение цепочки
                        if ($errorMessage) {
                            // цепочка не выполнена, throw
                            throw new \Exception($errorMessage);
                        }
                        
                    }

                    // сохранить 
                    $this->em->flush();

                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "Order" => $order,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    /**
     * Метод для вывода истории по статусам заказа
     * @Route("/{pk}-status-history", name="Nitra_OrderBundle_Order_Status_History")
     * @ParamConverter("order", class="NitraOrderBundle:Order", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderStatusHistory.html.twig")
     */
    public function orderStatusHistoryAction(Order $order)
    {
        // вернуть массив данных передаваемых в шаблон 
        return array(
            'Order' => $order,
        );
    }

    /**
     * Изменение статуса позиции заказа
     * @Route("/entry-set-status", name="Nitra_OrderBundle_OrderEntry_Set_Status", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\JsonResponse or application/javascript
     */
    public function orderEntrySetStatusAction()
    {

        // получить объект позиции заказа
        $orderEntry = $this->em->getRepository('NitraOrderBundle:OrderEntry')->find($this->getRequest()->get('orderEntryId', false));
        if (!$orderEntry) {
            // позиция заказа не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Позиция заказа не найдена."));
        }

        // статус заказа до обновления статуса позиции
        $oldOrderStatusMethodName = $orderEntry->getOrder()->getOrderStatus()->getMethodName();

        // новый статус позиции заказа
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => $this->getRequest()->get('toStatus', false)));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Новый статус позиции заказа не найден."));
        }

        // старыйй статус (отображенный пользователю) позиции заказа
        $oldStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => $this->getRequest()->get('fromStatus', false)));
        if (!$oldStatus) {
            // отображенный пользователю статус позиции закза не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Отображенный пользователю статус позиции заказа не найден."));
        }

        // валидировать обработчик цепочки
        $errorMessage = $this->em
            ->getRepository('NitraOrderBundle:OrderEntry')
            ->validToStatus($orderEntry, $newStatus, $oldStatus);

        // проверить вализацию 
        if ($errorMessage) {
            // ошибка валидации
            return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
        }

        // Направление изменения статуса позиции заказа
        $direction = $orderEntry->getOrderEntryStatus()->getMethodName() . '_to_' . $newStatus->getMethodName();

        // выполнить цепочку
        switch ($direction) {

            // с будильника
            case 'waiting_to_clarified':
            case 'waiting_to_ordered':
            case 'waiting_to_canceled':

            // с флажка
            case 'clarified_to_waiting':
            case 'clarified_to_ordered':
            case 'clarified_to_canceled':

            // с корзинки 
            case 'ordered_to_waiting':
            case 'ordered_to_clarified':
            case 'ordered_to_canceled':

            // с отмены
            case 'canceled_to_waiting':
            case 'canceled_to_ordered':
            case 'canceled_to_clarified':
            case 'canceled_to_arrived':
            case 'canceled_to_transport':

            // с коробка
            case 'arrived_to_waiting':
            case 'arrived_to_clarified':
            case 'arrived_to_canceled':
            case 'arrived_to_completed':
            case 'arrived_to_ordered':
            case 'arrived_to_sended':

            // с самолетика
            case 'transport_to_waiting':
            case 'transport_to_clarified':
            case 'transport_to_ordered':
            case 'transport_to_sended':
            case 'transport_to_completed':
            case 'transport_to_canceled':

            // с завершен
            case 'completed_to_arrived':
            case 'completed_to_transport':

            // c возвращено поставщику
            case 'returned_to_supplier_to_arrived':
            case 'returned_to_supplier_to_transport':

                // параметры валидации/выполнения цепочки
                $options = array(
                    'manager' => $this->getUser(),
                );

                // обновить статус позиции
                $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);

                // цепочка не выполнена, вернуть массив ошибок
                if ($errorMessage) {
                    return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                }

                // break
                break;

            // создание декларации 
            case 'waiting_to_sended':
            case 'clarified_to_sended':
            case 'ordered_to_sended':
            case 'canceled_to_sended':
                // colorbox форма оприходования
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Entry_Status_Sended', array('pk' => $orderEntry->getId()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                // break
                break;

            // удаление декларации с подтверждением
            case 'sended_to_waiting':
            case 'sended_to_clarified':
            case 'sended_to_ordered':

                // получить последню декларацию по позиции заказа
                $deLast = $orderEntry->getLastDeclarationEntry();

                // проверить, если была создана декларация 
                // цепочка требует подтверждения
                // для удаления декларации по позиции 
                if ($deLast) {
                    $result = array(
                        'type' => 'confirm',
                        'confirmUrl' => $this->generateUrl('Nitra_OrderBundle_Order_Entry_Delete_Declaration', array('pk' => $orderEntry->getId(), 'toStatus' => $newStatus->getMethodName()), true),
                        'confirmMessage' => "Для позиции \n" . (string) $orderEntry . " \ncформирована декларация. \nВы действительно хотите удалить декларацию?",
                    );
                    // вернуть результируюший массив 
                    return new JsonResponse($result);
                }

                // обновить статус позиции заказа
                // для позиции не было создано деклараций
                // параметры валидации/выполнения цепочки
                $options = array(
                    'manager' => $this->getUser(),
                );

                // обновить статус позиции 
                $errorMessage = $this->em
                    ->getRepository('NitraOrderBundle:OrderEntry')
                    ->setStatus($newStatus, $orderEntry, $options);

                // цепочка не выполнена, вернуть массив ошибок
                if ($errorMessage) {
                    return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                }

                // break
                break;


            // отмена, удаление связи с декларацией с подтверждением
            case 'sended_to_canceled':

                // получить последню декларацию по позиции заказа
                $deLast = $orderEntry->getLastDeclarationEntry();

                // проверить, если была создана декларация 
                // цепочка требует подтверждения
                // для отвязки декларации от позиции 
                if ($deLast) {
                    $result = array(
                        'type'=> 'confirm',
                        'confirmUrl'=> $this->generateUrl('Nitra_OrderBundle_Order_Entry_Unbind_Declaration', array('pk' => $orderEntry->getId(), 'toStatus'=>$newStatus->getMethodName()), true),
                        'confirmMessage'=> "При отмене позиции, по которой сформирована декларация,\n".
                                           "декларация будет сохранена, но не будет связана с заказом.\n".
                                           "Вы действительно хотите отменить позицию \n".(string)$orderEntry."?"
                    );
                    // вернуть результируюший массив 
                    return new JsonResponse($result);
                }

                // обновить статус позиции заказа
                // для позиции не было создано деклараций
                // параметры валидации/выполнения цепочки
                $options = array(
                    'manager' => $this->getUser(),
                );

                // обновить статус позиции 
                $errorMessage = $this->em
                    ->getRepository('NitraOrderBundle:OrderEntry')
                    ->setStatus($newStatus, $orderEntry, $options);
                
                // цепочка не выполнена, вернуть массив ошибок
                if ($errorMessage) {
                    return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                }

                // break
                break;


            // оприходование товара на своем складе
            case 'waiting_to_arrived':
            case 'clarified_to_arrived':
            case 'ordered_to_arrived':
            case 'transport_to_arrived':

                // оприходование товара со своего склада на свой
                // если товарная позиция находится на своем складе
                // то оприходовать товар без colorbox формы оприходования
                $oeAtWarehouse = $orderEntry->getAtWarehouse();
                if ($oeAtWarehouse->isOwn()) {
                    // массив параметров оприходования товарной позиции на складе
                    $options = array(
                        // склад получатель
                        'warehouse' => $oeAtWarehouse,
                        // цена по которой позиция входит на склад
                        'price' => $orderEntry->getPriceIn(),
                        // валюта 
                        'currency' => $orderEntry->getCurrency(),
                        // Дата
                        'date' => new \DateTime(),
                        // данные пользователя
                        'manager' => $this->getUser(),
                    );
                    // обновить статус позиции 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);

                    // цепочка не выполнена, вернуть массив ошибок
                    if ($errorMessage) {
                        return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                    }

                    // break
                    break;
                }

                // colorbox форма оприходования
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Entry_Status_Arrived', array('pk' => $orderEntry->getId()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                // break
                break;

            // с машинки
            case 'sended_to_arrived':
            case 'sended_to_transport':
                // colorbox форма уточнение цены оприходования
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Entry_Status_Sended_Confirm', array('pk' => $orderEntry->getId(), 'toStatus' => $newStatus->getMethodName()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                // break
                break;

            // оприходование товара на складе ТК
            case 'waiting_to_transport':
            case 'clarified_to_transport':
            case 'ordered_to_transport':
            case 'arrived_to_transport':

                // оприходование товара со склада ТК на склад ТК
                // если товарная позиция находится на складе ТК 
                // то оприходовать товар без colorbox формы оприходования
                $oeAtWarehouse = $orderEntry->getAtWarehouse();
                if ($oeAtWarehouse->isDelivery()) {
                    // массив параметров оприходования товарной позиции на складе
                    $options = array(
                        // склад получатель
                        'warehouse' => $oeAtWarehouse,
                        // цена по которой позиция входит на склад
                        'price' => $orderEntry->getPriceIn(),
                        // валюта 
                        'currency' => $orderEntry->getCurrency(),
                        // Дата
                        'date' => new \DateTime(),
                        // данные пользователя
                        'manager' => $this->getUser(),
                    );
                    // обновить статус позиции 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);

                    // цепочка не выполнена, вернуть массив ошибок
                    if ($errorMessage) {
                        return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
                    }

                    // break
                    break;
                }


                // colorbox форма оприходования
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Entry_Status_Transport', array('pk' => $orderEntry->getId()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                // break
                break;

            // вернуть поставщику
            case 'arrived_to_returned_to_supplier':
            case 'transport_to_returned_to_supplier':
                // colorbox форма оприходования
                $response = new Response('$.colorbox({transition: "none", opacity: 0.1, href:"' . $this->generateUrl('Nitra_OrderBundle_Order_Entry_Status_Returned_To_Supplier', array('pk' => $orderEntry->getId()), true) . ' "});');
                $response->headers->set('Content-Type', 'application/javascript');
                return $response;
                // break
                break;

            // действия по цепочке не определены
            default:
                // ошибка валидации
                return new JsonResponse(array('type' => 'error', 'message' => "Не указан порядок изменения статуса для позиции заказа. "));
                break;
        }

        // сохранить
        $this->em->flush();
        $this->dm->flush();

        // результирующий массив 
        $result = array('type' => 'success');
        // получаем статус order
        $orderStatusMethodName = $orderEntry->getOrder()->getOrderStatus()->getMethodName();
        // если изменен статус заказа на "готов"
        if(($oldOrderStatusMethodName != $orderStatusMethodName) && $orderStatusMethodName === 'ready') {
            //отправка сообщения
            $sendSms = $this->sendSmsAfterChangeStatusToReady($orderEntry->getOrder(), $options);
            // если error устанавливаем новое значение
            if(isset($sendSms['error'])) {
                $result['sms_error'] = $sendSms['error'];
            }
        }
               
        // цепочка выполнена успешно
        return new JsonResponse($result);
    }

    
    public function sendSmsAfterChangeStatusToReady($order, $options) {
        // параметры сообщения из parameters.yml
        $smsParam = $this->container->hasParameter('sms') ? $this->container->getParameter('sms') : null;

        if ($smsParam) {

            //получаем название продуктов
            $product = $this->em
                    ->getRepository('NitraOrderBundle:Order')
                    ->getOrderEntiesNameByOrder($order);

            $options['sms_phone'] = $order->getBuyer()->getPhone();
            //тест сообщения
            $options['sms_text'] = 'Ваш заказ №' . $order->getId() . ': ' . $product . ' на складе ' . (string) $order->getWarehouse();
            // параметр определяющий возврат ошибки.  true - array, если не существует  - throw exception
            $options['send_sms_change_to_status_ready'] = true;
            $options['sms_parameters'] = $smsParam;
            //сохраняем лог

            return $this->em->getRepository('NitraOrderBundle:Sms')->smsOrderLog($order, $options);
        }
    }

    /**
     * colorbox форма создание декларации
     * @Route("/entry/{pk}/status-sended", name="Nitra_OrderBundle_Order_Entry_Status_Sended")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderEntryStatusSended.html.twig")
     */
    public function orderEntryStatusSendedAction(OrderEntry $orderEntry, Request $request)
    {

        // получить устанавливаемый статус позиции
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'sended'));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена
            throw new \Exception("Новый статус позиции заказа не найден.");
        }

        // объект формы
        $form = $this->createForm(new OrderEntrySendedType($orderEntry));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                try {

                    // параметры валидации/выполнения цепочки
                    $options = $form->getData();
                    $options['declarationNumber'] = $form['declarationNumber']->getData();
                    $options['manager'] = $this->getUser();

                    // выполнить цепочку 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);

                    // проверить выполнение цепочки
                    if ($errorMessage) {
                        // цепочка не выполнена, throw
                        throw new \Exception($errorMessage);
                    }

                    // сохранить 
                    $this->em->flush();
                    $this->dm->flush();

                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "orderEntry" => $orderEntry,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    /**
     * colorbox форма уточнение цены по декларации
     * @Route("/entry/{pk}/status-sended-confirm", name="Nitra_OrderBundle_Order_Entry_Status_Sended_Confirm")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderEntryStatusSendedConfirm.html.twig")
     */
    public function orderEntryStatusSendedConfirmAction(OrderEntry $orderEntry, Request $request)
    {

        // новый статус позиции заказа
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => $this->getRequest()->get('toStatus', false)));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена
            throw new \Exception("Новый статус позиции заказа не найден.");
        }

        // объект формы 
        $form = $this->createForm(new OrderEntrySendedConfirmType($orderEntry, $newStatus));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                try {

                    // параметры валидации/выполнения цепочки
                    $options = $form->getData();
                    $options['manager'] = $this->getUser();

                    // выполнить цепочку 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);

                    // проверить выполнение цепочки
                    if ($errorMessage) {
                        // цепочка не выполнена, throw
                        throw new \Exception($errorMessage);
                    }

                    // сохранить 
                    $this->em->flush();
                    $this->dm->flush();

                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "orderEntry" => $orderEntry,
            "newStatus" => $newStatus,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    /**
     * Удаление декларации для позиции заказа
     * @Route("/entry/{pk}/unbind-declaration", name="Nitra_OrderBundle_Order_Entry_Unbind_Declaration")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function orderEntryUnbindDeclarationAction(OrderEntry $orderEntry, Request $request)
    {
        // новый статус позиции заказа
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => $this->getRequest()->get('toStatus', false)));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Новый статус позиции заказа не найден."));
        }

        // параметры валидации/выполнения цепочки
        $options = array(
            'manager' => $this->getUser(),
        );

        // выполнить цепочку 
        $errorMessage = $this->em
            ->getRepository('NitraOrderBundle:OrderEntry')
            ->setStatus($newStatus, $orderEntry, $options);

        // проверить выполнение цепочки
        if ($errorMessage) {
            // цепочка не выполнена, throw
            return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
        }

        // сохранить изменения
        $this->em->flush();
        $this->em->clear();

        // цепочка выполнена успешно
        $result = array('type' => 'success');

        // вернуть результируюший массив 
        return new JsonResponse($result);
    }

    /**
     * Удаление декларации для позиции заказа
     * @Route("/entry/{pk}/delete-declaration", name="Nitra_OrderBundle_Order_Entry_Delete_Declaration")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function orderEntryDeleteDeclarationAction(OrderEntry $orderEntry, Request $request)
    {
        // новый статус позиции заказа
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => $this->getRequest()->get('toStatus', false)));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена, вернуть массив ошибок 
            return new JsonResponse(array('type' => 'error', 'message' => "Новый статус позиции заказа не найден."));
        }

        // параметры валидации/выполнения цепочки
        $options = array(
            'manager' => $this->getUser(),
        );

        // выполнить цепочку 
        $errorMessage = $this->em
            ->getRepository('NitraOrderBundle:OrderEntry')
            ->setStatus($newStatus, $orderEntry, $options);

        // проверить выполнение цепочки
        if ($errorMessage) {
            // цепочка не выполнена, throw
            return new JsonResponse(array('type' => 'error', 'message' => $errorMessage));
        }

        // сохранить изменения
        $this->em->flush();
        $this->em->clear();

        // цепочка выполнена успешно
        $result = array('type' => 'success');

        // вернуть результируюший массив 
        return new JsonResponse($result);
    }

    /**
     * colorbox форма оприходования товара на свой склад
     * @Route("/entry/{pk}/status-arrived", name="Nitra_OrderBundle_Order_Entry_Status_Arrived")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderEntryStatusArrived.html.twig")
     */
    public function orderEntryStatusArrivedAction(OrderEntry $orderEntry, Request $request)
    {

        // получить устанавливаемый статус позиции
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'arrived'));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена
            throw new \Exception("Новый статус позиции заказа не найден.");
        }

        // объект формы
        $form = $this->createForm(new OrderEntryArrivedType($orderEntry));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                try {

                    // параметры валидации/выполнения цепочки
                    $options = $form->getData();
                    $options['manager'] = $this->getUser();

                    // выполнить цепочку 
                    $errorMessage = $this->em
                            ->getRepository('NitraOrderBundle:OrderEntry')
                            ->setStatus($newStatus, $orderEntry, $options);

                    // проверить выполнение цепочки
                    if ($errorMessage) {
                        // цепочка не выполнена, throw
                        throw new \Exception($errorMessage);
                    }

                    // сохранить 
                    $this->em->flush();
                    $this->dm->flush();

                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "orderEntry" => $orderEntry,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    
    /**
     * colorbox форма оприходования товара на склад ТК
     * @Route("/entry/{pk}/status-transport", name="Nitra_OrderBundle_Order_Entry_Status_Transport")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderEntryStatusTransport.html.twig")
     */
    public function orderEntryStatusTransportAction(OrderEntry $orderEntry, Request $request)
    {

        // получить устанавливаемый статус позиции
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'transport'));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена
            throw new \Exception("Новый статус позиции заказа не найден.");
        }

        // объект формы
        $form = $this->createForm(new OrderEntryTransportType($orderEntry));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                try {

                    // параметры валидации/выполнения цепочки
                    $options = $form->getData();
                    $options['declarationNumber'] = $form['declarationNumber']->getData();
                    $options['manager'] = $this->getUser();
                    
                    // выполнить цепочку 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);

                    // проверить выполнение цепочки
                    if ($errorMessage) {
                        // цепочка не выполнена, throw
                        throw new \Exception($errorMessage);
                    }
                    // сохранить 
                    $this->em->flush();
                    $this->dm->flush();

                    // форма отработала успешно
                    $formIsValid = true;

                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "orderEntry" => $orderEntry,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    /**
     * colorbox форма возврат товара на склад поставщику
     * @Route("/entry/{pk}/status-returned-to-supplier", name="Nitra_OrderBundle_Order_Entry_Status_Returned_To_Supplier")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderEntryStatusReturnedToSupplier.html.twig")
     */
    public function orderEntryStatusReturnedToSupplierAction(OrderEntry $orderEntry, Request $request)
    {

        // получить устанавливаемый статус позиции
        $newStatus = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'returned_to_supplier'));
        if (!$newStatus) {
            // устанавливаемый новый статус позиции закза не найдена
            throw new \Exception("Новый статус позиции заказа не найден.");
        }

        // объект формы
        $form = $this->createForm(new OrderEntryReturnedToSupplierType($orderEntry));

        // флаг успешной обработки формы
        $formIsValid = false;

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                try {

                    // параметры валидации/выполнения цепочки
                    $options = $form->getData();
                    $options['declarationNumber'] = $form['declarationNumber']->getData();
                    $options['manager'] = $this->getUser();

                    // выполнить цепочку 
                    $errorMessage = $this->em
                        ->getRepository('NitraOrderBundle:OrderEntry')
                        ->setStatus($newStatus, $orderEntry, $options);
                    // проверить выполнение цепочки
                    if ($errorMessage) {
                        // цепочка не выполнена, throw
                        throw new \Exception($errorMessage);
                    }

                    // сохранить 
                    $this->em->flush();
                    $this->dm->flush();

                    // форма отработала успешно
                    $formIsValid = true;
                    
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
                
            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
            }
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            "orderEntry" => $orderEntry,
            "form" => $form->createView(),
            "formIsValid" => $formIsValid,
        );
    }

    /**
     * Метод для вывода истории по статусам позиции заказа
     * @Route("/entry/{pk}/status-history", name="Nitra_OrderBundle_Order_Entry_Status_History")
     * @ParamConverter("orderEntry", class="NitraOrderBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraOrderBundle:OrderActions:orderEntryStatusHistory.html.twig")
     */
    public function orderEntryStatusHistoryAction(OrderEntry $orderEntry)
    {
        // вернуть массив данных передаваемых в шаблон 
        return array(
            'orderEntry' => $orderEntry,
        );
    }

    /**
     * выбор адресса доставки для покупателя
     * @Route("/get-buyer-order-address", name="Nitra_OrderBundle_Get_Buyer_Address")
     */
    public function buyerAddressAction()
    {

        $request = $this->getRequest();
        $buyerId = $request->get('buyer_id');
        $buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->find($buyerId);
        $query = $this->em->createQuery('SELECT o.address 
                 FROM NitraOrderBundle:Order o
                 WHERE o.buyer = :buyer AND o.address IS NOT NULL
                 ORDER BY o.createdAt desc')
                ->setMaxResults(1)
                ->setParameter('buyer', $buyer);
        $result = $query->getArrayResult();
        //Проверяем на пустоту и формируем массив
        //что бы минимизировать код на клиенте
        if (empty($result)) {
            $result[] = array('address' => '');
        }
        return new JsonResponse(json_encode($result));
    }

    /**
     * выбор декларации для автокомплита
     * @Route("/get-declaration-order-autocomplete", name="Nitra_OrderBundle_Get_Declaration_Autocomplete")
     */
    public function declarationAutocompleteAction()
    {
        // То что приходит из поля ввода (строка поиска)
        $search_str = $this->getRequest()->get('q');
        $type_search = $this->getRequest()->get('search_by', 'name');
        $words = preg_split('/\s+/', trim($search_str));
        $where = array();
        foreach ($words AS $word) {
            if ($type_search == 'name') {
                $where [] = " d.declarationNumber LIKE '%" . $word . "%' ";
            }
        }
        $declarations = $this->em->createQuery("SELECT d FROM NitraDeclarationBundle:Declaration d WHERE (" . implode(" OR ", $where) . " )")->getResult();

        $autocomplete_return = array();

        foreach ($declarations as $declaration) {

            if ($type_search == 'name') {

                // получить данные складов отпраитель и получатель по декларации
                $fromWarehouse = $declaration->getFromWarehouse();
                $toWarehouse = $declaration->getToWarehouse();

                // формат даты
                $formatter = \IntlDateFormatter::create(
                    \Locale::getDefault(), 
                    \IntlDateFormatter::MEDIUM, 
                    \IntlDateFormatter::NONE
                );

                // дата в string представлении
                $formatter->setPattern('d MMM yyyy');
                $dateString = $formatter->format($declaration->getDate()->getTimestamp());

                // возвращаемая строка в автокомплит
                $current_declaration_str = 
                    $declaration->getDeclarationNumber() . "|" .
                    $declaration->getDeclarationNumber() . "|" .
                    $declaration->getId() . "|" .
                    json_encode(array(
                        'id' => $declaration->getId(),
                        'fromCityId' => $fromWarehouse->getCity()->getId(),
                        'fromWarehouseId' => $fromWarehouse->getId(),
                        'toCityId' => $toWarehouse->getCity()->getId(),
                        'toWarehouseId' => $toWarehouse->getId(),
                        'declarationNumber' => $declaration->getDeclarationNumber(),
                        'dateTimestamp' => $declaration->getDate()->getTimestamp(),
                        'dateString' => $dateString,
                ));
            }

            $autocomplete_return[] = $current_declaration_str;
        }
        if ($type_search == 'name') {
            $autocomplete_return[] = $search_str . "\\\\Новая декларация|" . $search_str . "|" . "new" . '|' . json_encode(array());
        }

        // вернуть массив автокомплита
        return new Response(implode("\n", $autocomplete_return));
    }

}

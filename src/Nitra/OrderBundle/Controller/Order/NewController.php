<?php
namespace Nitra\OrderBundle\Controller\Order;

use Admingenerated\NitraOrderBundle\BaseOrderController\NewController as BaseNewController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\OrderBundle\Form\Type\Order\SearchProductFiltersType;
use Nitra\OrderBundle\Entity\Buyer;
use Nitra\OrderBundle\Entity\Order;
use Nitra\OrderBundle\Entity\OrderEntry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nitra\OrderBundle\Form\Type\Order\NewType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Form;

class NewController extends BaseNewController
{

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /**
     * @var \Doctrine\ORM\EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;
       
    
    public function createAction()
    {
        $Order = $this->getNewObject();

        $this->preBindRequest($Order);
        $form = $this->createForm($this->getNewType(), $Order);
        $form->bind($this->get('request'));
        
        // дополнительная проверка вложеных форм
        $childrenIsValid = $this->isFormChildrenValid($form);
        
        if ($form->isValid() && $childrenIsValid) {
            try {
                $this->preSave($form, $Order);
                $this->saveObject($Order);
                $this->postSave($form, $Order);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Nitra_OrderBundle_Order_edit", array('pk' => $Order->getId()) ));
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $Order);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('NitraOrderBundle:OrderNew:index.html.twig', $this->getAdditionalRenderParameters($Order) + array(
            "Order" => $Order,
            "form" => $form->createView(),
        ));
    }
    
    /**
     * получить объект нового заказа 
     * @return Nitra\OrderBundle\Entity\Order объект заказа
     */
    protected function getNewObject()
    {
        // получить объект заказа
        $order = new Order();
        // статус по умолчанию 
        $this->em->getRepository('NitraOrderBundle:Order')->setDefaultStatus($order);
        
        // получить объект сессии пользователя
        $session = $this->get('session');
        // проверить филиал пользователя по умолчанию 
        if ($session->has('myFilialDefault')) {
            // получить филиал по умолчанию для пользователя
            $filial = $this->em->getRepository('NitraFilialBundle:Filial')->find($session->get('myFilialDefault'));
            // установить заказу филиал пользователя
            $order->setFilial($filial);
        }
        
        // вернуть объект заказа
        return $order;
    }

    /**
     * @return Nitra\OrderBundle\Form\Type\Order\NewType
     */
    protected function getNewType()
    {
        $type = new NewType($this->em);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
  
    /**
     * Get additional parameters for rendering.
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param \Nitra\OrderBundle\Entity\Order $Order your \Nitra\OrderBundle\Entity\Order object
     * @return array
     **/
    protected function getAdditionalRenderParameters(\Nitra\OrderBundle\Entity\Order $Order)
    {
        
        // объект формы поиска продуктов
        $filterSearchType = new SearchProductFiltersType($this->dm);
        $filterSearch = $this->createForm($filterSearchType);
        
        // получить тип формы
        $formType = $this->getNewType();
        
        // получть данные формы 
        $formData = $this->getRequest()->get($formType->getName());
        
        // массив названий продуктов
        $productNames = array();
        
        // массив ID продуктов 
        $productIds = array();
        
        // наполнить массив ID продуктов из форомы заказов
        if (isset($formData['orderEntries']) && $formData['orderEntries']) {
            foreach($formData['orderEntries'] as $orderEntry) {
                $productIds[] = $orderEntry['productId'];
            }
        }
        
        // наполнить массив ID продуктов из объекта заказа
        foreach($Order->getOrderEntries() as $entry) {
            $productIds[] = $entry->getProductId();
        }
        
        // получить список продуктов по массиву ID 
        $productIds = array_unique($productIds);
        if ($productIds) {
            
            // объект запроса список проудуктов
            $mongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
            $mongoQuery->field('id')->in($productIds);
            
            // получить продукты
            $products = $mongoQuery
                ->getQuery()
                ->execute();
            
            // наполнить массив имен продуктов
            if ($products) {
                foreach($products as $product ) {
                    $productNames[$product->getId()] = (string)$product;
                }
            }
        }
        
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // форма фильтрации
            'filterSearch' => $filterSearch->createView(),
            // массив ID продуктов 
            'orderEntriesProductIds' => $productIds,
            // массив названий продуктов
            'orderEntriesProductNames' => $productNames,
        );
    }
    
    /**
     * preBindRequest
     * @param \Nitra\OrderBundle\Entity\Order $Order your \Nitra\OrderBundle\Entity\Order object
     */
    public function preBindRequest(\Nitra\OrderBundle\Entity\Order $Order)
    {
        // получить тип формы
        $formType = $this->getNewType();
        
        // получть данные формы 
        $formData = $this->getRequest()->get($formType->getName());
        
        // получить пользователя 
        $buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->processBuyerOderForm($formData);        
        // добавить покупателья в заказ
        $Order->setBuyer($buyer);
        
    }
    
     /**
     * preSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\OrderBundle\Entity\Order $Order your \Nitra\OrderBundle\Entity\Order object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\OrderBundle\Entity\Order $Order)
    {
        // получть данные формы 
        $formData = $this->getRequest()->get($form->getName());
        
        // добавить комментарий к заказу
        if (trim($formData['commentAdd'])) {
            $Order->addComment($formData['commentAdd'], $this->getUser()->getUsername());
        }
            
        // обойти все позиции заказов
        foreach ($Order->getOrderEntries() as $orderEntry) {
            // установить статус по умолчанию
            if (!$orderEntry->getOrderEntryStatus()) {
                $this->em->getRepository('NitraOrderBundle:OrderEntry')->setDefaultStatus($orderEntry);
            }
            // persist $orderEntry
            $this->em->persist($orderEntry);
        }
        
        // установить город исходя из выбранного склада
        $city = $Order->getWarehouse()->getCity();
        $Order->setCity($city);
    }
    
    protected function saveObject(\Nitra\OrderBundle\Entity\Order $Order)
    {
        // передаем на функцию сохранения заказа
        $this->em->getRepository('NitraOrderBundle:Order')->saveOrder($Order);
    }
    
    /**
     * функция нахождения стока по продукту и цене выхода и установка его параметров в позицию заказа
     * @param \Nitra\OrderBundle\Entity\OrderEntry $orderEntry
     * @return null - сток добавлен, string - сообщение об ошибке (сток не найден)
     */
    private function findAndAddStock(\Nitra\OrderBundle\Entity\OrderEntry $orderEntry){
               
        //Получаем товар
        $product = $this->dm->getRepository('NitraMainBundle:Product')->find($orderEntry->getProductId());
         // получаем коммент
        $orderComment = $orderEntry->getOrder()->getComment();
        $message = ' Товару: ' . (string) $product . ' устновлена цена входа равная цене выхода, так как у стока отсутствует цена входа';

        // получаем id магазина 
        $storeId = $orderEntry->getOrder()->getStoreId();

        // получаем id своих складов/складов ТК
        $ownWarehouseIds = $this->em->createQueryBuilder()
                ->select('w.id')
                ->from('NitraMainBundle:Warehouse', 'w', 'w.id')
                ->where('w.supplier IS NULL')
                ->andWhere('w.delivery IS NULL')
                ->getQuery()
                ->getResult();

        // ищем стоки по своим складам с указанной ценой
        $ownStocks = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->field('productId')->equals($orderEntry->getProductId())
            ->field('warehouseId')->in(array_keys($ownWarehouseIds))
            ->field('storePrice.' . $storeId . '.price')->equals($orderEntry->getPriceRecommended())
            ->getQuery()
            ->execute();

        // если были найдены стоки то устанавливаем данные из первого стока и выходим из функции 
        if(count($ownStocks)) {
            $stock = $ownStocks->getNext();
            
            if($stock->getPriceIn()){
                $orderEntry->setPriceIn($stock->getPriceIn());
            }else{
                $orderEntry->setPriceIn($orderEntry->getPriceOut());
                $orderEntry->getOrder()->setComment($orderComment . $message);
            }
          
            $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->findOneById($stock->getWarehouseId());
            
            $orderEntry->setWarehouse($warehouse);
            return;
        }

        // ищем по стокам поставщиков
        // выбираем id складов по поставщикам
        $suppliersWarehouseIds = $this->em->createQueryBuilder()
            ->select('w.id')
            ->from('NitraMainBundle:Warehouse', 'w', 'w.id')
            ->where('w.supplier IS NOT NULL')
            ->getQuery()
            ->getResult();
        
        $supplierStocks = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->field('productId')->equals($orderEntry->getProductId())
            ->field('warehouseId')->in(array_keys($suppliersWarehouseIds))
            ->field('storePrice.' . $storeId . '.price')->equals($orderEntry->getPriceRecommended())
            ->sort('priceIn', 'asc')    
            ->getQuery()
            ->execute();
        
        // если были найдены стоки то устанавливаем данные из первого стока и выходим из функции 
        if(count($supplierStocks)) {
            $stock = $supplierStocks->getNext();
           if($stock->getPriceIn()){
                $orderEntry->setPriceIn($stock->getPriceIn());
            }else{
                $orderEntry->setPriceIn($orderEntry->getPriceOut());
                $orderEntry->getOrder()->setComment($orderComment . $message);
            }
            $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->findOneById($stock->getWarehouseId());
            
            $orderEntry->setWarehouse($warehouse);
            return;
        }
        
        // ищем по своим складам без учета цены
         $ownStocks = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->field('productId')->equals($orderEntry->getProductId())
            ->field('warehouseId')->in(array_keys($ownWarehouseIds))
            ->getQuery()
            ->execute();
        
        // если были найдены стоки то устанавливаем данные из первого стока и выходим из функции 
        if(count($ownStocks)) {
            $stock = $ownStocks->getNext();
            if($stock->getPriceIn()){
                $orderEntry->setPriceIn($stock->getPriceIn());
            }else{
                $orderEntry->setPriceIn($orderEntry->getPriceOut());
                $orderEntry->getOrder()->setComment($orderComment . $message);
            }
            $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->findOneById($stock->getWarehouseId());
            $orderEntry->setWarehouse($warehouse);
            return;
        }

        // ищем по складам поставщика без учета цены
        $supplierStocks = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->field('productId')->equals($orderEntry->getProductId())
            ->field('warehouseId')->in(array_keys($suppliersWarehouseIds))
            ->sort('priceIn', 'asc')
            ->getQuery()
            ->execute();

        if(count($supplierStocks)) {
            $stock = $supplierStocks->getNext();
            if($stock->getPriceIn()){
                $orderEntry->setPriceIn($stock->getPriceIn());
            }else{
                $orderEntry->setPriceIn($orderEntry->getPriceOut());
                $orderEntry->getOrder()->setComment($orderComment . $message);
            }
            $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->findOneById($stock->getWarehouseId());
            $orderEntry->setWarehouse($warehouse);
            return;
        }
        
        // подставляем склад по умолчанию
        $orderEntry->setPriceIn($orderEntry->getPriceOut());
        // Получаем id склада с parameters.yml
        $defaultWrehouse = $this->container->hasParameter('default_warehouse') ? $this->container->getParameter('default_warehouse') : null;
        
        // Если не указан id 
        if(!$defaultWrehouse) {
            return 'Не указан склад по умолчанию';
        }
        // находим склад
        $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($defaultWrehouse);
        $orderEntry->setWarehouse($warehouse);

        $message = ' Товар: ' . (string) $product . ' не найден на стоках.';
        $orderEntry->getOrder()->setComment($orderComment . $message);
        return;

    }
    
    /**
     * получение данных из сайта, создание обьекта заказа и передача его в функцию сохранения заказа 
     * @Route("/create-order-from-site", name="create-order-from-site")
     * @return json array
     */
    public function createOrderFromSiteAction()
    {
        // получить передаваемые данные по заказу из сайта
        $orderData = $this->getRequest()->get('orderData');
        if(!$orderData) {
            return $this->sendError('не указан массив параметров для формирования заказа');
        }
        
        //декодировать данные из Json в масссив
        $orderData = json_decode($orderData, true); 
        if(!$orderData) {
            return $this->sendError('массив параметров для формирования заказа передан не верно');
        }
        
        // проверка входящих параметров
        if(!isset($orderData['products']) || !$orderData['products'] 
            || !isset($orderData['storeId']) || !$orderData['storeId']
            || !isset($orderData['fio']) || !$orderData['fio']
            || !isset($orderData['phone']) || !$orderData['phone']) {
            
            return $this->sendError('не указаны обязательные параметры для формирования заказа');
        }
        
        // получить объект заказа
        $order = new Order();
        
        // запомнить id магазина
        $order->setStoreId($orderData['storeId']);
        
        // установить статус заказа по умолчанию 
        $this->em->getRepository('NitraOrderBundle:Order')->setDefaultStatus($order);
        
        // установить филиал по умолчанию 
        $this->em->getRepository('NitraOrderBundle:Order')->setDefaultFilial($order);
        
        // добавление склада прибытия по заказу и установка филиала
        if(isset($orderData['warehouseId']) && $orderData['warehouseId']) {
            
            $orderWarehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($orderData['warehouseId']);
            
            if($orderWarehouse) {
                $order->setWarehouse($orderWarehouse);
            }
            else {
                return $this->sendError('не найден указанный склад прибытия по заказу');
            }
        }     
        
        // поиск пользователя по телефону
        $buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->findOneByPhone($orderData['phone']);
        
        // если пользователь не найден
        if (!$buyer) {
            // покупатель не найден создать нового 
            $buyer = new Buyer();
            $buyer->setName($orderData['fio']);
            $buyer->setPhone($orderData['phone']);
            $this->em->persist($buyer);
        }

        // добавить покупателя в заказ
        $order->setBuyer($buyer);
        
        // добавить комментарий к заказу
        if (isset($orderData['comment']) && trim($orderData['comment'])) {
            $order->addComment($orderData['comment'], 'Покупатель');
        }     
        
        // установить склад доставки заказа
        if (isset($orderData['delivery_warehouse']) && $orderData['delivery_warehouse']) {
            
            // получить склад доставка заказа на склад
            $warehouse = $this->em
                ->getRepository('NitraMainBundle:Warehouse')
                ->find($orderData['delivery_warehouse']);
            
            // склад найден, установить склад доставки заказа
            if ($warehouse) {
                $order->setWarehouse($warehouse);
                $order->setCity($warehouse->getCity());
            }
        }
        
        // установка города (если был передан и найден в базе) 
        if(isset($orderData['city']) && $orderData['city'] &&
            !$order->getCity() && // город не был установлен ранее
            !$order->getWarehouse() // склад не был установлен ранее
        ) {
            $city = $this->em->getRepository('NitraTetradkaGeoBundle:City')->findOneById($orderData['city']);
            if($city) {
               $order->setCity($city);
               // установить склад для заказа из данных города
               // если склад не установлен ранее отдельным параметом
               if (!$order->getWarehouse()) {
                   // получить склад филиала
                   $warehouse = $city->getRegion()->getFilial()->getWarehouse();
                   if ($warehouse) {
                       $order->setWarehouse($warehouse);
                   }
               }
            }
        }
        
        // установка адреса доставки если указан
        if(isset($orderData['delivery_address']) && $orderData['delivery_address']) {
            $order->setIsDirect(true);
            $order->setAddress($orderData['delivery_address']);
        }
        
        foreach($orderData['products'] as $product) {  
            // проверка массива продуктов на обязательные параметры
            if(!isset($product['quantity']) || !$product['quantity']
                || !isset($product['price']) || !$product['price']
                || !isset($product['productId']) || !$product['productId']) {
                
                return $this->sendError('не указаны обязательные параметры по продуктам для формирования заказа');
            }
            $orderEntry = new OrderEntry();
            $this->em->getRepository('NitraOrderBundle:OrderEntry')->setDefaultStatus($orderEntry);
            $orderEntry->setQuantity($product['quantity']);
            
            // сохранение скидки в процентах
            if(isset($product['discountPercent'])) {
                $orderEntry->setDiscountPercent($product['discountPercent']);
            }
            
            // если не пришла цена со скидкой то выбираем обычную цену
            $price = isset($product['priceDiscount']) ? $product['priceDiscount'] : $product['price'];
            $orderEntry->setPriceOut($price);
            
            $orderEntry->setProductId($product['productId']);
            
            // записываем в рекомендованную цену цену без учета скидки
            $orderEntry->setPriceRecommended($product['price']);
            
            //проверяем пришел ил код валюты
            if(isset($product['currencyCode'])){
                $curreny = $this->em->getRepository('NitraMainBundle:Currency')->findOneByCode($product['currencyCode']);     
                $orderEntry->setCurrency($curreny);
            }else{
                //Устанавливаем по умолчанию гривну
                $code = $this->container->getParameter('currency_code');
                $curreny = $this->em->getRepository('NitraMainBundle:Currency')->findOneByCode($code);     
                $orderEntry->setCurrency($curreny);
            }
            $order->addOrderEntry($orderEntry);
            
            // выбор стока для позиции (установка priceIn и warehouse)
            $errorMessage = $this->findAndAddStock($orderEntry);

            if($errorMessage) { 
                return $this->sendError($errorMessage);
            }
            
            $this->em->persist($orderEntry);
        }
        
        // обработка сохранения заказа
        try {
            $this->em->getRepository('NitraOrderBundle:Order')->saveOrder($order);
        }
        catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
        
        return new JsonResponse(array(
                                    'status' => 'success',
                                    'orderId' => $order->getId(),
                                ));
    }
    
    private function sendError($message) 
    {
        return new JsonResponse(array(
                                    'status' => 'error',
                                    'message' => $message,
                                ));
    }
    
    /**
     * выполняет валидацию вложеных форм, добавляет для отображения ошибки в формы
     * @param object Symfony\Component\Form\Form $form
     * @return true | false 
    **/
    private function isFormChildrenValid(Form $form)
    {
        // статическое кол-во ошибок формы, так как функция выполняется рекурсивно
        static $form_errors = 0;

        $validator = $this->container->get('validator');
        
        // рекурсивный вызов функции если форма содержит свои вложенные формы
        foreach($form->all() as $child) {
            if($child->all()) {
                $this->isFormChildrenValid($child);
            }
        }
        
        // валидация обьекта из формы формы
        $errors = $validator->validate($form->getData());
        
        // добавление в форму ошибок для отображения (только по полям указаным в форме)
        foreach($errors as $error) {
            foreach($form->all() as $formField) {
                if($error->getPropertyPath() === (string) $formField->getPropertyPath()) {
                    $formField->addError(new FormError($error->getMessage()));
                    $form_errors += 1;
                }
            }
        }

        return ($form_errors > 0) ? false : true;
        
    }
    
    /**
     * @Route("/get-buyer-info-site", name="get-buyer-info-site")
     * @return json array
     */
    public function getBuyerInfoAction() {
        // получить передаваемые данные по из сайта
        $id         = $this->getRequest()->request->get('id');
        $phone      = $this->getRequest()->request->get('phone');
        $orders     = $this->getRequest()->request->get('orders');
        $storeId    = $this->getRequest()->request->get('storeId');
        
        $json = array();
        
        if(!$id && !$phone) {
            $json['status'] = 'error';
            $json['msg']    = 'errors.input';
        } else {
            if($id) {
                // поиск пользователя по id
                $buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->find((int)$id);
                if($buyer) {
                    $json['status'] = 'success';
                } else {
                    $json['status'] = 'error';
                    $json['msg']    = 'errors.id';
                }
            } elseif($phone) {
                // поиск пользователя по телефону
                $buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->findOneByPhone($phone);
                if($buyer) {
                    $json['status'] = 'success';
                } else {
                    $json['status'] = 'error';
                    $json['msg']    = 'errors.phone';
                }
            }
            if($buyer && ($json['status'] == 'success')) {
                $json['buyer']  = array(
                    'id'        => $buyer->getId(),
                    'name'      => $buyer->getName(),
                    'phone'     => $buyer->getPhone(),
//                    'address'   => $buyer->getAddress(),
                );
                if($orders) {
                    $_orders = $this->em->getRepository('NitraOrderBundle:Order')->findBy(array(
                        'buyer'  => $buyer->getId(),
                        //'storeId'   => $storeId,
                    ), array('deliveryDate' => 'DESC'));
                    $json['orders'] = array();
                    $json['orders_history'] = array();
                    foreach($_orders as $order) {
                        $history_or_no = 'orders';
                        if ($order->getOrderStatus()->getMethodName() == 'completed') {
                            $history_or_no = 'orders_history';
                        }
                        $json[$history_or_no][$order->getId()]['status']       = $order->getOrderStatus()->getName();
                        $json[$history_or_no][$order->getId()]['updatedAt']    = $order->getUpdatedAt();                    // дата последнего обновления (для оповещений)
                        $json[$history_or_no][$order->getId()]['completedAt']  = $order->getCompletedAt();                  // дата завершения
                        $json[$history_or_no][$order->getId()]['deliveryDate'] = $order->getDeliveryDate();                 // дата доставки
                        $json[$history_or_no][$order->getId()]['deliveryCost'] = $order->getDeliveryCost();                 // стоимость доставки
                        $json[$history_or_no][$order->getId()]['isDirect']     = $order->getIsDirect();                     // адрессная ли доставка?
                        $json[$history_or_no][$order->getId()]['isCod']        = $order->getIsCod();                        // Наложенный платёж
                        $json[$history_or_no][$order->getId()]['address']      = $order->getAddress();
                        $json[$history_or_no][$order->getId()]['city']         = array(
                            'name'      => $order->getCity()->getName(),
                            'region'    => $order->getCity()->getRegion()->getName(),
                        );
                        $json[$history_or_no][$order->getId()]['orderEntries'] = array();
                        
                        foreach($order->getOrderEntries() as $order_entry) {
                            $json[$history_or_no][$order->getId()]['orderEntries'][$order_entry->getId()] = array(
                                'quantity'          => $order_entry->getQuantity(),
                                'currency'          => array(
                                    'name'              => $order_entry->getCurrency()->getName(),
                                    'exchange'          => $order_entry->getCurrency()->getExchange(),
                                    'symbol'            => $order_entry->getCurrency()->getSymbol(),
                                    'code'              => $order_entry->getCurrency()->getCode(),
                                ),
                                'priceOut'          => $order_entry->getPriceOut(),
                                'productId'         => $order_entry->getProductId(),
                                'discountPercent'   => $order_entry->getDiscountPercent(),
                                'status'            => $order_entry->getOrderEntryStatus()->getName(),
                            );
                        }
                    }
                }
            }
        }
        
        return new JsonResponse($json);
    }
}

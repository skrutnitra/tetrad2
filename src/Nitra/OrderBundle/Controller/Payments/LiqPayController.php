<?php

namespace Nitra\OrderBundle\Controller\Payments;

use Admingenerator\GeneratorBundle\Controller\Doctrine\BaseController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Nitra\IncomeBundle\Entity\Income;

class LiqPayController extends BaseController
{
    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager()
    {
        return $this->get('doctrine_mongodb.odm.document_manager');
    }

    /** @return \Doctrine\ORM\EntityManager */
    protected function getEntityManager()
    {
        return $this->get('doctrine.orm.entity_manager');
    }

    /**
     * Принять оплату с сервиса
     * 
     * @Route("/take-payment-liq-pay", name="take-payment-liq-pay")
     * 
     * @link https://www.liqpay.com/ru/doc#callback
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function takePaymentLiqPayAction(Request $request)
    {
        // get post parameters
        $data = $request->request->all();

        // if required parameters is missing
        if (!$this->validatePaymentData($data)) {
            throw $this->createNotFoundException();
        }

        // get order object
        $order = $this->getEntityManager()->find('NitraOrderBundle:Order', $data['order_id']);
        // if not order or signature is invalid
        if (!$order || !$this->checkPaymentSignature($data, $order)) {
            throw $this->createNotFoundException();
        }

        // process order and return empty response
        return $this->acceptPaymentLiqPay($order, $data);
    }

    /**
     * process payment
     * 
     * @param type $order
     * @param type $postData
     * 
     * @return Response
     */
    protected function acceptPaymentLiqPay($order, $postData)
    {
        // if status is sandbox or success
        if (in_array($postData['status'], array('success', 'sandbox', ))) {
            // create income
            $this->createIncome($order, $postData, $postData['status'] == 'sandbox');
        }

        // add comment message to order
        $this->addCommentToOrder($order, $postData);

        // update order and persist income if status is success or sandbox
        $this->getEntityManager()->flush();

        // return empty response
        return new Response();
    }

    /**
     * check required fields in post data
     * 
     * @param array $data
     * 
     * @return boolean
     */
    protected function validatePaymentData($data)
    {
        // define array of required keys in post array
        $requiredKeys = array(
            'public_key',
            'amount',
            'currency',
            'description',
            'order_id',
            'signature',
            'status',
        );

        // foreach by required keys
        foreach ($requiredKeys as $key) {
            // and if key non exists in post data
            if (!key_exists($key, $data)) {
                // return false
                return false;
            }
        }

        // if all keys exists - return true
        return true;
    }

    /**
     * check signature of payment
     * 
     * @param array $data
     * @param \Nitra\OrderBundle\Entity\Order $order
     * 
     * @return boolean
     */
    protected function checkPaymentSignature($data, $order)
    {
        // get store id from order filial
        $storeId = $order->getFilial()->getStoreId();

        // find store by id
        // and if not found - return false
        if (!($store = $this->getDocumentManager()->find('NitraMainBundle:Store', $storeId))) {
            return false;
        }

        // get payments from store
        $payments = $store->getPayments();

        // if payments or liqPay is not configured
        if (!$payments || !key_exists('liq_pay', $payments) || !key_exists('private_key', $payments['liq_pay']) || !key_exists('public_key', $payments['liq_pay'])) {
            return false;
        }

        // get private key
        $privateKey = $payments['liq_pay']['private_key'];
        // get public key
        $publicKey  = $payments['liq_pay']['public_key'];

        // if public key is different or signature is not confirmed
        if ($publicKey != $data['public_key'] || !$this->checkSignature($privateKey, $publicKey, $order, $data)) {
            return false;
        }

        return true;
    }

    /**
     * check payment signature
     * 
     * @param string $privateKey
     * @param string $publicKey
     * @param \Nitra\OrderBundle\Entity\Order $order
     * @param array $postData
     * 
     * @return boolean
     */
    protected function checkSignature($privateKey, $publicKey, $order, $postData)
    {
        // generate signature
        $signature = base64_encode(sha1(implode('', array(
            $privateKey,
            number_format($order->getTotal(), 2, '.', ''),
            $this->container->hasParameter('currency_code') ? strtoupper($this->container->getParameter('currency_code')) : 'UAH',
            $publicKey,
            $order->getId(),
            $postData['type'],
            $postData['description'],
            $postData['status'],
            $postData['transaction_id'],
            $postData['sender_phone'],
        )), true));

        // return comparision generated signature and from post data
        return $signature === $postData['signature'];
    }

    /**
     * create income by order
     * 
     * @param \Nitra\OrderBundle\Entity\Order   $order
     * @param array                             $postData
     * @param boolean                           $sandbox
     */
    protected function createIncome($order, $postData, $sandbox = false)
    {
        $account = $this->getEntityManager()->find('NitraAccountBundle:Account', $this->container->getParameter('liq_pay_account_id'));

        $income = new Income();
        $income
            ->setOrder($order)
            ->setAmount($sandbox ? 0 : $order->getTotal())
            ->setAccount($account)
            ->setDate(new \DateTime())
            ->setFilial($order->getFilial())
            ->setComment($this->createIncomeComment($postData, $sandbox));

        $this->getEntityManager()->persist($income);
    }

    /**
     * create comment for income by data from LiqPay service
     * 
     * @param array     $postData
     * @param boolean   $sandbox
     * 
     * @return string
     */
    protected function createIncomeComment($postData, $sandbox)
    {
        $comment = null;

        if ($sandbox) {
            $comment .= "<h3 style='color: red;'>ТЕСТОВЫЙ</h3><p></p>";
        }

        $comment .= "
            <p>Платеж принят системой оплаты LiqPay</p>
            <p><b>сумма</b>: {$postData['amount']}</p>
            <p><b>валюта</b>: {$postData['currency']}</p>
            <p><b>№ транзакции</b>: {$postData['transaction_id']}</p>
            <p><b>№ заказа в LiqPay</b>: {$postData['liqpay_order_id']}</p>
            <p><b>комиссия</b>: {$postData['receiver_commission']}</p>
            <p><b>телефон</b>: {$postData['sender_phone']}</p>
        ";

        return $comment;
    }

    /**
     * add comment to order by post data
     * 
     * @param \Nitra\OrderBundle\Entity\Order   $order
     * @param array                             $postData
     */
    protected function addCommentToOrder($order, $postData)
    {
        // define comment variable
        $comment = null;
        // switch by payment status
        switch ($postData['status']) {
            case 'success':
                $comment = "Заказ оплачен системой LiqPay";
                break;
            case 'failure':
                $comment = "Заказ <span style='color: red'>не оплачен</span> системой LiqPay";
                break;
            case 'wait_secure':
                $comment = "LiqPay - <span style='color: red'>платеж на проверке</span>";
                break;
            case 'sandbox':
                $comment = "Заказ оплачен системой LiqPay - <span style='color: red'>тестовый платеж</span>";
                break;
        }

        // if transaction_id exists in post data
        if (key_exists('transaction_id', $postData)) {
            // append him to order comment
            $comment .= "<br>№ транзакции</b>: {$postData['transaction_id']}";
        }

        $order->addComment($comment, 'liqPay');
    }
}
<?php

namespace Nitra\OrderBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class NewControllerTest extends WebTestCase
{
    /**
     * entity manager для проверки записан ли заказ в базу
     */
    private $em = null;
    
    /**
     * documnet manager для получения продуктов
     */
    private $dm = null;
    
     /**
     * номер выполняемого тетста
     */
    private $testNumber = 0;
    
    /**
     * статусы ответа
     */
    private $statuses = array('error', 'success');
    
    
    public function testIndex()
    {
        // получение менеджеров
        $client = static::createClient();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $this->dm = $client->getContainer()->get('doctrine_mongodb.odm.document_manager');
        
        // выбор правильных параметров для проверки формирования заказа
        $stocksCursor = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->getQuery()
            ->execute();
        
        $productsBase = array();
        
        $productsBase[] = array(
            'productId' => $stocksCursor->getNext()->getProductId(),
            'price' => 40723,
            'quantity' => 1,
        );
        
        $productsBase[] = array(
            'productId' => $stocksCursor->getNext()->getProductId(),
            'price' => 100,
            'priceDiscount' => 80,
            'quantity' => 3,
        );
        
        $productsBase[] = array(
            'productId' => $stocksCursor->getNext()->getProductId(),
            'price' => 150,
            'priceDiscount' => 150,
            'quantity' => 2,
        );
                
        $warehouses = $this->em->createQueryBuilder()
            ->select('p')
            ->from('NitraMainBundle:Warehouse', 'p')
            ->setMaxResults(1)
            ->getQuery()
            ->execute();
        
        $stores = $this->dm->getRepository('NitraMainBundle:Store')->findAll();
        $storeId = $stores->getNext()->getId();
        
        // тест №1 (все параметры верны)
        $orderData = $this->createOrderData(
            $productsBase, 
            $phone = '+39090902424', 
            $fio = 'Пупкин В.А.', 
            $email = null, 
            $warehouses[0]->getId(), 
            $storeId,
            $comment = null
        );

        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());

        $this->assertEquals('success', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №2 (не передан массив продуктов)
        $orderData = $this->createOrderData(
            $productsBase, 
            $phone = 7878789798, 
            $fio = 'Пупкин В.А.', 
            $email = null, 
            $warehouses[0]->getId(), 
            $storeId,
            $comment = null
        );
        
        unset($orderData['orderData']['products']);

        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №3 (массив продуктов передан как null)
        $orderData = $this->createOrderData(
            $products = null,
            $phone = 7878789798, 
            $fio = 'Пупкин В.А.', 
            $email = null, 
            $storeId,
            $warehouses[0]->getId(), 
            $comment = null
        );
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №4 (не передан id магазина)
        $orderData = $this->createOrderData(
            $productsBase, 
            $phone = 7878789798, 
            $fio = 'Пупкин В.А.', 
            $email = null, 
            $storeId,
            $warehouses[0]->getId(), 
            $comment = null
        );
        
        unset($orderData['orderData']['storeId']);
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №5 (не передан телефон)
        $orderData = $this->createOrderData(
            $productsBase,
            $phone = null, 
            $fio = 'Пупкин В.А.', 
            $warehouses[0]->getId(), 
            $email = null, 
            $comment = null
        );
        
        unset($orderData['orderData']['phone']);
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №6 (передан телефон null)
        $orderData = $this->createOrderData(
            $productsBase,
            $phone = null, 
            $fio = 'Пупкин В.А.', 
            $warehouses[0]->getId(), 
            $email = null, 
            $comment = null
        );
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №7 (не переданы ФИО)
        $orderData = $this->createOrderData(
            $productsBase, 
            $phone = '24242442424', 
            $fio = null, 
            $warehouses[0]->getId(), 
            $email = null, 
            $comment = null
        );
        
        unset($orderData['orderData']['fio']);
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №8 (ФИО переданы как null)
        $orderData = $this->createOrderData(
            $productsBase, 
            $phone = '24242442424', 
            $fio = null, 
            $warehouses[0]->getId(), 
            $email = null, 
            $comment = null
        );
        
        unset($orderData['orderData']['fio']);
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №9 (передан несуществующий склад)
        $orderData = $this->createOrderData(
                $productsBase,
                $phone = '24242442424', 
                $fio = 'Вася П.П', 
                999999333, 
                $email = null, 
                $comment = null
        );
        
        unset($orderData['orderData']['fio']);
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
        // тест №10 (передан несуществующий продукт
        $orderData = $this->createOrderData(array(
                $stocksCursor->getNext()->getId() => 3,
                $stocksCursor->getNext()->getId() => 3,
                'wrongProductId' => 1,
                $stocksCursor->getNext()->getId() => 2,
            ),
            $phone = null, 
            $fio = 'Пупкин В.А.', 
            $warehouses[0]->getId(), 
            $email = null, 
            $comment = null
        );
        
        $client = $this->createClientFromOrderData($orderData);
       
        $this->checkServerResponse($client, $orderData);
        
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertEquals('error', $content->status, 'Тест №'. $this->testNumber . ' провалился');
        
    }
    
    private function checkServerResponse(\Symfony\Bundle\FrameworkBundle\Client $client, array $orderData) 
    {
        // Проверка что ответ от сервера успешный
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Тест №'. $this->testNumber . ' код ответа от сервера не 2хх');
        
        // Проверка что ответ в формате json "application/json"
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'Тест №'. $this->testNumber . ' ответ от сервера не в формате json'
        );
        
        // Проверка статуса ответа 
        $content = json_decode($client->getResponse()->getContent());
        
        $this->assertTrue(isset($content->status),
            'Тест №'. $this->testNumber . ' не указан статус ответа'
        );

        $this->assertTrue(in_array($content->status, $this->statuses),
            'Тест №'. $this->testNumber . ' неправильный статус ответа'
        );
        
        // Проверка того что заказ создался если status=ok
        if($content->status == 'success') {
            
            $this->assertTrue(isset($content->orderId),
                'Тест №'. $this->testNumber . ' не указан id созданного заказа'
            );

            $order = $this->em->getRepository('NitraOrderBundle:Order')->findById($content->orderId);

            $this->assertTrue(!empty($order),
                'Тест №'. $this->testNumber . ' не найден созданный заказ по id'
            );
            
            // проверка количеста позиций в заказе (для теста когда они не были удалены)
            if(isset($orderData['orderData']['products']) && $orderData['orderData']['products']) {
                $this->assertEquals(count($orderData['orderData']['products']), count($order[0]->getOrderEntries()), 'Тест №'. $this->testNumber . ' количество позиций в созданном заказе не совпадает с количеством указанных продуктов');
            }
        }
        
    }
    
    /**
     * создание пост запроса с параметрами по заказу + указание номера теста
     * @param array $orderData
     * @return Symfony\Bundle\FrameworkBundle\Client client
     */
    private function createClientFromOrderData(array $orderData) 
    {
        
        $client = static::createClient(array(), array('HTTP_HOST' => 'tetradka2'));
        
        // отправление пост запроса на сохранение заказа в тетрадку
        $client->request(
            'POST',
            '/create-order-from-site',
            $orderData
        );

        if($client->getResponse() instanceof RedirectResponse) {
            $client->followRedirect();
            $client->request(
                'POST',
                $client->getHistory()->current()->getUri(),
                $orderData
            );
        }
        return $client;
    }
    
    /**
     * создание массива по заказу для передачи
     * @param array $products
     * @param string $phone
     * @param string $fio
     * @param integer $warehouseId
     * @param string $email
     * @param string $comment
     * @return array
     */
    private function createOrderData($products = array(), $phone = null, $fio = null, $email = null, $warehouseId = null, $storeId = null, $comment = null) 
    {
        // увеличения номера теста
        $this->testNumber++;
                
        return array('orderData' => array(
            'products' => $products,
            'phone' => $phone,
            'fio' => $fio,
            'email' => $email,
            'warehouseId' => $warehouseId,
            'storeId' => $storeId,
            'comment' => $comment,
        ));
    }
}
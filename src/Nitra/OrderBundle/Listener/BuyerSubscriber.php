<?php

namespace Nitra\OrderBundle\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Nitra\OrderBundle\Entity\Buyer as BuyerTetradka;
use Nitra\OrderBundle\Document\Buyer as BuyerSite;

class BuyerSubscriber implements EventSubscriber
{
    /**
     * @var DocumentManager $dm
     */
    protected $dm;

    /**
     * Конструктор
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }
    
    /**
     * получить массив саобытий
     */
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }
    
    /**
     * postUpdate
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }
    
    /**
     * postPersist
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
        
    }

    /**
     * основной метод обновления данных покупателя
     */
    protected function index(LifecycleEventArgs $args)
    {
        // получить данные 
        $entity = $args->getEntity();
        
        // если обновленмя сущность есть покупатель тетрадки
        if ($entity instanceof BuyerTetradka) {
            
            // найти (создать нового) покупателя на сайте
            $buyerSite = $this->findBuyerSite($entity);
            
            // обновить покупателя на сайте 
            $buyerSite
                ->setTetradkaId($entity->getId())
                ->setName($entity->getName())
                ->setPhone($entity->getPhone());
            if (method_exists($entity, 'getEmail')) {
                $buyerSite->setEmail($entity->getEmail());
            }
            
//            // получить город последнего заказа
//            $city = $entity->getCityLastOrder();
//            if ($city) {
//                // обновить город покупателя
//                $buyerSite
//                    ->setCityId($city->getId())
//                    ->setCityName($city->getName());
//            }
            
            // сохранить покупателя 
            // сохраняем каждого покупателя в отдельности 
            // что бы не обнулить массив документов 
            // которые будут сохраненны вне рамках данного листенера
            $this->dm->flush($buyerSite);
        }
    }

    /**
     * Получить покупателя на сайте
     * @param BuyerTetradka $buyerTetradka - покупатель в тетрадке;
     * @return BuyerSite - покупатель на сайте, если покупатель на сайте не найден то будет создан новый
     */
    protected function findBuyerSite(BuyerTetradka $buyerTetradka)
    {
        // получить покупателя на сайте по ключу 
        $buyerSite = $this->dm
            ->getRepository('NitraOrderBundle:Buyer')
            ->findOneBy(array(
                'tetradkaId' => $buyerTetradka->getId()
            ));
        
        // если покупатель найден 
        if ($buyerSite) {
            // вернуть покупателя
            return $buyerSite;
        }

        if (method_exists($buyerTetradka, 'getEmail')) {
            // получить покупателя по телефону и email 
            $buyerSite = $this->dm
                ->getRepository('NitraOrderBundle:Buyer')
                ->findOneBy(array(
                    'phone' => $buyerTetradka->getPhone(),
                    'email' => $buyerTetradka->getEmail(),
                ));

            // если покупатель найден 
            if ($buyerSite) {
                // вернуть покупателя
                return $buyerSite;
            }
        }
        
        // получить покупателя по телефону
        $buyerSite = $this->dm
            ->getRepository('NitraOrderBundle:Buyer')
            ->findOneBy(array(
                'phone' => $buyerTetradka->getPhone(),
            ));
        
        // если покупатель найден 
        if ($buyerSite) {
            // вернуть покупателя
            return $buyerSite;
        }
        
        if (method_exists($buyerTetradka, 'getEmail')) {
            // получить покупателя по телефону и email 
            $buyerSite = $this->dm
                ->getRepository('NitraOrderBundle:Buyer')
                ->findOneBy(array(
                    'email' => $buyerTetradka->getEmail(),
                ));

            // если покупатель найден 
            if ($buyerSite) {
                // вернуть покупателя
                return $buyerSite;
            }
        }
        
        // покупатель не найден 
        // создать нового покупателя
        $buyerSite = new BuyerSite();
        $this->dm->persist($buyerSite);

        return $buyerSite;
    }
}
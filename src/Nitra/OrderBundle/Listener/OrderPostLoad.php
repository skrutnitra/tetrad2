<?php

namespace Nitra\OrderBundle\Listener;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Переписать с использованием DI DoctrineListener
 * @DoctrineListener(
 *     events = {"postLoad"},
 *     connection = "default",
 *     lazy = true,
 *     priority = 0,
 * )
 * @Tag("doctrine.event_listener", attributes = {"event" = "postLoad", lazy=true})
 */
class OrderPostLoad
{
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function postLoad(LifecycleEventArgs $eventArgs)
    {
        // get the order entity 
        $entity = $eventArgs->getEntity();
        // Подгружаем только для обьектов содержащих ссылку на товар
        if (method_exists($entity, 'getProductId')) {
            // get odm reference to product_id 
            $productId  = $entity->getProductId();
            $product    = $this->dm->find('NitraMainBundle:Product', $productId);

            if ($product) {
                // set the product on the order 
                $em = $eventArgs->getEntityManager();
                $productReflProp = $em->getClassMetadata(get_class($entity))
                    ->reflClass->getProperty('product');
                $productReflProp->setAccessible(true);
                $productReflProp->setValue($entity, $product);
            }
        }
    }
}
<?php
namespace Nitra\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Nitra\OrderBundle\Entity\SmsStatus;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadSmsStatusData extends AbstractFixture implements OrderedFixtureInterface 
{

    public function load(ObjectManager $manager)
    {
        //sms_canceled
        $canceled = new SmsStatus();
        $canceled->setMethodName('sms_canceled');
        $canceled->setName('Отправка отменена');
        $canceled->setSortOrder(1);
        $manager->persist($canceled);
        
        // sms_delivered
        $delivered = new SmsStatus();
        $delivered->setMethodName('sms_delivered');
        $delivered->setName('Заказ оплачен');
        $delivered->setSortOrder(2);
        $manager->persist($delivered);
        
        // sms_gateway
        $gateway = new SmsStatus();
        $gateway->setMethodName('sms_gateway');
        $gateway->setName('Передано оператору');
        $gateway->setSortOrder(3);
        $manager->persist($gateway);
        
        // sms_waiting
        $waiting = new SmsStatus();
        $waiting->setMethodName('sms_waiting');
        $waiting->setName('Ожидание доставки');
        $waiting->setSortOrder(4);
        $manager->persist($waiting);
        
        //sms_other
        $other = new SmsStatus();
        $other->setMethodName('sms_other');
        $other->setName('Неизвестный статус');
        $other->setSortOrder(5);
        $manager->persist($other);
        
        
        // сохранить 
        $manager->flush();
        
    }

    public function getOrder()
    {
        return 0;
    }

}


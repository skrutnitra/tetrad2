<?php
namespace Nitra\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\OrderBundle\Entity\OrderStatus;
use Nitra\OrderBundle\Entity\OrderEntryStatus;


class LoadStatusesData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // массив статусов заказов
        $orderStatuses = array();
        $orderStatuses[] = array('methodName' => 'waiting',     'sortOrder' => 10,  'name' => 'Ожидание',           'cssClass' => 'i_clock');
        $orderStatuses[] = array('methodName' => 'ordered',     'sortOrder' => 20,  'name' => 'Заказан',            'cssClass' => 'i_cart orange');
        $orderStatuses[] = array('methodName' => 'ready',       'sortOrder' => 30,  'name' => 'Готов',              'cssClass' => 'i_save blue');
        $orderStatuses[] = array('methodName' => 'reported',    'sortOrder' => 40,  'name' => 'Отчет покупателю',   'cssClass' => 'i_message yellow');
        $orderStatuses[] = array('methodName' => 'completed',   'sortOrder' => 50,  'name' => 'Завершен',           'cssClass' => 'i_save green');
        $orderStatuses[] = array('methodName' => 'canceled',    'sortOrder' => 60,  'name' => 'Отменен',            'cssClass' => 'i_close gray');
        
        // обойти массив создаваемых статусов
        foreach($orderStatuses as $row) {
            // создать статус 
            $status = new OrderStatus();
            $status->setMethodName($row['methodName']);
            $status->setName($row['name']);
            $status->setSortOrder($row['sortOrder']);
            if ($row['cssClass']) {
                $status->setCssClass($row['cssClass']);
            }
            // persist
            $manager->persist($status);
        }
        
        
        // массив статусов позиций заказов
        $orderEntryStatuses = array();
        $orderEntryStatuses[] = array('methodName' => 'waiting',    'sortOrder' => 10,  'name' => 'Ожидание',   'cssClass' => 'i_clock');
        $orderEntryStatuses[] = array('methodName' => 'clarified',  'sortOrder' => 20,  'name' => 'Уточнено',   'cssClass' => 'i_comment blue');
        $orderEntryStatuses[] = array('methodName' => 'ordered',    'sortOrder' => 30,  'name' => 'Заказан',    'cssClass' => 'i_cart yellow');
        $orderEntryStatuses[] = array('methodName' => 'sended',     'sortOrder' => 40,  'name' => 'Отправлен',  'cssClass' => 'i_box orange');
        $orderEntryStatuses[] = array('methodName' => 'arrived',    'sortOrder' => 50,  'name' => 'На складе',  'cssClass' => 'i_warehouse  orange');
        $orderEntryStatuses[] = array('methodName' => 'transport',  'sortOrder' => 60,  'name' => 'ТК',         'cssClass' => 'i_logistic orange');
        $orderEntryStatuses[] = array('methodName' => 'completed',  'sortOrder' => 70,  'name' => 'Завершен',   'cssClass' => 'i_save green');
        $orderEntryStatuses[] = array('methodName' => 'canceled',   'sortOrder' => 80,  'name' => 'Отменен',    'cssClass' => 'i_close gray');
        $orderEntryStatuses[] = array('methodName' => 'returned_to_supplier',   'sortOrder' => 90,  'name' => 'Вернули поставщику', 'cssClass' => 'i_undo magenta');
        
        // обойти массив создаваемых статусов
        foreach($orderEntryStatuses as $row) {
            // создать статус 
            $status = new OrderEntryStatus();
            $status->setMethodName($row['methodName']);
            $status->setName($row['name']);
            $status->setSortOrder($row['sortOrder']);
            if ($row['cssClass']) {
                $status->setCssClass($row['cssClass']);
            }
            // persist
            $manager->persist($status);
        }
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10; // the order in which fixtures will be loaded
    }
}
<?php
namespace Nitra\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Nitra\OrderBundle\Entity\SmsTypeMessage;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadSmsTypeMessageData extends AbstractFixture implements OrderedFixtureInterface 
{

    public function load(ObjectManager $manager)
    {
        //ordered
        $ordered = new SmsTypeMessage();
        $ordered->setMethodName('ordered');
        $ordered->setName('Заказ принят');
        $ordered->setSortOrder(1);
        $manager->persist($ordered);
        
        // payed
        $payed = new SmsTypeMessage();
        $payed->setMethodName('payed');
        $payed->setName('Заказ оплачен');
        $payed->setSortOrder(2);
        $manager->persist($payed);
        
        // in_stock
        $stock = new SmsTypeMessage();
        $stock->setMethodName('in_stock');
        $stock->setName('На складе');
        $stock->setSortOrder(3);
        $manager->persist($stock);
        
        // completed
        $completed = new SmsTypeMessage();
        $completed->setMethodName('completed');
        $completed->setName('Заказ завершен');
        $completed->setSortOrder(4);
        $manager->persist($completed);
        
        
        // сохранить 
        $manager->flush();
        
    }

    public function getOrder()
    {
        return 0;
    }

}


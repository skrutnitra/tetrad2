<?php
namespace Nitra\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Nitra\OrderBundle\Entity\Order;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\MainBundle\Document\Stock;
use Nitra\MainBundle\Entity\Warehouse;

class LoadOrderData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    private $em;
    
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    private $dm;
    
    // данные для создания заказа
    private $warehouseOwn;
    private $warehouseSupplier;
    private $product;
    private $filial;
    private $store;
    private $buyer;
    private $city;
    // статусы закза 
    private $orderStatusReady;
    private $orderStatusReported;
    // статусы позиций заказов
    private $orderEntryStatusClarified;
    private $orderEntryStatusSended;
    private $orderEntryStatusArrived;
    private $orderEntryStatusReturnedToSupplier;
    // все продукты
    private $productsAll;

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 20; // the order in which fixtures will be loaded
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
        
        // установить EntityManager
        $this->em = $this->container->get('doctrine')->getManager();
        
        // установить DocumentManager
        $this->dm = $this->container->get('doctrine_mongodb')->getManager();
    }
    
    /**
     * переопределяем (копируем) метода получения стока
     * не можем использовать общий метод 
     * em, dm репозиторя StockRepository находятся не зоны видимости $this->em, $this->dm
     * и данном методе разрешено создавать сток на складе поставщика в общем нет 
     * @param  integer  $warehouseId    - идентификатор склада
     * @param  string   $productId      - идентификатор продукта
     * @param  string   $stockParams    - строка параметров стока
     * @return object   Nitra\MainBundle\Document\Stock - объект стока
     * @retun  false    - сток не найден
     */
    public function getStock($warehouseId, $productId, $stockParams = null)
    {
        
        // получить склад
        $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($warehouseId);
        if (!$warehouse) {
            throw new \Exception("getStock: Склад не найден.");
        }
        
        // получить продукт 
        $product = $this->dm->getRepository('NitraMainBundle:Product')->find($productId);
        if (!$product) {
            throw new \Exception("getStock: Продукт не найден.");
        }
        
        // получить сток
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->findOneBy(array(
            'warehouseId' => $warehouse->getId(),
            'productId' => $product->getId(),
            'stockParams' => $stockParams,
        ));
        
        // вернуть сток 
        if ($stock) {
            return $stock;
        }
        
//        // сток не найден 
//        // если склад не поставщик (наш склад или склад ТК) то создать сток
//        if ($warehouse->isSupplier() !== true) {
        
            // в фикстурах создание стока обязельное и для склада поставщика
            // создать объект стока 
            $stock = new Stock();
            $stock->setWarehouseId($warehouse->getId());
            $stock->setProductId($product->getId());            
            $stock->setQuantity(10); // начальное кол-во товара
            $stock->setStockParams($stockParams);
            $stock->setPriceName((string)$product);
            $stock->setPriceArticle($product->getArticle());
            //установить цены по магазинам из продукта
            $stock->setStorePrice($product->getStorePrice());
            $stock->setCurrencyPrice(100);
            $stock->setPriceIn(100);
            
            // валидировать сток
            $validator = $this->container->get('validator');
            $errors = $validator->validate($stock);
            
            // проверить результат валидации 
            if (count($errors) > 0) {
                // выкинкть ошибку валидации объекта стока
                foreach($errors as $error) {
                    throw new \Exception("getStock: Не указан обязательный параметр стока. \"".$error->getPropertyPath()."\" ".$error->getMessage());
                }
            }
            
            // сохранить сток 
            $this->dm->persist($stock);
            $this->dm->flush($stock);

            // вернуть сток 
            return $stock;
//        }
        
        // сток не найден
        return false;
    }
    
    
    /**
     * получить случайный ID товара
     * @return integer | null
     */
    public function getRandProduct()
    {
        // получить продукты 
        if (!$this->productsAll) {
            $this->productsAll = $this->dm->getRepository('NitraMainBundle:Product')->findAll();
        }
        
        // проверить продукты
        if (!$this->productsAll) {
            throw new \Exception("Нет ни одного продукта.");
        }
        
        // случайный продукт
        $randKey = array_rand(array_keys($this->productsAll));
        
        // вернуть случайный продукт
        if ($randKey !== null) {
            return $this->productsAll[$randKey];
        }
        
        return null;
    }
    
    
    /**
     * получить новую позицию заказа
     * @param Warehouse $warehouse склад с которого заказали позицию 
     * @param string $stockParams - параметры стока новой позиции 
     * @param string $currencyCode - код валюты
     * @return OrderEntry
     */
    public function getNewOrderEntry(Warehouse $warehouse, $stockParams = null, $currencyCode=null)
    {
        // случайный пролукт
        $product = $this->getRandProduct();
        
        // получить сток проукта 
        $stock = $this->getStock($warehouse->getId(), $product->getId(), $stockParams);
        
        // цена выхода для магазина
        $priceOut = $stock->getStorePriceOut($this->store->getId());
        
        // если валюта не определена
        if (!$currencyCode) {
            // получить валюту по умолчанию 
            $currency = $this->em->getRepository('NitraMainBundle:Currency')->getDefaultCurrency();
        } else {
            // имя валюты 
            $referenceName = 'currency'.strtoupper($currencyCode);
            // получить валюту из фикстур 
            $currency = $this->getReference($referenceName);
        }
        
        // создать позицию заказа
        $orderEntry = new OrderEntry();
        $orderEntry->setQuantity(1);
        $orderEntry->setPriceIn($stock->getPriceIn());
        $orderEntry->setCurrency($currency);
        $orderEntry->setPriceOut($priceOut);
        $orderEntry->setPriceRecommended($priceOut);
        $this->em->getRepository('NitraOrderBundle:OrderEntry')->setDefaultStatus($orderEntry);
        $orderEntry->setWarehouse($warehouse);
        $orderEntry->setProductId($product->getId());
        $orderEntry->setStockParams($stock->getStockParams());
        
        // вернуть объект позиции заказа
        return $orderEntry;
    }
    
    /**
     * получить новый заказ
     * @return Nitra\OrderBundle\Entity\Order
     */
    public function getNewOrder()
    {
        $order = new Order();
        $order->setWarehouse($this->warehouseOwn);
        $order->setDeliveryDate(new \DateTime());
        $order->setBuyer($this->buyer);
        $order->setFilial($this->filial);
        $this->em->getRepository('NitraOrderBundle:Order')->setDefaultStatus($order);
        $order->setCity($this->city);
        
        // вернуть объект заказа
        return $order;
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // получить свой склад
        $this->warehouseOwn = $this->em->getRepository('NitraMainBundle:Warehouse')
            ->buildQueryOwnWarehouses()
            ->getQuery()
            ->setMaxResults(1)
            ->getSingleResult()
            ;
        
        // получить склад поставщика
        $this->warehouseSupplier = $this->em->getRepository('NitraMainBundle:Warehouse')
            ->buildQuerySupplierWarehouses()
            ->getQuery()
            ->setMaxResults(1)
            ->getSingleResult()
            ;
        
        // получить филиал
        $this->filial = $this->em->getRepository('NitraFilialBundle:Filial')->findOneBy(array());
        
        // получить магазин
        $this->store = $this->dm->getRepository('NitraMainBundle:Store')->find($this->filial->getStoreId());
        
        // получить покупателя
        $this->buyer = $this->em->getRepository('NitraOrderBundle:Buyer')->findOneBy(array());
        
        // получиьть город
        $this->city = $this->em->getRepository('NitraTetradkaGeoBundle:City')->findOneBy(array());        
        
        // получить статусы заказа
        $this->orderStatusReady = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneBy(array('methodName' => 'ready'));
        $this->orderStatusReported = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneBy(array('methodName' => 'reported'));
        
        // получить статусы позиции заказа
        $this->orderEntryStatusClarified = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'clarified'));
        $this->orderEntryStatusSended = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'sended'));
        $this->orderEntryStatusArrived = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'arrived'));
        $this->orderEntryStatusReturnedToSupplier = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findOneBy(array('methodName' => 'returned_to_supplier'));
        
        
        // создать заказы
        for($i=1; $i<=15; $i++) {
            
            // создать заказ
            $order = $this->getNewOrder();

            // создать позицию заказа со своего склада
            $orderEntry1 = $this->getNewOrderEntry($this->warehouseOwn, null);
            $this->em->persist($orderEntry1);
            $order->addOrderEntry($orderEntry1);
            // создать позицию заказа со своего склада
            $orderEntry2 = $this->getNewOrderEntry($this->warehouseOwn, 'Некондиция');
            $this->em->persist($orderEntry2);
            $order->addOrderEntry($orderEntry2);

            // создать позицию заказ со склада поставщика
            $orderEntry5 = $this->getNewOrderEntry($this->warehouseSupplier, null);
            $this->em->persist($orderEntry5);
            $order->addOrderEntry($orderEntry5);
            // создать позицию заказ со склада поставщика
            $orderEntry6 = $this->getNewOrderEntry($this->warehouseSupplier, 'вмятина слева');
            $this->em->persist($orderEntry6);
            $order->addOrderEntry($orderEntry6);
            
            // для 13 заказа меняем статус настройка системы отображения статусов
            if ($i == 13) {
                $order->setOrderStatus($this->orderStatusReported);
                // обновить статус позиции заказ
                $orderEntry1->setOrderEntryStatus($this->orderEntryStatusClarified);
                $orderEntry2->setOrderEntryStatus($this->orderEntryStatusSended);
                $orderEntry5->setOrderEntryStatus($this->orderEntryStatusArrived);
                $orderEntry6->setOrderEntryStatus($this->orderEntryStatusReturnedToSupplier);
            }
            
            // для 12 заказа меняем статус настройка системы отображения статусов
            if ($i == 12) {
                $order->setOrderStatus($this->orderStatusReady);
            }
            
            // запомнить
            $this->em->persist($order);
        }
        
        // сохранить 
        $this->em->flush();
    }
    
}
<?php
namespace Nitra\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\OrderBundle\Entity\Buyer;

class LoadBuyerData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // создать записи 
        for ($index = 0; $index < 10; $index++) {
            
            // название переменной 
            $objName = 'buyer'.$index;
            
            // создать объект 
            $$objName = new Buyer();
            $$objName->setName('Покупатель '.$index);
            $$objName->setPhone('Телефон '.$index);
            $manager->persist($$objName);
            
            // запомнить
            $this->addReference($objName, $$objName);
        }
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
}
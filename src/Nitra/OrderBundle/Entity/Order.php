<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Nitra\OrderBundle\Repository\OrderRepository")
 */
class Order
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $comment
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;
    
    /**
     * @var string $cancelComment
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $cancelComment;
    
    /**
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(nullable=true)
     */
    private $warehouse;
    
    /**
     * @var Date - дата доставки
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $deliveryDate;
    
    /**
     * @var integer $delivery_cost - стоимость доставки
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Не указана стоимость доставки")
     * @Assert\Range(min=0, max=999999)
     */
    private $deliveryCost;
    
    /**
     * @var bool $isDirect - адрессная ли доставка?
     * @ORM\Column(type="boolean")
     */
    private $isDirect;
    
    /**
     * @var bool $isCod - Наложенный платёж
     * @ORM\Column(type="boolean" )
     */
    private $isCod;
    
    /**
     * @var string $address - адресс доставки
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $address;
    
    /**
     * @ORM\ManyToOne(targetEntity="Buyer")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Type(type="Nitra\OrderBundle\Entity\Buyer")
     * @Assert\NotBlank(message="Не указан покупатель")
     */
    private $buyer;
    
    /**
     * @ORM\ManyToOne(targetEntity="Nitra\FilialBundle\Entity\Filial")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан филиал")
     * @Assert\Type(type="Nitra\FilialBundle\Entity\Filial")
     */
    private $filial;
    
    /**
     * @ORM\OneToMany(targetEntity="OrderEntry", mappedBy="order")
     */
    private $orderEntries;

    /**
     * @ORM\OneToMany(targetEntity="Sms", mappedBy="order")
     */
    private $sms;

    /**
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан статус заказа")
     * @Assert\Type(type="Nitra\OrderBundle\Entity\OrderStatus")
     */
    private $orderStatus;
    
    /**
     * @ORM\OneToMany(targetEntity="OrderStatusHistory", mappedBy="order")
     */
    private $orderStatusHistory;

    /**
     * @ORM\OneToMany(targetEntity="\Nitra\IncomeBundle\Entity\Income", mappedBy="order")
     */
    private $incomes;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Nitra\GeoBundle\Entity\Model\CityInterface")
     * @ORM\JoinColumn(nullable=true)
     */
    private $city;
    
    /**
     * id магазина, с которого был сделан заказ (для формирования заказа с сайта)
     * @var string $storeId - адресс магазина
     */
    private $storeId;
    
    
    /**
     * Кем завершен заказ 
     * @ORM\ManyToOne(targetEntity="\Nitra\ManagerBundle\Entity\Manager")
     * @ORM\JoinColumn(nullable=true)
     */
    private $completedBy;
    
    /**
     * @var \DateTime - Когда завершен заказ
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */    
    private $completedAt;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deliveryCost = 0;
        $this->isDirect = false;
        $this->isCod = false;
        $this->orderEntries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orderStatusHistory = new \Doctrine\Common\Collections\ArrayCollection();
        $this->incomes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString()
    {
        return (string)$this->getId();
    }
    
    /**
     * @Assert\True(message = "Нет ни одной товарной позиции для заказа")
     */
    public function isOrderEntriesExists()
    {
        // проверить количество позиций
        if (count($this->orderEntries)) {
            return true;
        }
        
        // нет позиций
        return false;
    }
    
    /**
     *  Метод расчета оплаченной суммы по заказу
     */
    public function getPayed()
    {
        $left = 0;
        foreach ($this->incomes as $income) {
            $left += $income->getAmount();
        }

        return $left;
    }

    /**
     * Метод расчета, сколько осталось оплатить
     */
    public function getPayedLeft()
    {
        return $this->getTotal() - $this->getPayed();
    }
    
    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get amount
     * @return float
     */
    public function getAmount()
    {
        $summ = 0;
        foreach ($this->getOrderEntries() as $oe) {
            if ($oe->getOrderEntryStatus()->getMethodName()=='completed') {
                $summ += $oe->getPriceOut() * $oe->getQuantity();
            }
        }

        return $summ;
    }
    
    /**
     * Get profit
     * @return float
     */
    public function getProfit()
    {
        $summ = 0;
        foreach ($this->getOrderEntries() as $oe) {
            if (!in_array($oe->getOrderEntryStatus()->getMethodName(), array('canceled', 'returned_to_supplier'))) {
                $summ += ($oe->getPriceOut() - $oe->getPriceIn()) * $oe->getQuantity();
            }
        }
        
        return $summ;
    }
    
    /**
     * Get total
     * @return float
     */
    public function getTotal()
    {
        $summ = 0;
        foreach ($this->getOrderEntries() as $oe) {
            if ($oe->getOrderEntryStatus() &&
                !in_array($oe->getOrderEntryStatus()->getMethodName(), array('canceled', 'returned_to_supplier'))) {
                $summ += $oe->getPriceOut() * $oe->getQuantity();
            }
        }

        return $summ;
    }

    /**
     * Set comment
     * @param  string $comment
     * @return Order
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Get comment
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set buyer
     * @param  Nitra\OrderBundle\Entity\Buyer $buyer
     * @return Order
     */
    public function setBuyer(\Nitra\OrderBundle\Entity\Buyer $buyer = null)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * Get buyer
     * @return Nitra\OrderBundle\Entity\Buyer
     */
    public function getBuyer()
    {
        return $this->buyer;
    }


    /**
     * Get orderEntries
     *
     * @return OrderEntry[]
     */
    public function getOrderEntries()
    {
        return $this->orderEntries;
    }

    /**
     * Set orderStatus
     * @param \Nitra\OrderBundle\Entity\OrderStatus $orderStatus
     * @return Order
     */
    public function setOrderStatus(\Nitra\OrderBundle\Entity\OrderStatus $orderStatus)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get order_status
     * @return Nitra\OrderBundle\Entity\OrderStatus
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }
    
    
    /**
     * Добавление записей в историю статусов
     * @param  Nitra\OrderBundle\Entity\OrderStatusHistory $orderStatusHistory
     * @return Order
     */
    public function addOrderStatusHistory(\Nitra\OrderBundle\Entity\OrderStatusHistory $orderStatusHistory)
    {
        $this->orderStatusHistory[] = $orderStatusHistory;
        return $this;
    }

    /**
     * Remove order_status_history
     * @param Nitra\OrderBundle\Entity\OrderStatusHistory $orderStatusHistory
     */
    public function removeOrderStatusHistory(\Nitra\OrderBundle\Entity\OrderStatusHistory $orderStatusHistory)
    {
        $this->orderStatusHistory->removeElement($orderStatusHistory);
    }

    /**
     * Get order_status_history
     * @return Doctrine\Common\Collections\Collection
     */
    public function getOrderStatusHistory()
    {
        return $this->orderStatusHistory;
    }

    /**
     * Add incomes
     * @param  Nitra\IncomeBundle\Entity\Income $incomes
     * @return Order
     */
    public function addIncome(\Nitra\IncomeBundle\Entity\Income $incomes)
    {
        $this->incomes[] = $incomes;
        return $this;
    }

    /**
     * Remove incomes
     * @param Nitra\IncomeBundle\Entity\Income $incomes
     */
    public function removeIncome(\Nitra\IncomeBundle\Entity\Income $incomes)
    {
        $this->incomes->removeElement($incomes);
    }

    /**
     * Get incomes
     * @return Doctrine\Common\Collections\Collection
     */
    public function getIncomes()
    {
        return $this->incomes;
    }

    /**
     * Set delivery_date
     * @param \DateTime $deliveryDate
     * @return Order
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
        return $this;
    }

    /**
     * Get delivery_date
     *
     * @return \DateTime 
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }
    
    /**
     * Add autoincrement comment in order
     * @param string $comment
     * @param string $username
     * @return \Nitra\OrderBundle\Entity\Order
     */
    public function addComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);
        
        if ($comment && $username) {
            
            $formatter = \IntlDateFormatter::create(
                \Locale::getDefault(), 
                \IntlDateFormatter::MEDIUM, 
                \IntlDateFormatter::NONE
            );
            
            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setComment($this->getComment() . $commentText);
            
        }
        
        return $this;
    }
    
    /**
     * Add autoincrement comment in order
     * @param string $comment
     * @param string $username
     * @return \Nitra\OrderBundle\Entity\Order
     */
    public function addCancelComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);
        
        if ($comment && $username) {
            
            $formatter = \IntlDateFormatter::create(
                \Locale::getDefault(), 
                \IntlDateFormatter::MEDIUM, 
                \IntlDateFormatter::NONE
            );
            
            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setCancelComment($this->getCancelComment() . $commentText);
            
        }
        
        return $this;
    }
    
    /**
     * Set filial
     * @param Nitra\FilialBundle\Entity\Filial $filial
     * @return Order
     */
    public function setFilial(\Nitra\FilialBundle\Entity\Filial $filial = null)
    {
        $this->filial = $filial;
        return $this;
    }

    /**
     * Get filial
     *
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getFilial()
    {
        return $this->filial;
    }

    /**
     * Set is_direct
     * @param boolean $isDirect
     * @return Order
     */
    public function setIsDirect($isDirect)
    {
        $this->isDirect = $isDirect;
    
        return $this;
    }

    /**
     * Get is_direct
     * @return boolean 
     */
    public function getIsDirect()
    {
        return $this->isDirect;
    }

    /**
     * Set address
     * @param string $address
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set delivery_cost
     * @param integer $deliveryCost
     * @return Order
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;
        return $this;
    }

    /**
     * Get delivery_cost
     * @return integer 
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }
    

    public function setOrderEntries($orderEntyes) 
    {
        foreach ($orderEntyes as $oe) {
            $oe->setOrder($this);
        }
    }

    /**
     * Set cancelComment
     * @param string $cancelComment
     * @return Order
     */
    public function setCancelComment($cancelComment)
    {
        $this->cancelComment = $cancelComment;
        return $this;
    }

    /**
     * Get cancelComment
     * @return string 
     */
    public function getCancelComment()
    {
        return $this->cancelComment;
    }

    /**
     * Add orderEntries
     * @param \Nitra\OrderBundle\Entity\OrderEntry $orderEntries
     * @return Order
     */
    public function addOrderEntry(\Nitra\OrderBundle\Entity\OrderEntry $orderEntries)
    {
        $orderEntries->setOrder($this);
        $this->orderEntries[] = $orderEntries;
        return $this;
    }

    /**
     * Remove orderEntries
     * @param \Nitra\OrderBundle\Entity\OrderEntry $orderEntries
     */
    public function removeOrderEntry(\Nitra\OrderBundle\Entity\OrderEntry $orderEntries)
    {
        $this->orderEntries->removeElement($orderEntries);
    }
    
    /**
     * Устанавливаем значение города
     * @param  Nitra\GeoBundle\Entity\Model\CityInterface $city
     * @return Order
     */
    public function setCity(\Nitra\GeoBundle\Entity\Model\CityInterface $city = null)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     * @return Nitra\GeoBundle\Entity\Model\CityInterface
     */
    public function getCity()
    {
        return $this->city;
    }
        
    /**
     * Set isCod
     * @param boolean $isCod
     * @return Order
     */
    public function setIsCod($isCod)
    {
        $this->isCod = $isCod;
        return $this;
    }

    /**
     * Get isCod
     * @return boolean 
     */
    public function getIsCod()
    {
        return $this->isCod;
    }

    /**
     * Set warehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $warehouse
     * @return Order
     */
    public function setWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
        return $this;
    }

    /**
     * Get warehouse
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    
    /**
     * Get storeId
     * @return string $storeId
     */
    public function getStoreId()
    {
        return $this->storeId;
    }
    
    /**
     * Set storeId
     * @param string $storeId
     * @return Order
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
        return $this;
    }
    
    /**
     * Add sms
     * @param \Nitra\OrderBundle\Entity\Sms $sms
     * @return Order
     */
    public function addSms(\Nitra\OrderBundle\Entity\Sms $sms)
    {
        $this->sms[] = $sms;
        return $this;
    }

    /**
     * Remove sms
     *
     * @param \Nitra\OrderBundle\Entity\Sms $sms
     */
    public function removeSms(\Nitra\OrderBundle\Entity\Sms $sms)
    {
        $this->sms->removeElement($sms);
    }
    
    
    /**
     * Set completedAt
     * @param \DateTime $completedAt
     * @return Order
     */
    public function setCompletedAt(\DateTime $completedAt)
    {
        $this->completedAt = $completedAt;
        return $this;
    }

    /**
     * Get completedAt
     * @return \DateTime 
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }
    
    /**
     * Set completedBy
     * @param \Nitra\ManagerBundle\Entity\Manager $completedBy
     * @return Order
     */
    public function setCompletedBy(\Nitra\ManagerBundle\Entity\Manager $completedBy = null)
    {
        $this->completedBy = $completedBy;
        return $this;
    }

    /**
     * Get completedBy
     * @return \Nitra\ManagerBundle\Entity\Manager 
     */
    public function getCompletedBy()
    {
        return $this->completedBy;
    }
    
    /**
     * проверить завершен ли заказ
     * @return boolean - флаг завершен заказ или нет
     * @return true - заказ завершен
     * @return false - заказ не завершен
     */
    public function isCompleted()
    {
        // проверить время завершения заказа
        if ($this->getCompletedAt()) {
            // заказ завершен
            return true;
        }
        
        // заказ не завершен
        return false;
    }
    
    
}
<?php

namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SmsStatus
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SmsTypeMessage
{
    
    /**
     * @var string $methodName
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255)
     * @ORM\Id
     */
    private $methodName;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sortOrder;

    
    public function __toString() {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SmsStatus
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set methodName
     *
     * @param string $methodName
     * @return SmsStatus
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    
        return $this;
    }

    /**
     * Get methodName
     *
     * @return string 
     */
    public function getMethodName()
    {
        return $this->methodName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return SmsStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}

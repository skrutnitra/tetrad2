<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\OrderEntry
 *
 * @ORM\Table(name="order_entry")
 * @ORM\Entity(repositoryClass="Nitra\OrderBundle\Repository\OrderEntryRepository")
 */
class OrderEntry
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $quantity
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Не указано количество товара")
     * @Assert\Range(min=1, max=999999)
     */
    private $quantity;

    /**
     * @var float $price_in
     * @ORM\Column(name="price_in", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Не указана цена входа")
     * @Assert\Range(min=0, max=999999.99)
     */
    private $priceIn;
    
    /**
     * Валюта
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency", referencedColumnName="code")
     * @Assert\NotBlank(message="Не указана валюта")
     */
    private $currency;

    /**
     * @var float $price_out
     * @ORM\Column(name="price_out", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Не указана цена выхода")
     * @Assert\Range(min=0, max=999999.99)
     */
    private $priceOut;

    /**
     * @var float $price_recommended
     * Цена по которой товар был предложен покупателю в самый первый раз
     * @ORM\Column(name="price_recommended", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Не указана рекомендуемая цена")
     * @Assert\Range(min=0, max=999999.99)
     */
    private $priceRecommended;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderEntries")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntryStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан статус товарной позиции")
     */
    private $orderEntryStatus;

    /**
     * @ORM\OneToMany(targetEntity="OrderEntryStatusHistory", mappedBy="orderEntry")
     */
    private $orderEntryStatusHistory;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан склад с которого заказываем товарную позицию")
     */
    private $warehouse;
    
    /**
     * @var string $productId
     * @ORM\Column(name="product_id", type="string", length=24)
     * @Assert\NotBlank(message="Не указан продукт товарной позиции")
     * @Assert\Length(max=24)
     */
    private $productId;

    /**
     * @var \Nitra\OrderBundle\Document\Product
     */
    protected $product;
    
    /**
     * @var string $stockParams
     * @ORM\Column(name="stock_params", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $stockParams;
    
    /**
     * @var string $serialNumber
     * @ORM\Column(name="serial_number", type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    private $serialNumber;

    /**
     * @var integer $warrantyPeriod
     * @ORM\Column(name="warranty_period", type="integer", nullable=true)
     * @Assert\Range(min=0, max=999999)
     */
    private $warrantyPeriod;
    
    /**
     * позиция перемещения
     * @ORM\OneToMany(targetEntity="Nitra\MainBundle\Entity\MovementEntry", mappedBy="orderEntry")
     */
    private $movementEntry;
    
    /**
     * позиция декларации
     * @ORM\OneToMany(targetEntity="Nitra\DeclarationBundle\Entity\DeclarationEntry", mappedBy="orderEntry")
     */
    private $declarationEntry;
    
    /**
     * скидка в процентах (необязательный параметр при формировании заказа с сайта)
     * @var string $discountPercent
     * @ORM\Column(name="discount_percent", type="decimal", precision=3, scale=3, nullable=true) 
     * @Assert\Range(min=0, max=999.999)
     */
    private $discountPercent;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderEntryStatusHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * получить валидность для удаления
     * @return false
     */
    public function isValidForDelete()
    {
        // проверить заказ
        if ($this->getOrder() && $this->getOrder()->getOrderStatus() &&
            // проверить статус заказа
            in_array($this->getOrder()->getOrderStatus()->getMethodName(), array('waiting'))
        ) {
            
            // новая позиция заказа
            if (!$this->getId()) {
                // разрешено удалять позицию заказа
                return true;
            }
            
            // проверить статус позиции заказа
            // статус позиции заказа не указан
            if (!$this->getOrderEntryStatus()) {
                // разрешено удалять позицию заказа
                return true;
            }
            
            // проверить статус позиции заказа
            if (in_array($this->getOrderEntryStatus()->getMethodName(), array(
                'waiting', 'clarified'
            ))) {
                // разрешено удалять позицию заказа
                return true;
            }
            
            // разрешено удалять позицию заказа
            return true;
        }
        
        // запрет удаления позиции заказа 
        return false;
    }

    /**
     * object to string 
     * @return string
     */
    public function __toString()
    {
        
        // проверить кол-во продукта больше 1
        if ($this->getQuantity() > 1 ) {
            // вернуть навзание позиции с учетом кол-ва
            return $this->getQuantity() . " x " . $this->getEntryName();
        }
        
        // вернуть навзание позиции
        return $this->getEntryName();
    }
    
    /**
     * Получить навзание товарной позиции
     * @return string
     */
    public function getEntryName()
    {
        
        // возвразаемое имя 
        $entryName = '';
        
        // проверить связь с объектом продукта
        if ($this->getProduct()) {
            $entryName = (string)$this->getProduct();
        }
        
        // проверить параметры стока
        if ($this->getStockParams()) {
            $entryName .= " (".$this->getStockParams().")";
        }
        
        // вернуть string 
        return (string) $entryName;
    }
    
    /**
     * Set product
     *
     * @param \Nitra\OrderBundle\Document\Product $product
     */
    public function setProduct($product)
    {
        $this->productId = $product->getId();
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * получить позицию последнего перемещениия
     * @return \Nitra\MainBundle\Entity\MovementEntry
     */
    public function getLastMovementEntry() 
    {
        // получить коллекцию позиций перемещения
        $meCollection = $this->getMovementEntry();
        
        // получить позицию последнего перемещениия
        if ($meCollection && $meCollection->count()) {
            return $meCollection->last();
        }
        
        // позиция не найдена
        return null;
    }
    
    /**
     * получить склад на коротом находится позиция
     * @return \Nitra\MainBundle\Entity\Warehouse
     * @return null
     */
    public function getAtWarehouse() {
        
        // получить последнюю позицию перемещения
        $lastMovementEntry = $this->getLastMovementEntry();
        
        // проверить последнее перемещение
        if ($lastMovementEntry) {
            // позиция перемещалась по складам 
            // получить последнее перемещение
            $movement = $this->getLastMovementEntry()->getMovement();
            
            // если последнее перемещение было возвратом поставщику
            if ($movement->isReturn() === true) {
                // венруть склад с которого заказывали товарную позицию
                return $this->getWarehouse();
            }
            
            // последнее перемещение было 
            // закупка или перемещение со склада на склад
            // вернуть склад получатель последнего перемещения
            return $movement->getToWarehouse();
        }
        
        // перемещений позиции не было 
        // венруть склад с которого заказывали товарную позицию
        return $this->getWarehouse();
    }
    
    /**
     * получить позицию последней декларации
     * @return \Nitra\DeclarationBundle\Entity\DeclarationEntry
     */
    public function getLastDeclarationEntry()
    {
        // получить коллекцию позиций перемещения
        $deCollection = $this->getDeclarationEntry();
        
        // получить позицию последней декларации
        if ($deCollection && $deCollection->count()) {
            return $deCollection->last();
        }
        
        // позиция не найдена
        return null;
    }
    
    /**
     * Получить закупку
     * @return Movement - перемещение isIncome == true
     * @return false - закупка не найдена
     */
    public function getIncome()
    {
        // получить коллекцию позиций перемещения
        $meCollection = $this->getMovementEntry();
        
        // проверить, если были перемещения по позиции 
        if ($meCollection) {
            // обойти все позиции перемещений
            foreach($meCollection as $movementEntry) {
                // получить перемещение пол
                $movement = $movementEntry->getMovement();
                // если перемещение закупка
                if ($movement->isIncome()) {
                    return $movement;
                }
            }
        }
        
        // закупка не найдена
        return false;
    }
    
    /**
     * Set productId
     *
     * @param string $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * Get productId
     *
     * @return string 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Метод для возврата суммы по пункту заказа с учетом входа, выхода и количества.
     */
    public function getAmount()
    {
        return ($this->priceOut - $this->priceIn) * $this->quantity;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param  integer    $quantity
     * @return OrderEntry
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price_in
     *
     * @param  float      $priceIn
     * @return OrderEntry
     */
    public function setPriceIn($priceIn)
    {
        $this->priceIn = $priceIn;

        return $this;
    }

    /**
     * Get price_in
     *
     * @return float
     */
    public function getPriceIn()
    {
        return $this->priceIn;
    }
    
    /**
     * Set currency
     *
     * @param \Nitra\MainBundle\Entity\Currency $currency
     * @return OrderEntry
     */
    public function setCurrency(\Nitra\MainBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return \Nitra\MainBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    
    /**
     * Set price_out
     *
     * @param  float      $priceOut
     * @return OrderEntry
     */
    public function setPriceOut($priceOut)
    {
        $this->priceOut = $priceOut;

        return $this;
    }

    /**
     * Get price_out
     *
     * @return float
     */
    public function getPriceOut()
    {
        return $this->priceOut;
    }

    /**
     * Set price_recommended
     *
     * @param  float      $priceRecommended
     * @return OrderEntry
     */
    public function setPriceRecommended($priceRecommended)
    {
        $this->priceRecommended = $priceRecommended;

        return $this;
    }

    /**
     * Get price_recommended
     *
     * @return float
     */
    public function getPriceRecommended()
    {
        return $this->priceRecommended;
    }

    /**
     * Set order
     *
     * @param  Nitra\OrderBundle\Entity\Order $order
     * @return OrderEntry
     */
    public function setOrder(\Nitra\OrderBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Nitra\OrderBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set setOrderEntryStatus
     *
     * @param \Nitra\OrderBundle\Entity\OrderEntryStatus $orderEntryStatus
     * @return Order
     */
    public function setOrderEntryStatus(\Nitra\OrderBundle\Entity\OrderEntryStatus $orderEntryStatus)
    {
        $this->orderEntryStatus = $orderEntryStatus;

        return $this;
    }

    /**
     * Get order_entry_status
     *
     * @return Nitra\OrderBundle\Entity\OrderEntryStatus
     */
    public function getOrderEntryStatus()
    {
        return $this->orderEntryStatus;
    }

    /**
     * Add order_entry_status_history
     *
     * @param  Nitra\OrderBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory
     * @return OrderEntry
     */
    public function addOrderEntryStatusHistory(\Nitra\OrderBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory)
    {
        $this->orderEntryStatusHistory[] = $orderEntryStatusHistory;

        return $this;
    }

    /**
     * Remove order_entry_status_history
     *
     * @param Nitra\OrderBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory
     */
    public function removeOrderEntryStatusHistory(\Nitra\OrderBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory)
    {
        $this->orderEntryStatusHistory->removeElement($orderEntryStatusHistory);
    }

    /**
     * Get order_entry_status_history
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getOrderEntryStatusHistory()
    {
        return $this->orderEntryStatusHistory;
    }

    public function getTotal()
    {
        return $this->quantity * $this->priceOut;
    }

    /**
     * Set warehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $warehouse
     * @return OrderEntry
     */
    public function setWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     * 
     * @return \Nitra\MainBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    
    /**
     * Set stockParams
     * @param string $stockParams
     * @return OrderEntry
     */
    public function setStockParams($stockParams)
    {
        $this->stockParams = $stockParams;
    
        return $this;
    }

    /**
     * Get stockParams
     * @return string 
     */
    public function getStockParams()
    {
        return $this->stockParams;
    }
    
    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     * @return OrderEntry
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string 
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set warrantyPeriod
     *
     * @param integer $warrantyPeriod
     * @return OrderEntry
     */
    public function setWarrantyPeriod($warrantyPeriod)
    {
        $this->warrantyPeriod = $warrantyPeriod;

        return $this;
    }

    /**
     * Get warrantyPeriod
     *
     * @return integer 
     */
    public function getWarrantyPeriod()
    {
        return $this->warrantyPeriod;
    }
    
    /**
     * Add movementEntry
     *
     * @param \Nitra\MainBundle\Entity\MovementEntry $movementEntry
     * @return OrderEntry
     */
    public function addMovementEntry(\Nitra\MainBundle\Entity\MovementEntry $movementEntry)
    {
        $this->movementEntry[] = $movementEntry;

        return $this;
    }

    /**
     * Remove movementEntry
     *
     * @param \Nitra\MainBundle\Entity\MovementEntry $movementEntry
     */
    public function removeMovementEntry(\Nitra\MainBundle\Entity\MovementEntry $movementEntry)
    {
        $this->movementEntry->removeElement($movementEntry);
    }

    /**
     * Get movementEntry
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovementEntry()
    {
        return $this->movementEntry;
    }
    
    /**
     * Add declarationEntry
     *
     * @param \Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntry
     * @return OrderEntry
     */
    public function addDeclarationEntry(\Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntry)
    {
        $this->declarationEntry[] = $declarationEntry;

        return $this;
    }

    /**
     * Remove declarationEntry
     *
     * @param \Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntry
     */
    public function removeDeclarationEntry(\Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntry)
    {
        $this->declarationEntry->removeElement($declarationEntry);
    }

    /**
     * Get declarationEntry
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeclarationEntry()
    {
        return $this->declarationEntry;
    }
    
    /**
     * Set discountPercent
     *
     * @param string $discountPercent
     * @return OrderEntry
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;
    
        return $this;
    }

    /**
     * Get discountPercent
     *
     * @return string 
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

}
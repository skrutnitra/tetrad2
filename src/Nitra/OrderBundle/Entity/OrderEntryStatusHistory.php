<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\OrderEntryStatusHistory
 *
 * @ORM\Table(name="order_entry_status_history")
 * @ORM\Entity(repositoryClass="Nitra\OrderBundle\Repository\OrderEntryStatusHistoryRepository")
 */
class OrderEntryStatusHistory
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntry", inversedBy="orderEntryStatusHistory")
     * @ORM\JoinColumn(name="order_entry_id", referencedColumnName="id", nullable=false)
     * 
     * @Assert\NotBlank
     */
    private $orderEntry;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntryStatus")
     * @ORM\JoinColumn(nullable=false)
     * 
     * @Assert\NotBlank
     */
    private $fromStatus;
    
    /**
     * @ORM\ManyToOne(targetEntity="OrderEntryStatus")
     * @ORM\JoinColumn(nullable=false)
     * 
     * @Assert\NotBlank
     */
    private $toStatus;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set order_entry
     *
     * @param  Nitra\OrderBundle\Entity\OrderEntry $orderEntry
     * @return OrderEntryStatusHistory
     */
    public function setOrderEntry(\Nitra\OrderBundle\Entity\OrderEntry $orderEntry = null) {
        $this->orderEntry = $orderEntry;

        return $this;
    }

    /**
     * Get order_entry
     *
     * @return Nitra\OrderBundle\Entity\OrderEntry
     */
    public function getOrderEntry() {
        return $this->orderEntry;
    }

    /**
     * Set fromStatus
     *
     * @param \Nitra\OrderBundle\Entity\OrderEntryStatus $fromStatus
     * @return OrderEntryStatusHistory
     */
    public function setFromStatus(\Nitra\OrderBundle\Entity\OrderEntryStatus $fromStatus)
    {
        $this->fromStatus = $fromStatus;

        return $this;
    }

    /**
     * Get fromStatus
     *
     * @return \Nitra\OrderBundle\Entity\OrderEntryStatus 
     */
    public function getFromStatus()
    {
        return $this->fromStatus;
    }

    /**
     * Set toStatus
     *
     * @param \Nitra\OrderBundle\Entity\OrderEntryStatus $toStatus
     * @return OrderEntryStatusHistory
     */
    public function setToStatus(\Nitra\OrderBundle\Entity\OrderEntryStatus $toStatus)
    {
        $this->toStatus = $toStatus;

        return $this;
    }

    /**
     * Get toStatus
     *
     * @return \Nitra\OrderBundle\Entity\OrderEntryStatus 
     */
    public function getToStatus()
    {
        return $this->toStatus;
    }
}

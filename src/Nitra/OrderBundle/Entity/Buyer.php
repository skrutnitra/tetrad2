<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\Buyer
 *
 * @ORM\Table(name="buyer")
 * @ORM\Entity(repositoryClass="Nitra\OrderBundle\Repository\BuyerRepository")
 * @UniqueEntity(fields="phone", message="Клієнт з таким номером телефону вже існує")
 */
class Buyer
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @var string $phone
     *
     * @ORM\Column(type="string", length=15)
     * 
     * @Assert\NotBlank
     * @Assert\Length(min="10", max="15")
     * @Assert\Regex(pattern="/[\(\)\+\-0-9]{10,15}$/", message="Телефон указан не верно")
     */
    private $phone;

    /**
     * @var string $comment
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Assert\Length(max="255")
     */
    private $comment;
    
    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="buyer")
     */
    private $order;

    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string $name
     * @return Buyer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param  string $phone
     * @return Buyer
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

 
    /**
     * Set comment
     *
     * @param  string $comment
     * @return Buyer
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
      /**
     * Get orderEntries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getOrder()
    {
        return $this->order;
    }

}
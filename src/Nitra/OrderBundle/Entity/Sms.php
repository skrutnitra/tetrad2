<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\Sms
 *
 * @ORM\Table(name="sms")
 * @ORM\Entity(repositoryClass="Nitra\OrderBundle\Repository\SmsRepository")
 */
class Sms {
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer $id
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string $phone
     *
     * @ORM\Column( type="string", length=15)
     * 
     * @Assert\NotBlank
     * @Assert\Length(max="15")
     */
    private $phone;
    
    /**
     * @var string $text
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank
     */
    private $text;
    
   /**
    * @var float $cost
    * @ORM\Column(type="decimal", precision=2, scale=2)
    */
    private $cost;
    
    /**
     * @var object $order
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderEntries")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $order;
    
    /**
     * @var object $status
     * @ORM\ManyToOne(targetEntity="SmsStatus", inversedBy="methodName")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $status;
    
    /**
     * @var object $buyer
     * @ORM\ManyToOne(targetEntity="Buyer")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Type(type="Nitra\OrderBundle\Entity\Buyer")
     * Assert\NotBlank(message="Не указан покупатель")
     */
    private $buyer;
    
    /**
     * @var object $filial
     * @ORM\ManyToOne(targetEntity="Nitra\FilialBundle\Entity\Filial")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан филиал")
     * @Assert\Type(type="Nitra\FilialBundle\Entity\Filial")
     */
    private $filial;
    
    /**
     * @var object $status
     * @ORM\ManyToOne(targetEntity="SmsTypeMessage")
     * @ORM\JoinColumn(referencedColumnName="method_name", onDelete="SET NULL", nullable=true)
     */
    private $type;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return sms
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return sms
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return sms
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

     /**
     * Set status
     *
     * @param \Nitra\OrderBundle\Entity\SmsStatus $status
     * @return sms
     */
    public function setStatus(\Nitra\OrderBundle\Entity\SmsStatus $status = null)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get order
     *
     * @return \Nitra\OrderBundle\Entity\SmsStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return sms
     */
    public function setType(\Nitra\OrderBundle\Entity\SmsTypeMessage $type = null )
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return \Nitra\OrderBundle\Entity\SmsTypeMessage
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set order
     *
     * @param \Nitra\OrderBundle\Entity\Order $order
     * @return sms
     */
    public function setOrder(\Nitra\OrderBundle\Entity\Order $order = null)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return \Nitra\OrderBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set buyer
     *
     * @param \Nitra\OrderBundle\Entity\Buyer $buyer
     * @return sms
     */
    public function setBuyer(\Nitra\OrderBundle\Entity\Buyer $buyer)
    {
        $this->buyer = $buyer;
    
        return $this;
    }

    /**
     * Get buyer
     *
     * @return \Nitra\OrderBundle\Entity\Buyer 
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set filial
     *
     * @param \Nitra\FilialBundle\Entity\Filial $filial
     * @return sms
     */
    public function setFilial(\Nitra\FilialBundle\Entity\Filial $filial)
    {
        $this->filial = $filial;
    
        return $this;
    }

    /**
     * Get filial
     *
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getFilial()
    {
        return $this->filial;
    }
}
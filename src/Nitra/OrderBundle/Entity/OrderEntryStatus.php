<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\OrderEntryStatus
 *
 * @ORM\Table(name="order_entry_status")
 * @ORM\Entity(repositoryClass="Nitra\OrderBundle\Repository\OrderEntryStatusRepository")
 * @UniqueEntity(fields="methodName", message="Статус позиции заказа с таким ключем уже существует")
 */
class OrderEntryStatus
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private $name;

    /**
     * @var string $method_name
     *
     * @ORM\Column(name="method_name", type="string", length=255)
     * 
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private $methodName;

    /**
     * @var integer $sort_order
     *
     * @ORM\Column(name="sort_order", type="integer")
     * 
     * @Assert\Range(min=0, max=999999)
     */
    private $sortOrder;
    
    /**
     * @var string $name
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $cssClass;
    
    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string           $name
     * @return OrderEntryStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set method_name
     *
     * @param  string           $methodName
     * @return OrderEntryStatus
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;

        return $this;
    }

    /**
     * Get method_name
     *
     * @return string
     */
    public function getMethodName()
    {
        return $this->methodName;
    }

    /**
     * Set sort_order
     *
     * @param  integer          $sortOrder
     * @return OrderEntryStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sort_order
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
    
    /**
     * Set cssClass
     *
     * @param string $cssClass
     * @return OrderStatus
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * Get cssClass
     *
     * @return string 
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }
    
}
<?php
namespace Nitra\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\OrderBundle\Entity\OrderStatusHistory
 *
 * @ORM\Table(name="order_status_history")
 * @ORM\Entity
 */
class OrderStatusHistory
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderStatusHistory")
     * @ORM\JoinColumn()
     * 
     * @Assert\NotBlank
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     * 
     * @Assert\NotBlank
     */
    private $fromStatus;
    
    /**
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     * 
     * @Assert\NotBlank
     */
    private $toStatus;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param  Nitra\OrderBundle\Entity\Order $order
     * @return OrderStatusHistory
     */
    public function setOrder(\Nitra\OrderBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Nitra\OrderBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set fromStatus
     *
     * @param \Nitra\OrderBundle\Entity\OrderStatus $fromStatus
     * @return OrderStatusHistory
     */
    public function setFromStatus(\Nitra\OrderBundle\Entity\OrderStatus $fromStatus)
    {
        $this->fromStatus = $fromStatus;

        return $this;
    }

    /**
     * Get fromStatus
     *
     * @return \Nitra\OrderBundle\Entity\OrderStatus 
     */
    public function getFromStatus()
    {
        return $this->fromStatus;
    }

    /**
     * Set toStatus
     *
     * @param \Nitra\OrderBundle\Entity\OrderStatus $toStatus
     * @return OrderStatusHistory
     */
    public function setToStatus(\Nitra\OrderBundle\Entity\OrderStatus $toStatus)
    {
        $this->toStatus = $toStatus;

        return $this;
    }

    /**
     * Get toStatus
     *
     * @return \Nitra\OrderBundle\Entity\OrderStatus 
     */
    public function getToStatus()
    {
        return $this->toStatus;
    }
}

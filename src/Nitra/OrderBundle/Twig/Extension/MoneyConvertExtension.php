<?php
namespace Nitra\OrderBundle\Twig\Extension;

use Nitra\OrderBundle\Common\nlMoney;

/**
 * MoneyConvertExtension
 */
class MoneyConvertExtension extends \Twig_Extension
{
    
    /**
     * Returns a list of functions to add to the existing list.
     * @return array An array of functions
     */
    public function getFunctions() {
        return array(
            // IN: $sum - число, например 1256.18
            new \Twig_SimpleFunction('nlMoneyConvert', function($sum) {
                return nlMoney::Convert($sum);
            })            
        );
    }
    
    /**
     * Returns the name of the extension.
     * @return string The extension name
     */
    public function getName()
    {
        return 'nitra_money_convert_extension';
    }
    
}
<?php

namespace Nitra\OrderBundle\Common;

/**
 * Преобразование числа в текст
 *
 * Данный скрипт реализует преобразование чисел в текст, т.е., на примере денежной суммы,
 * из 1256.18 получим Одна тысяча двести пятьдесят шесть гривен 18 копеек.
 * @subpackage lib
 * @author     Nitra Labs
 * @version    SVN: $Id: nlMoney.php 409 2010-12-09 09:02:32Z sn $
 */
class nlMoney
{
    public static $Mant = array(0 =>'гривня',1 => 'гривні',2 => 'гривень'); // описания мантисс
    public static $Expon = array(0 => 'копійка',1 => 'копійки',2 => 'копійок'); // описания экспонент

    // функция возвращает необходимый индекс описаний разряда
    // ('миллион', 'миллиона', 'миллионов') для числа $ins
    // например для 29 вернется 2 (миллионов)
    // $ins максимум два числа

    public static function DescrIdx($ins)
    {
        if (intval($ins / 10) == 1) // числа 10 - 19: 10 миллионов, 17 миллионов
            return 2;
        else {
            // для остальных десятков возьмем единицу
            $tmp = $ins % 10;
            if ($tmp == 1) // 1: 21 миллион, 1 миллион
                return 0;
            else if ($tmp >= 2 && $tmp <= 4)
                return 1; // 2-4: 62 миллиона
            else
                return 2; // 5-9 48 миллионов
        }
    }

    // IN: $in - число,
    // $raz - разряд числа - 1, 1000, 1000000 и т.д.
    // внутри функции число $in меняется
    // $ar_descr - массив описаний разряда ('миллион', 'миллиона', 'миллионов') и т.д.
    // $fem - признак женского рода разряда числа (true для тысячи)
    public static function DescrSot(&$in, $raz, $ar_descr, $fem = false)
    {
        $ret = '';

        $conv = intval($in / $raz);
        $in %= $raz;

        $descr = $ar_descr[self::DescrIdx($conv % 100)];

        if ($conv >= 100) {
            $Sot = array('сто', 'двісті', 'триста', 'чотириста', "п'ятсот",
                'шістсот', 'сімсот', 'вісімсот', "дев'ятсот");
            $ret = $Sot[intval($conv / 100) - 1] . ' ';
            $conv %= 100;
        }

        if ($conv >= 10) {
            $i = intval($conv / 10);
            if ($i == 1) {
                $DesEd = array('десять', 'одинадцять', 'дванадцять', 'тринадцять',
                    'чотирнадцять', "п'ятнадцять", 'шістнадцять', 'сімнадцять',
                    'вісімнадцять', "дев'ятнадцять");
                $ret .= $DesEd[$conv - 10] . ' ';
                $ret .= $descr;
                // возвращаемся здесь
                return $ret;
            }
            $Des = array('двадцять', 'тридцять', 'сорок', "п'ятдесят", 'шістдесят',
                'сімдесят', 'вісімдесят', "дев'яносто");
            $ret .= $Des[$i - 2] . ' ';
        }

        $i = $conv % 10;
        if ($i > 0) {
            if ($fem && ($i == 1 || $i == 2)) {
                // для женского рода (сто одна тысяча)
                $Ed = array('одна', 'дві');
                $ret .= $Ed[$i - 1] . ' ';
            } else {
                $Ed = array('один', 'два', 'три', 'чотири', "п'ять",
                    'шість', 'сім', 'вісім', "дев'ять");
                $ret .= $Ed[$i - 1] . ' ';
            }
        }
        $ret .= $descr;

        return $ret;
    }

    // IN: $sum - число, например 1256.18
    public static function Convert($sum)
    {
        $ret = '';

        // имена данных перменных остались от предыдущей версии
        // когда скрипт конвертировал только денежные суммы
        $Kop = 0;
        $Rub = 0;

        $sum = trim($sum);
        // удалим пробелы внутри числа
        $sum = str_replace(' ', '', $sum);

        // флаг отрицательного числа
        $sign = false;
        if ($sum[0] == '-') {
            $sum = substr($sum, 1);
            $sign = true;
        }

        // заменим запятую на точку, если она есть
        $sum = str_replace(',', '.', $sum);

        $Rub = intval($sum);
        $Kop = $sum * 100 - $Rub * 100;

        if ($Rub) {
            // значение $Rub изменяется внутри функции DescrSot
            // новое значение: $Rub %= 1000000000 для миллиарда
            if ($Rub >= 1000000000)
                $ret .= self::DescrSot($Rub, 1000000000, array('мільярд', 'мільярди', 'мільярдів')) . ' ';
            if ($Rub >= 1000000)
                $ret .= self::DescrSot($Rub, 1000000, array('мільйон', 'мільйони', 'мільйонів')) . ' ';
            if ($Rub >= 1000)
                $ret .= self::DescrSot($Rub, 1000, array('тисяча', 'тисячі', 'тисяч'), true) . ' ';

            $ret .= self::DescrSot($Rub, 1, self::$Mant, true) . ' ';
        }

        if ($Kop < 10)
            $ret .= '0';

        $ret .= $Kop . ' ' . self::$Expon[self::DescrIdx($Kop)];

        // если число было отрицательным добавим минус
        if ($sign)
            $ret = '-' . $ret;

        return $ret;
    }

}

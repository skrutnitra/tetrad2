/**
 * объект работы с заказами
 **/
var order = {
    /**
     * ajax запрос 
     * @param string url    - url ajax запроса
     * @param object params - параметры 
     * @return json  result - результат 
     **/
    ajax: function(actionUrl, params) {

        // объект параметров отправки ajax запроса
        if (typeof (params) == 'undefined') {
            params = {};
        }

        // результирующие данные отправки запроса
        var result;

        // отправить ajax запрос
        $.ajax({
            url: actionUrl,
            type: (typeof (params.type) != 'undefined') ? params.type : 'POST',
            data: (typeof (params.data) != 'undefined') ? params.data : {},
            dataType: (typeof (params.dataType) != 'undefined') ? params.dataType : 'json', // если params.dataType = '' то Intelligent Guess (xml, json, script, or html)
            async: (typeof (params.async) != 'undefined') ? params.async : false,
            success: function(fromServer) {
                // ответ от сервера получен успешно
                if (fromServer) {
                    result = fromServer;
                }
            }
        });
        // венруть объект
        return result;
    },
    
    /**
     * обновление строки TR данных заказа
     * @param integer orderId - ID идентификатор заказа
     **/
    renderTr: function(orderId) {

        // ID идентификатор заказа
        var orderId = parseInt(orderId);

        // url контроллер получение html данных строки заказа
        var actionUrl = Routing.generate('Nitra_OrderBundle_Order_Render_Tr', {pk: orderId}, true);

        // отправить ajax запрос на получаение данных по строке заказа
        var fromServer = order.ajax(actionUrl, {
            dataType: 'html'
        });

        // обновить html контент строки заказа
        $('#tr_order_id' + orderId).replaceWith(fromServer);

        // Colorbox link
        $('#tr_order_id' + orderId).on('click', '.colorbox', function() {
            $(this).colorbox();
        })

        // спрятать popup
        $('.popup').removeClass('active');

        // Popup box
        $('#tr_order_id' + orderId + ' .popup').each(function() {
            $(this).html('<div class="popup_content">' + $(this).html() + '</div>');
            $(this).prepend('<div class="popup_arrow"></div>');
            $(this).addClass('popup_ready');
        })
        $('#tr_order_id' + orderId + ' .popup .popup_arrow').click(function() {
            $(this).parent('div.popup').toggleClass('popup_active');
            $('.popup').not($(this).parent('div.popup')).removeClass('popup_active');
        });

    },
    // Метод для поиска товаров в форме заказа
//    function getSearchProductList() {
//        $.ajax({
//            url: Routing.generate('Nl_OrderBundle_Order_SearchProducts'),
//            dataType: 'script',
//            data: {
//                product_name: $('#search_product_name').val()
//            }
//        });
//    }

//    // Метод для автокомплита по покупателям
//    function orderEditBuyerAuticimplete() {
//        $('input[name$="order[buyer][name]"]').autocomplete({
//            source: Routing.generate('Nl_OrderBundle_Order_Buyer_Autocomplete'),
//            minLength: 2,
//            select: function(event, ui) {
//                $.ajax({
//                    type: 'POST',
//                    url: Routing.generate('Nl_OrderBundle_Order_Buyer_Autocomplete_Info'),
//                    dataType: 'json',
//                    data: {
//                        buyer_id: ui.item.value
//                    },
//                    success: function(data) {
//                        console.log(data);
//                        
//                        $('input[name$="order[buyer][name]"]').val(data.buyer_name);
//                        $('input[name$="order[buyer][phone]"]').val(data.buyer_phone);
//                        $('input[name$="order[buyer][address]"]').val(data.buyer_address);
//                    }
//                });
//            }
//        });
//    }

    /**
     * изменить статус по заказу
     * @param integer orderId - ID идентификатор заказа
     * @param string  toStatus     - название нового метода статуса заказа
     * @param string  statusName   - текстовое название нового статуса заказа
     * @param string  fromStatus   - название метода статуса отображенного пользователю
     **/
    setStatus: function(orderId, toStatus, statusName, fromStatus) {

        // проверить параметр orderId
        if (typeof (orderId) == 'undefined') {
            console.error("Не указан параметр идентификатор заказа.");
            return false;
        }

        // проверить параметр toStatus
        if (typeof (toStatus) == 'undefined') {
            console.error("Не указан параметр название метода нового статуса позиции заказа.");
            return false;
        }

        // проверить параметр statusName
        if (typeof (statusName) == 'undefined') {
            console.error("Не указан параметр название нового статуса позиции заказа.");
            return false;
        }

        // проверить параметр fromStatus
        if (typeof (fromStatus) == 'undefined') {
            console.error("Не указан параметр название метода статуса отображенного пользователю.");
            return false;
        }

        // url контроллер установить статус позиции заказа
        var actionUrl = Routing.generate('Nitra_OrderBundle_Order_Set_Status', false, true);

        // отправить ajax запрос на изменение статуса позиции заказа
        var fromServer = order.ajax(actionUrl, {
            data: {
                orderId: orderId,
                toStatus: toStatus,
                fromStatus: fromStatus
            },
            dataType: '' // не указываем, может возвращать json, script в зависимости от логики цепочки
        });

        // проверить тип ответа от сервера
        if (typeof (fromServer) != 'undefined' && // ответ сервера
                typeof (fromServer.type) != 'undefined'// тип ответа
                ) {
            // обработка в зависимости от типа ответа сервера
            switch (fromServer.type) {

//                // подтверждение выполнения цепочки
//                case 'confirm':
//
//                    // подтвердить перевод в статус
//                    if (confirm(fromServer.confirmMessage)) {
//
//                        // отправить ajax запрос на перевод в статус
//                        var confirmResponse = order.ajax(fromServer.confirmUrl, {
//                            type: 'GET'
//                        });
//
//                        // проверить ответ сервера
//                        if (confirmResponse && // проверить ответ сервера
//                                typeof(confirmResponse.type) != 'undefined' && // тип ответа
//                                confirmResponse.type == 'error' // отображение ошибки
//                                ) {
//                            // отображение ошибки
//                            common.flash('Невозможно перевести позицию в статус "' + statusName + '". ' + confirmResponse.message, confirmResponse.type);
//                        }
//
//                        // проверить ответ сервера
//                        if (confirmResponse && // проверить ответ сервера
//                                typeof(confirmResponse.type) != 'undefined' && // тип ответа
//                                confirmResponse.type == 'success' // изменение статуса прошло успешно
//                                ) {
//                            // изменение статуса прошло успешно
//                            common.flash('Позиция переведена в статус "' + statusName + '".', 'notice');
//                            // обновить строку заказа
//                            order.renderTr(orderId);
//                            break;
//                        }
//
//                    }
//                    break;

                // отображение ошибки
                case 'error':
                    flashBox.show('Невозможно перевести заказ в статус "' + statusName + '". ' + fromServer.message, fromServer.type);
                    break;

                    // изменение статуса прошло успешно
                case 'success':
                    // Ошибка отправления SMS сообщения
                    if (typeof (fromServer.sms_error) != 'undefined') {
                        flashBox.show('Ошибка отправления SMS сообщения : ' + fromServer.sms_error + '. Позиция переведена в статус "' + statusName + '".', 'warning');
                    } else {
                        // изменение статуса прошло успешно
                        flashBox.show('Позиция переведена в статус "' + statusName + '".', 'notice');
                    }
                    // обновить строку заказа
                    order.renderTr(orderId);
                    break;
                default:
                    return;
                    break;
            }

        }

        // прерываем выполнение
        return;
    },
    /**
     * печать гарантийного талона по заказу
     * @param integer orderId - ID идентификатор заказа
     **/
    warrantyPrint: function(orderId) {

        // проверить параметр orderId
        if (typeof (orderId) == 'undefined') {
            console.error("Не указан параметр идентификатор заказа.");
            return false;
        }

        // url контроллер установить статус позиции заказа
        var actionUrl = Routing.generate('Nitra_OrderBundle_Order_Warranty_Save', {pk: orderId}, true);

        // отправить ajax запрос на изменение статуса позиции заказа
        var fromServer = order.ajax(actionUrl, {
            dataType: 'json',
            data: $('#warranty_form').serialize()
        });

        // проверить тип ответа от сервера
        if (typeof (fromServer) != 'undefined' && // ответ сервера
                typeof (fromServer.type) != 'undefined'// тип ответа
                ) {
            // обработка в зависимости от типа ответа сервера
            switch (fromServer.type) {
                // отображение ошибки
                case 'error':
                    flashBox.show('Невозможно распечатать гарантийный талон. ' + fromServer.message, fromServer.type);
                    break;

                    // гарантийка сохранена успешно
                case 'success':
                    window.print();
                    break;

                default:
                    return;
                    break;
            }
        }


    },
    /**
     * объект позиции заказа 
     **/
    orderEntry: {
        /**
         * изменить статус позиции по заказу
         * @param integer orderId - ID идентификатор заказа
         * @param integer orderEntryId - ID идентификатор позиции заказа
         * @param string  toStatus     - название нового метода статуса позиции заказа
         * @param string  statusName   - текстовое название нового статуса позиции заказа
         * @param string  fromStatus   - название метода статуса отображенного пользователю
         **/
        setStatus: function(orderId, orderEntryId, toStatus, statusName, fromStatus) {

            // проверить параметр orderId
            if (typeof (orderId) == 'undefined') {
                console.error("Не указан параметр идентификатор заказа.");
                return false;
            }

            // проверить параметр orderEntryId
            if (typeof (orderEntryId) == 'undefined') {
                console.error("Не указан параметр идентификатор позиции заказа.");
                return false;
            }

            // проверить параметр toStatus
            if (typeof (toStatus) == 'undefined') {
                console.error("Не указан параметр название метода нового статуса позиции заказа.");
                return false;
            }

            // проверить параметр statusName
            if (typeof (statusName) == 'undefined') {
                console.error("Не указан параметр название нового статуса позиции заказа.");
                return false;
            }

            // проверить параметр fromStatus
            if (typeof (fromStatus) == 'undefined') {
                console.error("Не указан параметр название метода статуса отображенного пользователю.");
                return false;
            }

            // url контроллер установить статус позиции заказа
            var actionUrl = Routing.generate('Nitra_OrderBundle_OrderEntry_Set_Status', false, true);

            // отправить ajax запрос на изменение статуса позиции заказа
            var fromServer = order.ajax(actionUrl, {
                data: {
                    orderEntryId: orderEntryId,
                    toStatus: toStatus,
                    fromStatus: fromStatus
                },
                dataType: '' // не указываем, может возвращать json, script в зависимости от логики цепочки
            });

            // проверить тип ответа от сервера
            if (typeof (fromServer) != 'undefined' && // ответ сервера
                    typeof (fromServer.type) != 'undefined'// тип ответа
                    ) {
                // обработка в зависимости от типа ответа сервера
                switch (fromServer.type) {

                    // подтверждение выполнения цепочки
                    case 'confirm':

                        // подтвердить перевод в статус
                        if (confirm(fromServer.confirmMessage)) {

                            // отправить ajax запрос на перевод в статус
                            var confirmResponse = order.ajax(fromServer.confirmUrl, {
                                type: 'GET'
                            });

                            // проверить ответ сервера
                            if (confirmResponse && // проверить ответ сервера
                                    typeof (confirmResponse.type) != 'undefined' && // тип ответа
                                    confirmResponse.type == 'error' // отображение ошибки
                                    ) {
                                // отображение ошибки
                                flashBox.show('Невозможно перевести позицию в статус "' + statusName + '". ' + confirmResponse.message, confirmResponse.type);
                            }

                            // проверить ответ сервера
                            if (confirmResponse && // проверить ответ сервера
                                    typeof (confirmResponse.type) != 'undefined' && // тип ответа
                                    confirmResponse.type == 'success' // изменение статуса прошло успешно
                                    ) {
                                // изменение статуса прошло успешно
                                flashBox.show('Позиция переведена в статус "' + statusName + '".', 'notice');
                                // обновить строку заказа
                                order.renderTr(orderId);
                                break;
                            }

                        }
                        break;

                        // отображение ошибки
                    case 'error':
                        flashBox.show('Невозможно перевести позицию в статус "' + statusName + '". ' + fromServer.message, fromServer.type);
                        break;

                        // изменение статуса прошло успешно
                    case 'success':
                        // Ошибка отправления SMS сообщения
                        if (typeof (fromServer.sms_error) != 'undefined') {
                            flashBox.show('Ошибка отправления SMS сообщения : ' + fromServer.sms_error + '. Позиция переведена в статус "' + statusName + '".', 'warning');
                        } else {
                            // изменение статуса прошло успешно
                            flashBox.show('Позиция переведена в статус "' + statusName + '".', 'notice');
                        }

                        // обновить строку заказа
                        order.renderTr(orderId);
                        break;

                    default:
                        return;
                        break;
                }

            }

            // прерываем выполнение
            return;
        }

    },
    /**
     * объект формы inPlaceEdit 
     * добавление комментария для заказа
     **/
    comment: {
        // объект на котром произошло событие клик редактирования
        tagObj: false,
        // объект формы 
        formObj: false,
        /**
         * отображение формы inPlaceEdit добавления коментария
         * @param integer orderId - ID заказ
         * @param object _this    - javascript this объект на котором произошло событие клик редактирования
         **/
        add: function(orderId, _this) {

            // проверить открыта ли форма 
            if (typeof (order.comment.formObj) == 'object') {
                // форма открыта, закрыть форму
                order.comment.formObj.children('*[name="cancel"]:first').click();
            }

            // установить объект на котором произошло событыие 
            order.comment.tagObj = $(_this);
            order.comment.tagObj.hide(); // спрятать ссылку по которой произошол клик 

            // получить форму
            order.comment.formObj = $('#orderCommentInPlaceEdit');

            // установить данные формы
            $('#order_comment_id').val(orderId);
            $('#order_comment_comment').val('').focus();

            // отображение формы 
            order.comment.formObj.insertBefore(order.comment.tagObj);
            order.comment.formObj.show();
        },
        /**
         * Отмена редактирования
         **/
        cancel: function() {
            order.comment.formObj.hide();
            $('#order_comment_order').val('');
            $('#order_comment_comment').val('');
            order.comment.tagObj.show(); // показать ссылку по которой произошол клик 
        },
        /**
         * сохранить форму inPlacedEdit
         **/
        send: function() {

            // url контроллер добавления комментария к заказу 
            var actionUrl = order.comment.formObj.attr('action');

            // оправить ajax форму на сервер 
            var fromServer = order.ajax(actionUrl, {
                data: order.comment.formObj.serialize()
            });

            // проверить тип ответа от сервера
            if (typeof (fromServer) != 'undefined' && // ответ сервера
                    typeof (fromServer.type) != 'undefined'// тип ответа
                    ) {
                // обработка в зависимости от типа ответа сервера
                switch (fromServer.type) {

                    // отображение ошибки
                    case 'error':
                        flashBox.show('Невозможно добавить комментарий для заказа. ' + fromServer.message, fromServer.type);
                        // прерываем выполнение
                        return;
                        break;

                        // обновление комментария прошло успешно
                    case 'success':
                    default:
                        flashBox.show('Комментарий к заказу успешно добавлен.', 'notice');

                        // показать комментарий
                        if (typeof (fromServer.comment) != 'undefined') {
                            $('#comment_text' + fromServer.orderId).html(fromServer.comment);
                        }
                        break;
                }

            }

            // обновление отображения формы
            order.comment.formObj.hide();
            order.comment.tagObj.show();
        }

    },
    
    /**
     * форма добавления редактирования 
     * продуктов в заказе
     **/
    form: {
        
        // флаг попадания скроллинга в самый низ
        activeScroll: false,
        
        // XMLHttpRequest 
        // поиск продуктов
        xhrSearchProduct: false,
        xhrEstimateDelivery: false,
        
        // массив статусов позиций заказов
        // для которых расчет стоимости доставки не производится 
        // все остальные статусы считаем доступными для расчетов
        deliverySyncDisallowEntryStatuses: ['canceled', 'returned_to_supplier'],
        
        /**
         * поиск продуктов в форме добавления/редактирования заказа
         * @param string - тип поиска 
         **/
        searchProduct: function(type) {

            // тип поиска по умолчанию
            if (typeof (type) == 'undefined') {
                var type = 'search';
            }

            // Отображаем loader
            $('#loader').show();

            // начать поиск сначала
            if (type == 'search') {
                // переводим скроллинг в верх 
                // для предотвращения двойного поиска по одному запросу
                $('#seacrh_product_list').scrollTop(0);
                // обнуляем результат поиска
                $('#filters_order_numberPageMain').val(0);
                $('#filters_order_numberPageStocks').val(0);
                $('#filters_order_numberPageStocksNotBinded').val(0);
                $('#filters_order_foundStocksId').val('');
                $('#seacrh_product_list').html('');
                order.form.activeScroll = true;
            }

            // получить форму поиска 
            var form = $('#search_product_filter');
            var actionUrl = form.attr('action');

            // перед выполнением поиска устанавливаем флаг попадания скроллинга в самый низ
            order.form.activeScroll = false;
            
            // проверить, если запушен предыдущий поиск продуктов
            if (order.form.xhrSearchProduct) {
                // прервать предыдущий поиск
                order.form.xhrSearchProduct.abort();
            }
            
            // отправить ajax запрос поиск продуктов
            // отправляем не через общий метод
            order.form.xhrSearchProduct = $.ajax({
                url: actionUrl,
                type: 'POST',
                data: form.serialize(),
                dataType: 'html',
                async: true,
                error: function() {
                    // обнулить xhrSearchProduct
                    order.form.xhrSearchProduct = false;
                    // спрятать loader
                    $('#loader').hide();            
                },
                success: function(fromServer){
                                        
                    // проверить ответ сервера
                    if (fromServer) {
                        
                        // добавить результат поиска 
                        $('#seacrh_product_list').append(fromServer);
                        
                        // обновляем флаг скроллинга
                        order.form.activeScroll = true;
                    }
                    
                    // обнулить xhrSearchProduct
                    order.form.xhrSearchProduct = false;
                    // спрятать loader
                    $('#loader').hide();
                    
                }
            });

        },
        
        /**
         * добавление продукта
         * из левого списка в правый
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        addProduct: function(_this) 
        {

            // получаем параметры добавляемого продукта
            var productId = $(_this).attr('productId');
            var productName = $(_this).attr('productName');

            var warehouseId = $(_this).attr('warehouseId');
            var warehouseName = $(_this).attr('warehouseName');
            var warehouseBusinessKey = $(_this).attr('warehouseBusinessKey');
            var cityBusinessKey = $(_this).attr('cityBusinessKey');

            var supplierId = $(_this).attr('supplierId');
            var supplierName = $(_this).attr('supplierName');

            var deliveryId = $(_this).attr('deliveryId');
            var deliveryName = $(_this).attr('deliveryName');

            var stockParams = $(_this).attr('stockParams');
            if (stockParams) {
                productName += ' (' + stockParams + ')';
            }

            var storePrice = $(_this).attr('storePrice');
            var currencyPriceIn = $(_this).attr('currencyPrice');
            var currency = $(_this).attr('currency');

            // счетчик продуктов
            var counter = (order.form.getProductsCounter() + 1);

            // html строка добавления
            var addTrHtml = $('#right_list_order_entrys_table').attr('add-block');

            // Заменяем названия - индексом и устанавливаем название продукта
            addTrHtml = addTrHtml
                    .replace(/__iteration__/g, counter)
                    .replace(/__name__/g, counter)
                    .replace(/__ProductId__/g, productId)
                    .replace(/__ProductName__/g, productName)
                    .replace(/__WarehouseId__/g, warehouseId)
                    .replace(/__WarehouseName__/g, warehouseName)
                    .replace(/__WarehouseBusinessKey__/g, warehouseBusinessKey)
                    .replace(/__StockParams__/g, stockParams)
                    .replace(/__CityBusinessKey__/g, cityBusinessKey)
                    .replace(/__SupplierId__/g, supplierId)
                    .replace(/__SupplierName__/g, supplierName)
                    .replace(/__DeliveryId__/g, deliveryId)
                    .replace(/__DeliveryName__/g, deliveryName)
                    .replace(/__Total__/g, storePrice)
                    ;
            var orderEntry = $("#cartList>tr[productid='" + productId + "'][warehouseid='" + warehouseId + "'][stockparams='" + stockParams + "']");
            // если позиция существует тогда увеличиваем количество           
            if (orderEntry.length > 0) {
                orderEntry.find("input.quantity").val(parseInt(orderEntry.find("input.quantity").val()) + 1);
                orderEntry.find("input.quantity").change();
            } else {
                // Добавляем позицию в правый список
                $('#cartList').append(addTrHtml);
            }


            // установить выбранный склад
            $('#edit_order_orderEntries_' + counter + '_warehouse').val(warehouseId);

            // заказ с поставщика
            if (supplierId) {
                
                // добавляем в отображение имя выбранного склада с сылкой на отображение виджета выбора склада
                $('#edit_order_orderEntries_' + counter + '_warehouse')
                        .closest('td')
                        .find('.warehouse_name')
                        // отключаем изменение поставщика
                        // .after("<a class='show_supplier_widget' href='javascript:;' onclick='order.form.productShowSupplierWidget(this);'>" + warehouseName + "</a>");
                        .after('<span class="warehouse_name">' + warehouseName + '</span>');
                
                // отключаем изменение поставщика
                // добавляем функцию изменения склада по поставщику
                // $('#edit_order_orderEntries_' + counter + '_supplier').change(function() {
                //     order.form.productChangeSupplier(this);
                // })
            }
            else {
                // заказ со своего склада тк, добавляем имя склада для отображения
                $('#edit_order_orderEntries_' + counter + '_warehouse').closest('td').find('.warehouse_name').html(warehouseName);
            }

            $('#edit_order_orderEntries_' + counter + '_productId').val(productId);
            $('#edit_order_orderEntries_' + counter + '_priceOut').val(storePrice)
                .on('keyup', function(e){
                    // автозамена символов в цене 
                    order.form.parseFloat(e, this);
                })
                .change(function() {
                    // пересчтитать тотал
                    order.form.recalcTotal(this); 
                })
                .change();
            $('#edit_order_orderEntries_' + counter + '_stockParams').val(stockParams);
            $('#edit_order_orderEntries_' + counter + '_priceIn').val(currencyPriceIn)
                .on('keyup', function(e){
                    // автозамена символов в цене 
                    order.form.parseFloat(e, this);
                });
            $('#edit_order_orderEntries_' + counter + '_currency').val(currency);
            $('#edit_order_orderEntries_' + counter + '_priceRecommended').val(storePrice);
            $('#edit_order_orderEntries_' + counter + '_quantity').val('1').change(function() {
                order.form.recalcTotal(this); // пересчтитать тотал
            }).change();

            // Получить ориентировочную дату доставки
            order.form.deliverySyncEstimateDelivery();
        },
        
        /**
         * удаление продукта
         * из левого списка
         * @param object _this - javascript this объект на котором произошло событие клик
         * @param integer productId - ID продукта
         **/
        delProduct: function(_this) 
        {
            $(_this).closest('tr').remove();
            order.form.recalcTotal(_this);
            order.form.deliverySyncEstimateDelivery();
        },
        
        /**
         * подсчет кол-ва TR продуктов 
         **/
        getProductsCounter: function() {

            var counter = 0;
            var iterations = [];

            // получить максивальный элемент массива
            iterations.max = function(arr) {
                return Math.max.apply(Math, arr);
            };

            // обойти все TR с продуктами
            $('#cartList tr').each(function() {
                // проверить установлен ли атрибут iteration для TR
                if (typeof ($(this).attr('iteration')) != 'undefined') {
                    // наполнить массив 
                    iterations.push(parseInt($(this).attr('iteration')));
                }
            });

            // проверить кол-во елементов в массиве, для выбора максимального значения
            if (iterations.length) {
                // получить максимальный элемент массива
                counter = iterations.max(iterations);
            }

            // вернуть счетчик
            return counter;
        },
        
        /**
         * todo: при изменении .price_in and .price_out заменять "," на "."
         * преобразвать к float
         * @param event e      - событие  
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        parseFloat: function(e, _this) 
        {
            // код нажатой клавиши
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            // игнорировать нажатие клавиш 
            // [Enter - код 13, esc - код 27]
            if (keyCode != 13 && keyCode != 27 ) {
                var newVal = $(_this)
                        .val()
                        .replace(',','.');
                // обновить цену
                $(_this).val(newVal);
            }
        },
        
        /**
         * Пересчитать тотал
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        recalcTotal: function(_this) 
        {
            var tr = $(_this).closest('tr');
            var quantity = parseInt(tr.find('.quantity').val());
            var price_out = parseFloat(tr.find('.price_out').val().replace(',','.'));

            tr.find('.order_entry_total span').html(quantity * price_out);

            // Обновление итого по заказу
            var total = 0
            $('.order_entry_total').each(function() {
                if (!$(this).is('tr.order_entry_canceled>td')) {
                    total += parseFloat($(this).text());
                }
            });
            
            $('#order_total').text(total);
        },
        /**
         * изменение поставщика 
         * получение складов для поставщика
         * @param object _this - javascript this объект на котором произошло событие
         * @param bool fromLeftList - флаг, обозначающий что функция была вызвана при перенесении продукта из левого списка в правый
         **/
        productChangeSupplier: function(_this, fromLeftList) {

            // объект TR на котором произошло событие
            var tr = $(_this).closest('tr');

            // данные выбранного поставщика
            var supplierId = $(_this).val();
            var supplierName = $(_this).find('option:selected').text();

            // объект выбора складов
            var warehouse_select = tr.find('.order_entry_select_warehouse');

            // обьект выбранного склада
            var warehouse = tr.find('.order_entry_warehouse');

            //очистить список складов 
            $(warehouse_select).empty();

            // url контроллер получить склады поставщика
            var actionUrl = Routing.generate('Nitra_OrderBundle_Get_Warehouses_For', false, true);

            // отправить ajax запрос получить список складов для поставщика
            var fromServer = order.ajax(actionUrl, {
                type: 'GET',
                data: {supplierId: supplierId}
            });

            // проверить тип ответа от сервера
            if (typeof (fromServer.type) != 'undefined' && // тип ответа
                    fromServer.type == 'error' // ошибка 
                    ) {
                // отображение ошибки
                flashBox.show('Невозможно получить список складов для поставщика "' + supplierName + '". ' + fromServer.message, fromServer.type);
                // прерываем выполнение
                return;
            }

            // проверрить наличие складов в ответе сервера
            if (typeof (fromServer.warehouses) != 'undefined') {

                // наполнить список складов 
                $.each(fromServer.warehouses, function(i, wh) {
                    $(warehouse_select).append($('<option value="' + wh.id + '">' + wh.name + '</option>'));
                });

                // если функция вызвана при добавлении продукта из левого списка в правый то устанавливаем значение склада с tr
                if (typeof (fromLeftList) != 'undefined' && fromLeftList == true) {

                    $(warehouse_select).find('option[value="' + tr.attr('warehouseId') + '"]').attr('selected', 'selected');
                    $(warehouse).val(tr.attr('warehouseId'));
                }
                // сделать активным складом первый элемент из списка складов по поставщику
                else {
                    $(warehouse_select).find('option:first').attr('selected', 'selected');
                    $(warehouse).val($(warehouse_select).find('option:first').val());
                }
            }
        },
        /**
         * изменение поставщика 
         * получение складов для поставщика
         * @param object _this - javascript this объект на котором произошло событие
         * @param bool fromLeftList - флаг, обозначающий что функция была вызвана при перенесении продукта из левого списка в правый
         **/
        productShowSupplierWidget: function(_this) 
        {
            // отключаем изменение поставщика
            return false;

            // объект TR на котором произошло событие
            var tr = $(_this).closest('tr');

            var supplierId = tr.attr('supplierId');

            var warehouseId = tr.attr('warehouseId');

            // удаление имени склада и ссылки на отображения виджета выбора складов
            $(tr).find('.warehouse_name, .show_supplier_widget').remove();

            // отображение виджета выбора складов по поставщику
            $(tr).find('.select_supplier_warehouse_widget').val(supplierId).show();
            
            // виджет выбоа поставщика
            var supplier = $(tr).find('.order_entry_supplier');
            // убрать выделение
            supplier.find('option:selected').removeAttr('selected');
            // выбрать поставщика 
            var supplierName = supplier.find('option[value="'+supplierId+'"]').attr('selected','selected').text();
            // отобразить имя выбранного поставщика для select2
            $(tr).find('.select2-chosen').html(supplierName);
            
            // виджет выбора склада
            var warehouse = $(tr).find('.order_entry_select_warehouse');

            //очистить список складов 
            $(warehouse).empty();

            // url контроллер получить склады поставщика
            var actionUrl = Routing.generate('Nitra_OrderBundle_Get_Warehouses_For', false, true);

            // отправить ajax запрос получить список складов для поставщика
            var fromServer = order.ajax(actionUrl, {
                type: 'GET',
                data: {supplierId: supplierId}
            });

            // проверить тип ответа от сервера
            if (typeof (fromServer.type) != 'undefined' && // тип ответа
                    fromServer.type == 'error' // ошибка 
                    ) {
                // отображение ошибки
                flashBox.show('Невозможно получить список складов для поставщика "' + supplierName + '". ' + fromServer.message, fromServer.type);
                // прерываем выполнение
                return;
            }

            // проверить наличие складов в ответе сервера
            if (typeof (fromServer.warehouses) != 'undefined') {

                // наполнить список складов 
                $.each(fromServer.warehouses, function(i, wh) {
                    $(warehouse).append($('<option value="' + wh.id + '">' + wh.name + '</option>'));
                });

                // выбрать элемент списка 
                $(warehouse).find('option[value="' + warehouseId + '"]').attr('selected', 'selected');

                // отображение виджета склада
                warehouse.show();
            }
        },
        /**
         * Отправить завпрос в DS 
         * для получения инфо по доставке
         **/
        deliverySyncEstimateDelivery: function()
        {
            // получить выбранный город
            var city = $('#edit_order_select_city_for_warehouse option:selected');
            if (typeof (city) == 'undefined') {
                return;
            }

            // проверить если не выбран город доставки прерываем выполенение
            if (!city.val()) {
                // не выбран город получатель
                return;
            }

            // счетчик продуктов
            var counter = (order.form.getProductsCounter());
            if (counter == 0) {
                // нет ни одного продукта для доставки
                return;
            }

            // получить выбранный склад
            var warehouse = $('#edit_order_warehouse option:selected');

            // спрятать результирующие данные доставки
            $('.estimate_delivery_product').remove();
            $('.estimate_delivery_deliveries').remove();

            // массив доставляемых продуктов
            var deliveryData = {
                // ID города получателя
                toCityId: city.val(),
                // ID склада получателя
                toWarehouseId: warehouse.val(),
                // массив доставляемых продуктов
                products: []
            };


            // обойти все TR с продуктами
            $('#cartList tr').each(function() {

                // проверить необходимые данные для расчета стоимости доставки
                if (typeof ($(this).attr('cityBusinessKey')) == 'undefined' ||
                        typeof ($(this).attr('iteration')) == 'undefined' ||
                        typeof ($(this).attr('productId')) == 'undefined' ||
                        typeof ($(this).attr('warehouseId')) == 'undefined' ||
                        typeof ($(this).attr('stockParams')) == 'undefined'
                        ) {
                    // continue
                    return true;
                }

                // проверить статусы позиций 
                if (typeof ($(this).attr('orderEntryStatus')) != 'undefined' &&
                        $(this).attr('orderEntryStatus') &&
                        jQuery.inArray($(this).attr('orderEntryStatus'), order.form.deliverySyncDisallowEntryStatuses) != -1
                        ) {
                    // continue
                    return true;
                }

                // наполнить массив доставляемых продуктов
                // JSON.stringify(
                deliveryData.products.push({
                    productId: $(this).attr('productId'),
                    iteration: $(this).attr('iteration'),
                    fromCityId: $(this).attr('cityBusinessKey'),
                    stockParams: $(this).attr('stockParams'),
                    quantity: $(this).find('.quantity').val(),
                    priceOut: parseFloat($(this).find('.price_out').val())
                });
            });

            // проверить кол-во доставляемых продуктов
            if (deliveryData.products.length == 0) {
                // прерываем выполенение
                return;
            }

            // показать процесс загрузки
            var htmlResult = $('#estimate_delivery_template')
                    .html()
                    .replace('<tbody>', '<tbody><tr><td colspan="6">' + $('#estimate_delivery_loader').html() + '</tr>');
            $('#estimate_delivery_result').html(htmlResult);

            // url контроллер получение данных по доставке 
            var actionUrl = Routing.generate('Nitra_OrderBundle_DeliverySync_EstimateDelivery', false, true);
            // отправить ajax запрос
            $.ajax({
                url: actionUrl,
                type: 'POST',
                data: deliveryData,
                dataType: 'json',
                async: true,
                // ощибка получения данных 
                error: function() {
                    order.form.deliverySyncEstimateDeliveryOnError('Сервис расчета стоимостей доставки не доступен.');
                },
                // данные полуены успешно
                success: function(fromServer) {

                    // проверить ответ сервера
                    if (typeof (fromServer) == 'undefined' ||
                            // ответ сервера должен содержать тип ответа
                            typeof (fromServer.type) == 'undefined'
                            ) {
                        // от сервера получен не корректный ответ
                        order.form.deliverySyncEstimateDeliveryOnError('Сервис расчета стоимостей доставки не доступен.');
                        return;
                    }

                    // ответ сервера ошибка
                    if (fromServer.type == 'error') {
                        // отображение ошибки 
                        order.form.deliverySyncEstimateDeliveryOnError(fromServer.message);
                        // прервать выполнение расчетов
                        return;
                    }

                    // отображение результатов расчета 
                    $('#estimate_delivery_result').html(fromServer.html);

                    // получить список доставляемых продуктов с расчетными данными
                    // обновление отображения списка продуктов
                    var products = fromServer.estimateDelivery.products;
                    $.each(products, function(productKey, product) {

                        var htmlDeliveries = '';
                        // флаг ТК может доставить продукт
                        var isAvailable = false;

                        // проверить возможность доставки продукта ТК
                        if (typeof (product.deliveries) != 'undefined') {
                            $.each(product.deliveries, function(deliveryKey, delivery) {

                                // проверить ключ ТК 
                                // ключь с отрицательным числом только для ТК тариф Интернет магазин 
                                if (deliveryKey <= 0) {
                                    // continue
                                    return true;
                                }

                                // проверить ТК
                                if (delivery &&
                                        delivery.isAvailable
                                        ) {
                                    isAvailable = true;
                                    htmlDeliveries += '<span class="green">' + delivery.name + '</span>; ';
                                } else {
                                    htmlDeliveries += '<span class="red">' + delivery.name + '</span>; ';
                                }
                            });
                        }


                        var divIdProduct = 'estimate_delivery_product_' + product.iteration;
                        var divIdDeliveries = 'estimate_delivery_product_' + product.iteration + '_by_deliveries';

                        var htmlProductDelivery
                                = '<div id="' + divIdProduct + '" class="estimate_delivery_product">'
                                + '<a href="javascript:void(0);" '
                                + " onclick=\"$('#" + divIdProduct + "').remove();$('#" + divIdDeliveries + "').show();\" "
                                + '>Доставка</a>: ';
                        if (isAvailable) {
                            htmlProductDelivery += '<img src="/bundles/nitraorder/images/delivery_sync/tk_is_available_1.png"> ';
                        } else {
                            htmlProductDelivery += '<img src="/bundles/nitraorder/images/delivery_sync/tk_is_available_0.png"> ';
                        }

                        htmlProductDelivery
                                // зактыть div id= divIdProduct
                                += '</div>'
                                + '<div id="' + divIdDeliveries + '" '
                                + ' class="estimate_delivery_deliveries" style="display:none">'
                                + htmlDeliveries
                                + '</div>';

                        // добавить в конец ячейки TD
                        $('#cartList')
                                .find('[iteration="' + product.iteration + '"]')
                                .find('.product_info')
                                .append(htmlProductDelivery);

                    });


                }
            });
        },
        /**
         * обработка ошибочной ситуации 
         * при расчете стоимостей доставки
         * @param string message - текст сообщения
         **/
        deliverySyncEstimateDeliveryOnError: function(message)
        {
            // проверить текст ошибки 
            if (typeof (message) == 'undefined') {
                var message = '';
            }

            // полказать процесс загрузки
            var htmlResult = $('#estimate_delivery_template')
                    .html()
                    .replace('<tbody>', '<tbody><tr><td colspan="6">' + message + '</tr>');
            $('#estimate_delivery_result').html(htmlResult);
        },
        /**
         * конвертировать расчетную дату доставки в формат для отображения
         **/
        estimateDeliveryDateConverter: function(UNIX_timestamp) {
            var a = new Date(UNIX_timestamp * 1000);
            var months = ['Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            return date + ' ' + ' ' + month + ' ' + year;
        },
        /**
         * отображение соответсвующих фильтров формы для позиций заказа
         * вызывается после отображение формы
         **/
//        processProductWarehouses: function() {
//
//            // массив ID поставщиков позиций заказов
//            var supplierIds = new Array();
//            
//            // наполнить массив
//            $('#cartList tr').each(function(){
//
//                // позиция заказа по складу поставщика
//                if($(this).attr('supplierId')) {
//                    supplierIds.push(parseInt($(this).attr('supplierId')));
//                }
//                
//                // позиция со своего склада/склада ТК
//                else {
//                    // добаваляем строку отображения адресса склада и устанавливаем значение скрытому инпуту
//                    $(this).find('.order_warehouse').append($(this).attr('warehouseName'));
//                }
//                
//                // установка выбранного id склада скрытому инпуту
//                $(this).find('.order_entry_warehouse').val($(this).attr('warehouseId'));
//                
//            });
//            
//            // проверить массив складов
//            // отобразить виджет поставщиков, наполнить соответсвующие склады по каждой позиции
//            if (supplierIds.length) {
//                
//                // url контроллер получить данные по складам
//                var actionUrl = Routing.generate('Nitra_OrderBundle_Get_Warehouses_For', false, true);
//                
//                // отправить ajax запрос получить список складов для ТК 
//                var fromServer = order.ajax(actionUrl, {
//                    type: 'POST',
//                    data: { 
//                        'supplierIds':  supplierIds, 
//                    }
//                });
//                
//                // проверить наличие поставщиков или ТК или своих складов в ответе сервера
//                if (typeof(fromServer) != 'undefined' && (
//                        typeof(fromServer.suppliers) != 'undefined'  // в ответе есть поставщики
//                        )
//                ) {
//                    // обойти все продукты 
//                    $('#cartList tr').each(function(){
//                        
//                        // продукт позиции с поставщика
//                        var supplierId = $(this).attr('supplierId');
//                        if (typeof(supplierId) != 'undefined' && // продукт позиции с поставщика
//                            typeof(fromServer.suppliers) != 'undefined' && // в ответе есть поставщики
//                            typeof(fromServer.suppliers[supplierId]) != 'undefined' && // в ответе есть поставщик для $(this) позиции заказа
//                            typeof(fromServer.suppliers[supplierId].warehouses) != 'undefined' // в ответе есть склады поставщика для $(this) позиции заказа
//                        ) {
//                            // виджет выбора склада
//                            var warehouse = $(this).find('.order_entry_select_warehouse');
//                        
//                            // отображение виджета выбора складов по поставщику
//                            $(this).find('.order_entry_supplier').val(supplierId).show();
//                            
//                            // выбранный склад
//                            var warehouseId = $(this).find('.order_entry_warehouse').val();
//                            
//                            //очистить список складов 
//                            $(warehouse).empty();
//                            
//                            // наполнить список складов 
//                            $.each(fromServer.suppliers[supplierId].warehouses, function(i,wh) {
//                                $(warehouse).append( $('<option value="'+wh.id+'">'+wh.name+'</option>') );
//                            });
//                            
//                            // выбрать элемент списка 
//                            $(warehouse).find('option[value="'+warehouseId+'"]').attr('selected','selected');
//
//                            // отображение виджета склада
//                            warehouse.show();
//                        }
//                    });
//                }
//            }
//        }
    },
    /** 
     * перевести объект в стринг
     * @param object - объект переводимый в string
     * @return string - объект преобразованный в строку
     **/
    toString: function(obj) {

        // проверить объект переводимы в стинг
        if (typeof (obj) == 'undefined') {
            console.error("Нет параметра obj для преобразования в строку");
        }

        // перевести в строку 
        var s = '{\n';
        for (var p in obj) {
            s += '    "' + p + '": "' + obj[p] + '"\n';
        }

        // вернуть строку
        return s + '}';
    }


}

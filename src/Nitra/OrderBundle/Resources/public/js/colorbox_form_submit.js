
    $(document).ready(function () {
          $('.colorbox_form').submit(function(){
              
                $.ajax({
                      url: $(this).attr('action'),
                      data: $(this).serialize(),
                      type: 'post',
                      dataType: 'html',
                      success:  function(data){
                          var close = true;
                          
                          if(data.search('error')>0) {
                            $(data).find('.error').each(function(){
                                if ($.trim($(this).text())!='') {
                                    close = false;
                                }
                            })
                          }
                          
                          if(close){
                            $.fn.colorbox.close();
                            window.location = data;
                          }else{
                              $('#cboxLoadedContent').html(data);
                              $.fn.colorbox.resize();
                          }
                          
                      }
                  });
              return false;
           })
       
    });
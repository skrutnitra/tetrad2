<?php

namespace Nitra\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\OrderEntryStatusHistory;

class OrderEntryStatusHistoryRepository extends EntityRepository
{

    /**
     * Находим предыдущий статус для статуса позиции
     * @param \Nitra\OrderBundle\Entity\OrderEntryStatusHistory $position
     * @return type
     */
    public function findPrevPositionDate(OrderEntryStatusHistory $position)
    {
        $res = $this->_em->createQuery("SELECT oesh.createdAt 
                FROM NitraOrderBundle:OrderEntryStatusHistory oesh
                WHERE oesh.toStatus = :status AND oesh.createdBy = :manager AND oesh.createdAt <= :date ")
                ->setParameters(array('status' => $position->getFromStatus(),
                    'manager' => $position->getCreatedBy(),
                    'date' => $position->getCreatedAt()))
                ->setMaxResults(1)
                ->execute();
        if ($res) {
            return $res[0]['createdAt'];
        } else {
            return $position->getOrderEntry()->getCreatedAt();
        }
    }

    /**
     * Переопределяем метод, для хардкодной фильтрации по дате OrderEntryStatus 
     * 
     * @param array $criteria
     * @param array $orderBy
     * @param type $limit
     * @param type $offset
     * @return type
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        if (!isset($criteria['fromDate']) && !isset($criteria['toDate'])) {

            return parent::findBy($criteria, $orderBy, $limit, $offset);
        } else {
            $qb = $this->_em->createQueryBuilder()
                    ->select("oesh")
                    ->from("NitraOrderBundle:OrderEntryStatusHistory", "oesh")
                    ->where("oesh.toStatus = :toStatus")
                    ->setParameter('toStatus', $criteria['toStatus']);
            if (isset($criteria['createdBy']) && is_object($criteria['createdBy'])) {
                $qb->andWhere('oesh.createdBy = :manager')
                        ->setParameter('manager', $criteria['createdBy']);
            }

            if (isset($criteria['fromDate']) && !empty($criteria['fromDate']) && !is_null($criteria['fromDate'])) {
                $qb->andWhere("oesh.createdAt >= :fromDate")
                        ->setParameter('fromDate', $criteria['fromDate']->setTime(00,00,00));
            }
            if (isset($criteria['toDate']) && !empty($criteria['toDate']) && !is_null($criteria['toDate'])) {
                $qb->andWhere("oesh.createdAt <= :toDate")
                        ->setParameter('toDate', $criteria['toDate']->setTime(23,59,59));
            }
            return $qb->getQuery()->execute();
        }
    }

}

<?php

namespace Nitra\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\Sms;

/**
 * BuyerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SmsRepository extends EntityRepository
{
    /**
     * 
     * @param type $order
     * @param type $options
     * @throws Exception
     */   
    public function smsOrderLog($order, $options = null) 
    {
       
        if (!isset($options['sms_phone']) || !isset($options['sms_text']) || !isset($options['manager'])) {
            $message['error'] = 'Укажите номер телефона, текст и manager';
            
            if(isset($options['send_sms_change_to_status_ready'])) {
                return $message;
            } else {
                throw new \Exception($message['error']);
            }
        }
        
        $buyer = $order->getBuyer();
        $sms = new Sms();
        $sms->setBuyer($buyer);
        $sms->setFilial($options['manager']->getDefaultFilial());
        $sms->setOrder($order);
        $sms->setPhone($buyer->getPhone());
        $sms->setText($options['sms_text']);
        $sms->setType(isset($options['sms_type']) ? $options['sms_type'] : null);

//       отправка сообщения
        $smsLog = $this->getSmsParameters($options);
        
        if(isset($smsLog['error'])) {
            if(isset($options['send_sms_change_to_status_ready'])) {
                return $smsLog;
            } else {
                throw new \Exception($smsLog['error']);
            }
        }
        
//      добавиляем стоимость сообщения
        $sms->setCost($smsLog['cost']);
//        получаем статус   
        
        $status = $this->getEntityManager()->getRepository('NitraOrderBundle:SmsStatus')->findOneByMethodName($smsLog['status']);

//        устанавливаем статус сообщения
        $sms->setStatus($status);
        
//      сохранение данных об отправленной СМС 
        $this->getEntityManager()->persist($sms);
    }
    
    
     /**
     * 
     * @param type $options  - $options['sms_phone'], $options['sms_text'], текст сообщения и номер телефона
     * @return type array() : status, cost - статус сообщения и стоимость
     * @throws Exception
     */
    public function getSmsParameters($options){
        
//        получаем параметры - настройки
        $smsParams = $options['sms_parameters'];
        
//        формируем массив для отправки
        $data = array(
            'gate' => $smsParams['gate'],
            'gateStatus' => $smsParams['gateStatus'],
            'login' => $smsParams['login'],
            'psw' => $smsParams['password'],
            'phones' => isset($smsParams['phone']) ? $smsParams['phone'] : $options['sms_phone'],
            'mes' => $options['sms_text'],
            'sender' => $smsParams['sender'],
        );
        
//        отправка
        $smsLog = $this->sendSms($data);

        return $smsLog;
    }
       
   /**
    * 
    * @param type $data - параметры для формирования запроса
    * @return array: стоимость сообщения, статус
    * @throws Exception
    */
    public function sendSms($data)
    {
        $prep_query = array(
            'login' => $data['login'],
            'psw' => $data['psw'],
            'phones' => $data['phones'],
            'mes' => $data['mes'],
            'sender' => $data['sender'],
            'translit' => 1,
            'cost' =>  2,
            'fmt' =>  1,
            'charset' => 'utf-8'
        );
        
        $generated_params = http_build_query($prep_query);

        $host_name = $data['gate'];

        $result = file_get_contents($host_name . '?' . $generated_params);

        if ($result === false) {
            $message['error'] = 'Нет ответа от сервера. (' . $host_name . ')';
           return $message;
        }

        // запрос прошел.
        $r = explode(',', $result);

        // если сервер вернул ошибку
        if (isset($r[1]) && $r[1] < 0) {
            $message['error'] = 'Возникла ошибка при отправлении СМС: ' . $result;
            return $message;
        }

        // Все прошло успешно, смс отправлено.
        // id, count, cost
        list($sms_id, $count, $cost) = explode(',', $result);

        $smsLog['cost'] = (int) $count * (float) $cost;
        
//    добавляем в массив айди сообщения
        $data['smsId'] = $sms_id;
        
//      добавляем статус в отет
        $smsLog['status'] = $this->getSmsStatus($data);

        return $smsLog;
    }
    
    
    /**
     * @param type $statusParam: array 
     * @return string
     */
     public function getSmsStatus($data) {
        
        $prep_querySt = array(
            'login' => $data['login'],
            'psw' => $data['psw'],
            'phone' => $data['phones'],
            'id' => $data['smsId'],
            'fmt' => 1,
        );
         
//        формируем строку
        $generated_paramsSt = http_build_query($prep_querySt);

//       хост
        $host_nameSt = $data['gateStatus'];
//        получаем ответ
        $resultSt = file_get_contents($host_nameSt . '?' . $generated_paramsSt);
        
        list($statusId) = explode(',', $resultSt);

        switch ($statusId) {
            case '-1':
                $status = 'sms_waiting';
                break;
            case '0':
                $status = 'sms_gateway';
                break;
            case '1':
                $status = 'sms_delivered';
                break;
            case '2':
            case '3':
            case '20':
            case '23':
                $status = 'sms_canceled';
                break;
            case '24':
                $status = 'sms_ballance';
                break;
            default:
                $status = 'sms_other';
        }

        return $status;
    }
    
}

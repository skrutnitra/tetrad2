<?php
namespace Nitra\OrderBundle\Repository;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Inflector\Inflector;
use Nitra\MainBundle\Common\ApplicationBoot;
use Nitra\OrderBundle\Entity\Order;
use Nitra\OrderBundle\Entity\OrderStatus;
use Nitra\OrderBundle\Entity\OrderStatusHistory;

/**
 * OrderRepository
 */
class OrderRepository extends EntityRepository
{
    
    /**
     * документ менеджер
     * @var Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;
    
    /**
     * var ProjectContainer
     */
    protected $container;
    
    /**
     * конструктор класса
     */
    public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
        // вызвать конструктор родителя
        parent::__construct($em, $class);
        
        // получить контейнер
        $this->container = ApplicationBoot::getContainer();
    }
    
    /**
     * установить $_dm
     * @DI\InjectParams({
     *      "documentManager" = @DI\Inject("doctrine_mongodb.odm.document_manager"),
     * })
     */
    public function setDocumentManager(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
        
    /**
     * @var array $permittedChainCommon
     * общий массив цепочек изменения статусов заказов
     */
    public static $permittedChainCommon = array(
        
        // waiting с будильника
        'waiting_to_ordered',
        'waiting_to_ready',
//        'waiting_to_reported', цепочка отсутсвует появляется только после ready
        'waiting_to_canceled',
//        'waiting_to_completed', цепочка отсутсвует появляется только после ready

        // ordered  с корзинки
        'ordered_to_waiting', 
        'ordered_to_ready', 
//        'ordered_to_reported', цепочка отсутсвует появляется только после ready
        'ordered_to_canceled', 
//        'ordered_to_completed', цепочка отсутсвует появляется только после ready

        // ready с готов 
        'ready_to_waiting', 
        'ready_to_ordered', 
        'ready_to_reported', 
        'ready_to_canceled', 
        'ready_to_completed',

        // reported с телефон 
//        'reported_to_waiting', цепочка отсутсвует появляется только после ready
//        'reported_to_ordered', цепочка отсутсвует появляется только после ready
        'reported_to_ready', 
        'reported_to_canceled',
        'reported_to_completed', 
        
        // completed с отмены ... только в предыдущий (определить) последний статус
//        'completed_to_waiting', обратная цепочка отсутсвует нет прямой 
//        'completed_to_ordered', братная цепочка отсутсвует нет прямой 
        'completed_to_ready', 
        'completed_to_reported', 
//        'completed_to_canceled',  цепочка отсутсвует, либо completed либо canceled
        
        // canceled с отмены ... только в предыдущий (определить) последний статус
        'canceled_to_waiting', 
        'canceled_to_ordered', 
        'canceled_to_ready', 
        'canceled_to_reported', 
//        'canceled_to_completed', цепочка отсутсвует, либо completed либо canceled
        
    );
    
    /**
     * @var array $permittedChainFilial  
     * для филиалов массив цепочек изменения статусов заказов
     */
    public static $permittedChainFilial  = array(
        
    );
    
    /**
     * Получаем строку с названиеми продуктов по Order
     * @param \Nitra\OrderBundle\Entity\Order $order
     * @return type string
     */
    public function getOrderEntiesNameByOrder(Order $order) {
         // получаем позиции заказа по ордеру
       $products = $this->getEntityManager()
                         ->getRepository('NitraOrderBundle:OrderEntry')
                         ->findByOrder($order);
       $pr = array();
       // получаем название товара из позиции заказа
       foreach($products as $product) {
           if($product) {
                $pr[] = (string)$product;
           }
       }
       
       //       получаем струку с продуктами
        $product = implode(', ', $pr);
       
       return $product;
    }
    
    /**
     * получить доступниые цепочки изменения статусов заказа
     * @param Order $order - заказ
     * @return array $permittedChain - массив доступных цепочек для заказа
     */
    public function getPermittedChain(Order $order) {
        
//        // проверка филиала пользователя
//        if (true) {
//            // вернуть общий набор для филиала
//            return self::$permittedChainFilial;
//        }
        
        // вернуть общий набор цепочек 
        return self::$permittedChainCommon;
    }
    
    /**
     * получить статус заказа по умолчанию
     * @return OrderStatus 
     * @throw \Exception ошибка получения статуса по умолчанию
     */
    public function getDefaultStatus()
    {
        // получить статус по умолчанию
        $status = $this->getEntityManager()
            ->getRepository('NitraOrderBundle:OrderStatus')
            ->findOneBy(array('methodName' => 'waiting'));
        
        // проверить статус
        if ($status) {
            return $status;
        }
        
        // статус не найден
        throw new \Exception('В базе данных не найден статус заказа с methodName = "waiting"');
    }    
    
    /**
     * установить статус заказа по умолчанию
     * @param Order $order
     */
    public function setDefaultStatus(Order $order)
    {
        // получить статус по умолчанию
        $status = $this->getDefaultStatus();
        // установить статус по умолчанию 
        $order->setOrderStatus($status);
    }
    
    /**
     * установить филиал заказа по умолчанию
     * @param Order $order
    */
    public function setDefaultFilial(Order $order)
    {
        // получить филиал
        $filial = $this->getEntityManager()->getRepository('NitraFilialBundle:Filial')->findByStoreId($order->getStoreId());
        
        if ($filial) {
            // устанавливаем первый из полученных филиалов
            $order->setFilial($filial[0]);
            return;
        }
        
        // статус не найден
        throw new \Exception('В базе данных не найдены филиалы по указанному магазину');
    }    
    
    /**
     * получить доступные статусы для заказа $order, 
     * в кторые можно перевести заказ
     * @param Order $order - заказ
     * @return array $orderStatuses - массив доступных цепочек для заказа
     * @return false доступные цепочки не найдены
     */
    public function getAllowedStatus(Order $order)
    {
        
        // массив доступных цепочек для позиии заказов
        $permittedChain = $this->getPermittedChain($order);
        
        // обойти все цепочки найти подходящие цепочки
        $chainArr = array();
        foreach ($permittedChain as $chain) {

            // цепочка из статуса
            $chainFrom = substr($chain, 0, strlen($order->getOrderStatus()->getMethodName()));

            // подходящая цепочка
            if ($chainFrom == $order->getOrderStatus()->getMethodName()) {
                // цепочка в статус
                $chainTo = substr($chain, strlen($chainFrom . '_to_'));
                // наполнить массив 
                // запоминаем в виде key => value 
                $chainArr[$chainTo] = $chainTo;
            }
        }
        
        // проверить текущий статус заказа
        // в зависимости от текущего статуса удаляем предыдущие статусы 
        // в массиве допустимых статусов
        switch($order->getOrderStatus()->getMethodName()) {
            
            // будильник 
            case 'waiting':
                // предыдущих статусов нет
                break;
            
            // корзинка
            case 'ordered':
                unset($chainArr['waiting']);
                break;
            
            // ready - чемодан
            case 'ready':
                unset($chainArr['waiting']);
                unset($chainArr['ordered']);
                break;
            
            // reported
            case 'reported':
                unset($chainArr['waiting']);
                unset($chainArr['ordered']);
                unset($chainArr['ready']);
                break;
            
            // отмена, можно вернуть только на предыдущий статус
            case 'canceled':
            // галочка, можно вернуть только на предыдущий статус
            case 'completed':
                $chainArr = array();
                break;
          
        }
        
        // добавить предыдущий статус в массив статусов
        $previousStatus = $this->getPreviousStatus($order);
        if ($previousStatus) {
            array_push($chainArr, $previousStatus->getMethodName());
        }
        
        // получить массив статусов в которые разрешено переводить из текущего
        $chainArr = array_values($chainArr);
        
        // получить статусы
        if ($chainArr) {
            // получить статусы по по массиву
            $orderStatuses = $this->getEntityManager()
                ->getRepository('NitraOrderBundle:OrderStatus')
                ->findBy(array('methodName' => $chainArr));
            
            // вернуть массив статусов 
            return $orderStatuses;
        }
        
        // доступные статусы не найдены
        return false;
    }

    /**
     * валидировать цепочку для заказа
     * @param Order $order - заказ
     * @param OrderStatus $newStatus - новый статус заказа
     * @param OrderStatus $oldStatus - отображенный пользователю статус заказа 
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validToStatus(Order $order, OrderStatus $newStatus, OrderStatus $oldStatus)
    {
        
        // проверяем соответствие страрого (отображенного пользователю) и текущего статуса 
        if ($oldStatus->getId() != $order->getOrderStatus()->getId()) {
            return "Отображенные ранее Вам данные устарели. Проверьте текущий статус заказа и при необходимости повторите попытку заново.";
        }
        
        // проверить склад для заказа 
        // для всех статусов заказа кроме отмены, ожидания
        // склад может быть не указан если заказ с сайта
        if (!$order->getWarehouse() && !in_array($newStatus->getMethodName(), array('canceled', 'waiting'))) {
            return "Заказ №".(string)$order." не указан склад получатель заказа.";
        }
        
        // проверить текущий статус позиции заказа
        if ($order->getOrderStatus()->getId() == $newStatus->getId()) {
            return "Заказ №".(string)$order." уже находиться в статусе ".(string)$newStatus.". ";
        }
        
        // массив доступных цепочек для заказа
        $permittedChain = $this->getPermittedChain($order);
        
        // Выбранное направление изменения статуса
        $direction = $order->getOrderStatus()->getMethodName() . '_to_' . $newStatus->getMethodName();
        
        // проверить цепочку
        if (!in_array($direction, $permittedChain)) {
            return "Нет цепочки ".$direction." для изменения статуса.";
        }
        
        // Если заказ отменен, или отгружена покупателю, то можно перевести только в предидущий статус
        if (in_array($order->getOrderStatus()->getMethodName(), array('canceled', 'completed'))) {
            // получить предидущий статус заказа
            $previousStatus = $this->getPreviousStatus($order);
            if ($previousStatus && $previousStatus->getMethodName() != $newStatus->getMethodName()) {
                return "Заказ №".(string)$order." можно перевести только в статус ".$previousStatus->getName().". ";
            }
        }
        
        // Влидация пройдена успешно
        return false;
    }
    
    /**
     * Предидущий статус заказа
     * @param Order $order - заказ
     * @return OrderStatus $orderStatus
     */
    public function getPreviousStatus(Order $order)
    {
        
        // получить историю статусов
        $statusHistory = $order->getOrderStatusHistory()->last();
        if ($statusHistory) {
            return $statusHistory->getFromStatus();
        }
        
        // история не найдена 
        return false;
    }
    
    /**
     * уствновить заказу $order новый статус $newStatus
     * @param OrderStatus $newStatus - статус заказа
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров для валидации и выполнения цепочки, 
     *                         для каждого метода валидатора или метода выполнения цепочки 
     *                         может быть задан свой массив параметров
     * @return string - текст сообщения об ошибке 
     * @return false - установка статуса заказа прошла без ошибок
     */
    public function setStatus(OrderStatus $newStatus, Order $order, array $options = null) 
    {

        // Направление изменения статуса
        $direction = $order->getOrderStatus()->getMethodName() . '_to_' . $newStatus->getMethodName();
        
        // название метода обработчика цепочки
        $methodProcessChain = 'processChain' . Inflector::classify($direction);
        
        // выполнить цепочку без предварительной проверки существования метода
        $errorMessage = $this->$methodProcessChain($order, $options);
        // цепочка не выполнена венуть ошибку
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // создать историю изменения статусов
        $orderStatusHistory = new OrderStatusHistory();
        $orderStatusHistory->setOrder($order);
        $orderStatusHistory->setFromStatus($order->getOrderStatus());
        $orderStatusHistory->setToStatus($newStatus);
        $this->getEntityManager()->persist($orderStatusHistory);
        
        // установить новый статус заказа
        $order->setOrderStatus($newStatus);
        $order->addOrderStatusHistory($orderStatusHistory);
        
        // установка статуса прошла успешно
        return false;
    }
    
    /**
     * уствновить заказу $order статус ready
     * @param Order $order - заказ
     * @throws \Exception - ошибка статус ready не найден в базе
     * @return string - текст сообщения об ошибке 
     * @return false  - статус установлен успешно 
     */
    public function setStatusReady(Order $order) 
    {
        
        // получить статус ready
        $orderStatusReady = $this->getEntityManager()
            ->getRepository('NitraOrderBundle:OrderStatus')
            ->findOneBy(array('methodName' => 'ready'));
        
        // проверить статус
        if (!$orderStatusReady) {
            // статус не найден
            throw new \Exception('В базе данных не найден статус заказа с methodName = "ready"');
        }
        
        // установить статус заказа 
        $errorMessage = $this->setStatus($orderStatusReady, $order);
        if ($errorMessage) {
            return $errorMessage;
        }
        
    }
    
    
    
################################################################################
# общие валидаторы
################################################################################
    
    /**
     * общий валидатор цепочки ...to_ready
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validCommonToReady(Order $order, array $options = null)
    {
        
        // проверить текущий статус заказа
        $allowOrderStatuses = array('waiting', 'ordered');
        if (!in_array($order->getOrderStatus()->getMethodName(), $allowOrderStatuses)) {
            
            // получить разрешенные статусы
            $allowOrderStatuses = $this->getEntityManager()
                ->getRepository('NitraOrderBundle:OrderStatus')
                ->findBy(array('methodName' => $allowOrderStatuses));
            
            // массив названий статусов
            $allowOrderStatusesNames = array();
            foreach($allowOrderStatuses as $status) {
                $allowOrderStatusesNames[] = (string)$status;
            }
            
            // вернуть текст сообщения обшибке заказ не в подходящем статусе
            return "Заказ №" . (string) $order . ' должен быть в статусах "'. implode('" или "', $allowOrderStatusesNames) . '".';
        }
        
        // флаг заказа 
        // есть хотябы одна позиция для выдачи покупателю
        $hasReadyOrderEntry = false;
        
        // позиции заказа готовность к выдаче покупателю
        $readyEntryStatuses = array('completed', 'transport', 'arrived');
        
        // позиции заказа действия по позиции завершены
        $ignorEntryStatuses = array('completed', 'canceled', 'returned_to_supplier');
        
        // проверить позиции заказа
        // все позиции заказа должны быть на скалде, указанном в заказе 
        // должна быть хотябы одна позиция к готова к выдаче статус 'transport', 'arrived'
        foreach($order->getOrderEntries() as $orderEntry) {
            // получить склад на котором окажется позиция заказа 
            $warehouse = $this->getEntityManager()
                ->getRepository('NitraOrderBundle:OrderEntry')
                ->getAtWarehousePreFlush($orderEntry);
            
            // проверить не заверщенную позицию 
            // все не завершенные позиции должны быть на складе указанном в заказе 
            // проверить склад на котором находится (или будет находится) позиция заказа
            // со складом указанным в заказе
            if (!in_array($orderEntry->getOrderEntryStatus()->getMethodName(), $ignorEntryStatuses) && // если позиция не завершена
                $warehouse->getId() != $order->getWarehouse()->getId()) // если позиция не на складе заказа (не на конечном складе)
            {
                // позиция заказа не на складе указанном в заказе
                return "Позиция заказа ".(string)$orderEntry." должна быть оприходована на складе ".(string)$order->getWarehouse().".";
            }
            
            // проверить текущий статус позиции 
            // проверить готовность к выдаче покупателю
            if (in_array($orderEntry->getOrderEntryStatus()->getMethodName(), $readyEntryStatuses)) {
                // есть хотябы одна позиция готовая к выдаче
                $hasReadyOrderEntry = true;
            }
        }
        
        // проверить наличие позиций заказа готовых к выдаче покупателю 
        // есть хотябы одна позиция готовая к выдаче
        if ($hasReadyOrderEntry !== true) {
            return "Заказ №" . (string) $order . " нет товаров готовых к выдаче покупателю.";
        }
        
        // валидация пройдена успешно
        return false;
    }

    /**
     * общий валидатор цепочки ...to_canceled
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validCommonToCanceled(Order $order, array $options = null)
    {
        // массив разрешенных статусов позиций заказа
        $allowEntryStatuses = array('canceled', 'returned_to_supplier');
        // проверить статусы позиций заказов 
        foreach($order->getOrderEntries() as $orderEntry) {
            if (!in_array($orderEntry->getOrderEntryStatus()->getMethodName(), $allowEntryStatuses)) {
                // получить разрешенные статусы
                $allowEntryStatuses = $this->getEntityManager()
                    ->getRepository('NitraOrderBundle:OrderEntryStatus')
                    ->findBy(array('methodName' => $allowEntryStatuses));

                // массив названий статусов
                $allowEntryStatusesNames = array();
                foreach ($allowEntryStatuses as $status) {
                    $allowEntryStatusesNames[] = (string) $status;
                }
                // вернуть текст сообщения обшибке позиция заказа не в подходящем статусе
                return "Позиция ".(string) $orderEntry.' должена быть в статусах "'. implode('" или "', $allowEntryStatusesNames) . '".';
            }
        }
        
        // валидация пройдена успешно
        return false;
    }
    
    /**
     * обработчик цепочки ...to_completed
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function validCommonToCompleted(Order $order, array $options = null)
    {
        
        // проверить пользователя устанавливающего статус
        if (!isset($options['manager']) || !$options['manager'] instanceof \Nitra\ManagerBundle\Entity\Manager) {
            // вернуть текст ошибки
            return "Не указан менеджер уставливащий статус.";
        }
        
        // массив разрешенных статусов позиций заказа
        // для перевода в статус завершен
        $allowEntryStatuses = array('transport', 'arrived', 'completed', 'canceled', 'returned_to_supplier');
        
        // флаг у заказа нет подходящих позиций
        $hasCompletedOrderEntry = false;
        
        // проверить статусы позиций заказов 
        foreach($order->getOrderEntries() as $orderEntry) {
            
            // проверить статус позиции заказа в массиве разрешенных статусов 
            // для перевода в статус завершен 
            if (!in_array($orderEntry->getOrderEntryStatus()->getMethodName(), $allowEntryStatuses)) {
                
                // получить разрешенные статусы
                $allowEntryStatuses = $this->getEntityManager()
                    ->getRepository('NitraOrderBundle:OrderEntryStatus')
                    ->findBy(array('methodName' => $allowEntryStatuses));
                
                // массив названий статусов
                $allowEntryStatusesNames = array();
                foreach ($allowEntryStatuses as $status) {
                    $allowEntryStatusesNames[] = (string) $status;
                }
                // вернуть текст сообщения обшибке позиция заказа не в подходящем статусе
                return "Позиция заказа ".(string) $orderEntry.' должна быть в статусах "'. implode('" или "', $allowEntryStatusesNames) . '".';
            }
            
            // для позиций готовых к выдаче 'transport', 'arrived'
            // проверить склад на котором находится позиция заказа
            // со складом указанным в заказе
            if (in_array($orderEntry->getOrderEntryStatus()->getMethodName(), array('transport', 'arrived')) && // позиция прибыла на конечный склад
                $orderEntry->getAtWarehouse()->getId() != $order->getWarehouse()->getId() // проверить склад позиции должен быть на складе заказа
            ) {
                // позиция заказа не на складе указанном в заказе
                return "Позиция заказа ".(string)$orderEntry . " должна быть оприходована на складе ".(string)$order->getWarehouse().".";
            }
            
            // проверить текущий статус позиции заказа готовность к выдаче покупателю либо позиция завершена
            // позиция должна быть в статусах 'transport', 'arrived', 'completed'
            if (in_array($orderEntry->getOrderEntryStatus()->getMethodName(), array('transport', 'arrived', 'completed'))) {
                $hasCompletedOrderEntry = true;
            }
        }
        
        // проверить наличие позиций заказа готовых к выдаче покупателю 
        if ($hasCompletedOrderEntry !== true) {
            return "Заказ №" . (string) $order . " нет товаров готовых к выдаче покупателю.";
        }        
        
        // цепочка обработана успешно
        return false;
    }
    
    
################################################################################
# общие цепочки
################################################################################
    
    /**
     * общий обработчик для цепочек 
     * waiting_to_canceled
     * ordered_to_canceled
     * ready_to_canceled
     * reported_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonToCanceled(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // проверить имя пользователя
        if (!isset($options['username']) || is_null($options['username'])) {
            return "Не указано имя пользователя.";
        }
        
        // установить комментарий отмены заказа
        if (isset($options['cancelComment']) && !is_null($options['cancelComment'])) {
            $order->addCancelComment($options['cancelComment'], $options['username']);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * общий обработчик для цепочек 
     * ready_to_completed
     * reported_to_completed
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonToCompleted(Order $order, array $options = null)
    {
        
        // получить статус позиции заказа completed
        $orderEntryStatusCompleted = $this->getEntityManager()
            ->getRepository('NitraOrderBundle:OrderEntryStatus')
            ->findOneBy(array('methodName' => 'completed'));
        if (!$orderEntryStatusCompleted) {
            // статус не найден
            throw new \Exception('В базе данных не найден статус заказа с methodName = "completed"');
        }
        
        // получить репозиторий OrderEntry
        $orderEntryRepository = $this->getEntityManager()->getRepository('NitraOrderBundle:OrderEntry');
        
        // установить DocumentManager для репозитория 
        $orderEntryRepository->setDocumentManager($this->dm);
        
        // все позиции заказа в статусах 'transport', 'arrived' переводим в статус completed
        foreach($order->getOrderEntries() as $orderEntry) {
            
            // для позиций готовых к выдаче 'transport', 'arrived'
            // проверить склад на котором находится позиция заказа
            // со складом указанным в заказе
            if (in_array($orderEntry->getOrderEntryStatus()->getMethodName(), array('transport', 'arrived')) && // позиция прибыла на конечный склад
                $orderEntry->getAtWarehouse()->getId() == $order->getWarehouse()->getId() // проверить склад позиции должен быть на складе заказа
            ) {
                // обновить статус позиции 
                $errorMessage = $orderEntryRepository->setStatus($orderEntryStatusCompleted, $orderEntry);
                // цепочка не выполнена, вернуть массив ошибок
                if ($errorMessage) {
                    return $errorMessage;
                }
                
            }                
        }
        
        // проверить если заказ не был завершен ранее
        // установить кто и когда завершил заказ
        if (!$order->isCompleted()) {
            // установить время завершения заказа
            $order->setCompletedAt(new \DateTime());
            // установить пользователя завершившего заказ
            $order->setCompletedBy($options['manager']);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с будильника 
################################################################################
    
    /**
     * обработчик цепочки waiting_to_canceled
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToCanceled(Order $order, array $options = null)
    {
        
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // вызвать общий обработчик цепочки
        $errorMessage = $this->processCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки waiting_to_ordered
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToOrdered(Order $order, array $options = null)
    {
        
//        // получить статус позиции заказа ordered
//        $orderEntryStatusOrdered = $this->getEntityManager()
//            ->getRepository('NitraOrderBundle:OrderEntryStatus')
//            ->findOneBy(array('methodName' => 'ordered'));
//        if (!$orderEntryStatusOrdered) {
//            // статус не найден
//            throw new \Exception('В базе данных не найден статус заказа с methodName = "ordered"');
//        }
//        
//        // проверить статусы всех позиций заказа
//        // каждая позиция должна быть ordered
//        foreach($order->getOrderEntries() as $orderEntry) {
//            // проверить статус позиции заказа со статусом ordered
//            if ($orderEntry->getOrderEntryStatus()->getId() != $orderEntryStatusOrdered->getId()) {
//                return "Позиция заказа ".(string)$orderEntry." должна быть в статусе ".(string)$orderEntryStatusOrdered.".";
//            }
//        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки waiting_to_ready
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToReady(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToReady($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с корзинки
################################################################################
    
    /**
     * обработчик цепочки ordered_to_waiting
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToWaiting(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ordered_to_ready
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToReady(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToReady($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ordered_to_canceled
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToCanceled(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // вызвать общий обработчик цепочки
        $errorMessage = $this->processCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с ready
################################################################################
    
    /**
     * обработчик цепочки ready_to_waiting
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReadyToWaiting(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ready_to_canceled
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReadyToCanceled(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // вызвать общий обработчик цепочки
        $errorMessage = $this->processCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ready_to_reported
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReadyToReported(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ready_to_ordered
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReadyToOrdered(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ready_to_completed
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReadyToCompleted(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCompleted($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // выполнить цепочку
        $errorMessage = $this->processCommonToCompleted($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с reported
################################################################################
    
    /**
     * обработчик цепочки reported_to_ready
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReportedToReady(Order $order, array $options = null)
    {
        // не валидируем обработчик цепочки обшим валидатором 
        // валидировали на предыдущих статусах
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки reported_to_completed
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReportedToCompleted(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCompleted($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // выполнить цепочку
        $errorMessage = $this->processCommonToCompleted($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
            
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки reported_to_canceled
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReportedToCanceled(Order $order, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // вызвать общий обработчик цепочки
        $errorMessage = $this->processCommonToCanceled($order, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с отмены 
################################################################################
    
    /**
     * обработчик цепочки canceled_to_waiting
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToWaiting(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки canceled_to_reported
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToReported(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки canceled_to_ordered
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToOrdered(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки canceled_to_ready
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToReady(Order $order, array $options = null)
    {
        // не валидируем обработчик цепочки обшим валидатором 
        // ошибочная рперация
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с завершен
################################################################################
    
    /**
     * обработчик цепочки completed_to_reported
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCompletedToReported(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки completed_to_ready
     * @param Order $order - заказ
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCompletedToReady(Order $order, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }

    
    
################################################################################
# методы репозиторя
################################################################################
    
    /**
     * сохранение заказа
     * @param Order $order - заказ
     */
    public function saveOrder(Order $order)
    {
//        $dm = $this->container->get('doctrine_mongodb.odm.document_manager');
        
        // проверка на то, что что существуют указанные продукты
        foreach($order->getOrderEntries() as $orderEntry) {
            $product = $this->dm->getRepository('NitraMainBundle:Product')->find($orderEntry->getProductId());
            
            if(!$product) {
                throw new \Exception('В базе данных не найден указанный продукт');
            }
        }
        
        // TO DO логика сохранения заказа 
        $em = $this->getEntityManager();
        $em->persist($order);
        $em->flush();
    }
    
}

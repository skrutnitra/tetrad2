<?php
namespace Nitra\OrderBundle\Repository;

//use Symfony\Component\DependencyInjection\ContainerInterface;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Inflector\Inflector;
use Nitra\MainBundle\Common\ApplicationBoot;
use Nitra\MainBundle\Entity\Movement;
use Nitra\MainBundle\Entity\MovementEntry;
//use Nitra\MainBundle\Entity\MovementIncome;
//use Nitra\MainBundle\Entity\MovementRelocation;
use Nitra\MainBundle\Entity\Warehouse;
use Nitra\MainBundle\Entity\Currency;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\OrderBundle\Entity\OrderEntryStatus;
use Nitra\OrderBundle\Entity\OrderEntryStatusHistory;
use Nitra\DeclarationBundle\Entity\Declaration;
use Nitra\DeclarationBundle\Entity\DeclarationEntry;
use Nitra\ContractBundle\Entity\Contract;

/**
 * OrderEntryRepository
 */
class OrderEntryRepository extends EntityRepository // implements ContainerAwareInterface
{

    /**
     * документ менеджер
     * @var Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;
    
    /**
     * @var ProjectContainer
     */
    private $container;
    
//    /**
//     * установить container
//     * @param ContainerInterface $container 
//     */
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }

    /**
     * конструктор класса
     */
    public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
        // вызвать конструктор родителя
        parent::__construct($em, $class);
        
        // получить контейнер
        $this->container = ApplicationBoot::getContainer();
    }
    
    /**
     * установить $_dm
     * @DI\InjectParams({
     *      "documentManager" = @DI\Inject("doctrine_mongodb.odm.document_manager"),
     * })
     */
    public function setDocumentManager(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
    
    /**
     * @var array $permittedChainSupplier 
     * заказ с поставщика
     * массив цепочек изменения статусов позиций заказов
     */
    public static $permittedChainSupplier = array(
        
        // waiting с будильника 
        'waiting_to_clarified',         // +
        'waiting_to_ordered',           // +    wh != supp r+
        'waiting_to_sended',            // +    форма создания декларации
        'waiting_to_arrived',           // +    if(whTo->isOwn == true) { r+ } else { whFrom->isDelivery == true { stockFrom q- };  if(whTo->isDelivery !== true) { stockTo q+ r+ }  }
        'waiting_to_transport',         // +
        'waiting_to_canceled',          // +    
        
        // clarified c флажка
        'clarified_to_waiting',         // +
        'clarified_to_ordered',         // +    wh != supp r+
        'clarified_to_sended',          // +    форма создания декларации
        'clarified_to_arrived',         // +    if(whTo->isOwn == true) { r+ } else { whFrom->isDelivery == true { stockFrom q- };  if(whTo->isDelivery !== true) { stockTo q+ r+ }  }
        'clarified_to_transport',       // + 
        'clarified_to_canceled',        // +    
        
        // ordered с корзины
        'ordered_to_waiting',           // +    wh != supp r-
        'ordered_to_clarified',         // +    wh != supp r-
        'ordered_to_sended',            // +    форма создания декларации
        'ordered_to_arrived',           // +    if (fromWh->isSupp !== true) { stockFrom q-; stockFrom r-; } if (toWh->isSupp !== true) { stockTo q+; stockTo r+; }        
        'ordered_to_transport',         // +    
        'ordered_to_canceled',          // +    
        
        // sended с машинка
        'sended_to_waiting',            // + удаление декларации 
        'sended_to_clarified',          // + удаление декларации 
        'sended_to_ordered',            // + удаление декларации 
        'sended_to_arrived',            // + оприходование на наш склад по данным декларации без колорбокс
//        'sended_to_transport',          // + todo: временно коментируем цепочку, оприходование на склад ТК по данным декларации без колорбокс
        'sended_to_canceled',           // + удаление декларации
        
        // arrived с коробка
        'arrived_to_waiting',           // + if whTo->isOwn == true { r- } else { stockFrom q- r-; if toWh->isSupp !== true { stockTo q+ } }
        'arrived_to_clarified',         // + if whTo->isOwn == true { r- } else { stockFrom q- r-; if toWh->isSupp !== true { stockTo q+ } }
        'arrived_to_sended',            // + удаление перемещения 
        'arrived_to_ordered',           // +
        'arrived_to_transport',         // + movement to ТК
        'arrived_to_canceled',          // + снять с резерва, товар остается у нас на складе
        'arrived_to_completed',         // + снять с резерва, снять кол-во 
        'arrived_to_returned_to_supplier', // + ВОЗВРАТ movement to supplier
        
        // transport с самолетика
        'transport_to_waiting',         // +    
        'transport_to_clarified',       // +    
//        'transport_to_sended',          // + // todo: временно комментируем цепочку
        'transport_to_ordered',         // +
        'transport_to_canceled',        // +
        'transport_to_arrived',         // +
        'transport_to_completed',       // + 
        'transport_to_returned_to_supplier',    // +    
        
        // canceled с отмены ... только в предыдущий (определить) последний статус
        'canceled_to_waiting',          // + 
        'canceled_to_clarified',        // +
        'canceled_to_ordered',          // + 
        'canceled_to_sended',           // +
        'canceled_to_arrived',          // +
        'canceled_to_transport',
         
        // completed с отмены ... только в предыдущий (определить) последний статус
        'completed_to_arrived',         // + 
        'completed_to_transport',       // +
         
        // returned_to_supplier 
        // вернули поставщику ... только в предыдущий (определить) последний статус
        'returned_to_supplier_to_arrived',      // +    
        'returned_to_supplier_to_transport',    // +
        
    );
    
    /**
     * @var array $permittedChainSupplier 
     * заказ со склада ТК или со свое склада
     * массив цепочек изменения статусов позиций заказов
     */
    public static $permittedChainWarehouse = array(
        
        // waiting с будильника 
//        'waiting_to_clarified',         // убираем цепочку // +
//        'waiting_to_ordered',           // убираем цепочку // +    wh != supp r+
        
//        'waiting_to_sended',            // +    форма создания декларации
        'waiting_to_arrived',           // +    if(whTo->isOwn == true) { r+ } else { whFrom->isDelivery == true { stockFrom q- };  if(whTo->isDelivery !== true) { stockTo q+ r+ }  }
        'waiting_to_transport',         // +
        'waiting_to_canceled',          // +    
        
        // clarified c флажка
//        'clarified_to_waiting',         // убираем цепочку // +
//        'clarified_to_ordered',         // убираем цепочку // +    wh != supp r+
//        'clarified_to_sended',          // +    форма создания декларации
        'clarified_to_arrived',         // +    if(whTo->isOwn == true) { r+ } else { whFrom->isDelivery == true { stockFrom q- };  if(whTo->isDelivery !== true) { stockTo q+ r+ }  }
        'clarified_to_transport',       // + 
        'clarified_to_canceled',        // +    
        
        // ordered с корзины
//        'ordered_to_waiting',           // убираем цепочку // +    wh != supp r-
//        'ordered_to_clarified',         // убираем цепочку // +    wh != supp r-
//        'ordered_to_sended',            // +    форма создания декларации
        'ordered_to_arrived',           // +    if (fromWh->isSupp !== true) { stockFrom q-; stockFrom r-; } if (toWh->isSupp !== true) { stockTo q+; stockTo r+; }        
        'ordered_to_transport',         // +    
        'ordered_to_canceled',          // +    
        
//        // sended с машинка
//        'sended_to_waiting',            // + удаление декларации 
//        'sended_to_clarified',          // + удаление декларации 
//        'sended_to_ordered',            // + удаление декларации 
//        'sended_to_arrived',            // + оприходование на наш склад по данным декларации без колорбокс
//        'sended_to_transport',          // + оприходование на склад ТК по данным декларации без колорбокс
//        'sended_to_canceled',           // + удаление декларации
        
        // arrived с коробка
        'arrived_to_waiting',           // + if whTo->isOwn == true { r- } else { stockFrom q- r-; if toWh->isSupp !== true { stockTo q+ } }
        'arrived_to_clarified',         // + if whTo->isOwn == true { r- } else { stockFrom q- r-; if toWh->isSupp !== true { stockTo q+ } }
//        'arrived_to_sended',            // + удаление перемещения 
        'arrived_to_ordered',           // +
        'arrived_to_transport',         // + movement to ТК
        'arrived_to_canceled',          // + снять с резерва, товар остается у нас на складе
        'arrived_to_completed',         // + снять с резерва, снять кол-во 
//        'arrived_to_returned_to_supplier', // + ВОЗВРАТ movement to supplier
        
        // transport с самолетика
        'transport_to_waiting',         // +    
        'transport_to_clarified',       // +    
//        'transport_to_sended',          // +
        'transport_to_ordered',         // +
        'transport_to_canceled',        // +
        'transport_to_arrived',         // +
        'transport_to_completed',       // + 
//        'transport_to_returned_to_supplier',    // +    
        
        // canceled с отмены ... только в предыдущий (определить) последний статус
        'canceled_to_waiting',          // + 
        'canceled_to_clarified',        // +
        'canceled_to_ordered',          // + 
//        'canceled_to_sended',           // +
        'canceled_to_arrived',          // +
        'canceled_to_transport',        // +
         
        // completed с отмены ... только в предыдущий (определить) последний статус
        'completed_to_arrived',         // + 
        'completed_to_transport',
         
//        // returned_to_supplier 
//        // вернули поставщику ... только в предыдущий (определить) последний статус
//        'returned_to_supplier_to_arrived',      // +    
//        'returned_to_supplier_to_transport',    // +
        
    );
    
    /**
     * получить доступниые цепочки изменения статусов для позиции заказа
     * @param OrderEntry $orderEntry - позиция заказа 
     * @return array $permittedChain - массив доступных цепочек для позиции заказа
     */
    public function getPermittedChain(OrderEntry $orderEntry) 
    {
        // заказ с нашего склада либо со склада ТК
        if ($orderEntry->getWarehouse()->isSupplier() !== true ) {
            // вернуть массив цепочек для заказ со склада 
            return self::$permittedChainWarehouse;
        }
        
        // заказ со склада поставщика вернуть массив цепочек для склада поставщика
        return self::$permittedChainSupplier;
    }
    
    /**
     * получить статус позиции по умолчанию
     * @return OrderEntryStatus 
     * @throw \Exception ошибка получения статуса по умолчанию 
     */
    public function getDefaultStatus()
    {
        // получить статус по умолчанию
        $status = $this->getEntityManager()
            ->getRepository('NitraOrderBundle:OrderEntryStatus')
            ->findOneBy(array('methodName' => 'waiting'));
        
        // проверить статус
        if ($status) {
            return $status;
        }
        
        // статус не найден
        throw new \Exception('В базе данных не найден статус позиции заказа с methodName = "waiting"');
    }   
    
    /**
     * установить статус позиции по умолчанию
     * @param OrderEntry $orderEntry
     */
    public function setDefaultStatus(OrderEntry $orderEntry)
    {
        // получить статус по умолчанию
        $status = $this->getDefaultStatus();
        // установить статус по умолчанию 
        $orderEntry->setOrderEntryStatus($status);
    }
    
    /**
     * получить доступные статусы для позиции заказа $orderEntry, 
     * в кторые можно перевести позицию заказа
     * @param OrderEntry $orderEntry - позиция заказа 
     * @return array $orderEntryStatuses - массив доступных цепочек для позиции заказа
     * @return false доступные цепочки не найдены
     */
    public function getAllowedStatus(OrderEntry $orderEntry)
    {
        
        // массив доступных цепочек для позиии заказов
        $permittedChain = $this->getPermittedChain($orderEntry);
        
        // обойти все цепочки найти подходящие цепочки
        $chainArr = array();
        foreach ($permittedChain as $chain) {

            // цепочка из статуса позиции 
            $chainFrom = substr($chain, 0, strlen($orderEntry->getOrderEntryStatus()->getMethodName()));

            // подходящая цепочка
            if ($chainFrom == $orderEntry->getOrderEntryStatus()->getMethodName()) {
                // цепочка в статус позиции 
                $chainTo = substr($chain, strlen($chainFrom . '_to_'));
                // наполнить массив 
                // запоминаем в виде key => value 
                $chainArr[$chainTo] = $chainTo;
            }
        }
        
        // проверить текущий статус позиции
        // в зависимости от текущего статуса удаляем предыдущие статусы 
        // в массиве допустимых статусов
        switch($orderEntry->getOrderEntryStatus()->getMethodName()) {
            
            // будильник 
            case 'waiting':
                // предыдущих статусов нет
                break;
            
            // флажек
            case 'clarified':
                unset($chainArr['waiting']);
                break;
            
            // корзинка
            case 'ordered':
                unset($chainArr['waiting']);
                unset($chainArr['clarified']);
                break;
            
            // машинка
            case 'sended':
                unset($chainArr['waiting']);
                unset($chainArr['clarified']);
                unset($chainArr['ordered']);
                break;
            
            // коробок
            case 'arrived':
                unset($chainArr['waiting']);
                unset($chainArr['clarified']);
                unset($chainArr['ordered']);
                unset($chainArr['sended']);
                break;
            
            // самолетик
            case 'transport':
                unset($chainArr['waiting']);
                unset($chainArr['clarified']);
                unset($chainArr['ordered']);
                unset($chainArr['sended']);
                break;
            
            // отмена, можно вернуть только на предыдущий статус
            case 'canceled':
            // галочка, можно вернуть только на предыдущий статус
            case 'completed':
            // сердце, можно вернуть только на предыдущий статус
            case 'returned_to_supplier':
                $chainArr = array();
                break;
                
        }
        
        // добавить предыдущий статус в массив статусов
        $previousStatus = $this->getPreviousStatus($orderEntry);
        if ($previousStatus) {
            array_push($chainArr, $previousStatus->getMethodName());
        }
        
        // получить массив статусов в которые разрешено переводить из текущего
        $chainArr = array_values($chainArr);
        
        // получить статусы
        if ($chainArr) {
            // получить статусы по по массиву
            $orderEntryStatuses = $this->getEntityManager()
                ->getRepository('NitraOrderBundle:OrderEntryStatus')
                ->findBy(array('methodName' => $chainArr));
            
            // вернуть массив статусов 
            return $orderEntryStatuses;
        }
        
        // доступные статусы не найдены
        return false;
    }

    /**
     * валидировать цепочку для позиции заказа
     * @param OrderEntry $orderEntry - позиция заказа
     * @param OrderEntryStatus $newStatus - новый статус позиции 
     * @param OrderEntryStatus $oldStatus - отображенный пользователю статус позиции 
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validToStatus(OrderEntry $orderEntry, OrderEntryStatus $newStatus, OrderEntryStatus $oldStatus)
    {
        // check if product is not removed
        if (!$orderEntry->getProduct()) {
            return "Товар, позиции заказа удален";
        }

        // проверяем соответствие страрого (отображенного пользователю) и текущего статуса 
        if ($oldStatus->getId() != $orderEntry->getOrderEntryStatus()->getId()) {
            return "Отображенные ранее Вам данные устарели. Проверьте текущий статус позиции и при необходимости повторите попытку заново.";
        }
        
        // проверить склад для заказа 
        // для всех статусов заказа кроме отмены, ожидания
        // склад может быть не указан если заказ с сайта
        if (!$orderEntry->getOrder()->getWarehouse() && !in_array($newStatus->getMethodName(), array('canceled'))) {
            return "Заказ №".(string)$orderEntry->getOrder()." не указан склад получатель заказа.";
        }
        
        // Проверяем статус заказа завершен
        if ($orderEntry->getOrder()->getOrderStatus()->getMethodName() == 'completed') {
            return 'Заказ завершен.';
        }
        
        // Проверяем статус заказа отменен
        if ($orderEntry->getOrder()->getOrderStatus()->getMethodName() == 'canceled') {
            return 'Заказ отменен.';
        }
        
        // проверить текущий статус позиции заказа
        if ($orderEntry->getOrderEntryStatus()->getId() == $newStatus->getId()) {
            return "Позиция ".(string) $orderEntry." уже находиться в статусе ".(string)$newStatus.". ";
        }
        
        // массив доступных цепочек для позиии заказов
        $permittedChain = $this->getPermittedChain($orderEntry);
        
        // Выбранное направление изменения статуса
        $direction = $orderEntry->getOrderEntryStatus()->getMethodName() . '_to_' . $newStatus->getMethodName();
        
        // проверить цепочку
        if (!in_array($direction, $permittedChain)) {
            return "Нет цепочки ".$direction." для изменения статуса.";
        }
        
//        // Запрет на отмену закупки или резервирования если есть перемещения товара
//        if (in_array($direction, array('order_entry_received_to_order_entry_confirm', 'order_entry_transport_to_order_entry_confirm'))) {
//
//            $has_movement = Doctrine_Query::create()
//                    ->from('OrderToDelivery')
//                    ->where('order_entry_id = ?', $this->getOrderEntryId())
//                    ->andWhere('stock_movement_id IS NOT NULL')
//                    ->limit(1)
//                    ->count();
//            if ($has_movement) {
//                return 'По позиции ' . $this . ' уже созданы перемещения';
//            }
//        }
         
        // Если позиция отменена, возвращена поставщику или отгружена покупателю, то можно перевести только в предыдущий статус
        if (in_array($orderEntry->getOrderEntryStatus()->getMethodName(), array('canceled', 'completed', 'returned_to_supplier'))) {
            // получить предыдущий статус позиции
            $previousStatus = $this->getPreviousStatus($orderEntry);
            if ($previousStatus && $previousStatus->getMethodName() != $newStatus->getMethodName()) {
                return "Позицию ".(string) $orderEntry." можно перевести только в статус ".$previousStatus->getName().". ";
            }
        }
        
        // Влидация пройдена успешно
        return false;
    }
    
    /**
     * предыдущий статус позиции
     * @param OrderEntry $orderEntry - объект позиции заказа
     * @return OrderEntryStatus $orderEntryStatus
     */
    public function getPreviousStatus(OrderEntry $orderEntry)
    {
        
        // получить историю статусов
        $statusHistory = $orderEntry->getOrderEntryStatusHistory()->last();
        if ($statusHistory) {
            return $statusHistory->getFromStatus();
        }
        
        // история не найдена 
        return false;
    }
    
    /**
     * Создать декларацию для позиции заказа
     * если декларация с указанным номером существует то будет создана новая позиция декларации 
     * @param array $declarationNumber массив декларации
     *              id - ID декларации выбранный из автокомплита, null если новая декларация
     *              name - текстовый номер декларации (введенный, выбранный) из автокомплита
     * @param OrderEntry $orderEntry - позиция заказа по которой производится действие
     * @param Warehouse $fromWarehouse - склад отправитель 
     * @param Warehouse $toWarehouse - склад получатель 
     * @param array $options - массив параметров
     * @throw \Exception - ошибка добавления декларации
     */
    private function addDeclaration($declarationNumber, OrderEntry $orderEntry, Warehouse $fromWarehouse, Warehouse $toWarehouse, array $options = null)
    {
        
        // проверить массив параметров 
        if (!$options) {
            // вернуть текст ошибки
            throw new \Exception('Не указан массив параметров, обязательный для создания декларации.');
        }
        
        // проверить дату
        if (!isset($options['date']) || !$options['date'] instanceof \DateTime) {
            // вернуть текст ошибки
            throw new \Exception('Не указана дата.');
        }
        
        // проверить данные декларации ID 
        if (!isset($declarationNumber['id'])) {
            // вернуть текст ошибки
            // должен быть 
            // $declarationNumber['id'] == string 'new' - новая декларация  или 
            // $declarationNumber['id'] == integer ID - найденной декларации автокомплит 
            throw new \Exception('Не указана идентификатор декаларации.');
        }
        
        // проверить данные декларации номер
        if (!isset($declarationNumber['name'])) {
            // вернуть текст ошибки
            // должен быть string 0 декларации номер 
            throw new \Exception('Не указана номер декаларации.');
        }
        
        // получить ID декларации
        // приводим к типу integer, потому что 
        // если декларация новая (автокомплит не нашел декларацию)
        // то $declarationNumber['id'] == new
        $declarationID = (int)$declarationNumber['id'];
        
        // получить декларацию по ID
        if ($declarationID) {
            // получить декларацию
            $declaration = $this->getEntityManager()
                ->getRepository('NitraDeclarationBundle:Declaration')
                ->find($declarationID);
            
            // проверить декларацию 
            if (!$declaration) {
                throw new \Exception('Декларация не найдена.');
            }
            
            // проверить склад отправитель по декларации 
            // и склад отправитель позиции заказа
            if ($declaration->getFromWarehouse()->getId() != $fromWarehouse->getId()) {
                // текст ошибки
                $errorMessage = 'Для декларации № '.$declaration->getDeclarationNumber() 
                    . ' от: ' . $declaration->getDate()->format('Y-m-d')
                    . ' склад отправитель ('. (string)$declaration->getFromWarehouse() .')'
                    . ' не равен складу отправителя товарной позиции ('. (string)$fromWarehouse .')'
                    . '.';
                // вернуть текст ошибки
                throw new \Exception($errorMessage);
            }
            
            // проверить склад получателя по декларации 
            // и склад получатель позиции заказа
            if ($declaration->getToWarehouse()->getId() != $toWarehouse->getId()) {
                // текст ошибки
                $errorMessage = 'Для декларации № '.$declaration->getDeclarationNumber() 
                    . ' от: ' . $declaration->getDate()->format('Y-m-d')
                    . ' склад получатель ('. (string)$declaration->getToWarehouse() .')'
                    . ' не равен складу получателя товарной позиции ('. (string)$toWarehouse .')'
                    . '.';
                // вернуть текст ошибки
                throw new \Exception($errorMessage);
            }
            
            
        } else {
            // поиск декларации по номеру
            // получить декларацию
            $declaration = $this->getEntityManager()
                ->getRepository('NitraDeclarationBundle:Declaration')
                ->findOneBy(array('declarationNumber' => $declarationNumber['name']));
            
            // проверить декларацию 
            // если декларация по номеру не была найдена
            // создаем новую декларацию
            if (!$declaration) {
                // создать декларацию
                $declaration = new Declaration();
                $declaration->setDeclarationNumber($declarationNumber['name']);
                // persist декларацию
                $this->getEntityManager()->persist($declaration);
            }           
        }
        
        // создать позицию декларации 
        $declarationEntry = new DeclarationEntry();
        $declarationEntry->setOrderEntry($orderEntry);
        $declarationEntry->setProductId($orderEntry->getProductId());
        $declarationEntry->setQuantity($orderEntry->getQuantity());
        $declarationEntry->setIsActive(true);
        $declarationEntry->setStockParams($orderEntry->getStockParams());
        
        // установить данне декларации
        $declaration->setDate($options['date']);
        $declaration->setFromWarehouse($fromWarehouse);
        $declaration->setToWarehouse($toWarehouse);
        if (isset($options['comment']) && !is_null($options['comment'])) {
            $declaration->addComment($options['comment'], $options['manager']);
        }
        $declaration->addDeclarationEntry($declarationEntry);
        
        // валидировать декларацию
        $this->processValidArgs($declarationEntry, $declaration);        
        
        // persist позицию декларации
        $this->getEntityManager()->persist($declarationEntry);
    }
    
    /**
     * удаление деклараций для позиции заказа
     * @param OrderEntry $orderEntry - позиция заказа
     */
    private function deleteDeclarations(OrderEntry $orderEntry)
    {
        // QueryBuilder
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        
        // получить позиции деклараций по позиции заказа
        $declarationEntries = $qb
            ->select('de')
            ->from("NitraDeclarationBundle:DeclarationEntry", 'de')
            ->where('de.orderEntry = :order_entry_id')
            ->setParameter('order_entry_id', $orderEntry->getId())
            ->getQuery()
            ->execute();
        
        // массив удаляемых деклараций
        $declarationIds = array();
        
        //Массив удаляемых позицый деклараций
        $declaratoinEntryIds = array();
        
        // для позиции есть позиции деклараций
        if ($declarationEntries) {
            foreach($declarationEntries as $entry) {
                $entry->setIsActive(false);
                $declarationIds[] = $entry->getDeclaration()->getId();
                $declaratoinEntryIds[] = $entry->getId();
                $this->getEntityManager()->remove($entry);
            }
        }
        
        // удалить декларации 
        $declarationIds = array_unique($declarationIds);
        //Запомнили Id's позиций
        $declaratoinEntryIds = array_unique($declaratoinEntryIds);
        if ($declarationIds) {
            
            // получить удаляемые декларации 
            $declarations = $qb
                ->select('d')
                ->from('NitraDeclarationBundle:Declaration', 'd')
                ->andWhere('d.id IN (:ids) AND NOT EXISTS (SELECT de1.id 
                        FROM NitraDeclarationBundle:DeclarationEntry de1
                        WHERE de1.id NOT IN (:deleteEntriesId))')
                ->setParameter('ids',$declarationIds)
                ->setParameter('deleteEntriesId',$declaratoinEntryIds)
                ->getQuery()
                ->execute();
            
            // удалить декларации 
            foreach($declarations as $declaration) {
                $this->getEntityManager()->remove($declaration);
            }
            
        }
    }
    
    /**
     * удаление перемещений для позиции заказа
     * @param OrderEntry $orderEntry - позиция заказа
     */
    private function deleteMovements(OrderEntry $orderEntry)
    {
        // QueryBuilder
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        
        // получить позиции перемещений по позиции заказа
        $movementEntries = $qb
            ->select('me')
            ->from("NitraMainBundle:MovementEntry", 'me')
            ->where('me.orderEntry = :order_entry_id')
            ->setParameter('order_entry_id', $orderEntry->getId())
            ->getQuery()
            ->execute();
        
        // массив удаляемых перемещений
        $movementIds = array();

        // для позиции есть перемещения
        foreach ($movementEntries as $entry) {
            $movementIds[] = $entry->getMovement()->getId();
        }
        
        // удалить перемещения 
        $movementIds = array_unique($movementIds);
        if ($movementIds) {
            
            // получить удаляемые перемещения
            $movements = $qb
                ->select('m')
                ->from('NitraMainBundle:Movement', 'm')
                ->andWhere('m.id IN (:ids)')
                ->setParameter('ids', $movementIds)
                ->getQuery()
                ->execute();
            
            // удалить перемещения
            foreach ($movements as $movement) {
                $this->getEntityManager()->remove($movement);
            }
        }
    }
    
    /**
     * получить склад на котором окажется позиция заказа 
     * в результате завершения операции, после выполнения flush
     * данные в БД еще не сохранены
     * @param OrderEntry $orderEntry - позиция заказа
     * @return Warehouse склад на который будет перемещена позиция либо склад на котором позиция находится в данныей момент
     */
    public function getAtWarehousePreFlush(OrderEntry $orderEntry)
    {
        
        // позиция последнего перемещения не сохраненная в БД 
        // для позиции заказа $orderEntry
        $lastInsertMovementEntry = null;
        
        // получить массив сущностей Entity для добавления в БД
        $entityInsertions = $this->getEntityManager()->getUnitOfWork()->getScheduledEntityInsertions();
        foreach($entityInsertions as $entityInsert) {
            // если добавляемая сушность экземпляр класса MovementEntry
            // если добавляемая сущность - позиция перемещения
            if ($entityInsert instanceof MovementEntry) {
                // запоминаем позицию перемещения 
                // break не используем
                // в массиве $entityInsertions может быть несколько MovementEntry
                // запоминаем последню для позиции заказа $orderEntry
                if ($orderEntry->getId() == $entityInsert->getOrderEntry()->getId()) {
                    $lastInsertMovementEntry = $entityInsert;
                }
            }
        }
        
        // если существует не сохраненное перемещение
        if ($lastInsertMovementEntry) {
            // вернуть склад на котором окажется позиция после сохранения
            // склад на котором будет позиция после flush
            // вернуть склад получатель
            return $lastInsertMovementEntry->getMovement()->getToWarehouse();
        }
        
        
        // позиция последнего перемещения удаляемого из в БД 
        // для позиции заказа $orderEntry
        $lastDeleteMovementEntry = null;
        
        // получить массив сущностей Entity для удаления из БД
        $entityDeletions = $this->getEntityManager()->getUnitOfWork()->getScheduledEntityDeletions();
        foreach($entityDeletions as $entityDelete) {
            // если удадяем сушность экземпляр класса MovementEntry
            // если удаляем сущность - позиция перемещения
            if ($entityDelete instanceof MovementEntry) {
                // запоминаем позицию перемещения 
                // break не используем
                // в массиве $entityDeletions может быть несколько MovementEntry
                // запоминаем последню для позиции заказа $orderEntry
                if ($orderEntry->getId() == $entityDelete->getOrderEntry()->getId()) {
                    $lastDeleteMovementEntry = $entityDelete;
                }
            }
        }
        
        // если существует удаляемое перемещение
        if ($lastDeleteMovementEntry) {
            // вернуть склад на котором окажется позиция после сохранения
            // склад на котором будет позиция после flush
            // вернуть склад отправитель
            
            // получить склад отправитель
            $fromWarehouse = $lastDeleteMovementEntry->getMovement()->getFromWarehouse();
            
            // проверить склад отправитель
            // если позиция была заказана со склада поставщика то $fromWarehouse будет null
            // если позиция была заказана с нашего склада или со склада ТК то $fromWarehouse НЕ будет null
            if ($fromWarehouse) {
                // позиция была заказана с нашего склада или со склада ТК
                return $fromWarehouse;
            }
        }
        
        // по умолчанию 
        // вернуть складна котором находится позиция в данный момент
        // склад на котором находится позиция по факту
        return $orderEntry->getAtWarehouse();
    }
    
    /**
     * уствновить позиции заказа $orderEntry новый статус $newStatus
     * @param OrderEntryStatus $newStatus - статус позиции 
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров для валидации и выполнения цепочки, 
     *                         для каждого метода валидатора или метода выполнения цепочки 
     *                         может быть задан свой массив параметров
     * @return string - текст сообщения об ошибке 
     * @return false - установка статуса позиции заказа прошла без ошибок
     */
    public function setStatus(OrderEntryStatus $newStatus, OrderEntry $orderEntry, array $options = null)
    {

        // Направление изменения статуса
        $direction = $orderEntry->getOrderEntryStatus()->getMethodName() . '_to_' . $newStatus->getMethodName();
        
        // название метода обработчика цепочки
        $methodProcessChain = 'processChain' . Inflector::classify($direction);
        
        // выполнить цепочку без предварительной проверки существования метода
        $errorMessage = $this->$methodProcessChain($orderEntry, $options);
        // цепочка не выполнена венуть ошибку
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // создать историю изменения статусов
        $orderEntryStatusHistory = new OrderEntryStatusHistory();
        $orderEntryStatusHistory->setOrderEntry($orderEntry);
        $orderEntryStatusHistory->setFromStatus($orderEntry->getOrderEntryStatus());
        $orderEntryStatusHistory->setToStatus($newStatus);
        $this->getEntityManager()->persist($orderEntryStatusHistory);
        
        // установить новый статус позиции заказа
        $orderEntry->setOrderEntryStatus($newStatus);
        $orderEntry->addOrderEntryStatusHistory($orderEntryStatusHistory);
        
        // проверить устанавливаемый статус позиции заказа
        // если статус не запрещен то обновить статус заказа в ready
        if (!in_array($newStatus->getMethodName(), array('canceled', 'returned_to_supplier'))) {
            
            // получить заказ позиции 
            $order = $orderEntry->getOrder();

            // валидировать заказ для перевода в статус ready
            $errorMessageOrderToReady = $this->getEntityManager()
                ->getRepository('NitraOrderBundle:Order')
                ->validCommonToReady($order);
            
            // валидация пройдена успешно 
            if ($errorMessageOrderToReady === false) {
                // установить статус ready
                $errorMessage = $this->getEntityManager()
                    ->getRepository('NitraOrderBundle:Order')
                    ->setStatusReady($order);
                // проверить рузельтат обновления статуса заказа
                if ($errorMessage) {
                    // ошибка обновления статуса заказа
                    return $errorMessage;
                }

            }
            
        }
        
        // установка статуса прошла успешно
        return false;
    }
    
 
################################################################################
# общие валидаторы
################################################################################
    
    /**
     * Валидировать список арнументов передаваемых в метод
     * @param mixed, валидируемые объекты, через запятую
     * @throw \Exception - ошибка валидации
     * @return true - валидация пройдена успешно
     */
    public function processValidArgs()
    {
        // получить массив валидируемых объектов
        $args = func_get_args();
        if (!$args) {
            throw new \Exception("Нет валидируемых объектов.");
        }
        
        // получить валидатор
        $validator = $this->container->get('validator');
        
        // валидировать каждый эдемент массива
        foreach($args as $obj) {
            // валидировать объект
            $errors = $validator->validate($obj);
            if ($errors->count()) {
                foreach($errors as $error) {
                    throw new \Exception("Ошибка валидации ".get_class($obj).". Параметр: \"".$error->getPropertyPath()."\". ". $error->getMessage());
                }
            }
        }
        
        // валидация пройдена успешно
        return true;
    }
    
    /**
     * общий валидатор цепочки ...to_sended
     * waiting_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validCommonToSended(OrderEntry $orderEntry, array $options = null)
    {
        
        // проверить массив параметров валидации
        if (!$options) {
            // вернуть текст ошибки
            return "Не указан массив параметров, обязательный для выполенения цепочки.";
        }
        
        // проверить склад получатель
        if (!isset($options['warehouse']) || !$options['warehouse'] instanceof Warehouse) {
            // вернуть текст ошибки
            return "Не указан склад получатель.";
        }
        
        // проверить дату
        if (!isset($options['date']) || !$options['date'] instanceof \DateTime) {
            // вернуть текст ошибки
            return "Не указана дата.";
        }
        
//        // проверить номер декларации 
//        if (is_null($options['declarationNumber']) || is_null($options['declarationNumber'])) {
//            // вернуть текст ошибки
//            return "Не указан номер декларации.";
//        }
        
        // валидация пройдена успешно
        return false;
    }
    
    /**
     * общий валидатор цепочки ...to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validCommonToArrived(OrderEntry $orderEntry, array $options = null)
    {
        
        // проверить массив параметров валидации
        if (!$options) {
            // вернуть текст ошибки
            return "Не указан массив параметров, обязательный для выполенения цепочки.";
        }
        
        // проверить склад получатель
        if (!isset($options['warehouse']) || !$options['warehouse'] instanceof Warehouse) {
            // вернуть текст ошибки
            return "Не указан склад получатель.";
        }
        
        // проверить наличие цены
        if (!isset($options['price']) || is_null($options['price'])) {
            // вернуть текст ошибки
            return "Не указана цена входа.";
        }
        
        // проверить склад получатель
        if (!isset($options['currency']) || !$options['currency'] instanceof Currency) {
            // вернуть текст ошибки
            return "Не указана валюта.";
        }
        
        // проверить дату
        if (!isset($options['date']) || !$options['date'] instanceof \DateTime) {
            // вернуть текст ошибки
            return "Не указана дата.";
        }
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // склад отправитель склад поставщика
        if ($fromWarehouse->isSupplier() === true) {
            
            // проверить контракт поставщика
            if (!isset($options['contract']) || !$options['contract'] instanceof Contract) {
                // вернуть текст ошибки
                return "Не указан контракт поставщика.";
            }
            
        } else {
            // склад отправитель наш склад или склад ТК
            // получить сток отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stockFrom) {
                // вернуть текст ошибки
                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
            }
        }
        
//        // склад отправитель наш склад или склад ТК
//        if ($fromWarehouse->isSupplier() !== true) {
//            // получить сток отправителя
//            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
//            if (!$stockFrom) {
//                // вернуть текст ошибки
//                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
//            }
//        }
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($options['warehouse']->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockTo) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$options['warehouse'].". ";
        }
        
        // валидация пройдена успешно
        return false;
    }
    
    /**
     * общий валидатор цепочки ...to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validCommonToTransport(OrderEntry $orderEntry, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToArrived($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // если склад отпраивтель не равен складу получателя
        // т.е. оприходование товара на складе заказа, оприходование товара 
        // на том склдае с которого была заказана позиция
        if ($fromWarehouse->getId() != $options['warehouse']->getId()) {
            
            // проверить наличие номера декларации
            if (!isset($options['declarationNumber']) ||
                !isset($options['declarationNumber']['name']) || 
                is_null($options['declarationNumber']['name'])) 
            {
                // вернуть текст ошибки
                return "Не указан номер декларации.";
            }
        }
        
        // валидация пройдена успешно
        return false;
    }
    
    
    
################################################################################
# общие цепочки
################################################################################
    
    /**
     * общий обработчик для цепочек 
     * waiting_to_ordered
     * clarified_to_ordered
     * canceled_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonWaitingAndClarifiedAndCanceledToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // получить склад на котором находится позиция
        $warehouse = $orderEntry->getWarehouse();
        
        // позиция с нашего склада или со склада ТК
        if ($warehouse->isSupplier() !== true)  {
            
            // получить сток
            $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stock) {
                // вернуть текст ошибки
                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$warehouse.". ";
            }
            
            // проверить достаточность количества проукта на складе
            if ($stock->getQuantityFree() < $orderEntry->getQuantity()) {
                // кол-во свободных продуктов меньше чем нужно для заказа 
                return "На складе " . (string) $warehouse . " не достаточно количество товара " . (string) $orderEntry . ".";
            }
            
            // резервируем заказываемое кол-во продукта на складе
            $stock->setReserved($stock->getReserved() + $orderEntry->getQuantity());
        }
        
        // цепочка обработана успешно
        return false;
    }

    /**
     * общий обработчик для цепочек 
     * ordered_to_waiting
     * ordered_to_clarified
     * ordered_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonOrderedToWaitingAndClarifiedAndCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // получить склад на котором находится позиция
        $warehouse = $orderEntry->getWarehouse();
        
        // позиция с нашего склада или со склада ТК
        if ($warehouse->isSupplier() !== true) {
            
            // получить сток
            $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stock) {
                // вернуть текст ошибки
                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$warehouse.". ";
            }
            
            // проверить достаточность резерва проукта на складе
            if ($stock->getReserved() < $orderEntry->getQuantity()) {
                // резерва меньше чем нужно
                return "На складе " . (string) $warehouse . " не достаточно резерва товара " . (string) $orderEntry . ".";
            }
            
            // отменяем резерв кол-во продукта со склада
            $stock->setReserved($stock->getReserved() - $orderEntry->getQuantity());
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * общий обработчик цепочки ...to_sended
     * waiting_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonToSended(OrderEntry $orderEntry, array $options = null)
    {     
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад отправитель 
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // проверить если введен номер декларации
        // создать декларацию по позиции заказа
        if (isset($options['declarationNumber']) &&
            isset($options['declarationNumber']['name']) &&
            !is_null($options['declarationNumber']['name'])
        ) {
            // создать декларации по позиции заказа
            $this->addDeclaration($options['declarationNumber'], $orderEntry, $fromWarehouse, $toWarehouse, $options);
        }
        
        // цепочка обработана успешно
        return false;
    }    

    /**
     * обработчик цепочки 
     * waiting_to_arrived
     * clarified_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonWaitingAndClarifiedToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToArrived($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // склад отправитель наш склад или склад ТК
        if ($fromWarehouse->isSupplier() !== true) {
            
            // получить сток отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stockFrom) {
                // вернуть текст ошибки
                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
            }
            
            // проверить доставточность товара на складе 
            if ($stockFrom->getQuantityFree() < $orderEntry->getQuantity()) {
                // кол-во свободных продуктов меньше чем нужно для заказа 
                return "На складе " . (string) $fromWarehouse. " не достаточно количество товара " . (string) $orderEntry . ".";
            }
            
        }
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        // если склад отпраивтель равен складу получателя
        // оприходование товара на складе с которого заказываем товарную позицию 
        if ($fromWarehouse->getId() == $toWarehouse->getId()) {
        
            // добавляем резерв продукта на нащ склад 
            $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            
        } else {
            
            // создать перемещение позиции 
            // оприходование товарной позиции на складе получателе
            
            // создать позицию перемещения
            $movementEntry = new MovementEntry();
            $movementEntry->setProductId($orderEntry->getProductId());
            $movementEntry->setQuantity($orderEntry->getQuantity());
            $movementEntry->setOrderEntry($orderEntry);
            $movementEntry->setPrice($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $movementEntry->setCurrency($orderEntry->getCurrency());
            $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
            $movementEntry->setStockParams($orderEntry->getStockParams());
            
            // создать перемещение 
            $movement = new Movement();
            
            // склад отправитель склад поставщика
            if ($fromWarehouse->isSupplier() === true) {
                // создать закупку
                $movement->setContract($options['contract']);
            } else {
                // склад отпраитель склад ТК или свой склад
                // создать перемещение
                $movement->setFromWarehouse($fromWarehouse);
            }
            
            // наполнить перемещение данными
            $movement->setToWarehouse($toWarehouse);
            $movement->setDate($options['date']);
            $movement->addMovementEntry($movementEntry);
            
            // валидировать перемещение
            $this->processValidArgs($movementEntry, $movement);
            
            // persist перемещение
            $this->getEntityManager()->persist($movement);
            $this->getEntityManager()->persist($movementEntry);

            // выполнить перемещение
            // склад отправитель наш склад или склад ТК
            if ($fromWarehouse->isSupplier() !== true) {
                // отнимаем перемещаемое кол-во продукта со склада отправителя
                $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
            }

            // выполнить перемещение
            $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
            $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
            $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
            
            // склад получатель наш склад или склад ТК
            if ($toWarehouse->isSupplier() !== true) {
                // добавляем перемещаемое кол-во продукта на склад получателя
                $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
                // добавляем перемещаемый резерв продукта на склад получателя
                $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            }
            
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * waiting_to_transport
     * clarified_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonWaitingAndClarifiedToTransport(OrderEntry $orderEntry, array $options = null)
    {     
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToTransport($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // склад отправитель наш склад или склад ТК
        if ($fromWarehouse->isSupplier() !== true) {
            
            // получить сток отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stockFrom) {
                // вернуть текст ошибки
                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
            }
            
            // проверить доставточность товара на складе 
            if ($stockFrom->getQuantityFree() < $orderEntry->getQuantity()) {
                // кол-во свободных продуктов меньше чем нужно для заказа 
                return "На складе " . (string) $fromWarehouse. " не достаточно количество товара " . (string) $orderEntry . ".";
            }
            
        }
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        // если склад отпраивтель равен складу получателя
        // оприходование товара на складе с которого заказываем товарную позицию 
        if ($fromWarehouse->getId() == $toWarehouse->getId()) {
        
            // добавляем резерв продукта на нащ склад 
            $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            
        } else {
            
            // создать перемещение позиции 
            // оприходование товарной позиции на складе получателе
            
            // создать позицию перемещения
            $movementEntry = new MovementEntry();
            $movementEntry->setProductId($orderEntry->getProductId());
            $movementEntry->setQuantity($orderEntry->getQuantity());
            $movementEntry->setOrderEntry($orderEntry);
            $movementEntry->setPrice($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $movementEntry->setCurrency($orderEntry->getCurrency());
            $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
            $movementEntry->setStockParams($orderEntry->getStockParams());
            
            // создать перемещение 
            $movement = new Movement();
            
            // склад отправитель склад поставщика
            if ($fromWarehouse->isSupplier() === true) {
                // создать закупку
                $movement->setContract($options['contract']);
            } else {
                // склад отпраитель склад ТК или свой склад
                // создать перемещение
                $movement->setFromWarehouse($fromWarehouse);
            }
            
            // наполнить перемещение данными
            $movement->setToWarehouse($toWarehouse);
            $movement->setDate($options['date']);
            $movement->addMovementEntry($movementEntry);
            
            // валидировать перемещение
            $this->processValidArgs($movementEntry, $movement);
                        
            // persist перемещение
            $this->getEntityManager()->persist($movement);
            $this->getEntityManager()->persist($movementEntry);
            
            // проверить если введен номер декларации
            // создать декларацию по позиции заказа
            if (isset($options['declarationNumber']) &&
                isset($options['declarationNumber']['name']) &&
                !is_null($options['declarationNumber']['name'])
            ) {
                // создать декларации по позиции заказа
                $this->addDeclaration($options['declarationNumber'], $orderEntry, $fromWarehouse, $toWarehouse, $options);
            }
            
            // выполнить перемещение
            // склад отправитель наш склад или склад ТК
            if ($fromWarehouse->isSupplier() !== true) {
                // отнимаем перемещаемое кол-во продукта со склада отправителя
                $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
            }
            
            // выполнить перемещение
            $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
            $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
            $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
            
            // склад получатель наш склад или склад ТК
            if ($toWarehouse->isSupplier() !== true) {
                // добавляем перемещаемое кол-во продукта на склад получателя
                $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
                // добавляем перемещаемый резерв продукта на склад получателя
                $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            }
            
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * transport_to_clarified
     * arrived_to_clarified
     * transport_to_waiting
     * arrived_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonTransportAndArrivedToWaitingAndClarified(OrderEntry $orderEntry, array $options = null)
    {     
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // получить сток отправитель
        $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockFrom) {
            // вернуть текст ошибки
            return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $fromWarehouse . ".";
        }
        
        // склад позиции на который позиция возвращается
        $toWarehouse = $orderEntry->getWarehouse();
        
        // склад получатель наш склад или склад ТК
        if ($toWarehouse->isSupplier() !== true) {
            // получить сток получатель
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stockTo) {
                // вернуть текст ошибки
                return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $toWarehouse. ".";
            }
        }
        
        // если склад отпраивтель равен складу получателя
        // т.е. товар был оприходован на складе с которого была заказана товарная позиция
        if ($fromWarehouse->getId() == $toWarehouse->getId()) {
            
            // проверить достаточность резерва на складе
            // кол-во резерва меньше чем нужно
            if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
                // вернуть текст ошибки
                return 'На складе "' . (string) $fromWarehouse . '" не достаточно резерва товара "' . (string) $orderEntry . '".';
            }
            
            // убираем резерв продукта со склада
            $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
            
        } else {
            // товар был оприходован на склад путем создания перемещения
            
            // проверить достаточность количества проукта на складе
            // кол-во продуктов меньше чем нужно
            if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
                // вернуть текст ошибки
                return 'На складе "' . (string) $fromWarehouse. '" не достаточно количество товара "' . (string) $orderEntry . '".';
            }

            // проверить достаточность резерва на складе
            // кол-во резерва меньше чем нужно
            if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
                // вернуть текст ошибки
                return 'На складе "' . (string) $fromWarehouse . '" не достаточно резерва товара "' . (string) $orderEntry . '".';
            }
            
            // убираем кол-во продукта со склада
            $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
            // убираем резерв продукта со склада
            $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
            
            // склад получатель наш склад или склад ТК
            if ($toWarehouse->isSupplier() !== true) {
                // добавляем перемещаемое кол-во продукта на склад получателя
                $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
            }

            // удалить декларации 
            $this->deleteDeclarations($orderEntry);
            // удалить перемещения
            $this->deleteMovements($orderEntry);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * transport_to_ordered
     * arrived_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonTransportAndArrivedToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // получить сток отправителя
        $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockFrom) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
        }
        
        // получить склад с которого была закзана позиция
        $toWarehouse = $orderEntry->getWarehouse();
        
        // склад получатель наш склад или склад ТК
        if ($toWarehouse->isSupplier() !== true) {
            // получить сток получателя
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
            if (!$stockTo) {
                // вернуть текст ошибки
                return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$toWarehouse.". ";
            }
        }
        
        // если склад отпраивтель не равен складу получателя
        // т.е. оприходование товара НЕ на склад с которого заказали позицию заказа
        // было создано перемещение
        if ($fromWarehouse->getId() != $toWarehouse->getId()) {
            
            // склад отправитель наш склад или склад ТК
            
            // проверить доставточность кол-ва на складе 
            if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
                // вернуть текст ошибки
                return "На складе " . (string) $fromWarehouse. " не достаточно количества товара " . (string) $orderEntry . ".";
            }
            
            // проверить доставточность резерва на складе 
            if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
                // вернуть текст ошибки
                return "На складе " . (string) $fromWarehouse. " не достаточно резерва товара " . (string) $orderEntry . ".";
            }
            
            // удалить декларации 
            $this->deleteDeclarations($orderEntry);
            // удалить перемещения
            $this->deleteMovements($orderEntry);

            // склад отправитель наш склад или склад ТК
            // отнимаем перемещаемое кол-во продукта со склада отправителя
            $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
            // отнимаем перемещаемый резерв продукта со склада отправителя
            $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
            
            // склад получатель наш склад или склад ТК
            if ($toWarehouse->isSupplier() !== true) {
                // добавляем перемещаемое кол-во продукта на склад получателя
                $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
                // добавляем перемещаемый резерв продукта на склад получателя
                $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            }
            
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * transport_to_sended
     * arrived_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonTransportAndArrivedToSended(OrderEntry $orderEntry, array $options = null)
    {
        
        // склад позиции
        $warehouse = $orderEntry->getAtWarehouse();
        
        // получить сток отправитель
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stock) {
            // вернуть текст ошибки
            return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $warehouse . ".";
        }
        
        // проверить достаточность количества проукта на складе
        // кол-во продуктов меньше чем нужно
        if ($stock->getQuantity() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return 'На складе "' . (string) $warehouse . '" не достаточно количество товара "' . (string) $orderEntry . '".';
        }
        
        // проверить достаточность резерва на складе
        // кол-во резерва меньше чем нужно
        if ($stock->getReserved() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return 'На складе "' . (string) $warehouse . '" не достаточно резерва товара "' . (string) $orderEntry . '".';
        }
        
        // удалить перемещения
        $this->deleteMovements($orderEntry);
        
        // убираем кол-во продукта со склада
        $stock->setQuantity($stock->getQuantity() - $orderEntry->getQuantity());
        // убираем резерв продукта со склада
        $stock->setReserved($stock->getReserved() - $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * transport_to_returned_to_supplier
     * arrived_to_returned_to_supplier
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonTransportAndArrivedToReturnedToSupplier(OrderEntry $orderEntry, array $options = null)
    {
        
        // проверить массив параметров валидации
        if (!$options) {
            // вернуть текст ошибки
            return "Не указан массив параметров, обязательный для выполенения цепочки.";
        }
        
        // проверить контракт поставщика
        if (!isset($options['contract']) || !$options['contract'] instanceof Contract) {
            // вернуть текст ошибки
            return "Не указан контракт поставщика.";
        }
        
        // проверить дату
        if (!isset($options['date']) || !$options['date'] instanceof \DateTime) {
            // вернуть текст ошибки
            return "Не указана дата.";
        }
        
        // склад отправитель
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // склад позиции на который позиция возвращается
        $toWarehouse = $orderEntry->getWarehouse();
        
        // получить сток отправителя
        $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockFrom) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
        }
        
        // проверить доставточность кол-ва на складе 
        if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return "На складе " . (string) $fromWarehouse. " не достаточно количества товара " . (string) $orderEntry . ".";
        }
        
        // проверить доставточность резерва на складе 
        if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return "На складе " . (string) $fromWarehouse. " не достаточно резерва товара " . (string) $orderEntry . ".";
        }
        
        // создать перемещение позиции 
        // оприходование товарной позиции на складе получателе
        
        // создать позицию перемещения
        $movementEntry = new MovementEntry();
        $movementEntry->setProductId($orderEntry->getProductId());
        $movementEntry->setQuantity($orderEntry->getQuantity());
        $movementEntry->setOrderEntry($orderEntry);
        $movementEntry->setPrice($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
        $movementEntry->setCurrency($orderEntry->getCurrency());
        $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
        $movementEntry->setStockParams($orderEntry->getStockParams());
        
        // создать перемещение 
        $movement = new Movement();
        
        // создать возврат
        $movement->setContract($options['contract']);
        
        // наполнить перемещение данными
        $movement->setFromWarehouse($fromWarehouse);
        $movement->setDate($options['date']);
        $movement->addMovementEntry($movementEntry);
        
        // валидировать перемещение
        $this->processValidArgs($movementEntry, $movement);
        
        // persist перемещение
        $this->getEntityManager()->persist($movement);
        $this->getEntityManager()->persist($movementEntry);
        
        // проверить если введен номер декларации
        // создать декларацию по позиции заказа
        if (isset($options['declarationNumber']) &&
            isset($options['declarationNumber']['name']) &&
            !is_null($options['declarationNumber']['name'])
        ) {
            // создать декларации по позиции заказа
            $this->addDeclaration($options['declarationNumber'], $orderEntry, $fromWarehouse, $toWarehouse, $options);
        }
        
        // выполнить перемещение
        // отнимаем перемещаемое кол-во продукта со склада отправителя
        $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
        // отнимаем перемещаемый резерв продукта со склада отправителя
        $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * transport_to_completed
     * arrived_to_completed
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonTransportAndArrivedToCompleted(OrderEntry $orderEntry, array $options = null)
    {
        
        // склад позиции
        $warehouse = $orderEntry->getAtWarehouse();
        
        // получить сток
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stock) {
            // вернуть текст ошибки
            return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $warehouse . ".";
        }

        // проверить достаточность количества проукта на складе
        // кол-во продуктов меньше чем нужно
        if ($stock->getQuantity() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return 'На складе "' . (string) $warehouse . '" не достаточно количество товара "' . (string) $orderEntry . '".';
        }

        // проверить достаточность резерва на складе
        // кол-во резерва меньше чем нужно
        if ($stock->getReserved() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return 'На складе "' . (string) $warehouse . '" не достаточно резерва товара "' . (string) $orderEntry . '".';
        }
        
        // убираем кол-во продукта со склада
        $stock->setQuantity($stock->getQuantity() - $orderEntry->getQuantity());

        // убираем резерв продукта со склада
        $stock->setReserved($stock->getReserved() - $orderEntry->getQuantity());

        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * transport_to_canceled
     * arrived_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonTransportAndArrivedToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // склад позиции
        $warehouse = $orderEntry->getAtWarehouse();
        
        // получить сток
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stock) {
            // вернуть текст ошибки
            return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $warehouse . ".";
        }

        // проверить достаточность резерва на складе
        // кол-во резерва меньше чем нужно
        if ($stock->getReserved() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return 'На складе "' . (string) $warehouse . '" не достаточно резерва товара  "' . (string) $orderEntry . '".';
        }
        
        // убираем резерв продукта со склада
        // количество не обновляем, продукт остается на складе
        $stock->setReserved($stock->getReserved() - $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * completed_to_transport
     * completed_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonCompletedToTransportAndArrived(OrderEntry $orderEntry, array $options = null)
    {
        // склад позиции
        $warehouse = $orderEntry->getAtWarehouse();
        
        // получить сток
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stock) {
            // вернуть текст ошибки
            return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $warehouse . ".";
        }

        // добавляем кол-во продукта на склад
        $stock->setQuantity($stock->getQuantity() + $orderEntry->getQuantity());

        // доьавляем резерв продукта на  склад
        $stock->setReserved($stock->getReserved() + $orderEntry->getQuantity());

        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * sended_to_transport
     * sended_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonSendedToTransportAndArrived(OrderEntry $orderEntry, array $options = null)
    {
        
        // проверить массив параметров валидации
        if (!$options) {
            // вернуть текст ошибки
            return "Не указан массив параметров, обязательный для выполенения цепочки.";
        }
        
        // проверить склад получатель
        if (!isset($options['warehouse']) || !$options['warehouse'] instanceof Warehouse) {
            // вернуть текст ошибки
            return "Не указан склад получатель.";
        }
        
        // проверить наличие цены
        if (!isset($options['price']) || is_null($options['price'])) {
            // вернуть текст ошибки
            return "Не указана цена входа.";
        }
        
        // проверить склад получатель
        if (!isset($options['currency']) || !$options['currency'] instanceof Currency) {
            // вернуть текст ошибки
            return "Не указана валюта.";
        }
        
        // проверить дату
        if (!isset($options['date']) || !$options['date'] instanceof \DateTime) {
            // вернуть текст ошибки
            return "Не указана дата.";
        }
        
        // проверить контракт поставщика
        if (!isset($options['contract']) || !$options['contract'] instanceof Contract) {
            // вернуть текст ошибки
            return "Не указан контракт поставщика.";
        }
        
//        // получить позицию декларации
//        $declarationEntry = $orderEntry->getLastDeclarationEntry();
//        
//        // проверить существование позиции декларации 
//        if (!$declarationEntry) {
//            return "Для позиции ".(string)$orderEntry." не создана декларация.";
//        }

        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($options['warehouse']->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockTo) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$options['warehouse'].". ";
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        
        // создать перемещение позиции 
        // оприходование товарной позиции на складе получателе
        
        // создать позицию перемещения
        $movementEntry = new MovementEntry();
        $movementEntry->setProductId($orderEntry->getProductId());
        $movementEntry->setQuantity($orderEntry->getQuantity());
        $movementEntry->setOrderEntry($orderEntry);
        $movementEntry->setPrice($orderEntry->getPriceIn() * $orderEntry->getCurrency()->getExchange());
        $movementEntry->setCurrency($orderEntry->getCurrency());
        $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
        $movementEntry->setStockParams($orderEntry->getStockParams());
        
        // создать перемещение 
        $movement = new Movement();
        
        // создать закупку
        $movement->setContract($options['contract']);
        
        // наполнить перемещение данными
        $movement->setToWarehouse($toWarehouse);
        $movement->setDate($options['date']);
        $movement->addMovementEntry($movementEntry);
        
        // валидировать перемещение
        $this->processValidArgs($movementEntry, $movement);
        
        // persist перемещение
        $this->getEntityManager()->persist($movement);
        $this->getEntityManager()->persist($movementEntry);
        
        // выполнить перемещение
        $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
        $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
        $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
        $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
        
        // добавляем перемещаемое кол-во продукта на склад получателя
        $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
        // добавляем перемещаемый резерв продукта на склад получателя
        $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * canceled_to_transport
     * canceled_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonCanceledToTransportAndArrived(OrderEntry $orderEntry, array $options = null)
    {
        
        // склад позиции
        $warehouse = $orderEntry->getAtWarehouse();
        
        // получить сток
        $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($warehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stock) {
            // вернуть текст ошибки
            return "Позиция " . (string) $orderEntry . " не найдена на складе " . (string) $warehouse . ".";
        }

        // проверить достаточность количества проукта на складе
        if ($stock->getQuantityFree() < $orderEntry->getQuantity()) {
            // кол-во свободных продуктов меньше чем нужно для заказа 
            return "На складе " . (string) $warehouse . " не достаточно количество товара " . (string) $orderEntry . ".";
        }
        
        // добавляем резерв продукта на склад
        // количество не обновляем, продукт есть на складе
        $stock->setReserved($stock->getReserved() + $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки 
     * returned_to_supplier_to_transport
     * returned_to_supplier_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    private function processCommonReturnedToSupplierToTransportAndArrived(OrderEntry $orderEntry, array $options = null)
    {
        // получить позицию перемещения
        $movementEntry = $orderEntry->getLastMovementEntry();
        if (!$movementEntry) {
            return "Не найдено перемещение позиции ".(string)$orderEntry.".";
        }
        
        // получить последнее перемещение
        $movement = $movementEntry->getMovement();
        
        // проверить перемещение
        if ($movement->isReturn() !== true) {
            return "Не найден возврат позиции ".(string)$orderEntry.".";
        }
        
        // склад получатель
        $toWarehouse = $movement->getFromWarehouse();
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockTo) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$toWarehouse.". ";
        }
        
        // Добавляем кол-во продукта на склад
        $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());

        // Добавляем резерв продукта на склад
        $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
        
        // удалить перемещение
        $this->getEntityManager()->remove($movement);
        
        // получить позицию декларации 
        $declarationEntry = $orderEntry->getLastDeclarationEntry();
        if ($declarationEntry) {
            // позиция не активна
            $declarationEntry->setIsActive(false);
            // получить декларацию 
            $declaration = $declarationEntry->getDeclaration();
            // удалить декларацию 
            $this->getEntityManager()->remove($declaration);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с будильника 
################################################################################
    
    /**
     * обработчик цепочки waiting_to_clarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToClarified(OrderEntry $orderEntry, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки waiting_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedAndCanceledToOrdered($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки waiting_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки waiting_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToSended(OrderEntry $orderEntry, array $options = null)
    {     
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // выполнить цепочку
        $errorMessage = $this->processCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки waiting_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedToArrived($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки waiting_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainWaitingToTransport(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedToTransport($orderEntry, $options);
    }
    
    
    
################################################################################
# с флажка
################################################################################
    
    /**
     * обработчик цепочки clarified_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainClarifiedToWaiting(OrderEntry $orderEntry, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки clarified_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainClarifiedToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedAndCanceledToOrdered($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки clarified_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainClarifiedToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки clarified_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainClarifiedToSended(OrderEntry $orderEntry, array $options = null)
    {     
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // выполнить цепочку
        $errorMessage = $this->processCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки clarified_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainClarifiedToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedToArrived($orderEntry, $options);
        
    }
    
    /**
     * обработчик цепочки clarified_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainClarifiedToTransport(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedToTransport($orderEntry, $options);
    }
    
    
    
################################################################################
# с корзинки
################################################################################
    
    /**
     * обработчик цепочки ordered_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToWaiting(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonOrderedToWaitingAndClarifiedAndCanceled($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки ordered_to_clarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToClarified(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonOrderedToWaitingAndClarifiedAndCanceled($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки ordered_to_clarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonOrderedToWaitingAndClarifiedAndCanceled($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки ordered_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToSended(OrderEntry $orderEntry, array $options = null)
    {     
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // выполнить цепочку
        $errorMessage = $this->processCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ordered_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToArrived(OrderEntry $orderEntry, array $options = null)
    {
        
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToArrived($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        // если склад отпраивтель не равен складу получателя
        // т.е. оприходование товара НЕ на склад с которого щакащали позицию заказа
        // создаем перемещение
        if ($fromWarehouse->getId() != $toWarehouse->getId()) {
            
            // склад отправитель наш склад или склад ТК
            if ($fromWarehouse->isSupplier() !== true) {
                
                // получить сток отправителя
                $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
                if (!$stockFrom) {
                    // вернуть текст ошибки
                    return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
                }
                
                // проверить доставточность товара на складе 
                if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
                    // вернуть текст ошибки
                    return "На складе " . (string) $fromWarehouse. " не достаточно количество товара " . (string) $orderEntry . ".";
                }

                // проверить доставточность резерва на складе 
                if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
                    // вернуть текст ошибки
                    return "На складе " . (string) $fromWarehouse. " не достаточно резерва товара " . (string) $orderEntry . ".";
                }
                
            }
            
            // создать перемещение позиции 
            // оприходование товарной позиции на складе получателе
            
            // создать позицию перемещения
            $movementEntry = new MovementEntry();
            $movementEntry->setProductId($orderEntry->getProductId());
            $movementEntry->setQuantity($orderEntry->getQuantity());
            $movementEntry->setOrderEntry($orderEntry);
            $movementEntry->setPrice($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $movementEntry->setCurrency($orderEntry->getCurrency());
            $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
            $movementEntry->setStockParams($orderEntry->getStockParams());
            
            // создать перемещение 
            $movement = new Movement();
            
            // склад отправитель склад поставщика
            if ($fromWarehouse->isSupplier() === true) {
                // создать закупку
                $movement->setContract($options['contract']);
            } else {
                // склад отпраитель склад ТК или свой склад
                // создать перемещение
                $movement->setFromWarehouse($fromWarehouse);
            }
            
            // наполнить перемещение данными
            $movement->setToWarehouse($toWarehouse);
            $movement->setDate($options['date']);
            $movement->addMovementEntry($movementEntry);
            
            // валидировать перемещение
            $this->processValidArgs($movementEntry, $movement);
            
            // persist перемещение
            $this->getEntityManager()->persist($movement);   
            $this->getEntityManager()->persist($movementEntry);
            
            // выполнить перемещение
            // склад отправитель наш склад или склад ТК
            if ($fromWarehouse->isSupplier() !== true) {
                // отнимаем перемещаемое кол-во продукта со склада отправителя
                $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
                // отнимаем перемещаемый резерв продукта со склада отправителя
                $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
            }

            // выполнить перемещение
            $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
            $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
            $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
            
            // склад получатель наш склад или склад ТК
            if ($toWarehouse->isSupplier() !== true) {
                // добавляем перемещаемое кол-во продукта на склад получателя
                $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
                // добавляем перемещаемый резерв продукта на склад получателя
                $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            }
            
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки ordered_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainOrderedToTransport(OrderEntry $orderEntry, array $options = null)
    {     
        
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToTransport($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        
        // получить склад на котором находится позиция
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        // если склад отпраивтель не равен складу получателя
        // т.е. оприходование товара НЕ на склад с которого заказали позицию заказа
        // создаем перемещение
        if ($fromWarehouse->getId() != $toWarehouse->getId()) {
            
            // склад отправитель наш склад или склад ТК
            if ($fromWarehouse->isSupplier() !== true) {
                
                // получить сток отправителя
                $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
                if (!$stockFrom) {
                    // вернуть текст ошибки
                    return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
                }
                
                // проверить доставточность товара на складе 
                if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
                    // вернуть текст ошибки
                    return "На складе " . (string) $fromWarehouse. " не достаточно количество товара " . (string) $orderEntry . ".";
                }

                // проверить доставточность резерва на складе 
                if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
                    // вернуть текст ошибки
                    return "На складе " . (string) $fromWarehouse. " не достаточно резерва товара " . (string) $orderEntry . ".";
                }
                
            }
            
            // создать перемещение позиции 
            // оприходование товарной позиции на складе получателе
            
            // создать позицию перемещения
            $movementEntry = new MovementEntry();
            $movementEntry->setProductId($orderEntry->getProductId());
            $movementEntry->setQuantity($orderEntry->getQuantity());
            $movementEntry->setOrderEntry($orderEntry);
            $movementEntry->setPrice($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $movementEntry->setCurrency($orderEntry->getCurrency());
            $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
            $movementEntry->setStockParams($orderEntry->getStockParams());

            // создать перемещение 
            $movement = new Movement();
            
            // склад отправитель склад поставщика
            if ($fromWarehouse->isSupplier() === true) {
                // создать закупку
                $movement->setContract($options['contract']);
            } else {
                // склад отпраитель склад ТК или свой склад
                // создать перемещение
                $movement->setFromWarehouse($fromWarehouse);
            }
            
            // наполнить перемещение данными
            $movement->setToWarehouse($toWarehouse);
            $movement->setDate($options['date']);
            $movement->addMovementEntry($movementEntry);
                        
            // валидировать перемещение
            $this->processValidArgs($movementEntry, $movement);
            
            // persist перемещение
            $this->getEntityManager()->persist($movement);
            $this->getEntityManager()->persist($movementEntry);
            
            // проверить если введен номер декларации
            // создать декларацию по позиции заказа
            if (isset($options['declarationNumber']) &&
                isset($options['declarationNumber']['name']) &&
                !is_null($options['declarationNumber']['name'])
            ) {
                // создать декларации по позиции заказа
                $this->addDeclaration($options['declarationNumber'], $orderEntry, $fromWarehouse, $toWarehouse, $options);
            }
            
            // выполнить перемещение
            // склад отправитель наш склад или склад ТК
            if ($fromWarehouse->isSupplier() !== true) {
                // отнимаем перемещаемое кол-во продукта со склада отправителя
                $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
                // отнимаем перемещаемый резерв продукта со склада отправителя
                $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
            }

            // выполнить перемещение
            $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
            $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
            $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
            $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
            
            // склад получатель наш склад или склад ТК
            if ($toWarehouse->isSupplier() !== true) {
                // добавляем перемещаемое кол-во продукта на склад получателя
                $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
                // добавляем перемещаемый резерв продукта на склад получателя
                $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
            }
            
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    
    
################################################################################
# с отмены
################################################################################
    
    /**
     * обработчик цепочки canceled_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToWaiting(OrderEntry $orderEntry, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки canceled_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonWaitingAndClarifiedAndCanceledToOrdered($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки canceled_to_clarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToClarified(OrderEntry $orderEntry, array $options = null)
    {
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки canceled_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToSended(OrderEntry $orderEntry, array $options = null)
    {     
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // выполнить цепочку
        $errorMessage = $this->processCommonToSended($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки canceled_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonCanceledToTransportAndArrived($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки canceled_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCanceledToTransport(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonCanceledToTransportAndArrived($orderEntry, $options);
    }

    
    
################################################################################
# с машинки
################################################################################
    
    /**
     * обработчик цепочки sended_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainSendedToWaiting(OrderEntry $orderEntry, array $options = null)
    {
        // удалить все декларации для позиции заказа
        $this->deleteDeclarations($orderEntry);
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки sended_to_clarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainSendedToClarified(OrderEntry $orderEntry, array $options = null)
    {
        // удалить все декларации для позиции заказа
        $this->deleteDeclarations($orderEntry);
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки sended_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainSendedToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // удалить все декларации для позиции заказа
        $this->deleteDeclarations($orderEntry);
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки sended_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainSendedToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // получить позицию декларации 
        $declarationEntry = $orderEntry->getLastDeclarationEntry();
        if ($declarationEntry) {
            // позиция не активна
            $declarationEntry->setIsActive(false);
            // отвязать от позиции заказа
            $declarationEntry->setOrderEntry(null);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки sended_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainSendedToTransport(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonSendedToTransportAndArrived($orderEntry, $options);
    }

    /**
     * обработчик цепочки sended_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainSendedToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonSendedToTransportAndArrived($orderEntry, $options);
    }
    
    
    
################################################################################
# с коробка
################################################################################
    
    /**
     * обработчик цепочки arrived_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToWaiting(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToWaitingAndClarified($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки arrived_to_clarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToClarified(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToWaitingAndClarified($orderEntry, $options);
    }
        
    /**
     * обработчик цепочки arrived_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToOrdered($orderEntry, $options);
    }
        
    /**
     * обработчик цепочки arrived_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToSended(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToSended($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки arrived_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToCanceled($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки arrived_to_completed
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToCompleted(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToCompleted($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки arrived_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToTransport(OrderEntry $orderEntry, array $options = null)
    {     
        
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToTransport($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        
        // склад позиции
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // получить сток отправителя
        $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockFrom) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
        }
        
        // склад отправитель наш склад
        // проверить доставточность кол-ва на складе 
        if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return "На складе " . (string) $fromWarehouse. " не достаточно количества товара " . (string) $orderEntry . ".";
        }
        
        // проверить доставточность резерва на складе 
        if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return "На складе " . (string) $fromWarehouse. " не достаточно резерва товара " . (string) $orderEntry . ".";
        }
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        // создать перемещение позиции 
        // оприходование товарной позиции на складе получателе
        
        // создать позицию перемещения
        $movementEntry = new MovementEntry();
        $movementEntry->setProductId($orderEntry->getProductId());
        $movementEntry->setQuantity($orderEntry->getQuantity());
        $movementEntry->setOrderEntry($orderEntry);
        $movementEntry->setPrice($orderEntry->getPriceIn() * $orderEntry->getCurrency()->getExchange());
        $movementEntry->setCurrency($orderEntry->getCurrency());
        $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
        $movementEntry->setStockParams($orderEntry->getStockParams());
        
        // создать перемещение 
        $movement = new Movement();
        
        // создать обычное перемещение со склада на склад
        $movement->setFromWarehouse($fromWarehouse);
        
        // наполнить перемещение данными
        $movement->setToWarehouse($toWarehouse);
        $movement->setDate($options['date']);
        $movement->addMovementEntry($movementEntry);
        
        // валидировать перемещение
        $this->processValidArgs($movementEntry, $movement);
        
        // persist перемещение
        $this->getEntityManager()->persist($movement);
        $this->getEntityManager()->persist($movementEntry);
        
        // проверить если введен номер декларации
        // создать декларацию по позиции заказа
        if (isset($options['declarationNumber']) &&
            isset($options['declarationNumber']['name']) &&
            !is_null($options['declarationNumber']['name'])
        ) {
            // создать декларации по позиции заказа
            $this->addDeclaration($options['declarationNumber'], $orderEntry, $fromWarehouse, $toWarehouse, $options);
        }
        
        // выполнить перемещение
        // склад отправитель наш склад 
        // отнимаем перемещаемое кол-во продукта со склада отправителя
        $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
        // отнимаем перемещаемый резерв продукта со склада отправителя
        $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
        
        // выполнить перемещение
        $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
        $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
        $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
        $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
        
        // склад получатель склад ТК
        // добавляем перемещаемое кол-во продукта на склад получателя
        $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
        // добавляем перемещаемый резерв продукта на склад получателя
        $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки arrived_to_returned_to_supplier
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainArrivedToReturnedToSupplier(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToReturnedToSupplier($orderEntry, $options);
    }
    
    
    
################################################################################
# с галочки
################################################################################
    
    /**
     * обработчик цепочки completed_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCompletedToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonCompletedToTransportAndArrived($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки completed_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainCompletedToTransport(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonCompletedToTransportAndArrived($orderEntry, $options);
    }    
    
    
    
################################################################################
# с самолетика
################################################################################
    
    /**
     * обработчик цепочки transport_to_waiting
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToWaiting(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToWaitingAndClarified($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки transport_to_сlarified
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToClarified(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToWaitingAndClarified($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки transport_to_ordered
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToOrdered(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToOrdered($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки transport_to_sended
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToSended(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToSended($orderEntry, $options);
    }    
    
    /**
     * обработчик цепочки transport_to_canceled
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToCanceled(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToCanceled($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки transport_to_completed
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToCompleted(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToCompleted($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки transport_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToArrived(OrderEntry $orderEntry, array $options = null)
    {     
        
        // валидировать обработчик цепочки
        $errorMessage = $this->validCommonToArrived($orderEntry, $options);
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // склад получатель
        $toWarehouse = $options['warehouse'];
        
        // получить сток получателя
        $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($toWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        
        // склад позиции
        $fromWarehouse = $orderEntry->getAtWarehouse();
        
        // получить сток отправителя
        $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($fromWarehouse->getId(), $orderEntry->getProductId(), $orderEntry->getStockParams());
        if (!$stockFrom) {
            // вернуть текст ошибки
            return "Позиция ".(string) $orderEntry." не найдена на складе ".(string)$fromWarehouse.". ";
        }
        
        // склад отправитель склад ТК
        // проверить доставточность кол-ва на складе 
        if ($stockFrom->getQuantity() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return "На складе " . (string) $fromWarehouse. " не достаточно количества товара " . (string) $orderEntry . ".";
        }
        
        // проверить доставточность резерва на складе 
        if ($stockFrom->getReserved() < $orderEntry->getQuantity()) {
            // вернуть текст ошибки
            return "На складе " . (string) $fromWarehouse. " не достаточно резерва товара " . (string) $orderEntry . ".";
        }
        
        // обновить цену входа для позиции заказа
        $orderEntry->setCurrency($options['currency']);
        $orderEntry->setPriceIn($options['price']);
        
        // создать перемещение позиции 
        // оприходование товарной позиции на складе получателе
        
        // создать позицию перемещения
        $movementEntry = new MovementEntry();
        $movementEntry->setProductId($orderEntry->getProductId());
        $movementEntry->setQuantity($orderEntry->getQuantity());
        $movementEntry->setOrderEntry($orderEntry);
        $movementEntry->setPrice($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
        $movementEntry->setCurrency($orderEntry->getCurrency());
        $movementEntry->setCurrencyPrice($orderEntry->getPriceIn());
        $movementEntry->setStockParams($orderEntry->getStockParams());

        // создать перемещение 
        $movement = new Movement();
        
        // создать обычное перемещение со склада на склад
        $movement->setFromWarehouse($fromWarehouse);
        
        // наполнить перемещение данными
        $movement->setToWarehouse($toWarehouse);
        $movement->setDate($options['date']);
        $movement->addMovementEntry($movementEntry);
        
        // валидировать перемещение
        $this->processValidArgs($movementEntry, $movement);
        
        // persist перемещение
        $this->getEntityManager()->persist($movement);
        $this->getEntityManager()->persist($movementEntry);
        
        // выполнить перемещение
        // склад отправитель склад ТК
        // отнимаем перемещаемое кол-во продукта со склада отправителя
        $stockFrom->setQuantity($stockFrom->getQuantity() - $orderEntry->getQuantity());
        // отнимаем перемещаемый резерв продукта со склада отправителя
        $stockFrom->setReserved($stockFrom->getReserved() - $orderEntry->getQuantity());
        
        // выполнить перемещение
        $stockTo->setCurrency($orderEntry->getCurrency()->getCode());
        $stockTo->setCurrencyPrice($orderEntry->getPriceIn());
        $stockTo->setPriceIn($orderEntry->getPriceIn()*$orderEntry->getCurrency()->getExchange());
        $stockTo->setStorePriceOut($orderEntry->getOrder()->getFilial()->getStoreId(), $orderEntry->getPriceOut());
        
        // склад получатель наш склад
        // добавляем перемещаемое кол-во продукта на склад получателя
        $stockTo->setQuantity($stockTo->getQuantity() + $orderEntry->getQuantity());
        // добавляем перемещаемый резерв продукта на склад получателя
        $stockTo->setReserved($stockTo->getReserved() + $orderEntry->getQuantity());
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * обработчик цепочки transport_to_returned_to_supplier
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainTransportToReturnedToSupplier(OrderEntry $orderEntry, array $options = null)
    {     
        // вызвать общий обработчик цепочки
        return $this->processCommonTransportAndArrivedToReturnedToSupplier($orderEntry, $options);
    }
    
    
    
################################################################################
# с разбитого сердца - со статуса вернули поставщику
################################################################################
    
    /**
     * обработчик цепочки returned_to_supplier_to_arrived
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReturnedToSupplierToArrived(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonReturnedToSupplierToTransportAndArrived($orderEntry, $options);
    }
    
    /**
     * обработчик цепочки returned_to_supplier_to_transport
     * @param OrderEntry $orderEntry - позиция заказа
     * @param array $options - массив дополнительных параметров
     * @return string - текст сообщения об ошибке 
     * @return false  - цепочка обработана успешно
     */
    public function processChainReturnedToSupplierToTransport(OrderEntry $orderEntry, array $options = null)
    {
        // вызвать общий обработчик цепочки
        return $this->processCommonReturnedToSupplierToTransportAndArrived($orderEntry, $options);
    }
    
    /**
     * 
     * функция возвращающая id продуктов с монги
     * 
     */
    public function getAllMongoProductId()
    {
        //берем id продуктов с монги
        $mongoProductIds = $this->dm->createQueryBuilder('NitraMainBundle:Product')
            ->hydrate(false)->select('_id')
            ->getQuery()->execute()->toArray();
        // возвращаем массив id продуктов с монги
        return $mongoProductIds;
    }
   
    
}

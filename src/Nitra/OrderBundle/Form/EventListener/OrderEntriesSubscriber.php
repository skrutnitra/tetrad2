<?php

namespace Nitra\OrderBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;

class OrderEntriesSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SUBMIT => 'postSubmit');
    }

    public function postSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $order = $event->getData();
        
        // проверка на то, нет ли одинаковых позиций в заказе
        $uniqueOrderEntries = array();
        
        foreach($order->getOrderEntries() as $orderEntry) {
            if(!isset($uniqueOrderEntries[$orderEntry->getProductId() . $orderEntry->getStockParams() . $orderEntry->getWarehouse()->getId() . $orderEntry->getPriceIn()])) {
                $uniqueOrderEntries[$orderEntry->getProductId() . $orderEntry->getStockParams() . $orderEntry->getWarehouse()->getId() . $orderEntry->getPriceIn()] = true;
            }
            else {
                $form->addError(new FormError('В заказе присутствуют позиции с одинаковыми параметрами'));
                break;
            }
        }
    }
}

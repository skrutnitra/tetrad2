<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
//use Nitra\OrderBundle\Form\Type\Order\EditType;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntryWarrantyType;

/**
 * форма гарантийный талон к заказу
 */
class OrderWarrantyType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        
        
        
        // виджет позиции заказа
        $builder->add('orderEntries', 'collection', array(
            'type' => new OrderEntryWarrantyType(),
            'prototype' => true,
            'by_reference' => false
        ));        
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'order_warranty';
    }    
    
    /**
     * default_options
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\OrderBundle\Entity\Order'
        ));
    }
    
}


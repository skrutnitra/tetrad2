<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Nitra\MainBundle\Form\Type\CommentType as CommonCommentType;
use Nitra\OrderBundle\Entity\Order;
use Nitra\OrderBundle\Form\DataTransformer\OrderIdToEntityTransformer;


/**
 * форма отчет покупателю по заказу
 */
class OrderReportedType extends CommonCommentType
{
    
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * @var Order
     */
    protected $order;
    
    /**
     * конструктор класса
     */
    public function __construct(EntityManager $entityManager, Order $order)
    {
        // установить Entitymanager
        $this->em = $entityManager;
        
        // установить заказ
        $this->order = $order;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // buildForm родитель
        parent::buildForm($builder, $options);
        
        // установить трансформер для идентификатора
        $builder
            ->get('id')
            ->addModelTransformer(new OrderIdToEntityTransformer($this->em));
        
        // установить boolean флаг обработки статуса
        $builder->add('is_reported', 'hidden', array('required' => false));
        
        // обновить виджет комментарий
        $builder->add('comment', 'textarea', array(
            'required' => false,
            'label' => 'Комментарий', 'translation_domain' => 'Admin',
        ));        
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'order_reported';
    }
    
    
    /**
     * установить значения формы по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // установить заказ
                'id' => $this->order,
                // флаг обработки
                'is_reported' => '0',
            ),
        ));
    }
    
}

<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Admingenerated\NitraOrderBundle\Form\BaseOrderType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Form\Type\OrderEntry\OrderEntryType;
use Nitra\MainBundle\Form\Type\AjaxAutocompleteType;
use Symfony\Component\Validator\Constraints;
use Nitra\OrderBundle\Form\EventListener\OrderEntriesSubscriber;

class EditType extends BaseEditType
{
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Constructor
     * 
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // проверить связь с объектом заказа в базе
        if ($options['data']->getId()) {
            // автокомплит объект полкупателя
            $options['buyer_object'] = $options['data']->getBuyer();

            // автокомплит  имя покупателя
            $options['buyer_name_add_options']['data']['name'] = $options['data']->getBuyer()->getName();
            $options['buyer_name_add_options']['data']['id'] = $options['data']->getBuyer()->getId();

            // автокомплит телефон покупателя
            $options['buyer_phone_add_options']['data']['name'] = $options['data']->getBuyer()->getPhone();
            $options['buyer_phone_add_options']['data']['id'] = $options['data']->getBuyer()->getId();

        }
        
        // родитель создать форму
        parent::buildForm($builder, $options);
        
        // автокомплит поле имя покупателя
        $afterJavaScriptBuyerName = "  
            json_data = eval('('+row.extra[2]+')');           
            $('#edit_order_buyer_phone_name').val( json_data.phone );";
        $builder->add('buyer_name', new AjaxAutocompleteType(), array_merge(array(
            'label' => 'Покупатель',
            'required' => true,
            'mapped' => false,
            'autocompleteActionRouting' => 'Nitra_OrderBundle_Get_Buyer_Autocomplete',
            'afterItemSelectJavascript' => $afterJavaScriptBuyerName,
            ), $options['buyer_name_add_options']
        ));
        
        // автокомплит поле телефон покупателя
        $afterJavaScriptBuyerPhone = "  
            json_data = eval('('+row.extra[2]+')');           
            $('#edit_order_buyer_name_name').val( json_data.name );
            $('#edit_order_buyer_name_id').val( json_data.id );";
        $builder->add('buyer_phone', new AjaxAutocompleteType(), array_merge(array(
            'required' => true,
            'mapped' => false,
            'label' => 'Телефон',
            'autocompleteActionRouting' => 'Nitra_OrderBundle_Get_Buyer_Autocomplete',
            'afterItemSelectJavascript' => $afterJavaScriptBuyerPhone,
            'extraParamsString' => 'search_by: \'phone\'',
            ), $options['buyer_phone_add_options']
        ));
        
        // виджет автоинкремента коментария к заказу
        $builder->add('commentAdd', 'text', array('required' => false, 'mapped' => false, 'label' => 'Добавить комментарий'));
        
        // виджет позиции заказа
        $builder->add('orderEntries', 'collection', array(
            'type' => new OrderEntryType($this->em),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false
        ));
        
        // добавление валидации складу прибытия товара и даты доставки
        $formOptions = $this->getFormOption('warehouse', array(
            'required' => true,  
            'label' => 'Склад', 
            'query_builder' => function($er) {
                return $er->buildQueryOwnAndDeliveryWarehouses();
            },
            'translation_domain' => 'Admin',
            'constraints' => array(
                new Constraints\NotBlank(),
            ),
        ));
        $builder->add('warehouse', 'select_warehouse', $formOptions);
        
        $formOptions = $this->getFormOption('deliveryDate', array(
            'required' => true,  
            'format' => 'd MMM y',  
            'widget' => 'single_text',  
            'label' => 'Доставка',  
            'translation_domain' => 'Admin',
            'constraints' => array(
                new Constraints\NotBlank(),
                new Constraints\Date(),
            ),));
        $builder->add('deliveryDate', 'date', $formOptions);
        
        // добавить листенер для проверки на то, что нет позиций в заказе с одинаковыми параметрами
        $builder->addEventSubscriber(new OrderEntriesSubscriber());

    }
        
    /**
     * установить массив $options по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        // установить $options по умолчанию
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\OrderBundle\Entity\Order',
            // объект полкупателя
            'buyer_object' => null,
            // данные автокомплита имя покупателя
            'buyer_name_add_options' => array('data' => array('name' => null, 'id' => null,)),
            // данные автокомплита телефон покупателя
            'buyer_phone_add_options' => array('data' => array('name' => null,'id' => null,)),
            // данные автокомплита город 
            'city_add_options' => array('data' => array('name' => null,'id' => null, )),
        ));
        
    }
    
}

<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Nitra\IncomeBundle\Form\Type\Income\IncomeManagerType as IncomeManagerType;

class OrderIncomeType extends IncomeManagerType
{
    
    public function getName()
    {
        return 'order_income';
    }
    
}

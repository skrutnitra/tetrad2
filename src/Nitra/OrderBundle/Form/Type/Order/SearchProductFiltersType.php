<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Admingenerated\NitraOrderBundle\Form\BaseOrderType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

class SearchProductFiltersType extends AbstractType
{

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;
    
    /**
     * конструктор класса
     */
    public function __construct(DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // получить массив категорий 
        $categoryChoices = $this->dm->getRepository('NitraMainBundle:Category')->getAllToChoice();
        
        // построить форму
        $builder
                ->add('supplierId', 'entity', array('required' => false, 
                    'class' => 'NitraMainBundle:Supplier',
                    'property' => 'name',                    
                    'label' => 'Поставщик'))
                ->add('categoryId', 'choice', array('required' => false, 'choices' => $categoryChoices, 'label' => 'Категория'))
                ->add('numberPageMain', 'hidden', array('required' => false))
                ->add('numberPageStocks', 'hidden', array('required' => false))
                ->add('numberPageStocksNotBinded', 'hidden', array('required' => false))
                ->add('foundStocksId', 'hidden', array('required' => false))
                ->add('name', 'text', array('required' => false))
                ->add('filialId', 'hidden');
  
    }

    public function getName()
    {
        return 'filters_order';
    }
    
}

<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Symfony\Component\Form\FormBuilderInterface;
use Nitra\OrderBundle\Form\Type\Sms\NewType as SmsType;

class OrderSmsType extends SmsType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        // массив используемых полей
        $useFields = array('text', 'type');
        
        // удалить элементы формы не указанные в массиве $useFields (для того, чтобы не переопределять поле типов сообщений)
        foreach($builder->all() as $children) {
            if (!in_array($children->getName(), $useFields)) {
                $builder->remove($children->getName());
            }
        }
    }

    public function getName()
    {
        return 'new_order_sms';
    }
}

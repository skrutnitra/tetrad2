<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Form\Type\Order\EditType as BaseEditType;

class OrderLogisticsType extends BaseEditType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) {

            // получить запрос свои склады и склады ТК
            $query = $er->buildQueryOwnAndDeliveryWarehouses();

            // вернуть запрос 
            return $query;
        },
        ));
        
        $formOptions = $this->getFormOption('deliveryDate', array('required' => true,  'format' => 'd MMM y',  'widget' => 'single_text',  'label' => 'Доставка',  'translation_domain' => 'Admin',));
        $builder->add('deliveryDate', 'date', $formOptions);
    }
    
    public function getName()
    {
        return 'edit_order_logistics';
    }
}

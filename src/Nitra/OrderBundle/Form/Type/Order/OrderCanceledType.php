<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderCanceledType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // виджет цена
        $builder->add('cancelComment', 'textarea', array(
            'required' => false,
            'label' => 'Причина отмены', 'translation_domain' => 'Admin',
        ));
        
    }

    public function getName()
    {
        return 'order_cancel';
    }
    
}

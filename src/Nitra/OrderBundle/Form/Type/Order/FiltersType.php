<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Admingenerated\NitraOrderBundle\Form\BaseOrderType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
//use Nitra\ManagerBundle\Form\DataTransformer\UsernameToEntityTransformer;

class FiltersType extends BaseFiltersType
{
        
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * конструктор класса
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // buildForm
        parent::buildForm($builder, $options);
        
//        // автокомплит покупатель
//        $formOptions = $this->getFormOption('buyerAutocomplete', array(
//            'route_name' => 'Nitra_OrderBundle_Buyer_Autocomplete',
//            'required' => false, 
//            'label' => 'Имя / телефон покупателя', 'translation_domain' => 'Admin',));
//        $builder->add('buyerAutocomplete', 'genemu_jqueryautocomplete_text', $formOptions);
        
        // виджет имя покупателя автокомплит
        $formOptions = $this->getFormOption('buyerName', array(
            'route_name' => 'Nitra_OrderBundle_Buyer_Name_Autocomplete',
            'class' => 'NitraOrderBundle:Buyer',
            'property' => 'name',
            'required' => false, 
            'label' => 'Имя покупателя', 'translation_domain' => 'Admin',));
        $builder->add('buyerName', 'genemu_jqueryautocompleter_entity', $formOptions);
        
        // виджет телефн покупателя
        $formOptions = $this->getFormOption('buyerPhone', array('required' => false, 'label' => 'Телефон', 'translation_domain' => 'Admin',));
        $builder->add('buyerPhone', 'text', $formOptions);
        
        // виджет оплата
        $choices = array('part' => "Частично", 'full' => "Полностью", 'no' => "Не оплачен", );
        $formOptions = $this->getFormOption('paidStatus', array('choices' => $choices, 'required' => false, 'multiple' => false, 'label' => 'Оплачен', 'translation_domain' => 'Admin',));
        $builder->add('paid', 'choice', $formOptions);
        
        // виджет название товара
        $formOptions = $this->getFormOption('productName', array('required' => false, 'label' => 'Название товара', 'translation_domain' => 'Admin',));
        $builder->add('productName', 'text', $formOptions);
        
        // получить статусы позиций заказа
        $orderEntryStatuses = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findAll();
        $orderEntryStatusesChoices = array();
        foreach($orderEntryStatuses as $entryStatus ) {
            $orderEntryStatusesChoices[$entryStatus->getId()] = (string)$entryStatus;
        }
        // виджет статус позиции заказа
        $formOptions = $this->getFormOption('orderEntryStatus', array(
            'multiple' => false, 
            'required' => false, 
            'choices' => $orderEntryStatusesChoices, 
            'label' => 'Статус позиции', 'translation_domain' => 'Admin',
        ));
        $builder->add('orderEntryStatus', 'choice', $formOptions);        
        
        // виджет дата завершения заказа
        $formOptions = $this->getFormOption('completedAt', array(
            'required' => false, 'format' => 'd MMM y', 
            'label' => 'Дата завершения заказа',  'translation_domain' => 'Admin',));
        $builder->add('completedAt', 'date_range', $formOptions);
        
        // получить склады
        $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findAll();
        $warehousesChoices = array();
        foreach($warehouses as $warehouse) {
            $warehousesChoices[$warehouse->getId()] = (string)$warehouse;
        }
        // виджет склад с которого заказали позицию 
        // определяем виждет через choice в связи с тем что бы меньше внолсить правок 
        // в методы контроллера setFilters и getFilters 
        // в контроллере обновляем только processFilters для данного виджета
        $formOptions = $this->getFormOption('warehouseIds', array(
            'multiple' => true, 
            'required' => false, 
            'choices' => $warehousesChoices, 
            'label' => 'Склад с которого заказали позицию', 'translation_domain' => 'Admin',
        ));
        $builder->add('warehouseIds', 'choice', $formOptions);        
        
    }
    
}

<?php
namespace Nitra\OrderBundle\Form\Type\Order;

use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use Nitra\MainBundle\Form\Type\CommentType as CommonCommentType;
use Nitra\OrderBundle\Form\DataTransformer\OrderIdToEntityTransformer;

/**
 * форма добавления комментаряи к заказу
 */
class OrderCommentType extends CommonCommentType
{
    
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * конструктор класса
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // buildForm родитель
        parent::buildForm($builder, $options);
        
        // установить трансформер для идентификатора
        $builder
            ->get('id')
            ->addModelTransformer(new OrderIdToEntityTransformer($this->em));
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'order_comment';
    }
    
}

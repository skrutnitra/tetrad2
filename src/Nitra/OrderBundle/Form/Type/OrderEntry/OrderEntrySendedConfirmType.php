<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\OrderBundle\Entity\OrderEntryStatus;

class OrderEntrySendedConfirmType extends AbstractType
{
    
    /**
     * @var OrderEntry $orderEntry позиция заказа
     */
    protected $orderEntry;
    
    /**
     * @var OrderEntryStatus $newStatus новый устанавливаемый статус позиции заказа
     */
    protected $newStatus;

    /**
     * конструктор
     * @param OrderEntry $orderEntry - позиция заказа
     * @param OrderEntryStatus $newStatus - новый статус позиции заказа
     */
    public function __construct(OrderEntry $orderEntry, OrderEntryStatus $newStatus)
    {
        $this->orderEntry = $orderEntry;
        $this->newStatus = $newStatus;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // получить склад на котором находится позиция
        $oeAtWarehouse = $this->orderEntry->getAtWarehouse();
        
        // получить новый статус для позиции заказа, передаем в queryBuilder
        $newStatus = $this->newStatus;
        
        // позиция на складе поставщика 
        if ($oeAtWarehouse->isSupplier() === true) {
            // добавляем виджет контракт с поставщиком
            $builder->add('contract', 'entity', array(
                'em' => 'default',
                'class' => 'NitraContractBundle:Contract',
                'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse) {
                // вернуть запрос контракты поставщика
                return $er->buildQuerySupplierContracts($oeAtWarehouse->getSupplier());
            },
                'required' => true,
                'label' => 'Контракт', 'translation_domain' => 'Admin',
            ));
        }
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse, $newStatus) {

            // в зависимости от нововго статуса позиции заказа
            // получаем запрос списка складов
            switch ($newStatus->getMethodName()) {
                
                // оприходовать товар на своем складе
                case 'arrived':
                    // получить запрос свои склады 
                    $query = $er->buildQueryOwnWarehouses();
                    break;
                
                // оприходовать товар на складе ТК
                case 'transport':
                    // получить запрос склады ТК
                    $query = $er->buildQueryDeliveryWarehouses();
                    break;
            }

            // вернуть запрос 
            return $query;
        },
        ));
        
        
        // виджет валюта
        $builder->add('currency', 'entity', array(
            'em' => 'default',
            'class' => 'NitraMainBundle:Currency',
            'required' => true,
            'label' => 'Валюта', 'translation_domain' => 'Admin',
        ));
        
        // дата
        $builder->add('date', 'date', array(
            'required' => true,
            'format' => 'd MMM y',
            'widget' => 'single_text',
            'label' => 'Дата', 'translation_domain' => 'Admin',
        ));
        
    }

    public function getName()
    {
        return 'order_entry_sended_confirm';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        // получить позицию декларации
        $declarationEntry = $this->orderEntry->getLastDeclarationEntry();
        
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // цена 
                'price' => $this->orderEntry->getPriceIn(),
                // валюта 
                'currency' => $this->orderEntry->getCurrency(),
                // Дата
                'date' => new \DateTime(),
                // склад по умолчанию 
                'warehouse' => (($declarationEntry)
                    // если указана декаларация то по умолчанию склад декларации 
                    ? $declarationEntry->getDeclaration()->getToWarehouse()
                    // иначе склад на который оформляется заказ
                    : $this->orderEntry->getOrder()->getWarehouse())
                ,
                
            ),
        ));
    }
    
}

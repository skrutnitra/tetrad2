<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrderEntryWarrantyType extends AbstractType
{


    /**
     * построить форму позиции заказа
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // виджет серийный номер товарной позиции
        $builder->add('serialNumber', 'text', array(
            'required' => true,  
            'constraints' => array(new NotBlank(array('message' => 'Не указан серийний номер.'))),
            'label' => 'Серийний номер', 'translation_domain' => 'Admin',
        ));
        
        // виджет гарантийный период
        $builder->add('warrantyPeriod', 'integer', array(
            'required' => true,  
            'constraints' => array(new NotBlank(array('message' => 'Не указан гарантийный период.'))),
            'label' => 'Гарантия, мес', 'translation_domain' => 'Admin',
        ));
    }
    
    /**
     * получить имя формы
     */
    public function getName()
    {
        return 'order_entry_warranty';
    }
    
    /**
     * default_options
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\OrderBundle\Entity\OrderEntry'
        ));
    }

}

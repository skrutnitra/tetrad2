<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\MainBundle\Form\Type\AjaxAutocompleteType;

class OrderEntrySendedType extends AbstractType
{
    
    /**
     * @var OrderEntry $orderEntry позиция заказа
     */
    protected $orderEntry;

    /**
     * конструктор
     * @param OrderEntry $orderEntry - позиция заказа
     */
    public function __construct(OrderEntry $orderEntry)
    {
        $this->orderEntry = $orderEntry;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // получить склад на котором находится позиция
        $oeAtWarehouse = $this->orderEntry->getAtWarehouse();
        
        // автокомплит номер декларации 
        $afterItemSelectJavascriptDeclarationNumber = "  
            jsonData = eval('('+row.extra[2]+')');
            // проверка если клик по найденой декларации
            if (typeof(jsonData.id) != 'undefined') {
                // убрать все выбранные города
                $('#order_entry_sended_select_city_for_warehouse option').removeAttr('selected');
                // отобразить склады в городе, выбрать склад
                changeCity_order_entry_sended_select_city_for_warehouse(jsonData.toCityId, jsonData.toWarehouseId);
                // установить выбранный город-получатель выбранной декларации
                $('#order_entry_sended_select_city_for_warehouse').val(jsonData.toCityId);
                // изменить выбранное значение для select2
                $('#s2id_order_entry_sended_select_city_for_warehouse span[class=select2-chosen]').text($('#order_entry_sended_select_city_for_warehouse option:selected').text());
                // установить дату декларации
                $('#order_entry_sended_date').val(jsonData.dateString);
            }
            ";
        $builder->add('declarationNumber', new AjaxAutocompleteType(), array_merge(array(
            'required' => false,  
            'label' => 'Номер декларации',
            'autocompleteActionRouting' => 'Nitra_OrderBundle_Get_Declaration_Autocomplete',
            'afterItemSelectJavascript' => $afterItemSelectJavascriptDeclarationNumber,
            'mapped' => false,
        )));
        
        // виджет Дата
        $builder->add('date', 'date', array(
            'required' => true,
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указана дата прибытия.'))),
            'format' => 'd MMM y',
            'widget' => 'single_text',
            'label' => 'Дата', 'translation_domain' => 'Admin',
        ));
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse) {

            // получить запрос свои склады и склады ТК
            $query = $er->buildQueryOwnAndDeliveryWarehouses()
                ->andWhere('q.id != :not_wh_id')
                ->setParameter('not_wh_id', $oeAtWarehouse->getId())
            ;

            // вернуть запрос 
            return $query;
        },
        ));

        // виджет комментарий
        $builder->add('comment', 'text', array(
            'required' => false,  
            'label' => 'Комментарий', 'translation_domain' => 'Admin',
        ));
        
    }

    /**
     * вернуть имя формы
     */
    public function getName()
    {
        return 'order_entry_sended';
    }

    /**
     * установить вернуть имя формы
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // Дата
                'date' => new \DateTime('+1 day'),
                // склад по умолчанию склад на который оформляется заказ
                'warehouse' => $this->orderEntry->getOrder()->getWarehouse(),
            ),
        ));
        
    }
    
}

<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Form\DataTransformer\WarehouseIdToEntityTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class OrderEntryType extends AbstractType
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Constructor
     * 
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * построить форму позиции заказа
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stockParams', 'hidden', array('required' => false))
            ->add('quantity',   'text', array('attr' => array('class' => 'quantity', 'pattern' => '^\d+$')))
            ->add('priceIn',    'text', array('attr' => array('class' => 'price_in', 'pattern' => '\d+((\.)\d+)?'))) // '\d+((,|\.)\d+)?'
            ->add('priceRecommended', 'hidden')
            ->add('priceOut',   'text', array('attr' => array('class' => 'price_out', 'pattern' => '\d+((\.)\d+)?'))) // '\d+((,|\.)\d+)?'
            ->add('productId', 'hidden')
        ;
        
        // поставщик
        $builder->add('supplier', 'entity', array(
            'class' => 'NitraMainBundle:Supplier',
            'required' => false,
            'property' => 'name',
            'mapped' => false,
            'attr' => array('class' => 'order_entry_supplier', /*'style'=>"display:none;"*/)
        ));
        
        // валюта
        $builder->add('currency', 'entity', array(
            'class' => 'NitraMainBundle:Currency', 
            'property' => 'symbol',
        ));
        
        // виджет выбора склада позиции для поставщиков
        $builder->add('select_warehouse', 'entity', array(
            'class' => 'NitraMainBundle:Warehouse', 
            'required' => false,
            'property' => 'address',
            'mapped' => false,
            'attr' => array('class' => 'order_entry_select_warehouse', /*'style'=>"display:none;"*/)
        ));

        // установить трансформер для склада позиции
        $builder->add(
            $builder->create('warehouse', 'hidden', array('required' => true, 'attr' => array('class' => 'order_entry_warehouse')))
                ->addModelTransformer(new WarehouseIdToEntityTransformer($this->em))
        );
    }

    public function getName()
    {
        return 'nitra_orderbundle_orderentrytype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\OrderBundle\Entity\OrderEntry'
        ));
    }

}

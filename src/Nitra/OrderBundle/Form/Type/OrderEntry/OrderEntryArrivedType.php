<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\OrderEntry;

class OrderEntryArrivedType extends AbstractType
{
    
    /**
     * @var OrderEntry $orderEntry позиция заказа
     */
    protected $orderEntry;

    /**
     * конструктор
     * @param OrderEntry $orderEntry - позиция заказа
     */
    public function __construct(OrderEntry $orderEntry)
    {
        $this->orderEntry = $orderEntry;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // получить склад на котором находится позиция
        $oeAtWarehouse = $this->orderEntry->getAtWarehouse();
        
        // позиция на складе поставщика 
        if ($oeAtWarehouse->isSupplier() === true) {
            // добавляем виджет контракт с поставщиком
            $builder->add('contract', 'entity', array(
                'em' => 'default',
                'class' => 'NitraContractBundle:Contract',
                'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse) {
                // вернуть запрос контракты поставщика
                return $er->buildQuerySupplierContracts($oeAtWarehouse->getSupplier());
            },
                'required' => true,
                'label' => 'Контракт', 'translation_domain' => 'Admin',
            ));
        }
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse) {

            // получить запрос свои склады 
            $query = $er->buildQueryOwnWarehouses();

//            // заказ позиции с нашего склада 
//            // можем оприходовать только на тот склда на котором она сейчас
//            if ($oeAtWarehouse->isOwn() === true) {
//                $query
//                    ->andWhere('w.id = :oe_at_wh_id')
//                    ->setParameter('oe_at_wh_id', $oeAtWarehouse->getId());
//            }
            // вернуть запрос 
            return $query;
        },
        ));
        
        // виджет цена
        $builder->add('price', 'text', array(
            'required' => true,
            'label' => 'Цена', 'translation_domain' => 'Admin',
        ));
        
        // виджет валюта
        $builder->add('currency', 'entity', array(
            'em' => 'default',
            'class' => 'NitraMainBundle:Currency',
            'required' => true,
            'label' => 'Валюта', 'translation_domain' => 'Admin',
        ));

        // дата
        $builder->add('date', 'date', array(
            'required' => true,
            'format' => 'd MMM y',
            'widget' => 'single_text',
            'label' => 'Дата', 'translation_domain' => 'Admin',
        ));
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'order_entry_arrived';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        // получить позицию декларации
        $declarationEntry = $this->orderEntry->getLastDeclarationEntry();
        
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // цена
                'price' => $this->orderEntry->getPriceIn(),
                // валюта 
                'currency' => $this->orderEntry->getCurrency(),
                // Дата
                'date' => new \DateTime(),
                // склад по умолчанию 
                'warehouse' => (($declarationEntry)
                    // если указана декаларация то по умолчанию склад декларации 
                    ? $declarationEntry->getDeclaration()->getToWarehouse()
                    // иначе склад на который оформляется заказ
                    : $this->orderEntry->getOrder()->getWarehouse())
                ,
                
            ),
        ));
    }
    
}

<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\MainBundle\Form\Type\AjaxAutocompleteType;

class OrderEntryReturnedToSupplierType extends AbstractType
{
    
    /**
     * @var OrderEntry $orderEntry позиция заказа
     */
    protected $orderEntry;

    /**
     * конструктор
     * @param OrderEntry $orderEntry - позиция заказа
     */
    public function __construct(OrderEntry $orderEntry)
    {
        $this->orderEntry = $orderEntry;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // получить склад c которого была заказана позиция
        $oeWarehouse = $this->orderEntry->getWarehouse();
        
        // позиция заказана со склада поставщика 
        // добавляем виджет контракт с поставщиком
        $builder->add('contract', 'entity', array(
            'em' => 'default',
            'class' => 'NitraContractBundle:Contract',
            'query_builder' => function(EntityRepository $er) use ($oeWarehouse) {
            // вернуть запрос контракты поставщика
            return $er->buildQuerySupplierContracts($oeWarehouse->getSupplier());
        },
            'required' => true,
            'label' => 'Контракт', 'translation_domain' => 'Admin',
        ));
        
        // автокомплит номер декларации 
        $afterItemSelectJavascriptDeclarationNumber = "  
            jsonData = eval('('+row.extra[2]+')');
            // проверка если клик по найденой декларации
            if (typeof(jsonData.id) != 'undefined') {
                // установить дату декларации
                $('#order_entry_returned_to_supplier_date').val(jsonData.dateString);
            }
            ";
        $builder->add('declarationNumber', new AjaxAutocompleteType(), array_merge(array(
            'required' => false,  
            'label' => 'Номер декларации',
            'autocompleteActionRouting' => 'Nitra_OrderBundle_Get_Declaration_Autocomplete',
            'afterItemSelectJavascript' => $afterItemSelectJavascriptDeclarationNumber,
            'mapped' => false,
        )));
        
        // дата 
        $builder->add('date', 'date', array(
            // обязательно для заполнения для movement
            'required' => true,
            'format' => 'd MMM y',
            'widget' => 'single_text',
            'label' => 'Дата', 'translation_domain' => 'Admin',
        ));
        
        // комментарий декларации
        $builder->add('comment', 'text', array(
            'required' => false,  
            'label' => 'Комментарий', 'translation_domain' => 'Admin',
        ));
        
    }

    public function getName()
    {
        return 'order_entry_returned_to_supplier';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // получить закупку
        $income = $this->orderEntry->getIncome();
        
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // Дата
                'date' => new \DateTime(),
                // контракт по умолчанию
                // если была закупка то контракт закупки 
                'contract' => (($income) ? $income->getContract() : null),
            ),
        ));
    }

}

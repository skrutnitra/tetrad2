<?php
namespace Nitra\OrderBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Doctrine\ORM\EntityRepository;
use Nitra\OrderBundle\Entity\OrderEntry;
use Nitra\MainBundle\Form\Type\AjaxAutocompleteType;

class OrderEntryTransportType extends AbstractType
{
    
    /**
     * @var OrderEntry $orderEntry позиция заказа
     */
    protected $orderEntry;

    /**
     * конструктор
     * @param OrderEntry $orderEntry - позиция заказа
     */
    public function __construct(OrderEntry $orderEntry)
    {
        $this->orderEntry = $orderEntry;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // получить склад на котором находится позиция
        $oeAtWarehouse = $this->orderEntry->getAtWarehouse();
        
        // позиция на складе поставщика 
        if ($oeAtWarehouse->isSupplier() === true) {
            // добавляем виджет контракт с поставщиком
            $builder->add('contract', 'entity', array(
                'em' => 'default',
                'class' => 'NitraContractBundle:Contract',
                'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse) {
                // вернуть запрос контракты поставщика
                return $er->buildQuerySupplierContracts($oeAtWarehouse->getSupplier());
            },
                'required' => true,
                'label' => 'Контракт', 'translation_domain' => 'Admin',
            ));
        }
        
        // автокомплит номер декларации 
        $afterItemSelectJavascriptDeclarationNumber = "  
            jsonData = eval('('+row.extra[2]+')');
            // проверка если клик по найденой декларации
            if (typeof(jsonData.id) != 'undefined') {
                // убрать все выбранные города
                $('#order_entry_transport_select_city_for_warehouse option').removeAttr('selected');
                // отобразить склады в городе, выбрать склад
                changeCity_order_entry_transport_select_city_for_warehouse(jsonData.toCityId, jsonData.toWarehouseId);
                // установить выбранный город-получатель выбранной декларации
                $('#order_entry_transport_select_city_for_warehouse').val(jsonData.toCityId);
                // изменить выбранное значение для select2
                $('#s2id_order_entry_transport_select_city_for_warehouse span[class=select2-chosen]').text($('#order_entry_transport_select_city_for_warehouse option:selected').text());
                // установить дату декларации
                $('#order_entry_transport_date').val(jsonData.dateString);
            }
            ";
        $builder->add('declarationNumber', new AjaxAutocompleteType(), array_merge(array(
            'required' => true,  
            'label' => 'Номер декларации',
            'autocompleteActionRouting' => 'Nitra_OrderBundle_Get_Declaration_Autocomplete',
            'afterItemSelectJavascript' => $afterItemSelectJavascriptDeclarationNumber,
            'mapped' => false,
        )));
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) use ($oeAtWarehouse) {

            // получить запрос склады ТК
            $query = $er->buildQueryDeliveryWarehouses()
//                    ->andWhere('q.id != :not_wh_id')
//                    ->setParameter('not_wh_id', $oeAtWarehouse->getId())
            ;

            // вернуть запрос 
            return $query;
        },
        ));

        // виджет цена
        $builder->add('price', 'text', array(
            'required' => true,
            'label' => 'Цена', 'translation_domain' => 'Admin',
        ));
        
        // виджет валюта
        $builder->add('currency', 'entity', array(
            'em' => 'default',
            'class' => 'NitraMainBundle:Currency',
            'required' => true,
            'label' => 'Валюта', 'translation_domain' => 'Admin',
        ));
        
        // дата 
        $builder->add('date', 'date', array(
            'required' => true,
            'format' => 'd MMM y',
            'widget' => 'single_text',
            'label' => 'Дата', 'translation_domain' => 'Admin',
        ));
        
        // комментарий декларации
        $builder->add('comment', 'text', array(
            'required' => false,  
            'label' => 'Комментарий', 'translation_domain' => 'Admin',
        ));
        
    }
    
    /**
     * получить имя формы
     */
    public function getName()
    {
        return 'order_entry_transport';
    }
    
    /**
     * установить значения формы по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        // получить позицию декларации
        $declarationEntry = $this->orderEntry->getLastDeclarationEntry();
        
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // цена
                'price' => $this->orderEntry->getPriceIn(),
                // валюта 
                'currency' => $this->orderEntry->getCurrency(),
                // Дата
                'date' => new \DateTime(),
                // склад по умолчанию 
                'warehouse' => (($declarationEntry)
                    // если указана декаларация то по умолчанию склад декларации 
                    ? $declarationEntry->getDeclaration()->getToWarehouse()
                    // иначе склад на который оформляется заказ
                    : $this->orderEntry->getOrder()->getWarehouse())
                ,
                
            ),
        ));
    }
    
}

<?php

namespace Nitra\OrderBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique;

/**
 * @MongoDB\Document()
 * @Unique(fields="email")
 * @Unique(fields="phone")
 */
class Buyer
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @MongoDB\Int
     */
    private $tetradkaId;

    /**
     * @MongoDB\String
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @MongoDB\String
     * @Assert\Email
     */
    private $email;

    /**
     * @MongoDB\String
     */
    private $phone;

    /**
     * @MongoDB\String
     */
    private $address;

    /**
     * @MongoDB\String
     */
    private $cityName;

    /**
     * @MongoDB\Int
     */
    private $cityId;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get tetradkaId
     * @return tetradkaId $tetradkaId
     */
    public function getTetradkaId()
    {
        return $this->tetradkaId;
    }

    /**
     * Set tetradkaId
     * @param string $tetradkaId
     * @return self
     */
    public function setTetradkaId($tetradkaId)
    {
        $this->tetradkaId = $tetradkaId;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);
        return $this;
    }

    /**
     * Get phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return self
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    /**
     * Get cityName
     *
     * @return string $cityName
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set cityId
     *
     * @param int $cityId
     * @return self
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * Get cityId
     *
     * @return int $cityId
     */
    public function getCityId()
    {
        return $this->cityId;
    }
}
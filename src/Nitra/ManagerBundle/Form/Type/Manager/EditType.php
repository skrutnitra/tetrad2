<?php

namespace Nitra\ManagerBundle\Form\Type\Manager;

use Admingenerated\NitraManagerBundle\Form\BaseManagerType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;

class EditType extends BaseEditType
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // родитель формы
        parent::buildForm($builder, $options);

        $allRoles = $this->em->createQueryBuilder('r')
                ->from('NitraManagerBundle:Role', 'r')
                ->select('r.name, r.description')
                ->orderBy('r.description')
                ->getQuery()
                ->getArrayResult();
        $choices = array();
        foreach ($allRoles as $role) {
            $choices[$role['name']] = $role['description'];
        }

        // роли пользователя
        $formOptions = $this->getFormOption('roles', array(
            'multiple' => true,
            'required' => false,
            'choices' => $choices,
            'label' => 'Роли'));
        $builder->add('roles', 'choice', $formOptions);

        // пароль при редактировании не обязательный
        $formOptions = $this->getFormOption('password', array('required' => false, 'data' => '', 'label' => 'Пароль', 'translation_domain' => 'Admin',));
        $builder->add('password', 'password', $formOptions);
    }

}

<?php
namespace Nitra\ManagerBundle\Form\Type\Manager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;

class MyFilialsType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // массив id филиалов пользователя
        if ($options['session']->has('myFilialsJoined') && $options['session']->get('myFilialsJoined')) {
            $filialIds = $options['session']->get('myFilialsJoined');
        } else {
            // для пользователя не определены филиалы
            $filialIds = array();
        }
        
        // виджет филиал по умолчанию
        $formOptions = $this->getFormOption('myFilialDefault', array(
            'em' => 'default',
            'class' => 'Nitra\\FilialBundle\\Entity\\Filial',  
            'query_builder' => function(EntityRepository $er) use ($filialIds){
                $query = $er->createQueryBuilder('f');
                if ($filialIds) {
                    $query->where('f.id IN(:ids)')->setParameter('ids', $filialIds);
                }
                // вернуть запрос 
                return $query;
                },
            'multiple' => false,
            'required' => true,
            'constraints' => array(new NotBlank(array('message' => 'Не указан филиал по умолчанию.'))),
            'empty_data' => '', 
            'empty_value' => false,
            'property' => 'name',
            'label' => 'Филиал по умолчанию', 'translation_domain' => 'Admin',));
        $builder->add('myFilialDefault', 'entity', $formOptions);
        
        // обновить виджет филиалы пользователя
        $formOptions = $this->getFormOption('myFilialIds', array(
            'em' => 'default',
            'class' => 'Nitra\\FilialBundle\\Entity\\Filial',  
            'query_builder' => function(EntityRepository $er) use ($filialIds) {
                $query = $er->createQueryBuilder('f');
                if ($filialIds) {
                    $query->where('f.id IN(:ids)')->setParameter('ids', $filialIds);
                }
                // вернуть запрос 
                return $query; 
            },
            'multiple' => true, 'expanded' => true,
            'required' => false,
            'property' => 'name',
            'label' => 'Филиалы для отображения', 'translation_domain' => 'Admin',));
        $builder->add('myFilialsDisplay', 'entity', $formOptions);
        
    }
    
    /**
     * getName
     */
    public function getName()
    {
        return 'my_filials';
    }
    
    /**
     * getFormOption
     */
    protected function getFormOption($name, array $formOptions)
    {
        return $formOptions;
    }
    
    /**
     * setDefaultOptions
     * установить значения формы по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        $resolver->setRequired(array(
            'session',
        ));
        
        $resolver->setAllowedTypes(array(
            'session' => 'Symfony\Component\HttpFoundation\Session\Session',
        ));        
        
    }

}


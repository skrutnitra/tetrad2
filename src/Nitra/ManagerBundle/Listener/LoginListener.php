<?php

namespace Nitra\ManagerBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Custom login listener.
 */
class LoginListener
{

    /** @var \Symfony\Component\Security\Core\SecurityContext */
    private $context;

    /**
     * Constructor
     *
     * @param SecurityContext $context
     */
    public function __construct(SecurityContext $context)
    {
        $this->context = $context;
    }

    /**
     * Add user filial id to session.
     *
     * @param Event $event
     */
    public function onSecurityInteractiveLogin(Event $event)
    {
        
        // поулчить объект сессий
        $session = $event->getRequest()->getSession();
        
        //получить филиал пользователя по умолчанию
        $defaultFilial = $this->context->getToken()->getUser()->getDefaultFilial();
        $session->set('myFilialDefault', $defaultFilial->getId());
        $session->set('myFilialsDisplay', array($defaultFilial->getId()));
        
        // получить филиалы пользователя
        $filials = $this->context->getToken()->getUser()->getFilials();
        
        // массив ID филиалов пользователя
        $filialIds = array();
        foreach ($filials as $filial) {
            $filialIds[] = $filial->getId();
        }
        
        // проверить филиалы пользователя
        if ($filialIds) {
            
            // запомнить ACL филиалы пользователя
            $session->set('myFilialsJoined', $filialIds);
            
        } else {
            
            // для пользователя не определен ни один ACL филиал
            $session->remove('myFilialsJoined');
        }
        
    }
}


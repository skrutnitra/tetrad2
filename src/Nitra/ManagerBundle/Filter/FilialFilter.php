<?php
namespace Nitra\ManagerBundle\Filter;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * добавление фильтра по филиалу для пользователей филиала
 */
class FilialFilter extends SQLFilter 
{
    
    /**
     * получить объект авторизации
     * @return Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken 
     * @return null - нет данных 
     */
    public static function getSecurity()
    {
        
        // получить дангные сессии
        // существует если пользователь залогинился
        if (isset($_SESSION["_sf2_attributes"]) &&
            isset($_SESSION["_sf2_attributes"]['_security_main']) 
        ) {
            // вернуть объект авторизации
            return unserialize($_SESSION["_sf2_attributes"]['_security_main']);
        }
        
        // нет данных 
        return null;
    }
    
    /**
     * получить массив ролей пользователя 
     * @return array - массив ролей
     * @return null - нет данных 
     */
    public static function getUserRoles()
    {
        // получить объект авторизации
        $security = self::getSecurity();
        if ($security) {
            // наполнить массив ролей
            $rolesIds = array();
            foreach($security->getRoles() as $role) {
                $rolesIds[] = $role->getRole();
            }
            
            // вернуть массив ролей
            return $rolesIds;
        }
        
        // нет данных 
        return null;
    }
    
    /**
     * возвращает id филиала из сессии
     * запись в сессию происходит в Nitra\ManagerBundle\Listener\LoginListener
     * @return integer ID филиал пользователя по умолчанию
     * @return null пользователь не принадлежит ни одному филиалу или пользователь админ
     */    
    public static function getUserFilialId(){
        
        // получить объект авторизации
        $security = self::getSecurity();
        if ($security &&
            isset($_SESSION['_sf2_attributes']) &&
            isset($_SESSION['_sf2_attributes']['myFilialDefault']) &&
            $_SESSION['_sf2_attributes']['myFilialDefault']
        ) {
            // вернуть ID филиала пользователя
            return $_SESSION['_sf2_attributes']['myFilialDefault'];
        }
        
        // пользователь не принадлежит не одному филиалу
        return null;
    }
    
    
    /**
     * возвращает массив id филиалов установленных для пользователя <br />
     * запись в сессию происходит в Nitra\ManagerBundle\Listener\LoginListener
     * @param boolean $forDisplay флаг получения списка филиалов пользователя <br />
     *                $forDisplay = true  вернуть массив филиалов для отображения <br /> 
     *                $forDisplay = false вернуть массив доступных филиалов <br />
     * @return array массив id филиалов
     * @return null пользователю не установлен ни один филиал
     */    
    public static function getUserFilials($forDisplay=true){
        
        // получить объект авторизации
        $security = self::getSecurity();
        if ($security) {
            
            // проверить флаг получения списка филиалов пользователя
            if ($forDisplay) {
                // массив филиалов для отображения
                $myFilialsKey = 'myFilialsDisplay';
            } else {
                // массив доступных филиалов
                $myFilialsKey = 'myFilialsJoined';
            }
            
            // получиь массив филиалов 
            if (isset($_SESSION['_sf2_attributes']) &&
                isset($_SESSION['_sf2_attributes'][$myFilialsKey]) &&
                $_SESSION['_sf2_attributes'][$myFilialsKey]
            ) {
                // вернуть массив филиалов
                return $_SESSION['_sf2_attributes'][$myFilialsKey];
            }
            
        }
        
        // нет данных 
        return null;
    }
    
    /**
     * возвращает массив id филиалов установленных для пользователя <br />
     * @return array массив id филиалов
     * @return null пользователю не установлен ни один филиал
     */    
    public static function getUserFilialsJoined(){
        return self::getUserFilials(false);
    }
    /**
     * возвращает массив id филиалов пользователя для отображения <br />
     * @return array массив id филиалов
     * @return null пользователю не установлен ни один филиал
     */    
    public static function getUserFilialsDisplay(){
        return self::getUserFilials();
    }
    
    
    /**
     * @param \Doctrine\ORM\Mapping\ClassMetaData $targetEntity - сущность к которой применяется фильтр по филиалу
     * @param string $targetTableAlias алиас названия таблицы
     * @return string
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        //сушности которые не ограничиваются по филиалу
        $ignorEntity = array(
            'Nitra\FilialBundle\Entity\Filial',
        );
        
        // получить массив доступных филиалов
        $filialsJoined = self::getUserFilialsJoined();
        
        //для запросов связаных с Filial
        if (in_array('filial', array_keys($targetEntity->reflFields)) && 
            $filialsJoined && 
            !in_array($targetEntity->rootEntityName, $ignorEntity)
        ) {
            // вернуть фильтр по фииалу
            return $targetTableAlias . '.filial_id IN ('.implode(', ', $filialsJoined).') ';
        }
        
        // для запросов из Filial
        if ($targetEntity->rootEntityName == 'Nitra\FilialBundle\Entity\Filial' &&
            $filialsJoined
        ) {
            // вернуть фильтр по фииалу
            return $targetTableAlias . '.id IN ('.implode(', ', $filialsJoined).') ';
        }
        
        // фильтр по филиалу не используется
        return '';
    }
    
}


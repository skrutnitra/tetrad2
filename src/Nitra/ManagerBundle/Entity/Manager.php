<?php
namespace Nitra\ManagerBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="manager_user")
 * @UniqueEntity(fields="email", message="Почтовый ящик уже используется")
 * @UniqueEntity(fields="username", message="Логин уже используется")
 */
class Manager extends BaseUser
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
   
    /**
     * @var string $firstname
     * @ORM\Column(name="firstname", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $firstname;
        
    /**
     * @var string $lastname
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $lastname;
    
    /**
     * 
     * @ORM\ManyToMany(targetEntity="Nitra\FilialBundle\Entity\Filial")
     */
    private $filials;
    
    /**
     * @ORM\ManyToOne(targetEntity="Nitra\FilialBundle\Entity\Filial")
     * @Assert\Type(type="object", message="The value {{ value }} is not a valid {{ type }}.")
     */
    private $defaultFilial;
    
    /**
     * конструктор
     */
    public function __construct()
    {
        parent::__construct();
        $this->filials = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * object to string
     * @return string
     */
    public function __toString() {
        return (string)$this->firstname . ' ' . (string)$this->lastname;
    }
    
    /**
     * @Assert\True(message = "Не указан филиал")
     */
    public function isDefaultFilialExists()
    {
        // проверить филиал
        if ($this->defaultFilial) {
            return true;
        }
        
        // нет филиала
        return false;
    }
    
    /**
     * Метод возвращает список ролей в текстовом виде
     * @return string 
     */
    public function getRolesText() {
    
        $roles = self::getRoleList();    
        
        $rolesText = '<ul>';
        foreach($this->getRoles() AS $role) {
            $rolesText .= '<li>' . (isset($roles[$role]) ? $roles[$role] : '') . '</li>';
        }
        return $rolesText . '</ul>';
    }
     
    /**
     * @Assert\True(message = "Не правильно указаны роли")
     */
    public function isRolesCorrect()
    {
        
        // массив роли пользователя
        $rolesIds = $this->getRoles();
        
        // массив Id филиалов пользователя
        $filialIds = array();
        
        if ($this->getFilials()) {
            foreach($this->getFilials() as $filial) {
                $filialIds[] = $filial->getId();
            }
        }
        
        // проверить роли
//        if( (array_uintersect($rolesIds, array('ROLE_ADMIN', 'ROLE_USER_MAIN', 'ROLE_ADMIN_MAIN'), "strcasecmp") && $filialIds) ||
//            (array_uintersect($rolesIds, array('ROLE_USER_FILIAL', 'ROLE_ADMIN_FILIAL') , "strcasecmp") && !$filialIds)
//        ) {
//            // ошибка не правильно указаны роли
//            return false;
//        }
        
        // роли указаны правильно
        return true;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Manager
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Manager
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Manager
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    
    /**
     * Add filial
     *
     * @param \Nitra\FilialBundle\Entity\Filial $filial
     * @return Manager
     */
    public function addFilial(\Nitra\FilialBundle\Entity\Filial $filial)
    {
        $this->filials[] = $filial;

        return $this;
    }

    /**
     * Remove filial
     *
     * @param \Nitra\FilialBundle\Entity\Filial $filial
     */
    public function removeFilial(\Nitra\FilialBundle\Entity\Filial $filial)
    {
        $this->filials->removeElement($filial);
    }

    /**
     * Get filials
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFilials()
    {
        return $this->filials;
    }
   

    /**
     * Set defaultFilial
     *
     * @param \Nitra\FilialBundle\Entity\Filial $defaultFilial
     * @return Manager
     */
    public function setDefaultFilial(\Nitra\FilialBundle\Entity\Filial $defaultFilial)
    {
        $this->defaultFilial = $defaultFilial;
    
        return $this;
    }

    /**
     * Get defaultFilial
     *
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getDefaultFilial()
    {
        return $this->defaultFilial;
    }
}
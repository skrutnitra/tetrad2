<?php
namespace Nitra\ManagerBundle\Controller\Manager;

use Admingenerated\NitraManagerBundle\BaseManagerController\NewController as BaseNewController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\ManagerBundle\Form\Type\Manager\NewType;

class NewController extends BaseNewController
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    protected function getNewType()
    {
        $type = new NewType($this->em);
        return $type;
    }

        /**
     * @var string 
     * новый пароль пользователья
     */
    private $newPassword;
    
    /**
     * preSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\ManagerBundle\Entity\Manager $Manager your \Nitra\ManagerBundle\Entity\Manager object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\ManagerBundle\Entity\Manager $Manager)
    {
        $this->newPassword = $Manager->getPassword();
    }
    
    /**
     * postSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\ManagerBundle\Entity\Manager $Manager your \Nitra\ManagerBundle\Entity\Manager object
     */
    public function postSave(\Symfony\Component\Form\Form $form, \Nitra\ManagerBundle\Entity\Manager $Manager)
    {
        // вернуть пароль менеджера
        $Manager->setPassword(null);
        $Manager->setPlainPassword($this->newPassword);
        
        // сохранить менеждера
        $em = $this->getDoctrine()->getManager();
        $em->persist($Manager);
        $em->flush();
    }
    
}

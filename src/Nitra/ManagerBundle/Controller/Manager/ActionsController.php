<?php
namespace Nitra\ManagerBundle\Controller\Manager;

use Admingenerated\NitraManagerBundle\BaseManagerController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Nitra\ManagerBundle\Form\Type\Manager\MyFilialsType;
use Symfony\Component\HttpFoundation\Response;

class ActionsController extends BaseActionsController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
  /**
   * 
   * @return \Symfony\Component\HttpFoundation\Response
   * возвращаем название филиала
   */
    public function getDefaultFilialNameAction() {
        // получить объект сессий 
        $session = $this->get('session');
        if(!$session->get('myFilialDefault')) {
            return new Response('Филиалы');
        }
//        Получаем филиал
        $filial = $this->em->getRepository('NitraFilialBundle:Filial')->findOneById($session->get('myFilialDefault'));
        $flilialName = (string)$filial;
 
        return new Response($flilialName);
    }
    
    /**
     * colorbox форма выбор филиалов пользователя
     * @Route("/my-filials", name="Nitra_ManagerlBundle_Manager_My_Filials")
     * @Template("NitraManagerBundle:ManagerActions:myFilials.html.twig")
     **/
    public function MyFilialsAction(Request $request)
    {
        // получить объект сессий 
        $session = $this->get('session');

        // объект формы
        $form = $this->createForm(new MyFilialsType(), null, array('session' => $session));
        
        // флаг успешной обработки формы
        $formIsValid = false;
        
        // обработка формы
        if ($request->getMethod() == 'POST') {
            
            // заполнить форму 
            $form->bind($request);
            
            // валидация формы
            if ($form->isValid()) {
                
                // получить данные формы
                $formData = $form->getData();
                
                // массив ID филиалов
                $filialIds = array();
                
                // запомнить ID филиала по умолчанию
                $filialIds[] = $formData['myFilialDefault']->getId();
                $session->set('myFilialDefault', $formData['myFilialDefault']->getId());
                
                // филиалы для отображения для пользователя
                if (isset($formData['myFilialsDisplay']) && $formData['myFilialsDisplay']) {
                    // запомнить ID филиалов для отображения
                    foreach($formData['myFilialsDisplay'] as $filial) {
                        $filialIds[] = $filial->getId();
                    }
                }
                
                // проверить установлены ли филиалы пользователя для отображения
                if ($filialIds) {
                    // добавить в сессию филиалы для отображения 
                    $session->set('myFilialsDisplay', $filialIds);
                }
                
                // форма отработала успешно
                $formIsValid = true;
                
            } else {
                // отображение ошибки валидации формы
                $session->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator')); 
            }
        }
        
        // вернуть массив данных передаваемых в шаблон 
        return array(
            'form' => $form->createView(),
            'formIsValid' => $formIsValid,
            'myFilialDefault' => $session->get('myFilialDefault'),
            'myFilialsDisplay' => $session->get('myFilialsDisplay'),
        );
        
    }
    
}

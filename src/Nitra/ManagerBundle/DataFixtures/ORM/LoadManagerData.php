<?php

namespace Nitra\ManagerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\ManagerBundle\Entity\Manager AS Manager;
use Doctrine\Common\DataFixtures\AbstractFixture;

class LoadManagerData extends AbstractFixture implements OrderedFixtureInterface
{  
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager_1 = new Manager();
        $manager_1->setUsername('admin');
        $manager_1->setFirstname('Имя');
        $manager_1->setLastname('Фамилия');
        $manager_1->setPlainPassword('admin');
        $manager_1->setSuperAdmin(true);
        $manager_1->setEnabled(true);
        $manager_1->setEmail('ad@ad.ad');
        $manager_1->setEmail('ad@ad.ad');
        $defaultFilial = $manager->getRepository('NitraFilialBundle:Filial')->findAll();
        $manager_1->setDefaultFilial($defaultFilial[0]);
        $manager->persist($manager_1);
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 6;
    }
}

function viewMoreMovementEntry(listId, ulClass, link, url, row ) {
//    если количество строк больше 3
    if ($(row).length > 3) {
        
        if (link.attr('title') == 'hide') {
            link.text('Показать').attr('title', 'show');
        } else {
            link.text('Скрыть').attr('title', 'hide');
        }
//        скрываем или отображаем строки
        var ul = $(row).slice(3);
        ul.slideToggle();

    } else {
        // пулучаем данные
        $.ajax({
            url: url,
            data: {id: listId},
            method: 'POST',
            success: function(data) {
                // если id небыл передан
                if(data == 'error') {
                    alert('Ошибка данных');
                    link.text('Показать').attr('title', 'show');
                } else {
                    $(ulClass).html(data);
                    link.text('Скрыть').attr('title', 'hide');
                }
            }
        });

    }
}
/**
 * объект работы со стоками
 **/
var stockRecount = {
    
    /**
     * ajax запрос 
     * @param string url    - url ajax запроса
     * @param object params - параметры 
     * @return json  result - результат 
     **/
    ajax: function (actionUrl, params) {
        
        // объект параметров отправки ajax запроса
        if (typeof(params) == 'undefined') {
            params = {};
        }
        
        // результирующие данные отправки запроса
        var result;
        
        // отправить ajax запрос
        $.ajax({
            url: actionUrl,
            type: (typeof(params.type) != 'undefined') ? params.type : 'POST',
            data: (typeof(params.data) != 'undefined') ? params.data : {},
            dataType: (typeof(params.dataType) != 'undefined') ? params.dataType : 'json', // если params.dataType = '' то Intelligent Guess (xml, json, script, or html)
            async: (typeof(params.async) != 'undefined') ? params.async : false,
            success: function(fromServer){
                // ответ от сервера получен успешно
                if (fromServer) {
                    result = fromServer;
                }
            }
        });
        // венруть объект
        return result;
    },
    
    /**
     * объект форма инвентаризации склада 
     **/
    form: {
        
        // флаг попадания скроллинга в самый низ
        activeScroll: false,
        
        // таймер задержка поиска по тестовому значению после начала набора
        searchTimer: false,
                   
        /**
         * добавление продукта
         * из левого списка в правый
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        addProduct: function(_this) {
            
            // идентификатор продукта
            var productId = $(_this).attr('productId');
            
            // имя продукта
            var productName = $(_this).attr('productName');
            
            // имя продукта
            var categoryName = $(_this).attr('categoryName');
            
            // счетсик продуктов
            var counter = (stockRecount.form.getProductsCounter() + 1);
            
            // html строка добавления
            var addTrHtml = $('#list_stock_recount_entrys_table').attr('add-block');

            // Заменяем названия - индексом и устанавливаем название продукта
            addTrHtml = addTrHtml
                .replace(/__iteration__/g, counter)
                .replace(/__name__/g, counter)
                .replace(/__ProductName__/g, productName)
                .replace(/__CategoryName__/g, categoryName)
                .replace(/__productId__/g, productId)
            ;

            // Добавляем позицию в правый список
            $('#list_stock_recount_entrys_table').prepend(addTrHtml);
            $('#edit_stockrecount_stockRecountEntries_' + counter + '_productId').val(productId);
            $('#edit_stockrecount_stockRecountEntries_' + counter + '_delta').val('0');
            $('#edit_stockrecount_stockRecountEntries_' + counter + '_delta').focus();
        },
        
        /**
         * подсчет кол-ва TR продуктов 
         **/
        getProductsCounter: function() {
            
            var counter = 0;
            var iterations = [];
            
            // получить максивальный элемент массива
            iterations.max = function(arr){ 
                return Math.max.apply(Math,arr);
            };
            
            // обойти все TR с продуктами
            $('#list_stock_recount_entrys_table tr').each(function(){
                // проверить установлен ли атрибут iteration для TR
                if (typeof($(this).attr('iteration')) != 'undefined') {
                    // наполнить массив 
                    iterations.push(parseInt($(this).attr('iteration')));
                }
            });
            
            // проверить кол-во елементов в массиве, для выбора максимального значения
            if (iterations.length) {
                // получить максимальный элемент массива
                counter = iterations.max(iterations);
            }
            
            // вернуть счетчик
            return counter;
        },
                
        /**
         * удаление продукта
         * из левого списка
         * @param object _this - javascript this объект на котором произошло событие клик
         * @param integer productId - ID продукта
        **/
       delProduct: function(_this) {
            $(_this).closest('tr').remove();
       }, 
               
        /**
         * javascript фильтр позиций по названию продукта
        */
        filterProducts: function() {

            // строка поиска 
            var searchVal = $('#edit_stockrecount_productFilter').val();
            
            searchVal = $.trim(searchVal);
            
            if (searchVal) {
                
                searchArr = searchVal.split(' ');

                // добавление функции сортировки в jquery из javascript массива (чтобы при выполнении prependTo не терялась сортировка)
                $.fn.reverse = [].reverse;
                
                $('#list_stock_recount_entrys_table').find('tbody > tr').reverse().each(function() {
                    var match = true;
                    
                    for(var i in searchArr) {
                        if(!($(this).attr('filterLine').indexOf(searchArr[i].toLowerCase()) + 1)) {
                            match = false;
                            break;
                        }
                    }
                    
                    if(match) {
                        $(this).removeClass('non-active');
                        $(this).prependTo($('#list_stock_recount_entrys_table tbody'));
                    }
                    else {
                         $(this).addClass('non-active');
                    }
                   
                });
            }
            else {
                $('#list_stock_recount_entrys_table').find('tr').each(function() {
                    $(this).removeClass('non-active');
                });
            }
        },
                
        confirmStockRecountEntryDelete: function(_this) {
            confirm('Вы действительно хотите удалить данный товар со склада?') ? stockRecount.form.delProduct(_this) : false;
        },
    }, 
    
   
    
    /** 
     * перевести объект в стринг
     * @param object - объект переводимый в string
     * @return string - объект преобразованный в строку
     **/
    toString: function(obj) {

        // проверить объект переводимы в стинг
        if (typeof(obj) == 'undefined') {
            console.error("Нет параметра obj для преобразования в строку");
        }

        // перевести в строку 
        var s = '{\n';
        for (var p in obj) {
            s += '    "' + p + '": "' + obj[p] + '"\n';
        }

        // вернуть строку
        return s + '}';
    }
    
    
}

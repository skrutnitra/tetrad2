/**
 * объект перемещений
 **/
var movement = {
    
    /**
     * ajax запрос 
     * @param string url    - url ajax запроса
     * @param object params - параметры 
     * @return json  result - результат 
     **/
    ajax: function (actionUrl, params) {
        
        // объект параметров отправки ajax запроса
        if (typeof(params) == 'undefined') {
            params = {};
        }
        
        // результирующие данные отправки запроса
        var result;
        
        // отправить ajax запрос
        $.ajax({
            url: actionUrl,
            type: (typeof(params.type) != 'undefined') ? params.type : 'POST',
            data: (typeof(params.data) != 'undefined') ? params.data : {},
            dataType: (typeof(params.dataType) != 'undefined') ? params.dataType : 'json', // если params.dataType = '' то Intelligent Guess (xml, json, script, or html)
            async: (typeof(params.async) != 'undefined') ? params.async : false,
            success: function(fromServer){
                // ответ от сервера получен успешно
                if (fromServer) {
                    result = fromServer;
                }
            }
        });
        // венруть объект
        return result;
    },
    
    /**
     * объект форма добавления редактирования 
     * продуктов
     **/
    form: {
        
        // флаг попадания скроллинга в самый низ
        activeScroll: false,
        
        /**
         * поиск продуктов в форме добавления/редактирования заказа
         * @param string - тип поиска 
         **/
        searchProduct: function(type) {
            
            // тип поиска по умолчанию
            if (typeof(type) == 'undefined') {
                var type = 'search';
            }

            // Отображаем loader
            $('#loader').show(); 

            // начать поиск сначала
            if(type == 'search') {
                $('#filters_movement_numberPage').val(0);
                $('#seacrh_product_list').html('');
                movement.form.activeScroll = true;
                // переводим скроллинг в верх 
                // для предотвращения двойного поиска по одному запросу
                $('#seacrh_product_list').scrollTop(0);
                
            } else {
                // продолжить поиск со страницы
                $('#filters_movement_numberPage').val( parseInt($('#filters_movement_numberPage').val()) + 1);
            }

            // получить форму поиска 
            var form = $('#search_product_filter');
            var actionUrl = form.attr('action');

            // перед выполнением поиска устанавливаем флаг попадания скроллинга в самый низ
            movement.form.activeScroll = false;
            
            // отправить ajax запрос поиск продуктов
            var fromServer = movement.ajax(actionUrl, {
                data: form.serialize(),
                dataType: 'html'
            });
            
            // проверить ответ сервера
            if (fromServer) {
                $('#seacrh_product_list').append(fromServer);
                movement.form.activeScroll = true;
            }
            
            // спрятать loader
            $('#loader').hide();
        },
            
        /**
         * добавление продукта
         * из левого списка в правый
         * @param object _this - javascript this объект на котором произошло событие клик
         * @param string movementType тип перемещения, используется для правильного добавления информации со стока в форму movementEntry
         **/
        addProduct: function(_this) {
            // идентификатор склад поставщика или склад ТК в зависимости от deliveryId / supplierId
             // идентификатор продукта
            var productId = $(_this).attr('productId');
            
            // имя продукта
            var productName = $(_this).attr('productName');
            
            // цена в валюте со стока
            var currencyPrice = $(_this).attr('currencyPrice');

            // валюта
            var currency = $(_this).attr('currency');

            // параметр стока
            var stockParams = $(_this).attr('stockParams');
            
            // счетсик продуктов
            var counter = (movement.form.getProductsCounter() + 1);
            
            // html строка добавления
            var addTrHtml = $('#right_list_movement_entries_table').attr('add-block');
            
            if(stockParams) {
                productName += ' (' + stockParams + ')';
            }
            
            // Заменяем названия - индексом и устанавливаем название продукта
            addTrHtml = addTrHtml
                .replace(/__iteration__/g, counter)
                .replace(/__name__/g, counter)
                .replace(/__ProductName__/g, productName)
                .replace(/__Total__/g, currencyPrice)
            ;

            // Добавляем позицию в правый список
            $('#cartList').append(addTrHtml);

            // Установка значения по умолчанию 
            $('#new_movement_movementEntries_' + counter + '_productId').val(productId);
            $('#new_movement_movementEntries_' + counter + '_stockParams').val(stockParams);
            $('#currency_'+counter).append(currency);
            $('#new_movement_movementEntries_' + counter + '_currencyPrice').val(currencyPrice);
            $('#new_movement_movementEntries_' + counter + '_quantity').val('1').change(function(){
                movement.form.recalcTotal(this); // пересчтитать тотал
            }).change();
            $('#new_movement_movementEntries_' + counter + '_currencyPrice').change(function(){
                movement.form.recalcTotal(this); // пересчтитать тотал
            }).change();
                
        },
        
        /**
         * удаление продукта
         * из левого списка
         * @param object _this - javascript this объект на котором произошло событие клик
         * @param integer productId - ID продукта
         **/
        delProduct: function(_this) {
            $(_this).closest('tr').remove();
            movement.form.recalcTotal(_this);
        }, 
        
        /**
         * подсчет кол-ва TR продуктов 
         **/
        getProductsCounter: function() {
            
            var counter = 0;
            var iterations = [];
            
            // получить максивальный элемент массива
            iterations.max = function(arr){ 
                return Math.max.apply(Math,arr);
            };
            
            // обойти все TR с продуктами
            $('#cartList tr').each(function(){
                // проверить установлен ли атрибут iteration для TR
                if (typeof($(this).attr('iteration')) != 'undefined') {
                    // наполнить массив 
                    iterations.push(parseInt($(this).attr('iteration')));
                }
            });
            
            // проверить кол-во елементов в массиве, для выбора максимального значения
            if (iterations.length) {
                // получить максимальный элемент массива
                counter = iterations.max(iterations);
            }
            
            // вернуть счетчик
            return counter;
        },
        
        /**
         * Пересчитать тотал
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        recalcTotal: function(_this) {
            
            var tr = $(_this).closest('tr');
            var quantity = parseInt(tr.find('.quantity').val());
            var price = parseFloat(tr.find('.price').val());

            tr.find('.movement_entry_total').html(quantity * price);
            
            // Обновление итого по заказу
            var total = 0
            $('.movement_entry_total').each(function(){
                total += parseInt($(this).text());
            });
            $('#movement_total').text(total);
        },
                
        /**
         * Удаление позиций с правого листа и обнуление общей суммы при изменении фильтров
        */
        removeFromCartList: function() {
            // удаление всех выбранных позиций перемещения
            $('#cartList').children('tr').empty();
            // обнуление суммы по всем позициям
            $('#movement_total').text(0);
        }
        
    },
    
    /** 
     * перевести объект в стринг
     * @param object - объект переводимый в string
     * @return string - объект преобразованный в строку
     **/
    toString: function(obj) {

        // проверить объект переводимы в стинг
        if (typeof(obj) == 'undefined') {
            console.error("Нет параметра obj для преобразования в строку");
        }

        // перевести в строку 
        var s = '{\n';
        for (var p in obj) {
            s += '    "' + p + '": "' + obj[p] + '"\n';
        }

        // вернуть строку
        return s + '}';
    }
    
    
}

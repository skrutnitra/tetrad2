flashBox = {
    show: function(message, type) {
        $().toastmessage('showToast', {
            text: message,
            sticky: true,
            position: 'bottom-right',
            type: type,
            closeText: ''
        });
    }
}
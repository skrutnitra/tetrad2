/**
 * объект работы со стоками
 **/
var stockWarehouse = {
    
    /**
     * ajax запрос 
     * @param string url    - url ajax запроса
     * @param object params - параметры 
     * @return json  result - результат 
     **/
    ajax: function (actionUrl, params) {
        
        // объект параметров отправки ajax запроса
        if (typeof(params) == 'undefined') {
            params = {};
        }
        
        // результирующие данные отправки запроса
        var result;
        
        // отправить ajax запрос
        $.ajax({
            url: actionUrl,
            type: (typeof(params.type) != 'undefined') ? params.type : 'POST',
            data: (typeof(params.data) != 'undefined') ? params.data : {},
            dataType: (typeof(params.dataType) != 'undefined') ? params.dataType : 'json', // если params.dataType = '' то Intelligent Guess (xml, json, script, or html)
            async: (typeof(params.async) != 'undefined') ? params.async : false,
            success: function(fromServer){
                // ответ от сервера получен успешно
                if (fromServer) {
                    result = fromServer;
                }
            }
        });
        // венруть объект
        return result;
    },
    
//    /**
//     * объект формы inPlaceEdit 
//     * добавление комментария для заказа
//     **/
//    comment: {
//        
//        // объект на котром произошло событие клик редактирования
//        tagObj: false,
//        
//        // объект формы 
//        formObj: false,
//        
//        /**
//         * отображение формы inPlaceEdit добавления коментария
//         * @param string id - ID стока
//         * @param object _this   - javascript this объект на котором произошло событие клик редактирования
//         **/
//        add: function (id, _this) {
//            
//            // проверить открыта ли форма 
//            if (typeof(stockWarehouse.comment.formObj) == 'object') {
//                // форма открыта, закрыть форму
//                stockWarehouse.comment.formObj.children('*[name="cancel"]:first').click();
//            }
//            
//            // установить объект на котором произошло событыие 
//            stockWarehouse.comment.tagObj = $(_this);
//            stockWarehouse.comment.tagObj.hide(); // спрятать ссылку по которой произошол клик 
//            
//            // получить форму
//            stockWarehouse.comment.formObj = $('#stockCommentInPlaceEdit');
//            // установить данные формы
//            $('#stock_warehouse_comment_id').val(id);
//            $('#stock_warehouse_comment_comment').val('').focus();
//            
//            // отображение формы 
//            stockWarehouse.comment.formObj.insertBefore(stockWarehouse.comment.tagObj);
//            stockWarehouse.comment.formObj.show();
//        },
//        
//        /**
//         * Отмена редактирования
//         **/
//        cancel: function () {
//            stockWarehouse.comment.formObj.hide();
//            $('#stock_warehouse_comment_id').val('');
//            $('#stock_warehouse_comment_comment').val('');
//            stockWarehouse.comment.tagObj.show(); // показать ссылку по которой произошол клик 
//        },
//        
//        /**
//         * сохранить форму inPlacedEdit
//         **/
//        send: function () {
//            
//            // url контроллер добавления комментария к заказу 
//            var actionUrl = stockWarehouse.comment.formObj.attr('action');
//            
//            // оправить ajax форму на сервер 
//            var fromServer = stockWarehouse.ajax(actionUrl, {
//                data: stockWarehouse.comment.formObj.serialize()
//            });
//            
//            // проверить тип ответа от сервера
//            if (typeof(fromServer) != 'undefined' && // ответ сервера
//                typeof(fromServer.type) != 'undefined'// тип ответа
//            ) {
//                // обработка в зависимости от типа ответа сервера
//                switch(fromServer.type) {
//                    
//                    // отображение ошибки
//                    case 'error':
//                        common.flash('Невозможно добавить комментарий. '+fromServer.message, fromServer.type);
//                        // прерываем выполнение
//                        return;
//                        break;
//                        
//                    // обновление комментария прошло успешно
//                    case 'success':
//                    default:
//                        common.flash('Комментарий успешно добавлен.','notice');
//                        
//                        // показать комментарий
//                        if (typeof(fromServer.comment) != 'undefined') {
//                            $('#comment_text'+fromServer.id).html(fromServer.comment);
//                        }
//                        break;
//                }
//                
//            }            
//            
//            // обновление отображения формы
//            stockWarehouse.comment.formObj.hide();
//            stockWarehouse.comment.tagObj.show();
//        }
//        
//    },
    
    /** 
     * перевести объект в стринг
     * @param object - объект переводимый в string
     * @return string - объект преобразованный в строку
     **/
    toString: function(obj) {

        // проверить объект переводимы в стинг
        if (typeof(obj) == 'undefined') {
            console.error("Нет параметра obj для преобразования в строку");
        }

        // перевести в строку 
        var s = '{\n';
        for (var p in obj) {
            s += '    "' + p + '": "' + obj[p] + '"\n';
        }

        // вернуть строку
        return s + '}';
    }
    
    
}

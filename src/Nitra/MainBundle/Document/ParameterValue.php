<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\EmbeddedDocument 
 */
class ParameterValue
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * Название параметра
     * @MongoDB\Field(type="string")
     * 
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * Значения параметра
     * @MongoDB\Field(type="hash")
     * 
     * @Assert\NotBlank
     */
    protected $paramValues;

    /**
     * Суффикс параметра
     * @MongoDB\Field(type="string")
     */
    protected $suffix;

    /**
     * Алиас параметра
     * @MongoDB\Field(type="string")
     * 
     * @Assert\NotBlank 
     */
    protected $alias;

    /**
     * 
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank
     */
    protected $paramType;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ParameterValue
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set paramValues
     *
     * @param hash $paramValues
     * @return ParameterValue
     */
    public function setParamValues($paramValues)
    {
        $this->paramValues = $paramValues;
        return $this;
    }

    /**
     * Add paramValues
     *
     * @param string $paramValues
     * @return ParameterValue
     */
    public function addParamValues($paramValues)
    {
        $this->paramValues[] = $paramValues;
        return $this;
    }

    /**
     * Get value
     *
     * @return hash $value
     */
    public function getParamValues()
    {
        return $this->paramValues;
    }

    /**
     * Set suffix
     *
     * @param string $suffix
     * @return ParameterValue
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * Get suffix
     *
     * @return string $suffix
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     * @return ParameterValue
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * Get prefix
     *
     * @return string $prefix
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return ParameterValue
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * Get alias
     *
     * @return string $alias
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set paramType
     *
     * @param string $paramType
     * @return ParameterValue
     */
    public function setParamType($paramType)
    {
        $this->paramType = $paramType;
        return $this;
    }

    /**
     * Get paramType
     *
     * @return string $paramType
     */
    public function getParamType()
    {
        return $this->paramType;
    }


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }
}

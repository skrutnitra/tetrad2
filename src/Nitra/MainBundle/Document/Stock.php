<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Nitra\MainBundle\Common\ApplicationBoot;

/**
 * @MongoDB\Document(repositoryClass="Nitra\MainBundle\Repository\StockRepository")
 */
class Stock implements Model\StockInterface
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * id склада
     * @MongoDB\Field(type="int")
     * @Assert\NotBlank(message="Не указан идентификатор склада")
     */
    private $warehouseId;

    /**
     * mongo id товара
     * @var string $productId
     * @MongoDB\String
     * @Assert\NotBlank(message="Не указан идентификатор продукта")
     * @Assert\Length(max="255")
     */
    private $productId;

    /**
     * параметры стока
     * @var string $stockParams
     * @MongoDB\String
     * @Assert\Length(max="255")
     */
    private $stockParams;

    /**
     * @MongoDB\Hash
     */
    private $storePrice;

    /**
     * название товара из прайса
     * @var string $priceName
     * @MongoDB\String
     * @Assert\NotBlank(message="Не указано название товара из прайса")
     * @Assert\Length(max="255")
     */
    private $priceName;

    /**
     * артикул товара из прайса
     * @var string $priceArticle
     * @MongoDB\String     
     * @Assert\Length(max="255")
     */
    private $priceArticle;
    
    /**
     * в черный список
     * @var Boolean $isBanned
     * @MongoDB\Boolean
     */
    private $isBanned;

    /**
     * дата создания связи
     * @var string $bindedAt
     * @MongoDB\Field(type="date")
     * @Assert\Date
     */
    private $bindedAt;

    /**
     * кем связано username менеджера
     * @var string $bindedBy
     * @MongoDB\String
     */
    private $bindedBy;

    /**
     * кол-во на складе
     * @var integer $quantity
     * @MongoDB\Field(type="int", options={"default" = 0})
     * @Assert\NotBlank(message="Не указано количество товара")
     * @Assert\Range(min = 0)
     */
    private $quantity;

    /**
     * кол-во в резерве
     * @var integer $reserved
     * @MongoDB\Field(type="int", options={"default" = 0})
     * @Assert\NotBlank(message="Не указан резерв товара")
     * @Assert\Range(min = 0)
     */
    private $reserved;

    /**
     * валюта
     * @var string $currency
     * @MongoDB\String
     * @Assert\NotBlank(message="Не указана валюта")
     * @Assert\Length(max="10")
     */
    private $currency;

    /**
     * цена в валюте из excell price
     * @var float $currencyPrice
     * @MongoDB\Field(type="float")
     * @Assert\NotBlank(message="Не указана цена в валюте")
     * @Assert\Range(min = 0)
     */
    private $currencyPrice;

    /**
     * цена входа пересчет из currencyPrice
     * @var float $priceIn
     * @MongoDB\Field(type="float")
     * @Assert\NotBlank
     * @Assert\NotBlank(message="Не указана цена входа")
     * @Assert\Range(min = 0)
     */
    private $priceIn;

    /**
     * @var string $comment
     * @MongoDB\String
     */
    private $comment;

    /**
     * цена1 в прайсе
     * @var float $price1
     * @MongoDB\Field(type="float")
     * @Assert\Range(min = 0)
     */
    private $price1;

    /**
     * цена2 в прайсе
     * @var float $price2
     * @MongoDB\Field(type="float")
     * @Assert\Range(min = 0)
     */
    private $price2;

    /**
     * цена3 в прайсе
     * @var float $price3
     * @MongoDB\Field(type="float")
     * @Assert\Range(min = 0)
     */
    private $price3;

    /**
     * цена4 в прайсе
     * @var float $price4
     * @MongoDB\Field(type="float")
     * @Assert\Range(min = 0)
     */
    private $price4;

    /**
     * В черном списке
     * @MongoDB\Boolean
     */
    private $inBlackList;

    /**
     * конструктор класса
     */
    public function __construct()
    {
        // получить контейнер
        $container = ApplicationBoot::getContainer();
        $this->currency = $container->getParameter('currency_code');
        $this->currencyPrice = 0;
        $this->stockParams = null;
        $this->priceIn = 0;
        $this->quantity = 0;
        $this->reserved = 0;
    }

    /**
     * проверить новый ли сток (существует ли у него цена)
     * @return bool
     */
    public function isNew()
    {
        return ($this->getPriceIn() && $this->getQuantity())
            ? false
            : true;
    }

    /**
     * получить количество свободных товаров
     * @return integer разница ($quantity-$reserved)
     */
    public function getQuantityFree()
    {
        return ($this->quantity - $this->reserved);
    }

    /**
     * Add autoincrement comment in Stock
     * @param string $comment
     * @param string $username
     * @return \Nitra\MainBundle\Document\Stock
     */
    public function addComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);

        if ($comment && $username) {

            $formatter = \IntlDateFormatter::create(
                            \Locale::getDefault(), \IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE
            );

            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setComment($this->getComment() . $commentText);
        }

        return $this;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set warehouseId
     *
     * @param integer $warehouseId
     * @return \Stock
     */
    public function setWarehouseId($warehouseId)
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * Get warehouseId
     * @return integer $warehouseId
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * Set productId
     *
     * @param string $productId
     * @return \Stock
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * Get productId
     *
     * @return string $productId
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set stockParams
     * @param string $stockParams
     * @return self
     */
    public function setStockParams($stockParams)
    {
        $this->stockParams = $stockParams;
        return $this;
    }

    /**
     * Get stockParams
     * @return string $stockParams
     */
    public function getStockParams()
    {
        return $this->stockParams;
    }

    /**
     * Set priceName
     *
     * @param string $priceName
     * @return \Stock
     */
    public function setPriceName($priceName)
    {
        $this->priceName = $priceName;
        return $this;
    }

    /**
     * Get priceName
     *
     * @return string $priceName
     */
    public function getPriceName()
    {
        return $this->priceName;
    }

    /**
     * Set priceArticle
     *
     * @param string $priceArticle
     * @return self
     */
    public function setPriceArticle($priceArticle)
    {
        $this->priceArticle = $priceArticle;
        return $this;
    }

    /**
     * Get priceArticle
     *
     * @return string $priceArticle
     */
    public function getPriceArticle()
    {
        return $this->priceArticle;
    }

    /**
     * Set isBanned
     *
     * @param boolean $isBanned
     * @return \Stock
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;
        return $this;
    }

    /**
     * Get isBanned
     *
     * @return boolean $isBanned
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * Set bindedAt
     *
     * @param date $bindedAt
     * @return \Stock
     */
    public function setBindedAt($bindedAt)
    {
        $this->bindedAt = $bindedAt;
        return $this;
    }

    /**
     * Get bindedAt
     *
     * @return date $bindedAt
     */
    public function getBindedAt()
    {
        return $this->bindedAt;
    }

    /**
     * Set bindedBy
     *
     * @param int $bindedBy
     * @return \Stock
     */
    public function setBindedBy($bindedBy)
    {
        $this->bindedBy = $bindedBy;
        return $this;
    }

    /**
     * Get bindedBy
     *
     * @return int $bindedBy
     */
    public function getBindedBy()
    {
        return $this->bindedBy;
    }

    /**
     * Set quantity
     *
     * @param int $quantity
     * @return \Stock
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Get quantity
     *
     * @return int $quantity
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set reserved
     *
     * @param int $reserved
     * @return \Stock
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;
        return $this;
    }

    /**
     * Get reserved
     *
     * @return int $reserved
     */
    public function getReserved()
    {
        return $this->reserved;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return \Stock
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get currency
     *
     * @return string $currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set currencyPrice
     *
     * @param float $currencyPrice
     * @return \Stock
     */
    public function setCurrencyPrice($currencyPrice)
    {
        $this->currencyPrice = $currencyPrice;
        return $this;
    }

    /**
     * Get currencyPrice
     *
     * @return float $currencyPrice
     */
    public function getCurrencyPrice()
    {
        return $this->currencyPrice;
    }

    /**
     * Set priceIn
     *
     * @param float $priceIn
     * @return \Stock
     */
    public function setPriceIn($priceIn)
    {
        $this->priceIn = $priceIn;
        return $this;
    }

    /**
     * Get priceIn
     *
     * @return float $priceIn
     */
    public function getPriceIn()
    {
        return $this->priceIn;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return \Stock
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Get comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set price1
     *
     * @param float $price1
     * @return \Stock
     */
    public function setPrice1($price1)
    {
        $this->price1 = $price1;
        return $this;
    }

    /**
     * Get price1
     *
     * @return float $price1
     */
    public function getPrice1()
    {
        return $this->price1;
    }

    /**
     * Set price2
     *
     * @param float $price2
     * @return \Stock
     */
    public function setPrice2($price2)
    {
        $this->price2 = $price2;
        return $this;
    }

    /**
     * Get price2
     *
     * @return float $price2
     */
    public function getPrice2()
    {
        return $this->price2;
    }

    /**
     * Set price3
     *
     * @param float $price3
     * @return \Stock
     */
    public function setPrice3($price3)
    {
        $this->price3 = $price3;
        return $this;
    }

    /**
     * Get price3
     *
     * @return float $price3
     */
    public function getPrice3()
    {
        return $this->price3;
    }

    /**
     * Set price4
     *
     * @param float $price4
     * @return \Stock
     */
    public function setPrice4($price4)
    {
        $this->price4 = $price4;
        return $this;
    }

    /**
     * Get price4
     *
     * @return float $price4
     */
    public function getPrice4()
    {
        return $this->price4;
    }

    /**
     * Add storePrice
     *
     * @param  $storePrice
     */
    public function addStorePrice($storePrice)
    {
        $this->storePrice[] = $storePrice;
    }

    /**
     * Set storePrice
     *
     * @param  $storePrice
     */
    public function setStorePrice($storePrice)
    {
        $this->storePrice = $storePrice;
    }

    /**
     * Get storePrice
     *
     * @return  $storePrice
     */
    public function getStorePrice()
    {
        return $this->storePrice;
    }

    /**
     * получить цену выхода для магазина
     * @param indeger $storeId идентификатор магазина
     * @return Float цена для магазина
     * @return 0 цена для магазина не найдена
     */
    public function getStorePriceOut($storeId)
    {

        // проверить существование цены для магазина 
        if (isset($this->storePrice[$storeId]) &&
                isset($this->storePrice[$storeId]['price'])
        ) {
            // вернуть цену для магазина
            return $this->storePrice[$storeId]['price'];
        }

        // цена не найдена вернуть 0
        return 0;
    }

    /**
     * установит цену выхода для магазина
     * @param indeger $storeId идентификатор магазина
     * @param float $price цена
     * @return Stock 
     */
    public function setStorePriceOut($storeId, $price)
    {

        // проверить существование цены для магазина 
        if (!isset($this->storePrice[$storeId])) {
            $this->storePrice[$storeId] = array();
        }

        // установить цену для магазина
        $this->storePrice[$storeId]['price'] = (float) $price;
        // вернуть Stock 
        return $this;
    }

    public function getAvgPriceOut()
    {
        $sum = 0;
        $count = count($this->storePrice);
        foreach ($this->storePrice as $key => $price) {
            $sum+=$price['price'];
        }
        return $sum / $count;
    }

    /**
     * Set inBlackList
     * @param bool $inBlackList
     */
    public function setInBlackList($inBlackList)
    {
        $this->inBlackList = $inBlackList;
    }

    /**
     * Get inBlackList
     * @return bool $inBlackList
     */
    public function getInBlackList()
    {
        return $this->inBlackList;
    }
}
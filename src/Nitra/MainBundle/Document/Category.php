<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="Nitra\MainBundle\Repository\CategoryRepository")
 * @Gedmo\Tree(type="materializedPath", activateLocking=true)
 */
class Category implements \Gedmo\Tree\Node, Model\CategoryInterface
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $integraId;
    
    /**
     * @MongoDB\Field(type="string")
     * @Gedmo\TreePathSource
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    protected $name;

    /**
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    private $image;

    /**
     * @MongoDB\Int
     * 
     */
    private $sortOrder;

    /**
     * @MongoDB\Boolean
     */
    private $isActive;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Category", nullable=true)
     * @Gedmo\TreeParent
     */
    private $parent;

    /**
     * @MongoDB\Field(type="string")
     * @Gedmo\TreePath(separator="|")
     */
    private $path;

    /**
     * @MongoDB\Field(type="int")
     * @Gedmo\TreeLevel
     */
    private $level;

    /**
     * @MongoDB\Field(type="date")
     * @Gedmo\TreeLockTime
     */
    private $lockTime;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Nitra\MainBundle\Document\Store")
     */
    private $stores;

    public function __toString()
    {
        return (String) $this->name;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set integraId
     *
     * @param string $integraId
     * @return \Category
     */
    public function setIntegraId($integraId)
    {
        $this->integraId = $integraId;
        return $this;
    }

    /**
     * Get integraId
     *
     * @return string $integraId
     */
    public function getIntegraId()
    {
        return $this->integraId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Return indent name
     * @return string
     */
    public function getIndentedName() {
        return str_repeat("--", $this->level-1) . $this->name;
    }
    
    /**
     * Set image
     *
     * @param string $image
     * @return \Category
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set sortOrder
     *
     * @param int $sortOrder
     * @return \Category
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isActive
     *
     * @param int $isActive
     * @return \Category
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return int $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set parent
     *
     * @param Nitra\MainBundle\Document\Category $parent
     * @return \Category
     */
    public function setParent(\Nitra\MainBundle\Document\Category $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return Nitra\MainBundle\Document\Category $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Remove parent
     *
     */
    public function removeParent()
    {
        $this->parent = null;
        return $this;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return \Category
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get path
     *
     * @return string $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set level
     *
     * @param int $level
     * @return \Category
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * Get level
     *
     * @return int $level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set lockTime
     *
     * @param date $lockTime
     * @return \Category
     */
    public function setLockTime($lockTime)
    {
        $this->lockTime = $lockTime;
        return $this;
    }

    /**
     * Get lockTime
     *
     * @return date $lockTime
     */
    public function getLockTime()
    {
        return $this->lockTime;
    }

    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add stores
     *
     * @param Nitra\MainBundle\Document\Store $stores
     */
    public function addStores(\Nitra\MainBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     *
     * @param Doctrine\Common\Collections\Collection $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

}

<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Цены для разных магазинов
 * 
 * @MongoDB\EmbeddedDocument
 */
class StorePrice
{

    /**
     * @MongoDB\Float
     * 
     * @Assert\NotBlank
     * @Assert\Range(min = 0, max = 999999.99)
     */
    protected $price;

    /**
     * @MongoDB\Float
     *
     * @Assert\NotBlank
     * @Assert\Range(min = 0, max = 999999.99)
     */
    protected $oldPrice;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Nitra\MainBundle\Document\Store")
     */
    private $store;

    /**
     * Set price
     *
     * @param float $price
     * @return \PriceToStore
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return float $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set oldPrice
     *
     * @param float $oldPrice
     * @return \PriceToStore
     */
    public function setOldPrice($oldPrice)
    {
        $this->oldPrice = $oldPrice;
        return $this;
    }

    /**
     * Get oldPrice
     *
     * @return float $oldPrice
     */
    public function getOldPrice()
    {
        return $this->oldPrice;
    }

    /**
     * Set store
     *
     * @param Nitra\MainBundle\Document\Store $store
     * @return \PriceToStore
     */
    public function setStore(\Nitra\MainBundle\Document\Store $store)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Get store
     *
     * @return Nitra\MainBundle\Document\Store $store
     */
    public function getStore()
    {
        return $this->store;
    }

}

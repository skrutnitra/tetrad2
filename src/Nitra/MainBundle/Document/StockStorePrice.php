<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Цены для разных магазинов
 * 
 * @MongoDB\EmbeddedDocument
 */
class StockStorePrice
{

    /**
     * @MongoDB\Float
     * 
     * @Assert\NotBlank
     * @Assert\Range(min=0, max=999999.99)
     */
    protected $priceOut;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Nitra\MainBundle\Document\Store")
     */
    private $storeId;


    /**
     * Set priceOut
     *
     * @param float $priceOut
     * @return self
     */
    public function setPriceOut($priceOut)
    {
        $this->priceOut = $priceOut;
        return $this;
    }

    /**
     * Get priceOut
     *
     * @return float $priceOut
     */
    public function getPriceOut()
    {
        return $this->priceOut;
    }

    /**
     * Set storeId
     *
     * @param Nitra\MainBundle\Document\Store $storeId
     * @return self
     */
    public function setStoreId(\Nitra\MainBundle\Document\Store $storeId)
    {
        $this->storeId = $storeId;
        return $this;
    }

    /**
     * Get storeId
     *
     * @return Nitra\MainBundle\Document\Store $storeId
     */
    public function getStoreId()
    {
        return $this->storeId;
    }
}

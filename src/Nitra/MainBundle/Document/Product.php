<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Nitra\MainBundle\Common\ApplicationBoot;

/**
 * @MongoDB\Document
 */
class Product implements Model\ProductInterface
{
    
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $integraId;

    /**
     * @MongoDB\String
     * @Assert\NotBlank
     */
    private $name;
    
    /**
     * Полное имя для поиска
     * содержимое настраивается в конфиге, по умолчанию берется имя товара
     * @MongoDB\String
     */
    private $fullNameForSearch;
    
    /**
     * @MongoDB\String
     */
    private $model;

    /**
     * @MongoDB\String
     */
    private $article;

    /**
     * Описание с сайта производителя
     * @MongoDB\String
     */
    private $description;
    
    /**
     * Дополнительное описание
     * @MongoDB\String
     */
    private $description2;
    
    /**
     * Дополнительное описание
     * @MongoDB\String
     */
    private $description3;

    /**
     * Ссылки на видео
     * 
     * @MongoDB\Hash
     */
    private $videoRef;

    /**
     * Ссылки на обзоры
     * 
     * @MongoDB\Hash
     */
    private $reviewRef;

    /**
     * @MongoDB\Hash
     */
    private $storePrice;

    /**
     * @MongoDB\String
     */
    private $image;

    /**
     * @MongoDB\String
     */
    private $image2;

    /**
     * Дополнительные изображения
     * @MongoDB\Field(type="hash")
     */
    private $images;

    /**
     * @MongoDB\String
     */
    private $flash;

    /**
     * @MongoDB\String
     */
    private $flashPreview;

    /**
     * @MongoDB\Boolean
     */
    private $isSpecial;

    /**
     * @MongoDB\Boolean
     */
    private $isActive;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Category")
     * 
     */
    private $category;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Brand")
     * 
     */
    private $brand;
    
    /**
     * Значения параметров продукта
     * 
     * @MongoDB\EmbedMany(targetDocument="ParameterValue")
     * 
     */
    private $parameters;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Nitra\MainBundle\Document\Store")
     */
    private $stores;

    /**
     * Связь с другими товарами (Акссесуарами)
     * @MongoDB\EmbedMany(targetDocument="Product")
     */
    private $accessories;
    
    /**
     * Ширина см.
     * @MongoDB\Field(type="float", nullable=true)
     */
    private $width;
    
    /**
     * Высота см.
     * @MongoDB\Field(type="float", nullable=true)
     */
    private $height;
    
    /**
     * Длина см.
     * @MongoDB\Field(type="float", nullable=true)
     */
    private $length;
    
    /**
     * Вес кг.
     * @MongoDB\Field(type="float", nullable=true)
     */
    private $weight;
    
    /**
     * obj to string 
     * @return string 
     */
    public function __toString() 
    {
        // получить контейнер
        $container = ApplicationBoot::getContainer();   
        // получить массив полей из которые участвуют в образовании
        $config = $container->getParameter('product_to_string');
        // массив слов
        $words = array();
        foreach ($config['fields'] as $field) {
            $words[] = eval('return $this->' . $field . ';');
        }
        
        // вернуть значение
        return (string)implode(' ', $words);
    }
    
    /**
     * Конструктор
     */
    public function __construct() {
        $this->accessories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->parameters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set integraId
     *
     * @param string $integraId
     * @return \Product
     */
    public function setIntegraId($integraId)
    {
        $this->integraId = $integraId;
        return $this;
    }

    /**
     * Get integraId
     *
     * @return string $integraId
     */
    public function getIntegraId()
    {
        return $this->integraId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set fullNameForSearch
     * 
     * @param string $fullNameForSearch
     * @return self
     */
    public function setFullNameForSearch($fullNameForSearch)
    {
        $this->fullNameForSearch = $fullNameForSearch;
        return $this;
    }
    
    /**
     * Get fullNameForSearch
     * @return string
     */
    public function getFullNameForSearch()
    {
        return $this->fullNameForSearch;
    }
    
    /**
     * Set model
     *
     * @param string $model
     * @return \Product
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Get model
     *
     * @return string $model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return \Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set videoRef
     *
     * @param hash $videoRef
     * @return \Product
     */
    public function setVideoRef($videoRef)
    {
        $this->videoRef = $videoRef;
        return $this;
    }

    /**
     * Get videoRef
     *
     * @return hash $videoRef
     */
    public function getVideoRef() 
    {
        return $this->videoRef;
    }

    /**
     * Set reviewRef
     *
     * @param hash $reviewRef
     * @return \Product
     */
    public function setReviewRef($reviewRef) 
    {
        $this->reviewRef = $reviewRef;
        return $this;
    }

    /**
     * Get reviewRef
     *
     * @return hash $reviewRef
     */
    public function getReviewRef() 
    {
        return $this->reviewRef;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return \Product
     */
    public function setImage($image) 
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage() 
    {
        return $this->image;
    }

    /**
     * Set images
     *
     * @param hash $images
     * @return \Product
     */
    public function setImages($images) 
    {
        $this->images = $images;
        return $this;
    }

    /**
     * Get images
     *
     * @return hash $images
     */
    public function getImages() 
    {
        return $this->images;
    }

    /**
     * Set flash
     *
     * @param string $flash
     * @return \Product
     */
    public function setFlash($flash) 
    {
        $this->flash = $flash;
        return $this;
    }

    /**
     * Get flash
     *
     * @return string $flash
     */
    public function getFlash() 
    {
        return $this->flash;
    }

    /**
     * Set isSpecial
     *
     * @param int $isSpecial
     * @return \Product
     */
    public function setIsSpecial($isSpecial) 
    {
        $this->isSpecial = $isSpecial;
        return $this;
    }

    /**
     * Get isSpecial
     *
     * @return int $isSpecial
     */
    public function getIsSpecial() 
    {
        return $this->isSpecial;
    }

    /**
     * Set isActive
     *
     * @param int $isActive
     * @return \Product
     */
    public function setIsActive($isActive) 
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return int $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set category
     *
     * @param Nitra\MainBundle\Document\Category $category
     * @return \Product
     */
    public function setCategory(\Nitra\MainBundle\Document\Category $category) 
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     *
     * @return Nitra\MainBundle\Document\Category $category
     */
    public function getCategory() 
    {
        return $this->category;
    }

    /**
     * Set brand
     *
     * @param Nitra\MainBundle\Document\Brand $brand
     * @return \Product
     */
    public function setBrand(\Nitra\MainBundle\Document\Brand $brand) 
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     *
     * @return Nitra\MainBundle\Document\Brand $brand
     */
    public function getBrand() 
    {
        return $this->brand;
    }

    /**
     * Add parameters
     *
     * @param Nitra\MainBundle\Document\ParameterValue $parameters
     */
    public function addParameters(\Nitra\MainBundle\Document\ParameterValue $parameters) 
    {
        $this->parameters[] = $parameters;
    }

    /**
     * Set parameters
     *
     * @param Doctrine\Common\Collections\Collection $parameters
     */
    public function setParameters($parameters) 
    {
        $this->parameters = $parameters;
    }

    /**
     * Get parameters
     *
     * @return Doctrine\Common\Collections\Collection $parameters
     */
    public function getParameters() 
    {
        return $this->parameters;
    }

    /**
     * Get parameters_alias
     *
     * @return array $alias
     */
    public function getParametersAlias() 
    {
        $alias = array();
        foreach ($this->parameters as $parameter) {
            $alias[] = $parameter->getAlias();
        }
        return $alias;
    }

    /**
     * Set image2
     *
     * @param string $image2
     * @return \Product
     */
    public function setImage2($image2) 
    {
        $this->image2 = $image2;
        return $this;
    }

    /**
     * Get image2
     *
     * @return string $image2
     */
    public function getImage2() 
    {
        return $this->image2;
    }

    /**
     * Set flashPreview
     *
     * @param string $flashPreview
     * @return \Product
     */
    public function setFlashPreview($flashPreview) 
    {
        $this->flashPreview = $flashPreview;
        return $this;
    }

    /**
     * Get flashPreview
     *
     * @return string $flashPreview
     */
    public function getFlashPreview() 
    {
        return $this->flashPreview;
    }

    /**
     * Add stores
     *
     * @param Nitra\MainBundle\Document\Store $stores
     */
    public function addStores(\Nitra\MainBundle\Document\Store $stores) 
    {
        $this->stores[] = $stores;
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores() 
    {
        return $this->stores;
    }

    /**
     * Set stores
     *
     * @param Doctrine\Common\Collections\Collection $stores
     */
    public function setStores($stores) 
    {
        $this->stores = $stores;
    }

    /**
     * Add storePrice
     *
     * @param  $storePrice
     */
    public function addStorePrice($storePrice) {
        $this->storePrice[] = $storePrice;
    }

    /**
     * Set storePrice
     *
     * @param  $storePrice
     */
    public function setStorePrice($storePrice) {
        $this->storePrice = $storePrice;
    }

    /**
     * Get storePrice
     *
     * @return  $storePrice
     */
    public function getStorePrice() {
        return $this->storePrice;
    }

    /**
     * Set article
     *
     * @param string $article
     * @return \Product
     */
    public function setArticle($article) 
    {
        $this->article = $article;
        return $this;
    }

    /**
     * Get article
     *
     * @return string $article
     */
    public function getArticle() 
    {
        return $this->article;
    }

    /**
     * Add accessories
     *
     * @param Nitra\MainBundle\Document\Product $accessories
     */
    public function addAccessorie(\Nitra\MainBundle\Document\Product $accessories) 
    {
        $this->accessories[] = $accessories;
    }

    /**
     * Remove accessories
     *
     * @param <variableType$accessories
     */
    public function removeAccessorie(\Nitra\MainBundle\Document\Product $accessories) 
    {
        $this->accessories->removeElement($accessories);
    }

    /**
     * Get accessories
     *
     * @return Doctrine\Common\Collections\Collection $accessories
     */
    public function getAccessories() 
    {
        return $this->accessories;
    }

    /**
     * Set description2
     *
     * @param string $description2
     * @return self
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;
        return $this;
    }

    /**
     * Get description2
     *
     * @return string $description2
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * Set description3
     *
     * @param string $description3
     * @return self
     */
    public function setDescription3($description3)
    {
        $this->description3 = $description3;
        return $this;
    }

    /**
     * Get description3
     *
     * @return string $description3
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * Add parameters
     *
     * @param Nitra\MainBundle\Document\ParameterValue $parameters
     */
    public function addParameter(\Nitra\MainBundle\Document\ParameterValue $parameters)
    {
        $this->parameters[] = $parameters;
    }

    /**
     * Remove parameters
     *
     * @param Nitra\MainBundle\Document\ParameterValue $parameters
     */
    public function removeParameter(\Nitra\MainBundle\Document\ParameterValue $parameters)
    {
        $this->parameters->removeElement($parameters);
    }

    /**
     * Add stores
     *
     * @param Nitra\MainBundle\Document\Store $stores
     */
    public function addStore(\Nitra\MainBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
    }

    /**
     * Remove stores
     *
     * @param Nitra\MainBundle\Document\Store $stores
     */
    public function removeStore(\Nitra\MainBundle\Document\Store $stores)
    {
        $this->stores->removeElement($stores);
    }
    
    
    /**
     * Set $width
     * @param hash $width
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * Get $width
     * @return hash $width
     */
    public function getWidth()
    {
        return $this->width;
    }
    
    
    /**
     * Set $height
     * @param hash $height
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * Get $height
     * @return hash $height
     */
    public function getHeight()
    {
        return $this->height;
    }
    
    
    /**
     * Set $length
     * @param hash $length
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * Get $length
     * @return hash $length
     */
    public function getLength()
    {
        return $this->length;
    }
    
    
    /**
     * Set $weight
     * @param hash $weight
     * @return self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * Get $weight
     * @return hash $weight
     */
    public function getWeight()
    {
        return $this->weight;
    }
    
}

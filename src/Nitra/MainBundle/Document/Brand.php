<?php

namespace Nitra\MainBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class Brand implements Model\BrandInterface
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $integraId;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    protected $name;

    /**
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    private $image;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Nitra\MainBundle\Document\Store")
     */
    private $stores;
    
    public function __toString()
    {
        return (String) $this->name;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set integraId
     *
     * @param string $integraId
     * @return \Brand
     */
    public function setIntegraId($integraId)
    {
        $this->integraId = $integraId;
        return $this;
    }

    /**
     * Get integraId
     *
     * @return string $integraId
     */
    public function getIntegraId()
    {
        return $this->integraId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Brand
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return \Brand
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add stores
     *
     * @param Nitra\MainBundle\Document\Store $stores
     */
    public function addStores(\Nitra\MainBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     *
     * @param Doctrine\Common\Collections\Collection $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

}

<?php
namespace Nitra\MainBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\Container;
use Nitra\MainBundle\Lib\NitraGlobals;

/**
 * @Service
 */
class KernelRequestListener 
{
    
    /**
     * Конструктор
     */
    function __construct(Container $container)
    {
        // установить контейнер 
        NitraGlobals::$container = $container;
        
        // установить EntityManager
        NitraGlobals::$em = $container->get('doctrine')->getManager();
        
        // установить DocumentManager
        NitraGlobals::$dm = $container->get('doctrine_mongodb')->getManager();
    }
    
    /**
     * @Observe("kernel.request")
     */
    public function onKernelRequest(GetResponseEvent $event) 
    {
        
    }

    
}
<?php
namespace Nitra\MainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nitra\MainBundle\Common\ApplicationBoot;

/**
 * CurrencyRepository
 */
class CurrencyRepository extends EntityRepository
{
    
    /**
     * var ProjectContainer
     */
    protected $container;
    
    /**
     * конструктор класса
     */
    public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
        // вызвать конструктор родителя
        parent::__construct($em, $class);
        
        // получить контейнер
        $this->container = ApplicationBoot::getContainer();
    }
    
    
    
    /**
     * получить код валюты по умолчанию
     * @return string currency_code
     * @throw \Exception ошибка код валюты по умолчснию не найден
     */
    public function getDefaultCurrencyCode()
    {
        // проверить существования переменной в нараметрах
        if ($this->container->hasParameter('currency_code')) {
            // вернуть код валюты 
            return $this->container->getParameter('currency_code');
        }
        
        // статус не найден
        throw new \Exception('Код валюты по умолчанию не найден.');
    }
    
    /**
     * получить валюту по умолчанию
     * @return Currency 
     * @throw \Exception ошибка получения валюты по умолчанию 
     */
    public function getDefaultCurrency()
    {
        
        // получить код валюты по умолчанию 
        $currencyCode = $this->getDefaultCurrencyCode();
        
        // получить валюту 
        $currency = $this->getEntityManager()
            ->getRepository('NitraMainBundle:Currency')
            ->find($currencyCode);
        
        // проверить валюту
        if ($currency) {
            return $currency;
        }
        
        // валюта не найдена
        throw new \Exception('В базе данных не найдена валюта по умолчанию currencyCode:'.$currencyCode);
    }   
    
    /**
     * выполнить округление
     * @param  float $val значение для округления
     * @return float результат округления
     * @example CurrencyRepository::round(343.259999999) ... return 343.25
     */
    public static function round($val)
    {
        return (intval($val*100)/100);
    }
    
}

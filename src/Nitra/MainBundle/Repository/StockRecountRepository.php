<?php
namespace Nitra\MainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\MainBundle\Exception\NotValidForSaveException;

/**
 * StockRecountRepository
 */
class StockRecountRepository extends EntityRepository
{

    /**
     * документ менеджер
     * @var Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;

    /**
     * установить $_dm
     * @DI\InjectParams({
     *      "documentManager" = @DI\Inject("doctrine_mongodb.odm.document_manager"),
     * })
     */
    public function setDocumentManager(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
    
    
    /**
     * валидация объекта инвентаризации 
     * @param object \Nitra\MainBundle\Entity\StockRecount $stockRecount валидируемый объект инвентаризации
     * @return string - сообщение об ошибке валидации 
     * @return false  - валидация пройдена успешно
     */
    public function validForAccept(\Nitra\MainBundle\Entity\StockRecount $stockRecount)
    {
        
        if ($stockRecount->getIsAccepted()) {
            return "Инвентаризация уже подписана.";
        }
        
        // массив id выбранных стоков по инвентаризации
        $stocksId = array();
        
        // обойти все позиции инвентаризации
        foreach($stockRecount->getStockRecountEntries() as $recountEntry) {

            // получить сток по продукту
            // если есть id стока то ищем сток по нему
            if($recountEntry->getStockId()) {
                $stock = $this->dm->getRepository('NitraMainBundle:Stock')->find($recountEntry->getStockId());
            }
            else {
                $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock(
                    $stockRecount->getWarehouse()->getId(), 
                    $recountEntry->getProductId(), 
                    $recountEntry->getStockParams()
                );
            }
            
            if(!$stock) {
                return 'Сток не найден';
            }
            
            // проверить достаточность устанавливаемого значения и резерва товара
            if($recountEntry->getDelta() < $stock->getReserved()) {
                $product = $this->dm->getRepository('NitraMainBundle:Product')->find($recountEntry->getProductId());
                return "Для продукта " . (string) $product . " нельзя установить значение меньше чем зарезервировано.";
            }
            
            // записать id стока
            $stocksId[] = $stock->getId();
        }

        // проверка на то, что можно обнулить стоки по инвентаризуемому складу, не вошедшие в саму инвентаризацию (на них нет резерва товара)
        $stocksToDelete = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->field('_id')->notin($stocksId)
            ->field('warehouseId')->equals($stockRecount->getWarehouse()->getId())
            ->getQuery()->execute();
        
        foreach($stocksToDelete as $stock) {
            if($stock->getReserved() > 0) {
                $product = $this->dm->getRepository('NitraMainBundle:Product')->find($stock->getProductId());
                return "Для продукта " . (string) $product . " нельзя установить значение меньше чем зарезервировано.";
            }
        }
        
        // валидация пройдена успешно, нет ошибок
        return false;
    }
    
    /**
     * принять инвентаризацию 
     * @param \Nitra\MainBundle\Entity\StockRecount $stockRecount - объект инвентаризации
     */
    public function acceptStockRecount(\Nitra\MainBundle\Entity\StockRecount $stockRecount)
    {
        // валидировать инвентаризацию перед подписанием
        $errorMessage = $this->validForAccept($stockRecount);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }
        
        // массив id выбранных стоков по инвентаризации
        $stocksId = array();
        
        foreach($stockRecount->getStockRecountEntries() as $recountEntry) {
            // если есть id стока то ищем сток по нему
            if($recountEntry->getStockId()) {
                $stock = $this->dm->getRepository('NitraMainBundle:Stock')->find($recountEntry->getStockId());
            }
            else {
                $stock = $this->dm->getRepository('NitraMainBundle:Stock')->getStock(
                    $stockRecount->getWarehouse()->getId(), 
                    $recountEntry->getProductId(), 
                    $recountEntry->getStockParams()
                );
            }
            
            // установить новое значение количества продукта
            $stock->setQuantity($recountEntry->getDelta());
            
            // установить параметр стока (на случай если он был изменен)
            $stock->setStockParams($recountEntry->getStockParams());
            
            // записать id стока
            $stocksId[] = $stock->getId();
        }
        
        // обнуление всех стоков по складу, не вошедших в инвентаризацию
        $stocksToDelete = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
            ->field('_id')->notin($stocksId)
            ->field('warehouseId')->equals($stockRecount->getWarehouse()->getId())
            ->getQuery()->execute();
        
        foreach($stocksToDelete as $stock) {
            $stock->setQuantity(0);
        }

        // установить флаг подписи инвентаризации
        $stockRecount->setIsAccepted(true);
        
        // подписание инвентаризации прошло успешно
        return false;
    }
    
}

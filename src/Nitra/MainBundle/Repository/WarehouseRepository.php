<?php
namespace Nitra\MainBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * WarehouseRepository
 */
class WarehouseRepository extends EntityRepository
{
    
    /**
     * получить запрос все склады
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQueryAll()
    {
        return $this->createQueryBuilder('q')
            ->addOrderBy('q.address');
    }

    /**
     * получить запрос свои склады 
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQueryOwnWarehouses()
    {
        return $this->buildQueryAll()
            ->andWhere('q.delivery IS NULL')
            ->andwhere('q.supplier IS NULL');
    }
    
    /**
     * получить свои склады
     */
    public function getOwnWarehouses() {
        return $this->buildQueryOwnWarehouses()
            ->getQuery()
            ->execute();
    }
    
    /**
     * получить запрос склады ТК
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQueryDeliveryWarehouses()
    {
        return $this->buildQueryAll()
            ->andwhere('q.delivery IS NOT NULL');
    }
    
    /**
     * получить склады ТК 
     */
    public function getDeliveryWarehouses() {
        return $this->buildQueryDeliveryWarehouses()
            ->getQuery()
            ->execute();
    }
    
    /**
     * получить запрос склады поставщиков
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQuerySupplierWarehouses()
    {
        return $this->buildQueryAll()
            ->andwhere('q.supplier IS NOT NULL');
    }
    
    /**
     * получить склады поставщиков 
     */
    public function getSupplierWarehouses() {
        return $this->buildQuerySupplierWarehouses()
            ->getQuery()
            ->execute();
    }
    
    /**
     * получить запрос свои склады и склады ТК
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQueryOwnAndDeliveryWarehouses()
    {
        return $this->buildQueryAll()
            ->andwhere('q.supplier IS NULL');
    }
    
    /**
     * получить свои склады и склады ТК
     * @return Doctrine\ORM\QueryBuilder
     */
    public function getOwnAndDeliveryWarehouses()
    {
       return $this->buildQueryOwnAndDeliveryWarehouses()
            ->getQuery()
            ->execute();
    }
}

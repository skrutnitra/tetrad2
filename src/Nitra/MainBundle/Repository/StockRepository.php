<?php

namespace Nitra\MainBundle\Repository;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Nitra\MainBundle\Document\Stock;
use Nitra\MainBundle\Common\ApplicationBoot;
use Nitra\MainBundle\Document\StockStorePrice;

/**
 * StockRepository
 */
class StockRepository extends DocumentRepository
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var ProjectContainer
     */
    protected $container;

    /**
     * конструктор класса
     */
    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $dm, \Doctrine\ODM\MongoDB\UnitOfWork $uow, \Doctrine\ODM\MongoDB\Mapping\ClassMetadata $class)
    {
        // вызвать конструктор родителя
        parent::__construct($dm, $uow, $class);

        // получить контейнер
        $this->container = ApplicationBoot::getContainer();

        // установить ORM\EntityManager
        $this->em = $this->container->get('doctrine')->getManager();
    }

    /**
     * получить коллекцию продуктов на складе
     * @param Nitra\MainBundle\Entity\Warehouse $warehouse склад 
     * @return array Nitra\MainBundle\Document\Stock - стоки продуктов для склада
     */
    public function getProductsAtWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouse)
    {

        // получить стоки для склада 
        $stockMongo = $this->getDocumentManager()
                ->createQueryBuilder('NitraMainBundle:Stock')
                ->field('warehouseId')->equals($warehouse->getId())
                ->where("function() { return (this.quantity) > 0; }") // отображаем товаров которые есть на складе
        ;

        // получить стоки для склада
        $stocks = $stockMongo->getQuery()->execute();

        // вернуть стоки для склдада
        return $stocks;
    }

    public function getProductsExcel(\Nitra\MainBundle\Entity\Warehouse $warehouse)
    {

        // получить свободные стоки для склада 
        $stockMongo = $this->getDocumentManager()
                ->createQueryBuilder('NitraMainBundle:Stock')
                ->field('warehouseId')->equals($warehouse->getId())
                ->where("function() { return (this.quantity>this.reserved); }")
        ;

        // получить стоки для склада
        $stocks = $stockMongo->getQuery()->execute();

        // вернуть стоки для склдада
        return $stocks;
    }

//    /**
//     * Возвращает позицию к инвентаризации на складе
//     * 
//     * @return array массив товаров для инвентаризации
//     */
//    public function getProductsToRecount($warehouse_id, $name = false)
//    {
//        $qb = $this->getDocumentManager()->createQueryBuilder('NitraMainBundle:Stock');
//
//        $products_to_recount = array();
//
//        $products = $qb
//                ->field('warehouse')->equals($warehouse_id)
//                ->field('productId')->notEqual(null);
//
//        if ($name) {
//            $products = $qb->field('priceName')->equals(new \MongoRegex("/.*" . $name . ".*/i"));
//        }
//
//        $products = $qb->getQuery()
//            ->execute();
//
//        foreach ($products as $product) {
//            $product->getQuantity();
//            $products_to_recount[$product->getProductId()]['quantity'] = $product->getQuantity();
//            $products_to_recount[$product->getProductId()]['priceName'] = $product->getPriceName();
//            $products_to_recount[$product->getProductId()]['productId'] = $product->getProductId();
//            $products_to_recount[$product->getProductId()]['warehouse'] = $product->getWarehouse();
//        }
//
//        return $products_to_recount;
//    }
//
//    /**
//     * Функция изменения кол-ва на складе согласно инвентаризации
//     * 
//     * @return boolean 
//     */
//    public function setRecountProductsInStock($warehouse_id, $products)
//    {
//        $dm = $this->getDocumentManager();
//
//        $qb = $this->getDocumentManager()->createQueryBuilder('NitraMainBundle:Stock');
//
//        $product_ids = array_flip($products);
//
//        $new_products = array();
//
//        if (!empty($product_ids)) {
//            //Выбераем существующие позиции в стоке
//            $stock = $qb
//                ->field('warehouse')->equals($warehouse_id)
//                ->field('productId')->in($product_ids)
//                ->getQuery()
//                ->execute();
//
//            //изменяем кол-во по каждой позиции в стоке в соответствии с результатами инвентаризации
//            if (count($stock) != 0) {
//                foreach ($stock as $entry) {
//                    if (isset($products[$entry->getProductId()])) {
//                        $entry->setQuantity($entry->getQuantity() + $products[$entry->getProductId()]);
//                        $dm->persist($entry);
//                    } else {
//                        $new_products[$entry->getProductId()] = $entry->getProductId();
//                    }
//                }
//            } else {
//                $new_products = $product_ids;
//            }
//
//            //если существуют позиции, которые есть в инвентаризации,
//            //но еще не забиндены в данном стоке
//            //забиндиваем данные позиции на сток
//            if (!empty($new_products)) {
//
//                $prods = $this->getDocumentManager()->createQueryBuilder('NitraMainBundle:Product')
//                    ->field('id')->in($product_ids)
//                    ->getQuery()
//                    ->execute();
//
//                foreach ($prods as $product) {
//
//                    if (isset($products[$product->getId()])) {
//                        $stock = new Stock();
//                        $stock->setBindedAt(date('Y-m-d H:i:s'));
//                        $stock->setBindedBy('inventarization');
//                        $stock->setQuantity($products[$product->getId()]);
//                        $stock->setPriceName($product->getName());
//                        $stock->setReserved(0);
//
//                        //поиск цен
//                        $prices = $product->getPrices()->toArray();
//                        $prices_array = array();
//
//                        foreach ($prices as $price) {
//
//                            if (!isset($prices_array['priceIn'])) {
//                                $prices_array['priceIn'] = $price->getPriceIn();
//                            } else {
//                                $prices_array['priceIn'] += $price->getPriceIn();
//                            }
//
//                            if (!isset($prices_array['priceOut'])) {
//                                $prices_array['priceOut'] = $price->getPriceOut();
//                            } else {
//                                $prices_array['priceOut'] += $price->getPriceOut();
//                            }
//                        }
//                        //берется средняя цена по всем поставщикам данной позиции
//                        if (!empty($prices_array)) {
//                            $priceIn = $prices_array['priceIn'] / count($prices_array);
//                            $priceOut = $prices_array['priceOut'] / count($prices_array);
//                        }
//
//
//                        $stock->setCurrency('uah');
//                        $stock->setCurrencyPrice($priceIn);
//                        $stock->setPriceIn(isset($priceIn) ? $priceIn : 0);
//                        $stock->setPriceOut(isset($priceOut) ? $priceOut : 0);
//                        $stock->setWarehouse($warehouse_id);
//                        $stock->setProductId($product->getId());
//
//                        $dm->persist($stock);
//                    }
//                }
//            }
//
//            $dm->flush();
//
//            return true;
//        }
//
//
//        return false;
//    }

    /**
     * получить объект сток
     * @param  integer  $warehouseId    - идентификатор склада
     * @param  string   $productId      - идентификатор продукта
     * @param  string   $stockParams    - строка параметров стока
     * @return object   Nitra\MainBundle\Document\Stock - объект стока
     * @retun  false    - сток не найден
     */
    public function getStock($warehouseId, $productId, $stockParams = null)
    {

        // получить склад
        $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($warehouseId);
        if (!$warehouse) {
            throw new \Exception("getStock: Склад не найден.");
        }

        // получить продукт 
        $product = $this->dm->getRepository('NitraMainBundle:Product')->find($productId);
        if (!$product) {
            throw new \Exception("getStock: Продукт не найден.");
        }

        // получить сток
        $stock = $this->findOneBy(array(
            'warehouseId' => $warehouse->getId(),
            'productId' => $product->getId(),
            'stockParams' => $stockParams,
        ));

        // вернуть сток 
        if ($stock) {
            return $stock;
        }

        // сток не найден 
        // если склад не поставщик (наш склад или склад ТК) то создать сток
        if ($warehouse->isSupplier() !== true) {

            // создать объект стока
            $stock = new Stock();
            $stock->setWarehouseId($warehouse->getId());
            $stock->setProductId($product->getId());
            $stock->setStockParams($stockParams);
            $stock->setPriceName((string)$product);
            $stock->setPriceArticle($product->getArticle());
            //установить цены по магазинам из продукта
            $stock->setStorePrice($product->getStorePrice());

            // валидировать сток
            $validator = $this->container->get('validator');
            $errors = $validator->validate($stock);

            // проверить результат валидации 
            if (count($errors) > 0) {
                // выкинкть ошибку валидации объекта стока
                foreach ($errors as $error) {
                    throw new \Exception("getStock: Не указан обязательный параметр стока. \"" . $error->getPropertyPath() . "\" " . $error->getMessage());
                }
            }

            // сохранить сток 
            $this->dm->persist($stock);
            $this->dm->flush($stock);

            // вернуть сток 
            return $stock;
        }

        // сток не найден
        return false;
    }

}

<?php
namespace Nitra\MainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\MainBundle\Document\Stock;
use Nitra\MainBundle\Exception\NotValidForSaveException;

/**
 * MovementRepository
 */
class MovementRepository extends EntityRepository
{

    /**
     * документ менеджер
     * @var Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;

    /**
     * установить $_dm
     * @DI\InjectParams({
     *      "documentManager" = @DI\Inject("doctrine_mongodb.odm.document_manager"),
     * })
     */
    public function setDocumentManager(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
    
    /**
     * ентити менеджер
     * @var Doctrine\ORM\
     */
    protected $em;

    /**
     * установить $_em
     * @DI\InjectParams({
     *      "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function setEntityManager(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
      
     /**
     * валидация объекта перемещения (проверка что по всем позициям в перемещении существуют стоки)
     * @param object \Nitra\MainBundle\Entity\Movement $movement - валидируемый объект перемещения
     * @return string - сообщение об ошибке валидации 
     * @return false  - валидация пройдена успешно
     */
    public function validMovementStockFrom(\Nitra\MainBundle\Entity\Movement $movement)
    {

        // обойти каждую позицию по перемещению
        foreach ($movement->getMovementEntries() as $movementEntry) {
            // получить сток склада отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->findOneBy(array(
                'warehouseId' => $movement->getFromWarehouse()->getId(),
                'productId' => $movementEntry->getProductId(),
                'stockParams' => $movementEntry->getStockParams()
            ));

            if (!$stockFrom) {
                return "Не найден сток склада отправителя.";
            }
            
            // проверяем достаточность кол-ва товара на складе-отправителе
            if ($stockFrom->getQuantityFree() < $movementEntry->getQuantity()) {
                // получить имя продукта для отображения и вернуть текст ошибки
                $product = $this->dm->getRepository('NitraMainBundle:Product')->find($movementEntry->getProductId());
                return 'На складе "' . (string) $movement->getFromWarehouse() . '" недостаточное количество продукта "'. (string) $product . '".';
            }
        }
        
        // валидация перемещения пройдена успешно, нет ошибок
        return false;
    }
    
     /**
     * валидация объекта перемещения при удалении (проверка что со стоке-получателе достаточное количество товара)
     * @param object \Nitra\MainBundle\Entity\Movement $movement - валидируемый объект перемещения
     * @return string - сообщение об ошибке валидации 
     * @return false  - валидация пройдена успешно
     */
    public function validDeleteMovementStockTo(\Nitra\MainBundle\Entity\Movement $movement)
    {
        
        // обойти каждую позицию по перемещению
        foreach ($movement->getMovementEntries() as $movementEntry) {
            // получить сток склада получателя
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->findOneBy(array(
                'warehouseId' => $movement->getToWarehouse()->getId(),
                'productId' => $movementEntry->getProductId(),
                'stockParams' => $movementEntry->getStockParams()
            ));

            if (!$stockTo) {
                return "Не найден сток склада получателя.";
            }
            
            // проверяем достаточность кол-ва товара на складе-получателе
            if ($stockTo->getQuantityFree() < $movementEntry->getQuantity()) {
                // вернуть текст ошибки
                return 'На складе "' . (string) $movement->getToWarehouse() . '" недостаточное количество продукта "'. (string)$movementEntry->getProduct() . '".';
            }
        }
        
        // валидация перемещения пройдена успешно, нет ошибок
        return false;
    }
    
    
    /**
     * выполнить перемещение со склада на склад
     * @param object \Nitra\MainBundle\Entity\Movement $movement - объект перемещения
     * @return string - сообщение об ошибке при выполнении перемещения
     * @return false  - перемещение выполнено успешно
     */
    public function processMovementRelocation(\Nitra\MainBundle\Entity\Movement $movement)
    {
        // валидировать перемещение перед выполнением
        $errorMessage = $this->validMovementStockFrom($movement);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }

        // обойти все позиции перемещения, переместить каждую позицию
        foreach ($movement->getMovementEntries() as $movementEntry) {
           
            // получить сток склада отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getFromWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());

            // получить сток склада получателя
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getToWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());

            // получаем цену в базовой валюте на стоке-отправителе
            $priceStockFrom = $stockFrom->getPriceIn();
            
            // получаем цену в базовой валюте на стоке-получателе
            $priceStockTo = $stockTo->getPriceIn();
            
            // если сток-получатель новый то устанавливаем цену и валюту из стока-отправителя 
            if($stockTo->isNew()) {
                $stockTo->setCurrency($stockFrom->getCurrency());
                $stockTo->setCurrencyPrice($stockFrom->getCurrencyPrice());
                $stockTo->setPriceIn($stockFrom->getPriceIn());
                $stockTo->setQuantity($movementEntry->getQuantity());
            }
            // если нет то пересчитываем цены
            else {
                // валюта на складе-получателе
                 $currency = $this->em->createQueryBuilder()
                    ->select('c')
                    ->from('NitraMainBundle:Currency', 'c')
                    ->where('c.code = :code')
                    ->setParameter('code', $stockTo->getCurrency())
                    ->getQuery()
                    ->getSingleResult();
                
                // получаем общую стоимость на все товары по стоку-отправителю
                $totalStockToSumm = $priceStockFrom * $movementEntry->getQuantity() + $priceStockTo * $stockTo->getQuantity();
                // увеличиваем количество продуктов в стоке-получателе
                $stockTo->setQuantity($stockTo->getQuantity() + $movementEntry->getQuantity());
                
                // устанавливаем новую цену и цену в валюте в стоке-получателе
                $stockTo->setPriceIn(round($totalStockToSumm / $stockTo->getQuantity(), 2));
                $stockTo->setCurrencyPrice(round($stockTo->getPriceIn() / $currency->getExchange(), 2));
            }
            
            // отнимаем перемещаемое кол-во продукта со склада отправителя
            $stockFrom->setQuantity($stockFrom->getQuantity() - $movementEntry->getQuantity());

            // записываем данные в MovementEntry 
            $movementEntry->setPrice($stockFrom->getPriceIn());
            
            $currency = $this->em->createQueryBuilder()
                ->select('c')
                ->from('NitraMainBundle:Currency', 'c')
                ->where('c.code = :code')
                ->setParameter('code', $stockFrom->getCurrency())
                ->getQuery()
                ->getSingleResult();

            $movementEntry->setCurrency($currency);
            $movementEntry->setCurrencyPrice($stockFrom->getCurrencyPrice());
            
            $this->em->persist($movementEntry);
            
        }
        
        // перемещение выполнено успешно
        return false;
    }
    
    /**
     * выполнить закупку
     * @param object \Nitra\MainBundle\Entity\Movement $movement - объект перемещения
     * @return string - сообщение об ошибке при выполнении перемещения
     * @return false  - перемещение выполнено успешно
     */
    public function processMovementIncome(\Nitra\MainBundle\Entity\Movement $movement)
    {
        // обойти все позиции перемещения, переместить каждую позицию
        foreach ($movement->getMovementEntries() as $movementEntry) {
            
            //устанавливаем валюту всем позициям по закупке
            $movementEntry->setCurrency($movement->getCurrency());

            // получить сток склада получателя
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getToWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());

            // получаем цену в базовой валюте на стоке-получателе
            $priceStockTo = $stockTo->getPriceIn();

            // если сток новый то устанавливаем цену и валюту из movementEntry
            if($stockTo->isNew()) {
                $stockTo->setCurrency($movementEntry->getCurrency()->getCode());
                $stockTo->setCurrencyPrice($movementEntry->getCurrencyPrice());
                $stockTo->setPriceIn(round($movementEntry->getCurrency()->getExchange() * $movementEntry->getCurrencyPrice(), 2));
                $stockTo->setQuantity($movementEntry->getQuantity());
            }
            // если нет то пересчитываем цены
            else {
                // переводим цену в валюте в цену в базовой валюте
                $newPriceIn = $movementEntry->getCurrency()->getExchange() * $movementEntry->getCurrencyPrice();
                                                
                // получаем общую стоимость на все товары по стоку-отправителю
                $totalStockToSumm = $newPriceIn * $movementEntry->getQuantity() + $priceStockTo * $stockTo->getQuantity();
                
                // увеличиваем количество продуктов в стоке-получателе
                $stockTo->setQuantity($stockTo->getQuantity() + $movementEntry->getQuantity());
                
                // устанавливаем новую цену и цену в валюте на стоке-получателе
                $stockTo->setPriceIn(round($totalStockToSumm / $stockTo->getQuantity(), 2));
                // устанавливаем валюту и по последней закупке
                $stockTo->setCurrency($movementEntry->getCurrency()->getCode());
                $stockTo->setCurrencyPrice(round($stockTo->getPriceIn() / $movementEntry->getCurrency()->getExchange(), 2));
            }
            
            // записываем данные в MovementEntry 
            $movementEntry->setPrice(round($movementEntry->getCurrencyPrice() * $movementEntry->getCurrency()->getExchange(), 2));
            $this->em->persist($movementEntry);
            
        }
        // перемещение выполнено успешно
        return false;
    }
    
    /**
     * выполнить возврат со своего склада/склада ТК поставщику
     * @param object \Nitra\MainBundle\Entity\Movement $movement - объект перемещения
     * @return string - сообщение об ошибке при выполнении перемещения
     * @return false  - перемещение выполнено успешно
     */
    public function processMovementReturn(\Nitra\MainBundle\Entity\Movement $movement)
    {
        // валидировать перемещение перед выполнением
        $errorMessage = $this->validMovementStockFrom($movement);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }
        
        // обойти все позиции перемещения, переместить каждую позицию
        foreach ($movement->getMovementEntries() as $movementEntry) {
            
            // получить сток склада отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getFromWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());

            // отнимаем перемещаемое кол-во продукта со склада-отправителя
            $stockFrom->setQuantity($stockFrom->getQuantity() - $movementEntry->getQuantity());

            // записываем данные в MovementEntry 
            $currency = $this->em->createQueryBuilder()
                ->select('c')
                ->from('NitraMainBundle:Currency', 'c')
                ->where('c.code = :code')
                ->setParameter('code', $stockFrom->getCurrency())
                ->getQuery()
                ->getSingleResult();
            
            $movementEntry->setCurrency($currency);
            $movementEntry->setPrice(round($movementEntry->getCurrencyPrice() * $currency->getExchange(), 2));
            $this->em->persist($movementEntry);
            
            
        }
        
        // перемещение выполнено успешно
        return false;
    }
    
    /**
     * удалить возврат
     * @param object \Nitra\MainBundle\Entity\Movement $movement - объект возврата
     * @return string - сообщение об ошибке при удалении возврата
     * @return false  - возврат удален успешно
     */
    public function processDeleteMovementReturn(\Nitra\MainBundle\Entity\Movement $movement)
    {

        // обойти перемещаемые продукты
        foreach ($movement->getMovementEntries() as $movementEntry) {
            // удалить каждую позицию перемещения
            
            // получить сток склада отправителя (свой склад/склад ТК)
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getFromWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());
            
            // прибавляем на свой склад/склад ТК количество, указанное из возврата
            $stockFrom->setQuantity($stockFrom->getQuantity() + $movementEntry->getQuantity());
            
            // удалить позицию
            $this->getEntityManager()->remove($movementEntry);
            
        }
        
        // перемещение удалено успешно 
        return false;
    }
    
       /**
     * удалить перемещение cо своего склада/склада ТК на свой склад/склад ТК
     * @param object \Nitra\MainBundle\Entity\Movement $movement - объект перемещения
     * @return string - сообщение об ошибке при удалении перемещения
     * @return false  - перемещение удалено успешно
     */
    public function processDeleteMovementRelocation(\Nitra\MainBundle\Entity\Movement $movement)
    {

        // валидировать перемещение перед удалением (что у стока-полчателя есть досмтаточное кол-во товара для удаления)
        $errorMessage = $this->validDeleteMovementStockTo($movement);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }
        
        // обойти все позиции
        foreach ($movement->getMovementEntries() as $movementEntry) {
            
            // получить сток склада-отправителя
            $stockFrom = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getFromWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());
            
            // получить сток склада-получателя
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getToWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());
            
            // прибавляем количество товара на стоке склада-отправителя
            $stockFrom->setQuantity($stockFrom->getQuantity() + $movementEntry->getQuantity());
             
            // выщитываем цену для товара которая была на стоке склада-получателя до перемещения
            // если цена в базовой валюте совпадает, то пересчитывать не надо
            if($movementEntry->getPrice() == $stockTo->getPriceIn()) {
                // отнимаем количество со стока-получателя
                $stockTo->setQuantity($stockTo->getQuantity() - $movementEntry->getQuantity());
            }
            else {
                // пересчитываем старую цену для стока-получателя
                // 
                // получаем текущую сумму на стоке-получателе в базовой валюте
                $fullSum = $stockTo->getQuantity() * $stockTo->getPriceIn();
                // получаем сумму по перемещению в базовой валюте
                $movementSum = $movementEntry->getQuantity() * $movementEntry->getPrice();
                // отнимаем количество со стока-получателя
                $stockTo->setQuantity($stockTo->getQuantity() - $movementEntry->getQuantity());
                // получаем цену в базовой валюте на стоке до перемещения 
                $stockToPriceIn = ($fullSum - $movementSum) / $stockTo->getQuantity();
                
                $stockTo->setPriceIn(round($stockToPriceIn, 2));
                
                // пересчитываем цену в валюте
                $currency = $this->em->createQueryBuilder()
                    ->select('c')   
                    ->from('NitraMainBundle:Currency', 'c')
                    ->where('c.code = :code')
                    ->setParameter('code', $stockTo->getCurrency())
                    ->getQuery()
                    ->getSingleResult();
                
                 $stockTo->setCurrencyPrice(round($stockToPriceIn / $currency->getExchange(), 2));
            }
            
            // удалить позицию
            $this->getEntityManager()->remove($movementEntry);
            
        }
        
        // перемещение удалено успешно 
        return false;
    }
    
    /**
     * удалить закупку
     * @param object \Nitra\MainBundle\Entity\Movement $movement - объект закупки
     * @return string - сообщение об ошибке при удалении закупки
     * @return false  - закупка удален успешно
     */
    public function processDeleteMovementIncome(\Nitra\MainBundle\Entity\Movement $movement)
    {
        // валидировать закупку перед удалением (что у стока-получателя есть достаточное кол-во товара для удаления)
        $errorMessage = $this->validDeleteMovementStockTo($movement);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }
        
        // обойти перемещаемые продукты
        foreach ($movement->getMovementEntries() as $movementEntry) {
            
            // получить сток получателя
            $stockTo = $this->dm->getRepository('NitraMainBundle:Stock')->getStock($movement->getToWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());
            
            // выщитываем цену для товара которая была на стоке склада-получателя до перемещения
            // если цена в базовой валюте совпадает, то пересчитывать не надо
            if($movementEntry->getPrice() == $stockTo->getPriceIn()) {
                // отнимаем количество со стока-получателя
                $stockTo->setQuantity($stockTo->getQuantity() - $movementEntry->getQuantity());
            }
            else {
                // пересчитываем старую цену для стока-получателя
                
                // получаем текущую сумму на стоке-получателе в базовой валюте
                $fullSum = $stockTo->getQuantity() * $stockTo->getPriceIn();
                // получаем сумму по перемещению в базовой валюте
                $movementSum = $movementEntry->getQuantity() * $movementEntry->getPrice();
                // отнимаем количество со стока-получателя
                $stockTo->setQuantity($stockTo->getQuantity() - $movementEntry->getQuantity());
                // получаем цену в базовой валюте на стоке до перемещения 
                $stockToPriceIn = ($fullSum - $movementSum) / $stockTo->getQuantity();
                
                $stockTo->setPriceIn(round($stockToPriceIn, 2));
                
                // пересчитываем цену в валюте
                $currency = $this->em->createQueryBuilder()
                    ->select('c')   
                    ->from('NitraMainBundle:Currency', 'c')
                    ->where('c.code = :code')
                    ->setParameter('code', $stockTo->getCurrency())
                    ->getQuery()
                    ->getSingleResult();
                
                 $stockTo->setCurrencyPrice(round($stockToPriceIn / $currency->getExchange(), 2));
            }
            
            // удалить позицию
            $this->getEntityManager()->remove($movementEntry);
            
        }
        
        // перемещение удалено успешно 
        return false;
    }
    
    /**
     * получить все закупки
     * @return Doctrine\ORM\QueryBuilder
    */
    public function getIncomeMovements() 
    {
        return $this->createQueryBuilder('q')
            ->innerJoin('q.toWarehouse', 's1')
            ->where('s1.supplier IS NULL')
            ->andWhere('q.contract IS NOT NULL');
    }
    
    /**
     * получить все перемещения (со своего склада на свой)
     * @return Doctrine\ORM\QueryBuilder
    */
    public function getRelocationMovements() 
    {
        return $this->createQueryBuilder('q')
            ->innerJoin('q.fromWarehouse', 's1')
            ->innerJoin('q.toWarehouse', 's2')
            ->where('s1.supplier IS NULL ')
            ->andWhere('s2.supplier IS NULL ');
    
    }
    
    /**
     * получить все возвраты
     * @return Doctrine\ORM\QueryBuilder
    */
    public function getReturnMovements() 
    {
        return $this->createQueryBuilder('q')
            ->innerJoin('q.fromWarehouse', 's1')
            ->where('s1.supplier IS NULL ')
            ->andWhere('q.contract IS NOT NULL');
    }
    
}

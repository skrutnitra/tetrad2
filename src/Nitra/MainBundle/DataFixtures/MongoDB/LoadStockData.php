<?php

namespace Nitra\MainBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\MainBundle\Document\Stock;

class LoadStockData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    private $em;
    
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    private $dm;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
        
        // установить EntityManager
        $this->em = $this->container->get('doctrine')->getManager();
        // установить DocumentManager
        $this->dm = $this->container->get('doctrine_mongodb')->getManager();
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // получить продукты
        $products = $this->dm->getRepository('NitraMainBundle:Product')->findAll();
        
        // получить склады поставщиков
        $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->getSupplierWarehouses();
        
        // создать стоки продуктов на складах поставщиков
        // обойти все склады поставщиков
        foreach($warehouses as $warehouse) {
            
            // обойти все продукты
            foreach($products as $product) {
                
                // создать объект стока
                $stock = new Stock();
                $stock->setWarehouseId($warehouse->getId());
                $stock->setProductId($product->getId());
                $stock->setPriceName($product->getName());
                $stock->setPriceArticle($product->getArticle());
                $stock->setStorePrice($product->getStorePrice());
                $stock->setQuantity(100);
                $stock->setReserved(0);
                $stock->setComment('Сток поставщика: '.(string)$warehouse->getSupplier().". Продукт: ".(string)$product);
                $stock->setPrice1(10);
                $stock->setPrice2(20);
                $stock->setPrice3(30);
                $stock->setPrice4(40);
                $manager->persist($stock);
            }       
            
        }
        
        // сохранить
        $manager->flush();
        
    }
    
    /**
     * индекс сортировки 
     * @return integer
     */
    public function getOrder() {
        return 20;
    }
    
}

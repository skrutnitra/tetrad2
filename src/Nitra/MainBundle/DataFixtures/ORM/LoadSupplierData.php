<?php
namespace Nitra\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\MainBundle\Entity\Supplier;

class LoadSupplierData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
     
        // получить валюту
        $currencyUAH = $this->getReference('currencyUAH');
        
        // создать записи 
        for ($index = 1; $index <= 10; $index++) {
            
            // название переменной 
            $objName = 'supplier'.$index;
            
            // создать объект
            $$objName = new Supplier();
            $$objName->setName('Поставщик '.$index);
            $$objName->setCurrency($currencyUAH);
            $$objName->setExchange($currencyUAH->getExchange());
            $manager->persist($$objName);
            
            // запомнить
            $this->addReference($objName, $$objName);
        }
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10; // the order in which fixtures will be loaded
    }

}
<?php
namespace Nitra\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\MainBundle\Entity\Delivery;

class LoadDeliveryData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // создать объект
        $deliveryNovaPoshta = new Delivery();
        $deliveryNovaPoshta->setName('Новая Почта');
        $deliveryNovaPoshta->setBusinessKey(1);
        $manager->persist($deliveryNovaPoshta);
        $this->addReference('deliveryNovaPoshta', $deliveryNovaPoshta);
        
        // создать объект
        $deliveryInTime = new Delivery();
        $deliveryInTime->setName('ИнТайм');
        $deliveryInTime->setBusinessKey(2);
        $manager->persist($deliveryInTime);
        $this->addReference('deliveryInTime', $deliveryInTime);
        
        // создать объект
        $deliveryAutolux = new Delivery();
        $deliveryAutolux->setName('АвтоЛюкс');
        $deliveryAutolux->setBusinessKey(3);
        $manager->persist($deliveryAutolux);
        $this->addReference('deliveryAutolux', $deliveryAutolux);
        
        // ТК МистЭкспресс
        $deliveryMeestExpress = new Delivery();
        $deliveryMeestExpress->setName('МистЭкспресс');
        $deliveryMeestExpress->setBusinessKey(6);
        $manager->persist($deliveryMeestExpress);
        $this->addReference('deliveryMeestExpress', $deliveryMeestExpress);
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10; // the order in which fixtures will be loaded
    }
}
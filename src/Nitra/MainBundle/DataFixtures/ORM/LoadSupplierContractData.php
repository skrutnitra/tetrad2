<?php
namespace Nitra\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\ContractBundle\Entity\Contract;

class LoadSupplierContractData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        // номер в очереди после загрузки поставщиков
        return 11;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
     
        // получить всех поставщиков
        $suppliers = $manager->getRepository('NitraMainBundle:Supplier')->findAll();
        
        // получить все филиалы
        $filials = $manager->getRepository('NitraFilialBundle:Filial')->findAll();
        
        // для каждого поставщика 
        // создать контракт с каждым филиалом
        // обойти каждого поставщика
        foreach($suppliers as $supplier) {
            
            // обойти каждый филиал
            foreach($filials as $filial) {
                
                // создать контракт филиала с поставщиком
                $contract = new Contract();
                $contract->setName('Контракт "'.(string)$supplier.'" и "'.(string)$filial.'"');
                $contract->setSupplier($supplier);
                $contract->setFilial($filial);
                $manager->persist($contract);
            }
            
        }
        
        // сохранить 
        $manager->flush();
    }
    
}

<?php
namespace Nitra\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\MainBundle\Entity\Warehouse;

class LoadWarehouseData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // склдаты ТК втягивается командой
        // php app/console ds:sync-warehouses --deliveryId="integer"
        
//        // получить $container
//        $container = \Nitra\MainBundle\Common\ApplicationBoot::getContainer();
//        $kernel = $container->get('kernel');
//        
//        // получить Application
//        $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
//        $application->setAutoExit(false);
//        
//        // параметры команды втягивания склдаов ТК Новая Почта
//        $options = array('command' => 'ds:sync-warehouses', "--deliveryId" => $this->getReference('deliveryNovaPoshta')->getId());
//        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));        
//        
//        // параметры команды втягивания склдаов ТК ИнТайм
//        $options = array('command' => 'ds:sync-warehouses', "--deliveryId" => $this->getReference('deliveryInTime')->getId());
//        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));        
        
        
        // создать записи свои склады
        for ($index = 1; $index < 5; $index++) {
            
            // название переменной 
            $objName = 'Warehouse'.$index;
            
            // создать объект 
            $$objName = new Warehouse();
            $$objName->setAddress('Свой склад '.$index);
            $$objName->setCity($this->getReference('TetradkaCity1'));
            $$objName->setPriority($index);
            $manager->persist($$objName);
        }
        
        // получить поставщиков, для каждого поставщика добавляем склад
        $suppliers = $manager->getRepository('NitraMainBundle:Supplier')->findAll();
        $iteration = 0;
        foreach($suppliers as $supplier) { ++$iteration;
            $warehouse = new Warehouse();
            $warehouse->setAddress('Склад поставшика: '.(string)$supplier);
            $warehouse->setSupplier($supplier);
            $warehouse->setCity($this->getReference('TetradkaCity1'));
            $manager->persist($warehouse);
            // запомнить
            $this->addReference('supplierWarehouse'.$iteration, $warehouse);
        }
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 15; // the order in which fixtures will be loaded
    }
}
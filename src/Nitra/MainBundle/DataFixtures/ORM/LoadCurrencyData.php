<?php
namespace Nitra\AccountBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Nitra\MainBundle\Entity\Currency;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCurrencyData extends AbstractFixture implements OrderedFixtureInterface 
{

    public function load(ObjectManager $manager)
    {
        // uah
        $currencyUAH = new Currency();
        $currencyUAH->setCode('uah');
        $currencyUAH->setName('Гривны');
        $currencyUAH->setExchange(1);
        $currencyUAH->setSymbol('грн.');
        $manager->persist($currencyUAH);
        $this->addReference('currencyUAH', $currencyUAH);
        
        // rub
        $currencyRUB = new Currency();
        $currencyRUB->setCode('rub');
        $currencyRUB->setName('Рубли');
        $currencyRUB->setExchange(0,25);
        $currencyRUB->setSymbol('руб.');
        $manager->persist($currencyRUB);
        $this->addReference('currencyRUB', $currencyRUB);
        
        // usd
        $currencyUSD = new Currency();
        $currencyUSD->setCode('usd');
        $currencyUSD->setName('Доллары');
        $currencyUSD->setExchange(8.15);
        $currencyUSD->setSymbol('$');
        $manager->persist($currencyUSD);
        $this->addReference('currencyUSD', $currencyUSD);
        
        // eur
        $currencyEUR = new Currency();
        $currencyEUR->setCode('eur');
        $currencyEUR->setName('Евро');
        $currencyEUR->setExchange(10.38);
        $currencyEUR->setSymbol('€');
        $manager->persist($currencyEUR);
        $this->addReference('currencyEUR', $currencyEUR);
        
        // сохранить 
        $manager->flush();
        
    }

    public function getOrder()
    {
        return 0;
    }

}


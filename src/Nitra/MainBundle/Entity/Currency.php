<?php
namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Currency
 * 
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="Nitra\MainBundle\Repository\CurrencyRepository")
 * @UniqueEntity(fields="code", message="Код валюты уже используется")
 * @UniqueEntity(fields="symbol", message="Символ уже используется")
 * @UniqueEntity(fields="name", message="Имя уже используется")
 */
class Currency implements Model\CurrencyInterface
{

    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=3, options={"comment"="Код валюты"})
     * @Assert\Length(max="3")
     * @Assert\NotBlank()
     * @ORM\Id
     */
    private $code;

    /**
     * @var string - Название валюты
     * @ORM\Column(name="name", type="string", length=16, options={"comment"="Название валюты"})
     * @Assert\Length(max="16")
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @var decimal - Курс обмена в базовой валюте
     * @ORM\Column(type="decimal", scale=6, precision=8, options={"comment"="Курс обмена в базовой валюте"})
     * @Assert\Range(min = 0, max = 99.999999)
     * @Assert\NotBlank()
     */
    private $exchange;
    
    /**
     * @var string - Символ валюты
     * @ORM\Column(name="symbol", type="string", length=8, options={"comment"="Символ валюты"})
     * @Assert\Length(max="8")
     */
    private $symbol;
    
    /**
     * @return string 
     */
    public function __toString()
    {
        return $this->getCode();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return Currency
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set exchange
     *
     * @param float $exchange
     * @return Currency
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;

        return $this;
    }

    /**
     * Get exchange
     *
     * @return float 
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return Currency
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }
}

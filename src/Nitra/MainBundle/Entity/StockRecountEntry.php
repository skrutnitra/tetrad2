<?php
namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\MainBundle\Entity\StockRecountEntry
 *
 * @ORM\Table(name="stock_recount_entry")
 * @ORM\Entity
 */
class StockRecountEntry
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $quantity
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Не указано количество товара")
     * @Assert\Range(min=0, max=999999)
     */
    private $quantity;

    /**
     * @var integer $delta
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Не указано дельта товара")
     * @Assert\Range(min=0, max=999999)
     */
    private $delta;
    
    /**
     * @ORM\ManyToOne(targetEntity="StockRecount", inversedBy="stockRecountEntries")
     * @ORM\JoinColumn(name="stock_recount_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $stockRecount;
    
    /**
     * @var string $productId
     * @ORM\Column(name="product_id", type="string", length=24)
     * @Assert\NotBlank(message="Не указан продукт товарной позиции")
     * @Assert\Length(max="24")
     */
    private $productId;
    
    /**
     * @var string $stockId
     * @ORM\Column(name="stock_id", type="string", length=24, nullable=true)
     * @Assert\Length(max="24")
     */
    private $stockId;
    
    /**
     * @var string $comment
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;
    
    /**
     * параметр стока
     * @var string $stockParams
     * @ORM\Column(name="stock_params", type="string", nullable=true, length=255)
     * @Assert\Length(max="255")
     * @Assert\Regex(pattern="/^[a-zа-яё0-9 ]{0,255}$/iu", message="Параметр стока указан не верно")
     */
    private $stockParams;

    /**
     * @var \Nitra\OrderBundle\Document\Product
     */
    protected $product;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quantity = 0;
        $this->delta = 0;
    }
    
    /**
     * object to string 
     * @return string
     */
    public function __toString()
    {
        if ($this->getProduct()) {
            return $this->getProduct()->getName() . " x " . $this->getQuantity();
        }
        return '';
    }
    
    /**
     * Set product
     * @param \Nitra\OrderBundle\Document\Product $product
     */
    public function setProduct($product)
    {
        $this->productId = $product->getId();
        $this->product = $product;
    }

    /**
     * Get product
     * @return Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return StockRecountEntry
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set delta
     *
     * @param integer $delta
     * @return StockRecountEntry
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;
    
        return $this;
    }

    /**
     * Get delta
     *
     * @return integer 
     */
    public function getDelta()
    {
        return $this->delta;
    }

    /**
     * Set productId
     *
     * @param string $productId
     * @return StockRecountEntry
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    
        return $this;
    }

    /**
     * Get productId
     *
     * @return string 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set stockRecount
     *
     * @param \Nitra\MainBundle\Entity\StockRecount $stockRecount
     * @return StockRecountEntry
     */
    public function setStockRecount(\Nitra\MainBundle\Entity\StockRecount $stockRecount = null)
    {
        $this->stockRecount = $stockRecount;
    
        return $this;
    }

    /**
     * Get stockRecount
     *
     * @return \Nitra\MainBundle\Entity\StockRecount 
     */
    public function getStockRecount()
    {
        return $this->stockRecount;
    }
    
    /**
     * Set comment
     *
     * @param string $comment
     * @return StockRecountEntry
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set stockParams
     *
     * @param string $stockParams
     * @return StockRecountEntry
     */
    public function setStockParams($stockParams = null) 
    {
        $this->stockParams = $stockParams;
    
        return $this;
    }

    /**
     * Get stockParams
     *
     * @return string 
     */
    public function getStockParams()
    {
        return $this->stockParams;
    }

    /**
     * Set stockId
     *
     * @param string $stockId
     * @return StockRecountEntry
     */
    public function setStockId($stockId)
    {
        $this->stockId = $stockId;
    
        return $this;
    }

    /**
     * Get stockId
     *
     * @return string 
     */
    public function getStockId()
    {
        return $this->stockId;
    }
    
}
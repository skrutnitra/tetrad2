<?php

namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
// UniqueEntity не используем потому что используем SoftDeletable
// @UniqueEntity(fields="name", message="ТК с таким названием уже существует")

/**
 * @ORM\Entity
 */
class Delivery implements Model\DeliveryInterface
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $businessKey
     *
     * @ORM\Column(type="integer")
     */
    protected $businessKey;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     */
    protected $name;

    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Delivery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set businessKey
     *
     * @param integer $businessKey
     * @return Delivery
     */
    public function setBusinessKey($businessKey)
    {
        $this->businessKey = $businessKey;

        return $this;
    }

    /**
     * Get businessKey
     *
     * @return integer 
     */
    public function getBusinessKey()
    {
        return $this->businessKey;
    }

}

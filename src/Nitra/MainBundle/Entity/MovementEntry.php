<?php
namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\MainBundle\Entity\MovementEntry
 *
 * @ORM\Table(name="movement_entry")
 * @ORM\Entity(repositoryClass="Nitra\MainBundle\Repository\MovementEntryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class MovementEntry
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Movement", inversedBy="movementEntries")
     * @ORM\JoinColumn(name="movement_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $movement;
    
    /**
     * @var string $productId
     *
     * @ORM\Column(name="product_id", type="string", length=24)
     * @Assert\NotBlank(message="Не указан продукт перемещаемой позиции")
     * @Assert\Length(max="24")
     */
    private $productId;
    
    /**
     * @var \Nitra\OrderBundle\Document\Product
     */
    protected $product;
        
    /**
     * @var integer $quantity
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1, max=999999)
     * @Assert\NotBlank(message="Не указано количество товара")
     */
    private $quantity;
    
    /**
     * позиция заказа
     * @ORM\ManyToOne(targetEntity="Nitra\OrderBundle\Entity\OrderEntry", inversedBy="movementEntry")
     * @ORM\JoinColumn(name="order_entry_id", referencedColumnName="id")
     */
    private $orderEntry;
    
    /**
     * @var float $price
     * @ORM\Column(name="price", type="decimal", precision=8, scale=2)
     * @Assert\Range(min=0, max=999999.99)
     * @Assert\NotBlank(message="Не указана цена для позиции")
     */
    private $price;
    
    /**
     * Валюта
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency", referencedColumnName="code")
     * @Assert\NotBlank(message="Не указана валюта для позиции")
     */
    private $currency;
    
    /**
     * @var float $currencyPrice
     * @ORM\Column(name="currency_price", type="decimal", precision=8, scale=2)
     * @Assert\Range(min=0, max=999999.99)
     * @Assert\NotBlank(message="Не указана цена в валюте")
     */
    private $currencyPrice;
    
    /**
     * параметр стока
     * @var string $stockParams
     * @ORM\Column(name="stock_params", type="string", nullable=true, length=255)
     * @Assert\Length(max="255")
     * @Assert\Regex(pattern="/^[a-zа-яё0-9 ]{0,255}$/iu", message="Параметр стока указан не верно")
     */
    private $stockParams;
    
    /**
     * object to string 
     * @return string
     */
    public function __toString()
    {
        
        // проверить кол-во продукта больше 1
        if ($this->getQuantity() > 1 ) {
            // вернуть навзание позиции с учетом кол-ва
            return $this->getQuantity() . " x " . $this->getEntryName();
        }
        
        // вернуть навзание позиции
        return $this->getEntryName();
    }
    
    /**
     * Получить навзание товарной позиции
     * @return string
     */
    public function getEntryName()
    {
        
        // возвразаемое имя 
        $entryName = '';
        
        // проверить связь с объектом продукта
        if ($this->getProduct()) {
            $entryName = (string)$this->getProduct();
        }
        
        // проверить параметры стока
        if ($this->getStockParams()) {
            $entryName .= " (".$this->getStockParams().")";
        }
        
        // вернуть string 
        return $entryName;
    }
    
    
    /**
     * get total в валюте
     * @return float ($quantity * $currencyPrice)
     */
    public function getTotal()
    {
        return ($this->getQuantity() * $this->getCurrencyPrice());
    }
    
    /**
     * get total в базовой валюте
     * @return float ($quantity * $price )
     */
    public function getTotalInBaseCurrency()
    {
        return ($this->getQuantity() * $this->getPrice());
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set product
     *
     * @param \Nitra\OrderBundle\Document\Product $product
     */
    public function setProduct($product)
    {
        $this->productId = $product->getId();
        $this->product = $product;
    }
    
    /**
     * Get product
     *
     * @return Nitra\OrderBundle\Document\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * Set productId
     *
     * @param string $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }
    
    /**
     * Get productId
     *
     * @return string 
     */
    public function getProductId()
    {
        return $this->productId;
    }
    
    /**
     * Get stockParams
     * @return string 
     */
    public function getStockParams()
    {
        return $this->stockParams;
    }
    
    /**
     * Set stockParams
     * @param string $stockParams
     * @return $movementEntry
     */
    public function setStockParams($stockParams)
    {
        $this->stockParams = $stockParams;
        
        return $this;
    }
    
    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return MovementEntry
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Set price
     *
     * @param float $price
     * @return MovementEntry
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set currencyPrice
     *
     * @param float $currencyPrice
     * @return MovementEntry
     */
    public function setCurrencyPrice($currencyPrice)
    {
        $this->currencyPrice = $currencyPrice;

        return $this;
    }

    /**
     * Get currencyPrice
     *
     * @return float 
     */
    public function getCurrencyPrice()
    {
        return $this->currencyPrice;
    }

    /**
     * Set movement
     *
     * @param \Nitra\MainBundle\Entity\Movement $movement
     * @return MovementEntry
     */
    public function setMovement(\Nitra\MainBundle\Entity\Movement $movement = null)
    {
        $this->movement = $movement;

        return $this;
    }

    /**
     * Get movement
     *
     * @return \Nitra\MainBundle\Entity\Movement 
     */
    public function getMovement()
    {
        return $this->movement;
    }

    /**
     * Set orderEntry
     *
     * @param \Nitra\OrderBundle\Entity\OrderEntry $orderEntry
     * @return MovementEntry
     */
    public function setOrderEntry(\Nitra\OrderBundle\Entity\OrderEntry $orderEntry = null)
    {
        $this->orderEntry = $orderEntry;

        return $this;
    }

    /**
     * Get orderEntry
     *
     * @return \Nitra\OrderBundle\Entity\OrderEntry 
     */
    public function getOrderEntry()
    {
        return $this->orderEntry;
    }

    /**
     * Set currency
     *
     * @param \Nitra\MainBundle\Entity\Currency $currency
     * @return MovementEntry
     */
    public function setCurrency(\Nitra\MainBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Nitra\MainBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}

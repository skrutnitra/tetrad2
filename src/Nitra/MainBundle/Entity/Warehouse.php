<?php

namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
// UniqueEntity не используем потому что используем SoftDeletable
// @UniqueEntity(fields={"address", "city"}, message="Склад с таким адресом уже существует")

/**
 * Nitra\MainBundle\Entity\Warehouse
 * @ORM\Table(name="warehouse")
 * @ORM\Entity(repositoryClass="Nitra\MainBundle\Repository\WarehouseRepository")
 */
class Warehouse implements Model\WarehouseInterface
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $businessKey
     * @ORM\Column(type="integer", nullable=true)
     */
    private $businessKey;

    /**
     * адрес склада
     * @var string $address
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Assert\Length(max="255")
     */
    private $address;

    /**
     * транспортная компания
     * @ORM\ManyToOne(targetEntity="Delivery")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $delivery;

    /**
     * поставщик
     * @ORM\ManyToOne(targetEntity="Supplier")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $supplier;

    /**
     * город
     * @ORM\ManyToOne(targetEntity="Nitra\GeoBundle\Entity\Model\CityInterface")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан город")
     */
    private $city;
    
    /**
     * Приоритет склада для интегры
     * @var int $priority
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priority;
    
    /**
     * Entity to string 
     * @return string
     */
    public function __toString()
    {
        
        // склад ТК
        if ($this->isDelivery()) {
            return (string)$this->getDelivery() . ' - ' . $this->getAddress();
        }
        
        // венуть адрес склада
        return (string) $this->getAddress();
    }
    
    /**
     * Склад ТК
     * @return boolean - true если склад ТК, false - скдад не ТК
     */
    public function isDelivery() {
        
        // проверить связь с объектом delivery
        if ($this->delivery) {
            // склад ТК
            return true;
        }
        
        // склад не ТК
        return false;
    }
    
    /**
     * Склад поставщика
     * @return boolean - true если склад поставщика, false - скдад не поставщика
     */
    public function isSupplier() {
        
        // проверить связь с объектом supplier
        if ($this->supplier) {
            // склад поставщика
            return true;
        }
        
        // склад не поставщика
        return false;
    }
    
    /**
     * Свой склад 
     * @return boolean - true если свой склад, false - не свой скдад
     */
    public function isOwn() {
        
        // проверить поставщика и ТК
        if ($this->isSupplier() === false && $this->isDelivery() === false) {
            // свой склад
            return true;
        }
        
        // не свой склад
        return false;
    }


    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     * @param string $address
     * @return Warehouse
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set delivery
     * @param \Nitra\MainBundle\Entity\Delivery $delivery
     * @return Warehouse
     */
    public function setDelivery(\Nitra\MainBundle\Entity\Delivery $delivery = null)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     * @return \Nitra\MainBundle\Entity\Delivery 
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set supplier
     * @param \Nitra\MainBundle\Entity\Supplier $supplier
     * @return Warehouse
     */
    public function setSupplier(\Nitra\MainBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;
        return $this;
    }

    /**
     * Get supplier
     * @return \Nitra\MainBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set city
     * @param \Nitra\GeoBundle\Entity\Model\CityInterface $city
     * @return Warehouse
     */
    public function setCity(\Nitra\GeoBundle\Entity\Model\CityInterface $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     * @return \Nitra\GeoBundle\Entity\Model\CityInterface 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set businessKey
     * @param integer $businessKey
     * @return Warehouse
     */
    public function setBusinessKey($businessKey)
    {
        $this->businessKey = $businessKey;
        return $this;
    }

    /**
     * Get businessKey
     * @return integer 
     */
    public function getBusinessKey()
    {
        return $this->businessKey;
    }
    
    /**
     * Set priority
     * @param int $priority
     * @return Warehouse
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Get priority
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
}

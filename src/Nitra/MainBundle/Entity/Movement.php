<?php
namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Nitra\MainBundle\Entity\Movement
 * @ORM\Table(name="movement")
 * @ORM\Entity(repositoryClass="Nitra\MainBundle\Repository\MovementRepository")
 */
class Movement
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * склад отправитель 
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(name="from_warehouse_id", referencedColumnName="id", nullable=true)
     */
    private $fromWarehouse;

    /**
     * склад получатель
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(name="to_warehouse_id", referencedColumnName="id", nullable=true)
     */
    private $toWarehouse;
    
    /**
     * контракт с поставщиком
     * @ORM\ManyToOne(targetEntity="Nitra\ContractBundle\Entity\Contract")
     * @ORM\JoinColumn(name="contract_id", referencedColumnName="id", nullable=true)
    */
    private $contract;
    
    /**
     * @var date - Дата прибытия
     * @ORM\Column(type="date")
     * @Assert\Date()
     * @Assert\NotBlank(message="Не указана дата")
     */
    private $date;
    
    /**
     * @ORM\OneToMany(targetEntity="MovementEntry", mappedBy="movement", cascade={"remove"})
     */
    private $movementEntries;
    
    /**
     * валюта для позиций в закупке (одна на всех)
     */
    private $currency;
    
    /**
     * @var string $comment
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->movementEntries = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @return string 
     */
    public function __toString()
    {
        return $this->getId();
    }
    
    /**
     * @Assert\False(message = "Склад отправитель не должен быть равен складу получателя.")
     */
    public function isWarehousesEqual()
    {
        return ($this->fromWarehouse && $this->toWarehouse && $this->fromWarehouse->getId() == $this->toWarehouse->getId());
    }
    
    /**
     * Обычное перемещение
     * со склада на склад 
     * со своего склада на склад ТК и обратно
     * @return boolean
     */
    public function isRelocation()
    {
        // склад отправитель и склад получатель
        if ($this->fromWarehouse && $this->toWarehouse) {
            return true;
        }
        // условие не выполнено
        return false;
    }
    
    /**
     * Закупка
     * с контракта поставщика на свой склад или на склад ТК
     * @return boolean
     */
    public function isIncome()
    {
        // контракт и склад получатель
        if ($this->contract && $this->toWarehouse) {
            return true;
        }
        // условие не выполнено
        return false;
    }
    
    /**
     * Возврат
     * со склада ТК или со своего склада на контракт поставщика
     * с контракта поставщика на свой склад или на склад ТК
     * @return boolean
     */
    public function isReturn()
    {
        // склад отправитель и контракт
        if ($this->fromWarehouse && $this->contract) {
            return true;
        }
        // условие не выполнено
        return false;
    }
    
    /**
     * Валидация объекта для сохранения
     * $this должен быть 
     * isRelocation - перемещение или
     * isIncome - закупка или 
     * isReturn - возврат поставщику 
     * в противном случе объект $this не должен сохраняться в базу
     * @Assert\True(message = "Создаваемое перемещение должно быть закупкой или возвратом или обычным перемещением.")
     * @return boolean
     */
    public function isValidForFlush()
    {
        // проверить сущность
        if ($this->isRelocation() === false && // не перемещение
            $this->isIncome() === false && // не закупка
            $this->isReturn() === false // не возврат
        ) {
            // объект нельзя сохранить в базу
            return false;
        }
        
        // проверить как перемещение contract должен быть null 
        if ($this->isRelocation() === true && $this->getContract()) {
            // не корректное перемещение
            return false;
        }
        
        // проверить как закупку fromWarehouse должен быть null 
        if ($this->isIncome() === true && $this->getFromWarehouse()) {
            // не корректная закупка
            return false;
        }
        
        // проверить как возврат поставщику toWarehouse должен быть null 
        if ($this->isReturn() === true && $this->getToWarehouse()) {
            // не корректная закупка
            return false;
        }
        
        // объект правильный для сохранения 
        return true;
    }
    
//    /**
//     * @Assert\True(message = "Не было выбрано контракт с поставщиком")
//     */
//    public function isContractExists()
//    {
//        // если не было выбрано склад-отправитель или склад-получатель, то должен быть выбран контракт
//        if ((!$this->fromWarehouse || !$this->toWarehouse) &&  !$this->contract) {
//            return false;
//        }
//        
//        return true;
//    }
    
    /**
     * @Assert\True(message = "Нет ни одного продукта для перемещения")
     */
    public function isMovementEntriesExists()
    {
        // проверить количество позиций
        if (count($this->movementEntries)) {
            return true;
        }
        
        // нет позиций
        return false;
    }
    
    /**
     * @Assert\False(message = "Нельзя выполнить перемещение с поставщика на поставщика.")
     */
    public function isMovementFromSupplierToSupplier()
    {
        return ($this->fromWarehouse && $this->toWarehouse && $this->fromWarehouse->isSupplier() && $this->toWarehouse->isSupplier());
    }
    
    /**
     * get total по всем $movementEntries (в валюте)
     * @return float 
     */
    public function getTotal()
    {
        // суммы по всем позициям перемещения
        $summ = 0;
        foreach ($this->getMovementEntries() AS $entry) {
            $summ += $entry->getTotal();
        }
        
        // вернуть сумму
        return $summ;
    }
    
    /**
     * get total по всем $movementEntries (в базовой валюте)
     * @return float 
    */
    public function getTotalInBaseCurrency()
    {
        // суммы по всем позициям перемещения в базовой валюте
        $summ = 0;
        foreach ($this->getMovementEntries() AS $entry) {
            $summ += $entry->getTotalInBaseCurrency();
        }
        // вернуть сумму
        return $summ;
    }
    
    /**
     * получить заказ если закупка осуществлялась по заказу
     * @return Nitra\OrderBundle\Entity\Order
    */
    public function getOrder()
    {
        $orderEntry = $this->getMovementEntries()->first()->getOrderEntry();
        
        if(!$orderEntry) {
            return false;
        }
        
        return $orderEntry->getOrder();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set contract
     *
     * @param string $contract
     * @return Movement
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return string 
     */
    public function getContract()
    {
        return $this->contract;
    }
    
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Declaration
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Set fromWarehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $fromWarehouse
     * @return Movement
     */
    public function setFromWarehouse(\Nitra\MainBundle\Entity\Warehouse $fromWarehouse = null)
    {
        $this->fromWarehouse = $fromWarehouse;

        return $this;
    }

    /**
     * Get fromWarehouse
     *
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getFromWarehouse()
    {
        return $this->fromWarehouse;
    }

    /**
     * Set toWarehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $toWarehouse
     * @return Movement
     */
    public function setToWarehouse(\Nitra\MainBundle\Entity\Warehouse $toWarehouse = null)
    {
        $this->toWarehouse = $toWarehouse;

        return $this;
    }

    /**
     * Get toWarehouse
     *
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getToWarehouse()
    {
        return $this->toWarehouse;
    }
    
    /**
     * Set movementEntries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function setMovementEntries($movementEntyes) 
    {
        foreach ($movementEntyes as $oe) {
            $oe->setMovement($this);
        }
    }
    
    /**
     * Get movementEntries
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getMovementEntries()
    {
        return $this->movementEntries;
    }
    
    /**
     * Add movementEntries
     *
     * @param \Nitra\MainBundle\Entity\MovementEntry $movementEntries
     * @return Movement
     */
    public function addMovementEntry(\Nitra\MainBundle\Entity\MovementEntry $movementEntries)
    {
        
        $movementEntries->setMovement($this);
        
        $this->movementEntries[] = $movementEntries;
    
        return $this;
    }

    /**
     * Remove movementEntries
     *
     * @param \Nitra\MainBundle\Entity\MovementEntry $movementEntries
     */
    public function removeMovementEntry(\Nitra\MainBundle\Entity\MovementEntry $movementEntries)
    {
        $this->movementEntries->removeElement($movementEntries);
    }
    
    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    /**
     * Set currency
     *  
     * @return string 
     */
    public function SetCurrency($currency)
    {
        $this->currency = $currency;
        
        return $this;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Movement
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Add autoincrement comment in movement
     * 
     * @param string $comment
     * @param string $username
     * @return \Nitra\OrderBundle\Entity\Order
     */
    public function addComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);
        
        if ($comment && $username) {
            
            $formatter = \IntlDateFormatter::create(
                \Locale::getDefault(), 
                \IntlDateFormatter::MEDIUM, 
                \IntlDateFormatter::NONE
            );
            
            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setComment($this->getComment() . $commentText);
            
        }
        
        return $this;
    }
}
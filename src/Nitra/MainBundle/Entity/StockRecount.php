<?php
namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\MainBundle\Entity\StockRecount
 *
 * @ORM\Table(name="stock_recount")
 * @ORM\Entity(repositoryClass="Nitra\MainBundle\Repository\StockRecountRepository")
 */
class StockRecount
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $comment
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     * @ORM\Column(name="is_accepted", type="boolean")
     * @Assert\NotNull(message="Не указано принятие инвентаризации")
     */
    private $isAccepted;
    
    /**
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан склад")
     * @Assert\Type(type="Nitra\MainBundle\Entity\Warehouse")
     */
    private $warehouse;
    
    /**
     * @ORM\OneToMany(targetEntity="StockRecountEntry", mappedBy="stockRecount", cascade={"remove"})
     */
    private $stockRecountEntries;
       
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stockRecountEntries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isAccepted = false;
    }
    
    public function __toString()
    {
        return (string)$this->getId();
    }
    
    /**
     * @Assert\True(message = "Нет ни одного продукта для инвентаризации")
     */
    public function isStockRecountEntriesExists()
    {
        // проверить количество позиций
        if (count($this->stockRecountEntries)) {
            return true;
        }
        
        // нет позиций
        return false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return StockRecount
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Add autoincrement comment in order
     * 
     * @param string $comment
     * @param string $username
     * @return StockRecount
     */
    public function addComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);
        
        if ($comment && $username) {
            
            $formatter = \IntlDateFormatter::create(
                \Locale::getDefault(), 
                \IntlDateFormatter::MEDIUM, 
                \IntlDateFormatter::NONE
            );
            
            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setComment($this->getComment() . $commentText);
            
        }
        
        return $this;
    }
    
    /**
     * Set isAccepted
     *
     * @param boolean $isAccepted
     * @return StockRecount
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;
    
        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return boolean 
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set warehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $warehouse
     * @return StockRecount
     */
    public function setWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
    
        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Get stockRecountEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStockRecountEntries()
    {
        return $this->stockRecountEntries;
    }
        
    public function setStockRecountEntries($stockRecountEntries) 
    {
        foreach ($stockRecountEntries as $entry) {
            $entry->setStockRecount($this);
        }
    }
    
    /**
     * Add stockRecountEntries
     *
     * @param \Nitra\MainBundle\Entity\StockRecountEntry $stockRecountEntries
     * @return StockRecount
     */
    public function addStockRecountEntry(\Nitra\MainBundle\Entity\StockRecountEntry $stockRecountEntries)
    {
        $this->stockRecountEntries[] = $stockRecountEntries;
    
        return $this;
    }

    /**
     * Remove stockRecountEntries
     *
     * @param \Nitra\MainBundle\Entity\StockRecountEntry $stockRecountEntries
     */
    public function removeStockRecountEntry(\Nitra\MainBundle\Entity\StockRecountEntry $stockRecountEntries)
    {
        $this->stockRecountEntries->removeElement($stockRecountEntries);
    }

}
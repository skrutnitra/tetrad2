<?php

namespace Nitra\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity("name")
 */
class Supplier implements Model\SupplierInterface
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
     /**
     * @ORM\OneToMany(targetEntity="Warehouse", mappedBy="supplier", cascade={"remove"} , indexBy="id")
     */
    private $warehouses;

   
    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $name;
    
   
    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(referencedColumnName="code")
     */
    private $currency;
    
   
    /**
     * @var string $priceInColumn1
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Assert\Length(max="255")
     */
    private $priceInColumn1;
    
   
    /**
     * @var string $priceInColumn2
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Assert\Length(max="255")
     */
    private $priceInColumn2;
    
   
    /**
     * @var string $priceInColumn3
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Assert\Length(max="255")
     */
    private $priceInColumn3;
    
   
    /**
     * @var string $priceInColumn4
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Assert\Length(max="255")
     */
    private $priceInColumn4;
    
   
    /**
     * @var decimal $exchange
     *
     * @ORM\Column(type="decimal", scale=3, nullable=true)
     * @Assert\Range(min=0.00001, max=999.999)
     */
    private $exchange;
    
        
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Supplier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set exchange
     *
     * @param float $exchange
     * @return Supplier
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;
    
        return $this;
    }

    /**
     * Get exchange
     *
     * @return float
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * Set priceInColumn1
     *
     * @param string $priceInColumn1
     * @return Supplier
     */
    public function setPriceInColumn1($priceInColumn1)
    {
        $this->priceInColumn1 = $priceInColumn1;
    
        return $this;
    }

    /**
     * Get priceInColumn1
     *
     * @return string 
     */
    public function getPriceInColumn1()
    {
        return $this->priceInColumn1;
    }

    /**
     * Set priceInColumn2
     *
     * @param string $priceInColumn2
     * @return Supplier
     */
    public function setPriceInColumn2($priceInColumn2)
    {
        $this->priceInColumn2 = $priceInColumn2;
    
        return $this;
    }

    /**
     * Get priceInColumn2
     *
     * @return string 
     */
    public function getPriceInColumn2()
    {
        return $this->priceInColumn2;
    }

    /**
     * Set priceInColumn3
     *
     * @param string $priceInColumn3
     * @return Supplier
     */
    public function setPriceInColumn3($priceInColumn3)
    {
        $this->priceInColumn3 = $priceInColumn3;
    
        return $this;
    }

    /**
     * Get priceInColumn3
     *
     * @return string 
     */
    public function getPriceInColumn3()
    {
        return $this->priceInColumn3;
    }

    /**
     * Set priceInColumn4
     *
     * @param string $priceInColumn4
     * @return Supplier
     */
    public function setPriceInColumn4($priceInColumn4)
    {
        $this->priceInColumn4 = $priceInColumn4;
    
        return $this;
    }

    /**
     * Get priceInColumn4
     *
     * @return string 
     */
    public function getPriceInColumn4()
    {
        return $this->priceInColumn4;
    }
    
    /**
     * Set currency
     *
     * @param \Nitra\MainBundle\Entity\Currency $currency
     * @return Supplier
     */
    public function setCurrency(\Nitra\MainBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return \Nitra\MainBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    public function getPricesNames() {
        return array(
            $this->getPriceInColumn1(),
            $this->getPriceInColumn2(),
            $this->getPriceInColumn3(),
            $this->getPriceInColumn4(),
        );
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->warehouses = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add warehouses
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $warehouses
     * @return Supplier
     */
    public function addWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouses)
    {
        $this->warehouses[] = $warehouses;
    
        return $this;
    }

    /**
     * Remove warehouses
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $warehouses
     */
    public function removeWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouses)
    {
        $this->warehouses->removeElement($warehouses);
    }

    /**
     * Get warehouses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWarehouses()
    {
        return $this->warehouses;
    }
}
<?php
namespace Nitra\MainBundle\Lib;

/**
 * NitraGlobals
 * Container для NitraMainBundle
 */
class NitraGlobals
{
    
    /** 
     * @var Symfony\Component\DependencyInjection\Container 
     */
    public static $container;
    
    /**
     * @var Doctrine\ORM\EntityManager
     */
    public static $em;
    
    /**
     * @var Doctrine\ODM\MongoDB\DocumentManager
     */
    public static $dm;
    
    /**
     * Получить пользователя
     * @return mixed
     * @throws \LogicException If SecurityBundle is not available
     * @see Symfony\Component\Security\Core\Authentication\Token\TokenInterface::getUser()
     */
    public static function getUser()
    {
        if (!self::$container->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }
        
        if (null === $token = self::$container->get('security.context')->getToken()) {
            return null;
        }
        
        if (!is_object($user = $token->getUser())) {
            return null;
        }
        
        // вернуть пользователя 
        return $user;
    }
    
}



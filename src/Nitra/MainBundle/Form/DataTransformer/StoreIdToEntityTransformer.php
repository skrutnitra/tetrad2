<?php
namespace Nitra\MainBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * StoreIdToEntityTransformer
 */
class StoreIdToEntityTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om = null)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (store) to a string (Store name).
     *
     * @param  Store|null $store
     * @return string
     */
    public function transform($store)
    {
        if (null === $store) {
            return "";
        }

        return $store->getName();
    }

    /**
     * Transforms a string (id) to an object (store).
     *
     * @param  string $storeId
     *
     * @return Store|null
     *
     * @throws TransformationFailedException if object (store) is not found.
     */
    public function reverseTransform($storeId)
    {
        if(!$this->om) {
            return null;
        }
        
        if (!$storeId) {
            return null;
        }

        $store = $this->om
            ->getRepository('NitraMainBundle:Store')
            ->findOneBy(array('id' => $store))
        ;

        if (null === $store) {
            throw new TransformationFailedException(sprintf('Store with Id "%s" does not exist!', $storeId));
        }

        return $store;
    }
}

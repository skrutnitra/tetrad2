<?php
namespace Nitra\MainBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * WarehouseIdToEntityTransformer
 */
class WarehouseIdToEntityTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object to a string.
     * @param  object|null $transformObj
     * @return string
     */
    public function transform($transformObj)
    {
        if (null === $transformObj) {
            return "";
        }

        return $transformObj->getId();
    }

    /**
     * Transforms a string (id) to an object.
     * @param  string $id
     * @return $transformObj|null
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $transformObj = $this->om
            ->getRepository('NitraMainBundle:Warehouse')
            ->findOneBy(array('id' => $id))
        ;

        if (null === $transformObj) {
            throw new TransformationFailedException(sprintf('An object with Id "%s" does not exist!', $id));
        }

        return $transformObj;
    }
}

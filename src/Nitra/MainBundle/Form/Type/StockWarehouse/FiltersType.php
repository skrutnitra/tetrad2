<?php
namespace Nitra\MainBundle\Form\Type\StockWarehouse;

use Admingenerated\NitraMainBundle\Form\BaseStockWarehouseType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class FiltersType extends BaseFiltersType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => false,
            'label' => 'Склад',
            'query_builder' => function(EntityRepository $er) {

            // получить запрос свои склады и склады ТК
            $query = $er->buildQueryOwnAndDeliveryWarehouses();

            // вернуть запрос 
            return $query;
        },
        ));        
        
        // фильтр кто выполнил подвязку
        $formOptions = $this->getFormOption('bindedBy', array(
            'em' => 'default', 
            'class' => 'Nitra\\ManagerBundle\\Entity\\Manager', 
            'property'=>'username', 
            'multiple' => false, 'required' => false, 
            'label' => 'Подвязал', 'translation_domain' => 'Admin'
        ));
        $builder->add('bindedBy', 'entity', $formOptions);
        
        // фильтр дата подвязки
        $formOptions = $this->getFormOption('bindedAt', array(
            'required' => false, 
            'format' => 'd MMM y',  
            'label' => 'Дата подвязки', 'translation_domain' => 'Admin',
        ));
        $builder->add('bindedAt', 'date_range', $formOptions);

        $formOptions = $this->getFormOption('warehouseType', array(
            'multiple' => false,
            'required' => false,
            'label' => 'Тип складов', 'translation_domain' => 'Admin',
            'choices' => array('OWN' => 'Свои склады', 'TK' => 'Траспортных компаний', 'SUP' => 'Поставщиков'),));
        $builder->add('warehouseType', 'choice', $formOptions);
    }
    
}

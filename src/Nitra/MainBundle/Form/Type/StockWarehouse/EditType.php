<?php
namespace Nitra\MainBundle\Form\Type\StockWarehouse;

use Admingenerated\NitraMainBundle\Form\BaseStockWarehouseType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;

class EditType extends BaseEditType
{
    
    /**
     * @var Doctrine\ORM\EntityManager\EntityManager
     */
    private $em;
    
    /**
     * @var Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;
    
    /**
     * конструктор
     * @param Doctrine\ORM\EntityManager\EntityManager - EntityManager
     */
    public function __construct(EntityManager $em, $dm)
    {
        $this->em = $em;
        $this->dm = $dm;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // построить форму родителем
        parent::buildForm($builder, $options);
        
        // получить коллекцию валют
        $currencies = $this->em->getRepository('NitraMainBundle:Currency')->findAll();
        $currencyChoices = array();
        foreach($currencies as $currency) {
            $currencyChoices[$currency->getCode()] = (string)$currency;
        }
        // виджет валюта 
        $formOptions = $this->getFormOption('currency', array(
            'multiple' => false, 
            'required' => true, 
            'choices' => $currencyChoices, 'label' =>'Валюта',));
        $builder->add('currency', 'choice', $formOptions);
        
        
        // получить склад 
        $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($options['data']->getWarehouseId());
        
        // для своих складов добавляем возможность редактирования цены выхода по магазину
        if($warehouse && $warehouse->isOwn()) {
            
            $stores = array();
            
            // если больше одного магазина то передаем их названия
            if(count($options['data']->getStorePrice()) > 1) {
                $storesDb = $this->dm->getRepository('NitraMainBundle:Store')->findAll();
                
                foreach($storesDb as $store) {
                    $stores[$store->getId()] = (string) $store;
                }
            }
            
            $builder->add('storePrice', 'collection', array(
                'label' => ' ',
                'type' => new StockStorePriceType($stores),
                'allow_add' => false,
                'allow_delete' => false,
                'prototype' => false,
                'by_reference' => false,
            ));
        }
        else {
            // удалить поле из формы
            $builder->remove('storePrice');
        }
    }
}

<?php
namespace Nitra\MainBundle\Form\Type\StockWarehouse;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
//use Nitra\MainBundle\Form\DataTransformer\StoreIdToEntityTransformer;

class StockStorePriceType extends AbstractType
{
    
    /**
     * массив имен магазинов
     * @param Doctrine\ORM\EntityManager\EntityManager - EntityManager
     */
    private $stores;
    
    /**
     * конструктор
     * @param Doctrine\ORM\EntityManager\EntityManager - EntityManager
     */
    public function __construct($stores)
    {
        $this->stores = $stores;
    }
    
    /**
     * виджит отображения магазинов для стока
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label = 'Цена выхода';
        
        if($this->stores) {
            $storeId = $builder->getForm()->getName();
            $label .= ' по магазину ' . $this->stores[$storeId];
        }
        
        $builder->add('price', 'text', array(
            'label' => $label,
            'required' => true,
            'constraints' => array(
                new Constraints\NotBlank(array('message' => 'Не указана цена выхода')),
                new Constraints\GreaterThan(array('value' => 0, 'message' => 'Значение должно быть больше 0')),
                new Constraints\Type(array('type' => 'numeric', 'message' => 'Значение должно быть числом'))
            ),
        ));
        
    }

    public function getName()
    {
        return 'nitra_mainbundle_stockstorepricetype';
    }
    
}

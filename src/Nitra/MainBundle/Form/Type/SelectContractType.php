<?php
namespace Nitra\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;

/**
 * SelectContractType
 * Виджет для выбора контрактов по поставщику
 */
class SelectContractType extends AbstractType
{
    
    /**
     * @var Doctrine\ORM\EntityManager $em
     */
    private $em;
    
    /**
     * @var array $suppliers - массив поставщиков
     */
    private $suppliers;
    
    /**
     * @var array $contracts - массив контрактов
     */
    private $contracts;
    
    /**
     * constructor
     * @param Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * buildView
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        
        // анонимная функция из формы, возвращающая query_builder
        $queryBuilderClosure = $options['query_builder'];
        $contractRepository = $this->em->getRepository('NitraContractBundle:Contract');
        
        // queryBuilder
        $queryBuilder = $queryBuilderClosure($contractRepository);
        $r = $queryBuilder->getRootAlias();
        
        // получить контракты
        $contracts = $queryBuilder
            ->addSelect('sup, f')
            ->innerJoin($r.'.supplier', 'sup')
            ->innerJoin($r.'.filial', 'f')
            ->getQuery()
            ->execute();        
        
        // обнуление массива поставщиков и контрактов
        // чтобы не задваивались склады 
        // если виджет используется на странице больше одного раза
        $this->suppliers = array();
        $this->contracts = array();
        
        // массив сортировки по названию контракта
        $sortByContractName = array(); 
        // массив сортировки по названию поставщика
        $sortBySupplierName = array(); 
        
        // обойти все контракты сформировать массивы
        foreach($contracts as $contract) {
            // получить данные по филиалу для быстрого обращения
            $filial = $contract->getFilial();
            $supplier = $contract->getSupplier();
            
            // обновить массив сортировки названий контракта
            $sortByContractName[] = (string)$contract;
            
            // запомнить контракт
            $this->contracts[] = array(
                'id' => $contract->getId(),
                'supplierId' => $supplier->getId(),
                'name' => (string)$contract,
                'startBalance' => $contract->getStartBalance(),
                'filialId' => $filial->getId(),
                'filialName' => (string)$filial,
            );
            
            // проверить добавлен ли поставщик  в массив поставщиков
            // составить массив поставщиков
            if(!isset($this->suppliers[$supplier->getId()])){
                
                // обновить массив сортировки названий поставщиков
                $sortBySupplierName[] = (string)$supplier;
                
                // обновить массив поставщиков
                $this->suppliers[$supplier->getId()] = array(
                    'id' => $supplier->getId(),
                    'name' => (string)$supplier,
                );
            }
        }
        
        // выполнить сортировку городов
        array_multisort(
            $sortBySupplierName, SORT_STRING, SORT_ASC,
            $this->suppliers
        );
        
        // выполнить сортировку складов
        array_multisort(
            $sortByContractName, SORT_STRING, SORT_ASC,
            $this->contracts
        );
        
        // запомнить данные виджета
        $view->vars['suppliers'] = $this->suppliers;
        $view->vars['contracts'] = $this->contracts;
        $view->vars['name'] = $form->getName();
        
        // сформировать ID виджетов для поставщика и контракта
        $view->vars['widget_id_supplier'] = $form->getParent()->getName().'_select_contract_for_'.$form->getName();
        $view->vars['widget_id_contract'] = $form->getParent()->getName().'_'.$form->getName();
        
        // вызов родителя Builds the form view
        parent::buildView($view, $form, $options);
    }
    
    /**
     * значения по умолчанию
     * setDefaultOptions
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'virtual' => false,
            'required' => true,
            'empty_value' => ' ',
            'class' => 'NitraContractBundle:Contract',
            'property' => 'name',
            'query_builder' => function(EntityRepository $er) {
                $query = $er
                    ->createQueryBuilder('q')
                    ->addOrderBy('q.name', 'ASC');
                return $query;
            },
        ));
    }
    
    public function getParent()
    {
        return 'entity';
    }
    
    public function getName()
    {
        return 'select_contract';
    }

}


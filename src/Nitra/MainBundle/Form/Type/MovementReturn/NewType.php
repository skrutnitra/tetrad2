<?php

namespace Nitra\MainBundle\Form\Type\MovementReturn;

use Admingenerated\NitraMainBundle\Form\BaseMovementReturnType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\MainBundle\Form\DataTransformer\WarehouseIdToEntityTransformer;
use Nitra\MainBundle\Form\Type\MovementEntry\MovementEntryReturnType;
use Doctrine\ORM\EntityRepository;
use Nitra\MainBundle\Form\EventListener\MovementEntriesSubscriber;

class NewType extends BaseNewType
{
     /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    protected $dm;
    
    /**
     * конструктор класса
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, \Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->em = $entityManager;
        $this->dm = $documentManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // создать трасформер склада
        $warehouseTransformer = new WarehouseIdToEntityTransformer($this->em);
        
        // установить виджет трансформер для ID склада 
        $builder->add(
            $builder->create('fromWarehouse', 'hidden')
                ->addModelTransformer($warehouseTransformer)
        );
        
        // перемещаемые продукты
        $builder->add('movementEntries', 'collection', array(
            'type' => new MovementEntryReturnType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false
            )
        );
        
        // добавить листенер для дополнительной валидации формы
        $builder->addEventSubscriber(new MovementEntriesSubscriber($this->dm));
    }
}

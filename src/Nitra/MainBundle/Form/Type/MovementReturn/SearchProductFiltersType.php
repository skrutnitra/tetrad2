<?php
namespace Nitra\MainBundle\Form\Type\MovementReturn;

use Admingenerated\NitraMainBundle\Form\BaseMovementReturnType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class SearchProductFiltersType extends BaseFiltersType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // родитель формы
        parent::buildForm($builder, $options);
        
        // массив используемых полей
        $useFields = array('fromWarehouse');
        
        // удалить эелементы формы не указанные в массиве $useFields
        foreach($builder->all() as $children) {
            if (!in_array($children->getName(), $useFields)) {
                $builder->remove($children->getName());
            }
        }
        
         $formOptions = $this->getFormOption('fromWarehouse', array(
            'required' => true,  
            'label' => 'Склад-отправитель',  
            'translation_domain' => 'Admin',
            'class' => 'NitraMainBundle:Warehouse',
            'property' => 'address',
            'query_builder' => function(EntityRepository $er)   {
                return $er->buildQueryOwnAndDeliveryWarehouses();
            },
        ));
        
        $builder->add('fromWarehouse', 'select_warehouse', $formOptions);
        
        // добавить виджет название для поиска
        $formOptions = $this->getFormOption('name', array('required' => false, 'label' => 'Товар', 'translation_domain' => 'Admin',));
        $builder->add('name', 'text', $formOptions);
        
        // добавить виджет выбора категорий для поиска
        $formOptions = $this->getFormOption('category', array(
            'required' => false,  
            'label' => 'Категория',
            'class' => 'NitraMainBundle:Category',
            'property' => 'name',
            'translation_domain' => 'Admin',
        ));
        $builder->add('category', 'document', $formOptions);
        
        // добавить виджет страница отображенных результатов
        $formOptions = $this->getFormOption('numberPage', array('required' => false,));
        $builder->add('numberPage', 'hidden', $formOptions);

        }

}

<?php
namespace Nitra\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;

/**
 * SelectWarehouseType
 * Виджет для выбора складов по городам
 */
class SelectWarehouseType extends AbstractType
{
    
    /**
     * @var Doctrine\ORM\EntityManager $em
     */
    private $em;
    
    /**
     * @var array $cities - массив городов
     */
    private $cities;
    
    /**
     * @var array $warehouses - массив складов
     */
    private $warehouses;
    
    /**
     * constructor
     * @param Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * buildView
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // анонимная функция из формы, возвращающая query_builder
        $queryBuilderClosure = $options['query_builder'];
        $warehouseRepository = $this->em->getRepository('NitraMainBundle:Warehouse');
        
        // queryBuilder
        $queryBuilder = $queryBuilderClosure($warehouseRepository);
        $r = $queryBuilder->getRootAlias();
        
        // получить склады
        $warehouses = $queryBuilder
            ->addSelect('dw, sw, cw')
            ->innerJoin($r.'.city', 'cw')
            // добавляем к общему запросу delivery и supplier
            // добавлем через leftJoin потому что не знаем для кого имеено строится виджет 
            // по схеме может быть для складов ТК или для своих склдов или поставщика
            ->leftJoin($r.'.delivery', 'dw')
            ->leftJoin($r.'.supplier', 'sw')
            ->getQuery()
            ->execute();
        
        // обнуление массива городов и складов 
        // чтобы не задваивались склады 
        // если виджет используется на странице больше одного раза
        $this->cities = array(); 
        $this->warehouses = array();
        
        // массив сортировки свой склад 
        $sortByIsOwn = array(); 
        // массив сортировки по названию города
        $sortByCityName = array(); 
        // массив сортировки по названию склада
        $sortByWarehouseName = array(); 
        
        // обойти все склады сформировать массивы
        foreach ($warehouses as $warehouse) {
            // получить данные по складу для быстрого обращения
            $whDelivery = $warehouse->getDelivery();
            $whSupplier = $warehouse->getSupplier();
            $whCity = $warehouse->getCity();
            
            // обновить массив сортировки свой склад
            $sortByIsOwn[] = $warehouse->isOwn();
            // обновить массив сортировки названий складов
            $sortByWarehouseName[] = (string)$warehouse;
            
            // запомнить склад
            $this->warehouses[] = array(
                'id' => $warehouse->getId(),
                'deliveryId' => ($whDelivery) ? $whDelivery->getId() : false,
                'deliveryBusinessKey' => ($whDelivery) ? $whDelivery->getBusinessKey() : false,
                'supplierId' => ($whSupplier) ? $whSupplier->getId() : false,
                'cityId' => $whCity->getId(),
                'warehouseBusinessKey' => $warehouse->getBusinessKey(),
                'address' => (string)$warehouse,
            );
            
            // проверить добавлен ли город в массив городов
            // составить массив городов
            if(!isset($this->cities[$whCity->getId()])){
                
                // обновить массив сортировки названий городов
                $sortByCityName[] = (string)$whCity;
                
                // обновить массив городов
                $this->cities[$whCity->getId()] = array(
                    'id' => $whCity->getId(),
                    'cityBusinessKey' => $whCity->getBusinessKey(),
                    'name' => (string)$whCity,
                );
            }
        }
        
        // выполнить сортировку городов
        array_multisort(
            $sortByCityName, SORT_STRING, SORT_ASC,
            $this->cities
        );
        
        // выполнить сортировку складов
        array_multisort(
            $sortByIsOwn, SORT_NUMERIC, SORT_DESC,
            $sortByWarehouseName, SORT_STRING, SORT_ASC,
            $this->warehouses
        );
        
        // запомнить данные виджета
        $view->vars['cities'] = $this->cities;
        $view->vars['warehouses'] = $this->warehouses;
        $view->vars['name'] = $form->getName();
        
        // сформировать ID виджетов для города и склада
        $view->vars['widget_id_city'] = $form->getParent()->getName().'_select_city_for_'.$form->getName();
        $view->vars['widget_id_warehouse'] = $form->getParent()->getName().'_'.$form->getName();
        
        // вызов родителя Builds the form view
        parent::buildView($view, $form, $options);
    }
    
    /**
     * значения по умолчанию
     * setDefaultOptions
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'virtual' => false,
            'required' => true,
            'empty_value' => ' ',
            'class' => 'NitraMainBundle:Warehouse',
            'property' => 'address',
            'query_builder' => function(EntityRepository $er) {
                return $er->buildQueryAll();
            },
        ));
    }
    
    public function getParent()
    {
        return 'entity';
    }
    
    public function getName()
    {
        return 'select_warehouse';
    }

}


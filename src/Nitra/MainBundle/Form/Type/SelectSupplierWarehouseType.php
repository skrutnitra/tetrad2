<?php
namespace Nitra\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;

/**
 * SelectSupplierWarehouseType
 * Виджет для выбора складов по городам
 */
class SelectSupplierWarehouseType extends AbstractType
{
    
    /**
     * @var Doctrine\ORM\EntityManager $em
     */
    private $em;
    
    /**
     * @var array $suppliers - массив поставщиков
     */
    private $suppliers;
    
    /**
     * @var array $warehouses - массив складов
     */
    private $warehouses;
    
    /**
     * constructor
     * @param Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * buildView
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // анонимная функция из формы, возвращающая query_builder
        $queryBuilderClosure = $options['query_builder'];
        $warehouseRepository = $this->em->getRepository('NitraMainBundle:Warehouse');
        
        // queryBuilder
        $queryBuilder = $queryBuilderClosure($warehouseRepository);
        $r = $queryBuilder->getRootAlias();
        
        // получить склады
        $warehouses = $queryBuilder
            ->addSelect('sw, cw')
            ->innerJoin($r.'.city', 'cw')
            // добавляем к общему запросу delivery и supplier
            // добавлем через leftJoin потому что не знаем для кого имеено строится виджет 
            // по схеме может быть для складов ТК или для своих склдов или поставщика
            ->leftJoin($r.'.supplier', 'sw')
            ->getQuery()
            ->execute();
        
        // обнуление массива поставщиков и складов 
        // чтобы не задваивались склады 
        // если виджет используется на странице больше одного раза
        $this->suppliers = array(); 
        $this->warehouses = array();
        
        // массив сортировки по названию поставщика
        $sortBySupplierName = array(); 
        // массив сортировки по названию склада
        $sortByWarehouseName = array(); 
        
        // обойти все склады сформировать массивы
        foreach ($warehouses as $warehouse) {
            // получить данные по складу для быстрого обращения
            $whSupplier = $warehouse->getSupplier();
            $whCity = $warehouse->getCity();
            
            // обновить массив сортировки свой склад
            $sortByIsOwn[] = $warehouse->isOwn();
            // обновить массив сортировки названий складов
            $sortByWarehouseName[] = (string)$warehouse;
            
            // запомнить склад
            $this->warehouses[] = array(
                'id' => $warehouse->getId(),
                'supplierId' => ($whSupplier) ? $whSupplier->getId() : false,
                'cityId' => $whCity->getId(),
                'address' => (string)$warehouse,
            );
            
            // проверить добавлен ли поставщик в массив поставщиков
            // составить массив поставщиков
            if($whSupplier && !isset($this->suppliers[$whSupplier->getId()])){
                
                // обновить массив сортировки названий городов
                $sortBySupplierName[] = (string)$whSupplier;
                
                // обновить массив поставщиков
                $this->suppliers[$whSupplier->getId()] = array(
                    'id' => $whSupplier->getId(),
                    'name' => (string)$whSupplier,
                );
            }
        }
        
        // выполнить сортировку городов
        array_multisort(
            $sortBySupplierName, SORT_STRING, SORT_ASC,
            $this->suppliers
        );
        
        // выполнить сортировку складов
        array_multisort(
            $sortByWarehouseName, SORT_STRING, SORT_ASC,
            $this->warehouses
        );
        
        // запомнить данные виджета
        $view->vars['suppliers'] = $this->suppliers;
        $view->vars['warehouses'] = $this->warehouses;
        $view->vars['name'] = $form->getName();
        
        // сформировать ID виджетов для поставщика и склада
        $view->vars['widget_id_supplier'] = $form->getParent()->getName().'_select_supplier_for_'.$form->getName();
        $view->vars['widget_id_warehouse'] = $form->getParent()->getName().'_'.$form->getName();
        
        // вызов родителя Builds the form view
        parent::buildView($view, $form, $options);
    }
    
    /**
     * значения по умолчанию
     * setDefaultOptions
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'virtual' => false,
            'required' => true,
            'empty_value' => ' ',
            'class' => 'NitraMainBundle:Warehouse',
            'property' => 'address',
            'query_builder' => function(EntityRepository $er) {
                return $er->buildQuerySupplierWarehouses();
            },
        ));
    }
    
    public function getParent()
    {
        return 'entity';
    }
    
    public function getName()
    {
        return 'select_supplier_warehouse';
    }

}


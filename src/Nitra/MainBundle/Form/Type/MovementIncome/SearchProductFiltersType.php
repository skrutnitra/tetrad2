<?php
namespace Nitra\MainBundle\Form\Type\MovementIncome;

use Admingenerated\NitraMainBundle\Form\BaseMovementIncomeType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ODM\MongoDB\DocumentRepository;


class SearchProductFiltersType extends BaseFiltersType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // родитель формы
        parent::buildForm($builder, $options);
        
        // массив используемых полей
        $useFields = array('contract');
        
        // удалить элементы формы не указанные в массиве $useFields
        foreach($builder->all() as $children) {
            if (!in_array($children->getName(), $useFields)) {
                $builder->remove($children->getName());
            }
        }
        
        $formOptions = $this->getFormOption('contract', array('required' => true,  'label' => 'Контракт',  'translation_domain' => 'Admin',));
        $builder->add('contract', 'select_contract', $formOptions);
        
        // добавить виджет название для поиска
        $formOptions = $this->getFormOption('name', array('required' => false, 'label' => 'Товар', 'translation_domain' => 'Admin',));
        $builder->add('name', 'text', $formOptions);
        
        // добавить виджет выбора категорий для поиска
        $formOptions = $this->getFormOption('category', array(
            'required' => false,  
            'label' => 'Категория',
            'class' => 'NitraMainBundle:Category',
            'property' => 'name',
            'query_builder' => function(DocumentRepository $dr)   {
                return $dr->createQueryBuilder()
                        ->sort(array('name'=>'asc'));
            },
            'translation_domain' => 'Admin',
        ));
        $builder->add('category', 'document', $formOptions);
        
        // добавить фильтр для поиска продуктов только по стокам
        $formOptions = $this->getFormOption('search_only_in_stocks', array(
            'required' => false,  
            'label' => 'Искать только по стокам',
            'attr' => array('checked' => 'checked'),
            'translation_domain' => 'Admin',
        ));
        $builder->add('search_only_in_stocks', 'checkbox', $formOptions);
        
        // добавить виджет страница отображенных результатов
        $formOptions = $this->getFormOption('numberPage', array('required' => false,));
        $builder->add('numberPage', 'hidden', $formOptions);

        }

}

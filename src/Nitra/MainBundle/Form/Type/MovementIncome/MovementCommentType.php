<?php
namespace Nitra\MainBundle\Form\Type\MovementIncome;

use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use Nitra\MainBundle\Form\Type\CommentType as CommonCommentType;
use Nitra\MainBundle\Form\DataTransformer\MovementIdToEntityTransformer;

/**
 * форма добавления комментаряи к закупке
 */
class MovementCommentType extends CommonCommentType
{
    
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * конструктор класса
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // buildForm родитель
        parent::buildForm($builder, $options);
        
        // установить трансформер для идентификатора
        $builder
            ->get('id')
            ->addModelTransformer(new MovementIdToEntityTransformer($this->em));
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'movement_comment';
    }
    
}

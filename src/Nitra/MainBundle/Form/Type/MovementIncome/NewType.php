<?php

namespace Nitra\MainBundle\Form\Type\MovementIncome;

use Admingenerated\NitraMainBundle\Form\BaseMovementIncomeType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\MainBundle\Form\Type\SelectContractType;
use Nitra\MainBundle\Form\Type\MovementEntry\MovementEntryIncomeType;
use Doctrine\ORM\EntityRepository;
use Nitra\MainBundle\Form\DataTransformer\ContractIdToEntityTransformer;
use Nitra\MainBundle\Form\DataTransformer\WarehouseIdToEntityTransformer;
use Nitra\MainBundle\Form\EventListener\MovementEntriesSubscriber;


class NewType extends BaseNewType
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    protected $dm;


    /**
     * конструктор класса
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, \Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->em = $entityManager;
        $this->dm = $documentManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $formOptions = $this->getFormOption('currency', array(
            'required' => true,
            'label' => 'Валюта',
            'virtual' => false,
            'translation_domain' => 'Admin',
            'class' => 'NitraMainBundle:Currency',
            'property' => 'symbol',
        ));
            
        $builder->add('currency', 'entity', $formOptions);
        
        $formOptions = $this->getFormOption('toWarehouse', array(
            'required' => true,  
            'label' => 'Склад-получатель',  
            'translation_domain' => 'Admin',
            'class' => 'NitraMainBundle:Warehouse',
            'property' => 'address',
            'query_builder' => function(EntityRepository $er)   {
                return $er->buildQueryOwnWarehouses();
            },
        ));
        $builder->add('toWarehouse', 'select_warehouse', $formOptions);
        
        // создать трасформер контракта
        $contractTransformer = new ContractIdToEntityTransformer($this->em);
        
        // установить виджет трансформер для ID контракта (устанавливает значение ID контракта из фильтра по контракта)
        $builder->add(
            $builder->create('contract', 'hidden')
                ->addModelTransformer($contractTransformer)
        );
        
        // перемещаемые продукты
        $builder->add('movementEntries', 'collection', array(
            'type' => new MovementEntryIncomeType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            )
        );
        
        // виджет автоинкремента коментария к заказу
        $builder->add('commentAdd', 'text', array('required' => false, 'mapped' => false, 'label' => 'Добавить комментарий'));
        
        // добавить листенер для дополнительной валидации формы
        $builder->addEventSubscriber(new MovementEntriesSubscriber($this->dm));
        
    }
    
}

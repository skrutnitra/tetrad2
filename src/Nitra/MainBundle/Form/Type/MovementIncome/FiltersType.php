<?php

namespace Nitra\MainBundle\Form\Type\MovementIncome;

use Symfony\Component\Form\FormBuilderInterface;
use Admingenerated\NitraMainBundle\Form\BaseMovementIncomeType\FiltersType as BaseFiltersType;

class FiltersType extends BaseFiltersType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        //фильтр по номеру заказа
        $formOptions = $this->getFormOption('orderId', array('required' => false, 'label' => 'Номер заказа',));
        $builder->add('orderId', 'integer', $formOptions);
        //фильтр по названию товара
        $formOptions = $this->getFormOption('productName', array(  'required' => false,  'label' => 'Название товара',  'translation_domain' => 'Admin',));
        $builder->add('productName', 'text', $formOptions);
    }
    
    /**
     * получить массив настроек виджета
     * @param string    $name           - название виджета
     * @param array     $formOptions    - массив настроек виджета
     * @return array 
     */
    protected function getFormOption($name, array $formOptions)
    {
        // убрать configs select2 для виджетов
        if (in_array($name, array('toWarehouse', 'contract'))) {
            unset($formOptions['configs']);
        }
        
        // вернуть массив настроек виджета
        return $formOptions;
    }
    
}

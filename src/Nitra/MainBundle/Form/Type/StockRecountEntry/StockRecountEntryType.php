<?php
namespace Nitra\MainBundle\Form\Type\StockRecountEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class StockRecountEntryType extends AbstractType
{
    
    /**
     * построить форму позиции заказа
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stockParams', 'text', array('required' => false))
            ->add('delta', 'text', array('attr' => array('class' => 'delta')) )
            ->add('comment', 'text', array('required' => false))
            ->add('productId', 'hidden')
            ->add('stockId', 'hidden')
        ;
        
    }

    public function getName()
    {
        return 'nitra_mainbundle_stockrecountentrytype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\MainBundle\Entity\StockRecountEntry'
        ));
    }

}

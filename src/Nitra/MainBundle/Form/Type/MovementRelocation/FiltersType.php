<?php

namespace Nitra\MainBundle\Form\Type\MovementRelocation;

use Admingenerated\NitraMainBundle\Form\BaseMovementRelocationType\FiltersType as BaseFiltersType;

class FiltersType extends BaseFiltersType
{
    
    /**
     * получить массив настроек виджета
     * @param string    $name           - название виджета
     * @param array     $formOptions    - массив настроек виджета
     * @return array 
     */
    protected function getFormOption($name, array $formOptions)
    {
        // убрать configs select2 для виджетов
        if (in_array($name, array('fromWarehouse', 'toWarehouse'))) {
            unset($formOptions['configs']);
        }
        
        // вернуть массив настроек виджета
        return $formOptions;
    }
    
}

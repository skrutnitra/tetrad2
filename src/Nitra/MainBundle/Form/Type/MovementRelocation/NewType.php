<?php

namespace Nitra\MainBundle\Form\Type\MovementRelocation;

use Admingenerated\NitraMainBundle\Form\BaseMovementRelocationType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\MainBundle\Form\DataTransformer\WarehouseIdToEntityTransformer;
use Nitra\MainBundle\Form\Type\MovementEntry\MovementEntryRelocationType;
use Doctrine\ORM\EntityRepository;
use Nitra\MainBundle\Form\EventListener\MovementEntriesSubscriber;

class NewType extends BaseNewType
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $dm;
    
    /**
     * конструктор класса
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, \Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->em = $entityManager;
        $this->dm = $documentManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $formOptions = $this->getFormOption('toWarehouse', array(
            'required' => true,  
            'label' => 'Склад-получатель',  
            'translation_domain' => 'Admin',
            'class' => 'NitraMainBundle:Warehouse',
            'property' => 'address',
            'query_builder' => function(EntityRepository $er)   {
                return $er->buildQueryOwnWarehouses();
            },
        ));
        $builder->add('toWarehouse', 'select_warehouse', $formOptions);
        
        // создать трасформер склада
        $warehouseTransformer = new WarehouseIdToEntityTransformer($this->em);
        
        // установить виджет трансформер для ID склада (устанавливает значение ID склада из фильтра по складу)
        $builder->add(
            $builder->create('fromWarehouse', 'hidden')
                ->addModelTransformer($warehouseTransformer)
        );
        
        // перемещаемые продукты
        $builder->add('movementEntries', 'collection', array(
            'type' => new MovementEntryRelocationType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false
            )
        );
             
        // добавить листенер для дополнительной валидации формы
        $builder->addEventSubscriber(new MovementEntriesSubscriber($this->dm));
    }
    
    /**
     * получить массив настроек виджета
     * @param string    $name           - название виджета
     * @param array     $formOptions    - массив настроек виджета
     * @return array 
     */
    protected function getFormOption($name, array $formOptions)
    {
        // убрать configs select2 для виджетов
        if (in_array($name, array('fromWarehouse', 'toWarehouse'))) {
            unset($formOptions['configs']);
        }
        
        // вернуть массив настроек виджета
        return $formOptions;
    }
    
}

<?php
namespace Nitra\MainBundle\Form\Type\MovementEntry;

use Symfony\Component\Form\FormBuilderInterface;

class MovementEntryReturnType extends MovementEntryRelocationType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder
            ->add('currencyPrice', 'text', array('attr' => array('class' => 'price', 'pattern' => '\d+((\.)\d+)?')));
    }
}

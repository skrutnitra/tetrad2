<?php
namespace Nitra\MainBundle\Form\Type\MovementEntry;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;

class MovementEntryIncomeType extends MovementEntryRelocationType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder
            ->add('currencyPrice', 'text', array(
                'attr' => array(
                    'class' => 'price',
                    'pattern' => '\d+((\.)\d+)?'
                )
            ))
            
            ->add('stockParams', 'text', array(
                'required' => false, 
                'constraints' => new Regex('/^[a-zа-яё0-9 ]{0,255}$/iu')
            ));
    }
}

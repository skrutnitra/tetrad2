<?php
namespace Nitra\MainBundle\Form\Type\MovementEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class MovementEntryRelocationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('productId', 'hidden')
            ->add('stockParams', 'hidden')
            ->add('quantity', 'text', array('attr' => array('class' => 'quantity', 'min'=> 1, 'pattern' => '^\d+$')))
        ;
                
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\MainBundle\Entity\MovementEntry'
        ));
    }

    public function getName()
    {
        return 'nitra_mainbundle_movemententrytype';
    }
    
}

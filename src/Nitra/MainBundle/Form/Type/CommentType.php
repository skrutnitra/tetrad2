<?php
namespace Nitra\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * общая форма добавления комментаряи
 */
class CommentType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // виджет ID Entity/Document для которого добавляется комментарий 
        $builder->add('id', 'hidden', array(
            'required' => true,
            'constraints' => array(new NotBlank(array('message' => 'Не указан идентификатор.'))),
        ));
        
        // виджет комментарий
        $builder->add('comment', 'textarea', array(
            'required' => true,
            'constraints' => array(new NotBlank(array('message' => 'Не указан комментарий.'))),
            'label' => 'Комментарий', 'translation_domain' => 'Admin',
        ));
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'comment';
    }
    
}

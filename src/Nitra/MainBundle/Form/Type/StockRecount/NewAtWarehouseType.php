<?php
namespace Nitra\MainBundle\Form\Type\StockRecount;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints;

class NewAtWarehouseType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) {
                return $er->buildQueryOwnWarehouses();
        },
        ));
        
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'new_stock_recount_at_warehouse';
    }
    
}

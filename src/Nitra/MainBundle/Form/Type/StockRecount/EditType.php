<?php
namespace Nitra\MainBundle\Form\Type\StockRecount;

use Admingenerated\NitraMainBundle\Form\BaseStockRecountType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Nitra\MainBundle\Form\DataTransformer\WarehouseIdToEntityTransformer;
use Nitra\MainBundle\Form\Type\StockRecountEntry\StockRecountEntryType;
use Nitra\MainBundle\Form\EventListener\StockRecountEntriesSubscriber;


class EditType extends BaseEditType
{
    
    /**
     * @var \Doctrine\ORM\EntityManager $entityManager - EntityManager
     */
    protected $em;
    
    /**
     * конструктор класса
     * @param \Doctrine\ORM\EntityManager $entityManager - EntityManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // buildForm
        parent::buildForm($builder, $options);
        // создать трасформер склада
        $warehouseTransformer = new WarehouseIdToEntityTransformer($this->em);
        
        // установить виджет трансформер для ID склада 
        $builder->add(
            $builder->create('warehouse', 'hidden')
                ->addModelTransformer($warehouseTransformer)
        );
        
        // виджет позиции инвентаризации
        $builder->add('stockRecountEntries', 'collection', array(
            'type' => new StockRecountEntryType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false
        ));
        
        // поле для javacript-фильтра по продуктам на складе
        $builder->add('productFilter', 'text', array(
            'label' => ' ',
            'required' => false,
            'mapped' => false,
            'attr' => array('placeholder' => 'Фильтр по названию'),
        ));
        
        // инвентаризация не подписана 
        // добавляем кнопку подписи 
        if (!$options['data']->getIsAccepted()) {
            $builder->add('saveAndAccept', 'hidden', array('mapped' => false));
        }
        
        // добавить листенер для дополнительной валидации формы (проверка есть ли одинаковые позиции инвернтаризации)
        $builder->addEventSubscriber(new StockRecountEntriesSubscriber());
        
    }
    
    /**
     * установить массив $options по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\MainBundle\Entity\StockRecount',
        ));
    }
    
}


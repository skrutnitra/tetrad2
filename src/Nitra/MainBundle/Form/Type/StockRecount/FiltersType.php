<?php

namespace Nitra\MainBundle\Form\Type\StockRecount;

use Admingenerated\NitraMainBundle\Form\BaseStockRecountType\FiltersType as BaseFiltersType;

class FiltersType extends BaseFiltersType
{
    
    /**
     * получить массив настроек виджета
     * @param string    $name           - название виджета
     * @param array     $formOptions    - массив настроек виджета
     * @return array 
     */
    protected function getFormOption($name, array $formOptions)
    {
        // склад
        if ($name == 'warehouse') {
            unset($formOptions['configs']);
        }
        
        // вернуть массив настроек виджета
        return $formOptions;
    }
    
}

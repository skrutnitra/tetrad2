<?php
namespace Nitra\MainBundle\Form\Type\StockRecount;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Nitra\MainBundle\Form\Type\AjaxAutocompleteType;


class SearchProductFiltersType extends AbstractType
{

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;
    
    /**
     * конструктор класса
     */
    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $afterJavaScriptProductName = "  
            json_data = eval('('+row.extra[2]+')');       
            $('#filters_stock_recount_product_name').attr('categoryName', json_data.category);
            $('#filters_stock_recount_product_name').attr('productId', json_data.id);
            $('#filters_stock_recount_product_name').attr('productName', json_data.name);
            ";
        
        $builder->add('product', new AjaxAutocompleteType(), array_merge(array(
            'label' => 'поиск по названию',
            'required' => true,
            'mapped' => false,
            'autocompleteActionRouting' => 'Nitra_MainBundle_StockRecount_Search_Product_Autocomplete_Test',
            'afterItemSelectJavascript' => $afterJavaScriptProductName,
            ), $options['product_name_add_options']
        ));
  
    }

    public function getName()
    {
        return 'filters_stock_recount';
    }
    
    /**
     * установить массив $options по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            // данные автокомплита по продуктам
            'product_name_add_options' => array('data' => array('name' => null)),
        ));
        
    }
}

<?php

namespace Nitra\MainBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;

class MovementEntriesSubscriber implements EventSubscriberInterface
{

    private $dm;

    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }

    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SUBMIT => 'postSubmit');
    }

    public function postSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $movement = $event->getData();

        // проверка на то, нет ли позиций в закупке с одинаковыми параметрами стока и продукта
        $uniqueMovementEntries = array();

        foreach ($movement->getMovementEntries() as $movementEntry) {
            if (!isset($uniqueMovementEntries[$movementEntry->getProductId() . $movementEntry->getStockParams()])) {
                $uniqueMovementEntries[$movementEntry->getProductId() . $movementEntry->getStockParams()] = true;
            } else {
                $product = $this->dm->getRepository("NitraMainBundle:Product")
                        ->find($movementEntry->getProductId());

                $productName = $product->getName();

                $form->addError(new FormError('Возникла ошибка. Присутствуют позиции с одинаковыми параметрами: ' . $productName));
                break;
            }
        }
    }

}

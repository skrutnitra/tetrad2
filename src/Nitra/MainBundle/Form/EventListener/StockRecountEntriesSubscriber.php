<?php

namespace Nitra\MainBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;

class StockRecountEntriesSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SUBMIT => 'postSubmit');
    }

    public function postSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $stockRecount = $event->getData();

        // проверка на то, нет ли позиций в инвентаризации с одинаковыми параметрами стока и продукта
        $uniqueStockRecountEntries = array();
        
        foreach($stockRecount->getStockRecountEntries() as $stockRecountEntry) {
            if(!isset($uniqueStockRecountEntries[$stockRecountEntry->getProductId() . $stockRecountEntry->getStockParams()])) {
                $uniqueStockRecountEntries[$stockRecountEntry->getProductId() . $stockRecountEntry->getStockParams()] = true;
            }
            else {
                $form->addError(new FormError('В инвентаризации присутствуют позиции с одинаковыми параметрами'));
                break;
            }
        }
    }
}

<?php
namespace Nitra\MainBundle\Controller\DeliveryWarehouse;

use Admingenerated\NitraMainBundle\BaseDeliveryWarehouseController\ListController as BaseListController;

class ListController extends BaseListController
{

    protected function buildQuery()
    {
        return $this->getDoctrine()->getManager()
            ->getRepository('NitraMainBundle:Warehouse')
            ->buildQueryDeliveryWarehouses();
    }

}

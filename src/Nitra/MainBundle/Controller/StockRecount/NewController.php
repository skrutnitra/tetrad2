<?php
namespace Nitra\MainBundle\Controller\StockRecount;

use Admingenerated\NitraMainBundle\BaseStockRecountController\NewController as BaseNewController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\MainBundle\Entity\StockRecount;
use Nitra\MainBundle\Entity\StockRecountEntry;
use Nitra\MainBundle\Form\Type\StockRecount\NewType;
use Nitra\MainBundle\Form\Type\StockRecount\NewAtWarehouseType;
use Nitra\MainBundle\Form\Type\StockRecount\SearchProductFiltersType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Form;
use Nitra\MainBundle\Exception\NotValidForSaveException;


class NewController extends BaseNewController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * склад инвентаризации
     */
    private $warehouse;
    
    /**
     * отображение формы создания инвентаризации 
     */
    public function indexAction()
    {
        
        // проверить склад инвентаризации
        if (!$this->getRequest()->get('warehouse')) {
            // склад не указан, перенаправить пользователя на форму инвентаризации выбора склада инвентаризации 
            return new RedirectResponse($this->generateUrl('Nitra_MainBundle_StockRecount_newAtWarehouse'));
        }
        
        // получить склад 
        $warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($this->getRequest()->get('warehouse'));
        if (!$warehouse) {
            // склад не найден // перенаправить пользователя на форму инвентаризации выбора склада инвентаризации 
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("Склад не найден.", array(), 'Admingenerator') );
            return new RedirectResponse($this->generateUrl('Nitra_MainBundle_StockRecount_newAtWarehouse'));
        }
        
        // вызвать родителя
        return parent::indexAction();
    }
    
    public function createAction()
    {
        $StockRecount = $this->getNewObject();
        $this->preBindRequest($StockRecount);
        $form = $this->createForm($this->getNewType(), $StockRecount);
        $form->bind($this->get('request'));
        
        // дополнительная проверка вложеных форм
        $childrenIsValid = $this->isFormChildrenValid($form);
        
        if ($form->isValid() && $childrenIsValid) {
            try {
                $this->preSave($form, $StockRecount);
                $this->saveObject($StockRecount);
                $this->postSave($form, $StockRecount);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_edit", array('pk' => $StockRecount->getId()) ));
            } 
            catch (NotValidForSaveException $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans($e->getMessage(), array(), 'Admingenerator') );
            }
            catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $StockRecount);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('NitraMainBundle:StockRecountNew:index.html.twig', $this->getAdditionalRenderParameters($StockRecount) + array(
            "StockRecount" => $StockRecount,
            "form" => $form->createView(),
        ));
    }
    
    /**
     * форма инвентаризации склада
     * @Route("/new-at-warehouse", name="Nitra_MainBundle_StockRecount_newAtWarehouse")
     * @Template("NitraMainBundle:StockRecountNew:formAtWarehouse.html.twig")
     */
    public function newAtWarehouseAction() {
        
        // объект формы 
        $form = $this->createForm(new NewAtWarehouseType());
        // обработка формы
        if ($this->getRequest()->getMethod() == 'POST') {
            
            // заполнить форму 
            $form->bind($this->getRequest());
            
            // проверить валидность формы
            if ($form->isValid()) {
                
                // получить обект данных форм
                $formData = $form->getData();
            
                // перенапраивть польхователя на форму инвентаризируемого склада
                return new RedirectResponse($this->generateUrl('Nitra_MainBundle_StockRecount_new', array('warehouse'=>$formData['warehouse']->getId())));
                    
            } else {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
            }

        }
        
        // передать массив параметров в шаблон 
        return array(
            "form" => $form->createView(),
        );
    }
    
    /**
     * получить объект формы инвентаризации
     */
    protected function getNewType()
    {
        $type = new NewType($this->em);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    
    /**
     * получить объект новой инвентаризации 
     * @return Nitra\MainBundle\Entity\StockRecount объект новой инвентаризации 
     */
    protected function getNewObject()
    {
        // создать инвентаризацию 
        $newObject = parent::getNewObject();
        
        // получить объект инвентаризации для POST контроллера (createAction)
        if ($this->getRequest()->getMethod() == 'POST') {
            
            // получить тип формы
            $formType = $this->getNewType();
            
            // получть данные формы 
            $formData = $this->getRequest()->get($formType->getName());
            
            // запомнить склад по которому создаем инвентаризацию 
            $this->warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($formData['warehouse']);
            
        } else {
            // склад инвентаризации указан в URL запросе (indexAction)
            // запомнить склад по которому создаем инвентаризацию 
            $this->warehouse = $this->em->getRepository('NitraMainBundle:Warehouse')->find($this->getRequest()->get('warehouse', false));
        }
        
        // вернуть объект новой инвентаризации
        return $newObject;
    }

    /**
     * создать форму 
     * Creates and returns a Form instance from the type of the form.
     * @param string|FormTypeInterface $type    The built type of the form
     * @param mixed                    $data    The initial data for the form
     * @param array                    $options Options for the form
     * @return Form
     */
    public function createForm($type, $data = null, array $options = array())
    {
        // создание новой формы по инвентаризации 
        if ($type instanceof NewType && $data instanceof StockRecount) {
            // установить склад для объекта инвентаризации 
            $data->setWarehouse($this->warehouse);

            // получить продукты на складе
            $stocks = $this->dm->getRepository('NitraMainBundle:Stock')->getProductsAtWarehouse($this->warehouse);

            foreach($stocks as $stock) {
                $stockRecountEntry = new StockRecountEntry();
                $stockRecountEntry->setProductId($stock->getProductId());
                $stockRecountEntry->setStockId($stock->getId());
                $stockRecountEntry->setStockParams($stock->getStockParams());
                $stockRecountEntry->setQuantity($stock->getQuantity());
                $stockRecountEntry->setDelta($stock->getQuantity());
                $stockRecountEntry->setStockRecount($data);
                $data->addStockRecountEntry($stockRecountEntry);
            }
        }
        
        // родитель создания формы
        return parent::createForm($type, $data, $options);
    }
    
    /**
     * Get additional parameters for rendering.
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount 
     * @return array
     **/
    protected function getAdditionalRenderParameters(\Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        
        // массив названий продуктов
        $productNames = array();
        
        // массив названий категорий продуктов
        $categoryNames = array();
        
        // массив ID продуктов 
        $productIds = array();
        
        // наполнить массив ID продуктов из объекта
        foreach($StockRecount->getStockRecountEntries() as $entry) {
            $productIds[] = $entry->getProductId();
        }
        
        // получить список продуктов по массиву ID 
        $productIds = array_unique($productIds);
        if ($productIds) {
            // объект запроса список проудуктов
            $mongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
            $mongoQuery->field('id')->in($productIds);
            
            // получить продукты
            $products = $mongoQuery
                ->getQuery()
                ->execute();
            
            // наполнить массив имен продуктов
            if ($products) {
                foreach($products as $product) {
                    $productNames[$product->getId()] = (string)$product;
                    $categoryNames[$product->getId()] = (string) $product->getCategory();
                }
            }
        }
        
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // массив названий продуктов
            'stockRecountEntriesProductNames' => $productNames,
            // массив названий категорий продуктов
            'stockRecountEntriesCategoryNames' => $categoryNames,
        );
    }
    
    /**
     * preSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount your \Nitra\MainBundle\Entity\StockRecount object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        // получить данные формы 
        $formData = $this->getRequest()->get($form->getName());
        
        // валидировать инвентаризацию 
        $errorMessage = $this->em->getRepository('NitraMainBundle:StockRecount')->validForAccept($StockRecount);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }

        // связать позиции $recountEntry с объектом инвентаризации $StockRecount
        foreach ($StockRecount->getStockRecountEntries() as $recountEntry) {

            $recountEntry->setStockRecount($StockRecount);
            $this->em->persist($recountEntry);
        }

    }
    
    /**
     * postSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount your \Nitra\MainBundle\Entity\StockRecount object
     */
    public function postSave(\Symfony\Component\Form\Form $form, \Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        // получить данные формы 
        $formData = $this->getRequest()->get($form->getName());

        // проверить нажата кнопка сохранить и принять инвентаризацию 
        if ($formData['saveAndAccept']) {
            
            // принять инвентаризацию 
            $this->em->getRepository('NitraMainBundle:StockRecount')->acceptStockRecount($StockRecount);
                    
            // сохранить подписанную инвентаризацию 
            $this->em->flush();
            
            // сохранить dm
            try {
                $this->dm->flush();
            }
            // при ошибке сохранения стоков убираем у инвентаризации флаг подписи
            catch (\Exception $e) {
                $StockRecount->setIsAccepted(false);
                $this->em->flush();
                throw new \Exception($e->getMessage());
            }
        }
    }
    
    /**
     * форма добавления новых продуктов на склад
     * @Route("/add-new-products-{warehouseId}", name="Nitra_MainBundle_StockRecount_AddNewProducts")
     * @ParamConverter("warehouse", class="NitraMainBundle:Warehouse", options={"id" = "warehouseId"})
     * @Template("NitraMainBundle:StockRecountEdit:AddNewProducts.html.twig")
     */
    public function AddNewProducts(\Nitra\MainBundle\Entity\Warehouse $warehouse) 
    {
        // фильтр поиска по продуктам
        $filterSearchType = new SearchProductFiltersType($this->dm);
        $filterSearch = $this->createForm($filterSearchType);
        
        return array(
            'filterSearch' => $filterSearch->createView(),
        );
    }
    
    /**
     * выполняет валидацию вложеных форм, добавляет для отображения ошибки в формы
     * @param object Symfony\Component\Form\Form $form
     * @return true | false 
    **/
    private function isFormChildrenValid(Form $form)
    {
        // статическое кол-во ошибок формы, так как функция выполняется рекурсивно
        static $form_errors = 0;

        $validator = $this->container->get('validator');
        
        // рекурсивный вызов функции если форма содержит свои вложенные формы
        foreach($form->all() as $child) {
            if($child->all()) {
                $this->isFormChildrenValid($child);
            }
        }
        
        // валидация обьекта из формы формы
        $errors = $validator->validate($form->getData());
        
        // добавление в форму ошибок для отображения (только по полям указаным в форме)
        foreach($errors as $error) {
            foreach($form->all() as $formField) {
                if($error->getPropertyPath() === (string) $formField->getPropertyPath()) {
                    $formField->addError(new \Symfony\Component\Form\FormError($error->getMessage()));
                    $form_errors += 1;
                }
            }
        }

        return ($form_errors > 0) ? false : true;
        
    }
}

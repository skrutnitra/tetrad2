<?php
namespace Nitra\MainBundle\Controller\StockRecount;

use Admingenerated\NitraMainBundle\BaseStockRecountController\EditController as BaseEditController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\MainBundle\Form\Type\StockRecount\EditType;
use Nitra\MainBundle\Form\Type\StockRecount\SearchProductFiltersType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Nitra\MainBundle\Exception\NotValidForSaveException;
    
class EditController extends BaseEditController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * получить объект форма инвентаризации
     */
    protected function getEditType()
    {
        $type = new EditType($this->em);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    
    public function updateAction($pk)
    {
        $StockRecount = $this->getObject($pk);

        if (!$StockRecount) {
            throw new NotFoundHttpException("The Nitra\MainBundle\Entity\StockRecount with id $pk can't be found");
        }

        $this->preBindRequest($StockRecount);
        $form = $this->createForm($this->getEditType(), $StockRecount);
        $form->bind($this->get('request'));

        // дополнительная проверка вложеных форм
        $childrenIsValid = $this->isFormChildrenValid($form);
        
        if ($form->isValid() && $childrenIsValid) {
            try {
                
                $this->preSave($form, $StockRecount);
                $this->saveObject($StockRecount);
                $this->postSave($form, $StockRecount);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_edit", array('pk' => $pk) ));
                } 
                catch (NotValidForSaveException $e) {
                    $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans($e->getMessage(), array(), 'Admingenerator') );
                }
                catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                    $this->onException($e, $form, $StockRecount);
                }

        } else {
            $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('NitraMainBundle:StockRecountEdit:index.html.twig', $this->getAdditionalRenderParameters($StockRecount) + array(
            "StockRecount" => $StockRecount,
            "form" => $form->createView(),
        ));
    }
    
    /**
     * Get additional parameters for rendering.
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount 
     * @return array
     **/
    protected function getAdditionalRenderParameters(\Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        
        // массив названий продуктов
        $productNames = array();
        
        // массив названий категорий продуктов
        $categoryNames = array();
        
        // массив ID продуктов 
        $productIds = array();
        
        // наполнить массив ID продуктов из объекта
        foreach($StockRecount->getStockRecountEntries() as $entry) {
            $productIds[] = $entry->getProductId();
        }
        
        // получить список продуктов по массиву ID 
        $productIds = array_unique($productIds);
        if ($productIds) {
            
            // объект запроса список проудуктов
            $mongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
            $mongoQuery->field('id')->in($productIds);
            
            // получить продукты
            $products = $mongoQuery
                ->getQuery()
                ->execute();
            
            // наполнить массив имен продуктов
            if ($products) {
                foreach($products as $product ) {
                    $productNames[$product->getId()] = (string)$product;
                    $categoryNames[$product->getId()] = (string) $product->getCategory();
                }
            }
        }
        
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // массив названий продуктов
            'stockRecountEntriesProductNames' => $productNames,
            // массив названий категорий продуктов
            'stockRecountEntriesCategoryNames' => $categoryNames,
        );
    }
    
    
    /**
     * preSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount your \Nitra\MainBundle\Entity\StockRecount object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        // валидировать инвентаризацию 
        $errorMessage = $this->em->getRepository('NitraMainBundle:StockRecount')->validForAccept($StockRecount);
        
        if ($errorMessage) {
            throw new NotValidForSaveException($errorMessage);
        }
        
        // связать позиции $recountEntry с объектом инвентаризации $StockRecount
        foreach ($StockRecount->getStockRecountEntries() as $recountEntry) {
            $recountEntry->setStockRecount($StockRecount);
            $this->em->persist($recountEntry);
        }
    }
     
    /**
     * postSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount your \Nitra\MainBundle\Entity\StockRecount object
     */
    public function postSave(\Symfony\Component\Form\Form $form, \Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        // получть данные формы 
        $formData = $this->getRequest()->get($form->getName());
        
        // проверить нажата кнопка сохранить и принять инвентаризацию 
        if ($formData['saveAndAccept']) {
            
            // принять инвентаризацию 
            $this->em->getRepository('NitraMainBundle:StockRecount')->acceptStockRecount($StockRecount);
            
            // сохранить em
            $this->em->flush();
            
            // сохранить dm
            try {
                $this->dm->flush();
            }
            // при ошибке сохранения стоков убираем у инвентаризации флаг подписи
            catch (\Exception $e) {
                $StockRecount->setIsAccepted(false);
                $this->em->flush();
                throw new \Exception($e->getMessage());
            }
        }
    }
    
    /**
     * выполняет валидацию вложеных форм, добавляет для отображения ошибки в формы
     * @param object Symfony\Component\Form\Form $form
     * @return true | false 
    **/
    private function isFormChildrenValid(Form $form)
    {
        // статическое кол-во ошибок формы, так как функция выполняется рекурсивно
        static $form_errors = 0;

        $validator = $this->container->get('validator');
        
        // рекурсивный вызов функции если форма содержит свои вложенные формы
        foreach($form->all() as $child) {
            if($child->all()) {
                $this->isFormChildrenValid($child);
            }
        }
        
        // валидация обьекта из формы формы
        $errors = $validator->validate($form->getData());
        
        // добавление в форму ошибок для отображения (только по полям указаным в форме)
        foreach($errors as $error) {
            foreach($form->all() as $formField) {
                if($error->getPropertyPath() === (string) $formField->getPropertyPath()) {
                    $formField->addError(new \Symfony\Component\Form\FormError($error->getMessage()));
                    $form_errors += 1;
                }
            }
        }

        return ($form_errors > 0) ? false : true;
        
    }
}

<?php
namespace Nitra\MainBundle\Controller\StockRecount;

use Admingenerated\NitraMainBundle\BaseStockRecountController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\MainBundle\Form\Type\StockRecount\SearchProductFiltersType;
use Symfony\Component\HttpFoundation\Response;

class ActionsController extends BaseActionsController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * автокомплит поиска товаров 
     * @Route("/search-product-autocomplete-test", name="Nitra_MainBundle_StockRecount_Search_Product_Autocomplete_Test")
     */
    public function searchProductAutocompleteTestAction()
    {
        // получаем строку запроса
        $productName = $this->getRequest()->get('q');
        
         // объект QueryBuilder поиск продуктов
        $productsMongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
        
        // Фраза поиска по продуктам
        $productName = trim($productName);
        if ($productName) {
            // Исключаем лишние пробелы
            $productName = preg_replace("/[[:blank:]]+/", ' ', $productName);
            // Если введено несколко фраз - разбиваем по пробелу
            if (strstr($productName, ' ')) {
                // Поиск с учетом регистра всех вхождений
                $productNameRegex = '/(?=.*' . str_replace(' ', ')(?=.*', $productName) . ')/i';
            } else {
                $productNameRegex = '/' . $productName . '/i';
            }

            $productsMongoQuery->field('name')->equals(new \MongoRegex($productNameRegex));
        }
        
        // получить список товаров удовлетворяющих условиям поиска
        $productsMongo = $productsMongoQuery
            ->sort('name', 'asc')
            ->getQuery()
            ->execute();      
        
        // результирующий массив 
        $autocomplete_return = array();

        // создать данные в результирующем массиве 
        foreach($productsMongo as $product) {
            $current_product_str =   (string) $product->getCategory() . '/' . (string) $product . "|" 
                                    . (string) $product . "|" 
                                    . $product->getId() . '|' 
                                    . json_encode(array(
                                        'category' => (string) $product->getCategory(), 
                                        'name' => (string) $product, 
                                        'id' => $product->getId()
                                    ));
            
            $autocomplete_return[] = $current_product_str;
        }
        
        if(empty($autocomplete_return)) {
            $autocomplete_return[] = 'продукты не найдены' . "|" . "" . '|' . json_encode(array());
        }
            
        return new Response(implode("\n", $autocomplete_return));
    }
       
    /**
     * удалить инвентаризацию
     * @param Nitra\MainBundle\Entity\StockRecount $StockRecount - объект инвентаризации
     */
    protected function executeObjectDelete(\Nitra\MainBundle\Entity\StockRecount $StockRecount)
    {
        // нельзя удалить подписанную инвентаризацию 
        if ($StockRecount->getIsAccepted()) {
            throw new \Exception('Нельзя удалить подписанную инвентаризацию.');
        }
        
        // удалить инвентаризацию 
        parent::executeObjectDelete($StockRecount);
    }
    
    /**
     * Ошибка удаления инвентаризации
     * @param \Exception $e Exception
     * @param \Nitra\MainBundle\Entity\StockRecount $StockRecount Your \Nitra\MainBundle\Entity\StockRecount object
     * @return Response Must return a response!
     */
    protected function errorObjectDelete(\Exception $e, \Nitra\MainBundle\Entity\StockRecount $StockRecount = null)
    {
        $this->get('session')->getFlashBag()->add(
            'error',
            $this->get('translator')->trans("action.object.delete.error", array('%name%' => 'delete'),'Admingenerator')
            .' ' .$e->getMessage()
        );
        
        // перенаправить пользователя на список 
        return new RedirectResponse($this->generateUrl("Nitra_MainBundle_StockRecount_list"));
    }
    
    
}

<?php

namespace Nitra\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Request;

class AutocompleteController extends Controller
{
        /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
     /**
     * Автоподсказка для товаров
     * @Route("/get-product-autocomplete", name="Product_Autocomplete")
     * 
     */ 
    public function productAutocompleteAction () {
   
        // То что приходит из поля ввода (строка поиска)
        $search_str = $this->getRequest()->get('q');
        
        $names = preg_split('/\s+/', trim($search_str));
        
        $products = $this->dm->createQueryBuilder('NitraMainBundle:Product')
                ->field('name')->equals(new \MongoRegex('/(' . implode('|', $names) . ')/i'))
                ->limit(20)
                ->getQuery()
                ->execute();
        
        $autocomplete_return = array();
        
        foreach($products as $product){
            $autocomplete_return[] = $product->getName() . "|" . $product->getName() . "|" . $product->getId() . '|';
        }
        
        return new Response( implode("\n", $autocomplete_return) );
   
    }
    
     /**
     * Автоподсказка для категорий
     * @Route("/get-category-autocomplete", name="Category_Autocomplete")
     * 
     */ 
    public function categoryAutocompleteAction () {
   
        // То что приходит из поля ввода (строка поиска)
        $search_str = $this->getRequest()->get('q');
        
        $names = preg_split('/\s+/', trim($search_str));
        
        $categories = $this->dm->createQueryBuilder('NitraMainBundle:Category')
                ->field('name')->equals(new \MongoRegex('/(' . implode('|', $names) . ')/i'))
                ->limit(20)
                ->getQuery()
                ->execute();
        
        $autocomplete_return = array();
        
        foreach($categories as $category){
            $autocomplete_return[] = $category->getName() . "|" . $category->getName() . "|" . $category->getId() . '|';
        }
        
        return new Response( implode("\n", $autocomplete_return) );
   
    }
    
    /**
     * Автоподсказка для бренда
     * @Route("/get-brand-autocomplete", name="Brand_Autocomplete")
     * 
     */ 
    public function brandAutocompleteAction () {
   
        // То что приходит из поля ввода (строка поиска)
        $search_str = $this->getRequest()->get('q');
        
        $names = preg_split('/\s+/', trim($search_str));
        
        $brands = $this->dm->createQueryBuilder('NitraMainBundle:Brand')
                ->field('name')->equals(new \MongoRegex('/(' . implode('|', $names) . ')/i'))
                ->limit(20)
                ->getQuery()
                ->execute();
        
        $autocomplete_return = array();
        
        foreach($brands as $brand){
            $autocomplete_return[] = $brand->getName() . "|" . $brand->getName() . "|" . $brand->getId() . '|';
        }
        
        return new Response( implode("\n", $autocomplete_return) );
   
    }
}

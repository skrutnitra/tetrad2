<?php
namespace Nitra\MainBundle\Controller\StockWarehouse;

use Admingenerated\NitraMainBundle\BaseStockWarehouseController\EditController as BaseEditController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\MainBundle\Form\Type\StockWarehouse\EditType;

class EditController extends BaseEditController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * getEditType
     */
    protected function getEditType()
    {
        
        $type = new EditType($this->em, $this->dm);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    
    /**
     * получить массив дополнительных параметров передаваемых в шаблон
     * @param \Nitra\MainBundle\Document\Stock $Stock объект стока
     * @return array
     */
    protected function getAdditionalRenderParameters(\Nitra\MainBundle\Document\Stock $Stock)
    {
        // вернуть массив параметров передаваемых в шаблон
        return array(
            // объект склада
            'Warehouse' => $this->em->getRepository('NitraMainBundle:Warehouse')->find($Stock->getWarehouseId()),
            // объект продукта
            'Product' => $this->dm->getRepository('NitraMainBundle:Product')->find($Stock->getProductId()),
        );
    }
    
    
}

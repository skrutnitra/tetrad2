<?php

namespace Nitra\MainBundle\Controller\StockWarehouse;

use Admingenerated\NitraMainBundle\BaseStockWarehouseController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nitra\MainBundle\Form\Type\StockWarehouse\CommentType;
use \Symfony\Component\HttpFoundation\Response;
use PHPExcel;
use PHPExcel_IOFactory;

class ActionsController extends BaseActionsController
{

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**
     * добавление коментаряи для стока
     * @Route("/comment-add", name="Nitra_MainBundle_StockWarehouse_Comment_Add")
     * @return JsonResponse
     */
    public function stockWarehouseCommentAddAction()
    {

        // объект формы 
        $form = $this->createForm(new CommentType($this->dm));

        // обработка формы
        if ($this->getRequest()->getMethod() == 'POST') {

            // заполнить форму 
            $form->bind($this->getRequest());

            // получить обект данных форм
            $formData = $form->getData();

            // проверить валидность формы
            if ($form->isValid()) {

                // получить объект стока
                $stock = $this->dm->getRepository('NitraMainBundle:Stock')->find($formData['id']);
                if (!$stock) {
                    // сток не найден, вернуть массив ошибок 
                    return new JsonResponse(array('type' => 'error', 'message' => "Сток не найден."));
                }

                // проверить комментарий
                if (is_null($formData['comment'])) {
                    // вернуть ошибку
                    return new JsonResponse(array('type' => 'error', 'message' => "Добавьте комментарий."));
                }

                // сохранить 
                try {

                    // автоинкримент комментария
                    $stock = $stock->addComment($formData['comment'], $this->getUser()->getUsername());

                    // сохранить 
                    $this->dm->flush();
                } catch (\Exception $e) {
                    // вернуть ошибку
                    return new JsonResponse(array('type' => 'error', 'message' => $e->getMessage()));
                }

                // комментарий успешно добавлен, вернуть текст комментария
                return new JsonResponse(array('type' => 'success', 'id' => $stock->getId(), 'comment' => $stock->getComment()));
            } else {
                // отображение ошибки валидации формы
                return new JsonResponse(array('type' => 'error', 'message' => $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator')));
            }
        }

        // вернуть пустой ответ
        return new JsonResponse();
    }

    /**
     * @Route("/grouped-exel", name="Nitra_MainBundle_StockWarehouse_Grouped_Excel")
     */
    public function stockWarehouseGroupedProduct()
    {
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ),
            )
        );
        $excel = new PHPExcel();
        $currentRow = 1;
        $currentCol = 0;
        $sheetIndex = 0;
        //Находим свои склады
        $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')
                ->buildQueryOwnWarehouses()
                ->getQuery()
                ->execute();
        //Репозитории которые нам понадобятся
        $stocksRepo = $this->dm->getRepository("NitraMainBundle:Stock");
        $productRepo = $this->dm->getRepository("NitraMainBundle:Product");
        $currencyRepo = $this->em->getRepository("NitraMainBundle:Currency");

        foreach ($warehouses as $warehouse) {
            //Начальная разметка
            $warehouseStocks = $stocksRepo->getProductsAtWarehouse($warehouse);
            $countStocks = count($warehouseStocks);
            $excel->setActiveSheetIndex($sheetIndex);
            $excel->getDefaultStyle()->getFont()->setName("Arial");
            $excel->getDefaultStyle()->getFont()->setSize(10);
            $fActiveSheet = $excel->getActiveSheet();
            $fActiveSheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4)
                    ->setFitToWidth(1)
                    ->setFitToHeight(1);
            $head = $warehouse->getCity() . " " . $warehouse->getAddress();

            $fActiveSheet->getColumnDimension('A')->setWidth(15);
            $fActiveSheet->getColumnDimension('B')->setWidth(40);
            $fActiveSheet->getColumnDimension('C')->setWidth(15);
            $fActiveSheet->getColumnDimension('D')->setWidth(20);
            $fActiveSheet->getColumnDimension('E')->setWidth(15);
            $fActiveSheet->getColumnDimension('F')->setWidth(15);

            $fActiveSheet->setTitle($head);
            if ($countStocks > 0) {
                $fActiveSheet->mergeCells("A" . $currentRow . ":F" . $currentRow);
                $fActiveSheet->setCellValue('A' . $currentRow, 'Общий остаток матрериалов по складу ' . $head);
                $fActiveSheet->getStyle('A' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('A' . $currentRow)->getFont()->setBold(true)->setSize(13);
                $currentRow++;
                $fActiveSheet->setCellValue('A' . $currentRow, 'Категория');
                $fActiveSheet->getStyle('A' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('A' . $currentRow)->getFont()->setBold(true)->setSize(12);

                $fActiveSheet->setCellValue('B' . $currentRow, 'Товар');
                $fActiveSheet->getStyle('B' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('B' . $currentRow)->getFont()->setBold(true)->setSize(12);

                $fActiveSheet->setCellValue('C' . $currentRow, 'Количество');
                $fActiveSheet->getStyle('C' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('C' . $currentRow)->getFont()->setBold(true)->setSize(12);

                $fActiveSheet->setCellValue('D' . $currentRow, 'Зарезервировано');
                $fActiveSheet->getStyle('D' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('D' . $currentRow)->getFont()->setBold(true)->setSize(12);

                $fActiveSheet->setCellValue('E' . $currentRow, 'Цена вх');
                $fActiveSheet->getStyle('E' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('E' . $currentRow)->getFont()->setBold(true)->setSize(12);

                $fActiveSheet->setCellValue('F' . $currentRow, 'Цена вых');
                $fActiveSheet->getStyle('F' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('F' . $currentRow)->getFont()->setBold(true)->setSize(12);

                $currentRow++;
            } else {
                $fActiveSheet->mergeCells("A" . $currentRow . ":F" . $currentRow);
                $fActiveSheet->setCellValue('A' . $currentRow, 'На складе ' . $head . " нет остатков");
                $fActiveSheet->getStyle('A' . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle('A' . $currentRow)->getFont()->setBold(true)->setSize(13);
            }

            foreach ($warehouseStocks as $stock) {
                //Находим категорию
                $productCategory = $productRepo->find($stock->getProductId())
                        ->getCategory()
                        ->getName();
                $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, $productCategory);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getFont()->setSize(10);
                $currentCol++;
                $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, $stock->getPriceName());
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getFont()->setSize(10);
                $currentCol++;
                $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, $stock->getQuantity() . " шт.");
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getFont()->setSize(10);
                $currentCol++;
                $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, $stock->getReserved() . " шт.");
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getFont()->setSize(10);
                $currentCol++;
                $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, number_format($stock->getPriceIn(),2, ',', ' ') . " " . $currencyRepo->findOneBy(array('code' => $stock->getCurrency()))->getSymbol());
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getFont()->setSize(10);
                $currentCol++;
                $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, number_format($stock->getAvgPriceOut(),2, ',', ' ') . " " . $currencyRepo->findOneBy(array('code' => $stock->getCurrency()))->getSymbol());
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getFont()->setSize(10);
                $currentRow++;
                $currentCol = 0;
            }
            if ($countStocks > 0) {
                $fActiveSheet->getStyle('A1:F' . ($currentRow - 1))->applyFromArray(array_merge($styleArray));
            }
            $excel->createSheet($sheetIndex + 1);
            $sheetIndex++;
            $currentRow = 1;
            $currentCol = 0;
        }

        $excel->setActiveSheetIndex(0);
        $extension = "xls";
        $filename = 'grouped_products';

        $response = new Response();
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', "attachment;filename=$filename.$extension");
        $response->headers->set('Cache-Control', 'max-age=0');

        $response->sendHeaders();
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    /**
     * Незарезервированые остатки
     * @Route("/unreserved-exel", name="Nitra_MainBundle_StockWarehouse_Unreserved")
     */
    public function stockWarehouseUnreservedExcel()
    {

        $productsOnWarehouses = array();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ),
            )
        );
        $excel = new PHPExcel();
        //Начальная разметка
        $excel->setActiveSheetIndex(0);
        $excel->getDefaultStyle()->getFont()->setName("Arial");
        $excel->getDefaultStyle()->getFont()->setSize(10);
        $fActiveSheet = $excel->getActiveSheet();
        $fActiveSheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
                ->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4)
                ->setFitToWidth(1)
                ->setFitToHeight(1);
        //Находим свои склады
        $warehouses = $this->em->getRepository('NitraMainBundle:Warehouse')
                ->buildQueryOwnWarehouses()
                ->getQuery()
                ->execute();
        //Репозитории которые нам понадобятся
        $stocksRepo = $this->dm->getRepository("NitraMainBundle:Stock");
        $productRepo = $this->dm->getRepository("NitraMainBundle:Product");
        // Наши колонки
        $fActiveSheet->getColumnDimension('A')->setWidth(40);  // Категория
        $fActiveSheet->getColumnDimension('B')->setWidth(40);  // Товар
        $fActiveSheet->getColumnDimension('C')->setWidth(30);  
        $currentRow = 1;
        $fActiveSheet->mergeCells('A' . $currentRow . ':H' . $currentRow);
        $fActiveSheet->setCellValue('A' . $currentRow, 'Незарезервированные остатки в разрезе складов');
        $fActiveSheet->getStyle('A' . $currentRow)->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $fActiveSheet->getStyle('A' . $currentRow)->getFont()->setBold(true)->setSize(14);
        $currentRow += 1;
        $fActiveSheet->setCellValue('A' . $currentRow, 'Категория');
        $fActiveSheet->getStyle('A' . $currentRow)->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $fActiveSheet->getStyle('A' . $currentRow)->getFont()->setBold(true);
        $fActiveSheet->setCellValue('B' . $currentRow, 'Товар');
        $fActiveSheet->getStyle('B' . $currentRow)->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $fActiveSheet->getStyle('B' . $currentRow)->getFont()->setBold(true);

        $currentCol = 3;

//        вывод шапки (склады)
        $left_border = 2;
        //Бужим по нашим складам
        foreach ($warehouses as $key => $warehouse) {
            //Отрисовываем склады 
            $head = $warehouse->getCity() . " " . $warehouse->getAddress();
            $fActiveSheet->getColumnDimension($this->getExcelColumn($currentCol))->setWidth(strlen($head));
            $fActiveSheet->setCellValue($this->getExcelColumn($left_border) . $currentRow, $head);
            $fActiveSheet->getStyle($this->getExcelColumn($left_border) . $currentRow)->getAlignment()
                    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $fActiveSheet->getStyle($this->getExcelColumn($left_border) . $currentRow)->getFont()->setBold(true);
            $stocks = $stocksRepo->getProductsExcel($warehouse);
            //Бужим по стокам
            foreach ($stocks as $stock) {
                //Находим категорию
                $productCategory = $productRepo->find($stock->getProductId())
                                ->getCategory()->getName();
                //Делаем массив для стока и его категории
                $productsOnWarehouses[$stock->getWarehouseId() . "_" . $stock->getProductId()] = array(
                    "stock" => $stock,
                    "productCategory" => $productCategory);
            }
            $left_border = $currentCol;
            $currentCol++;
        }

        //Бежим по свеженькому массивчику
        foreach ($productsOnWarehouses as $key => $product) {
            //Индексы там всякие....
            $currentCol = 2;
            $currentRow++;
            //Считаем количество досупного товарa
            $freeQuantity = $product['stock']->getQuantity() - $product['stock']->getReserved();
            //Отрисовываем торвар и его категорию
            $fActiveSheet->setCellValue('A' . $currentRow, $product['productCategory']);
            $fActiveSheet->setCellValue('B' . $currentRow, $product['stock']->getPriceName());
            //Бежим снова по складам, что бы узнать есть ли на нем текущий товар
            foreach ($warehouses as $key => $warehouse) {

                //Проверяем есть ли, если есть то отврисовываем
                if (isset($productsOnWarehouses[$warehouse->getId() . "_" . $product['stock']->getProductId()])) {

                    $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, $freeQuantity . 'шт. / ' . number_format($product['stock']->getPriceIn(),2, ',', ' ') . ' грн.');
                    $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                }
                //Нет? прочерк...
                else {
                    $fActiveSheet->setCellValue($this->getExcelColumn($currentCol) . $currentRow, '-');
                    $fActiveSheet->getStyle($this->getExcelColumn($currentCol) . $currentRow)->getAlignment()
                            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                //Индекс колонки вверх
                $currentCol++;
            }
        }


        //Применяем стили
        $fActiveSheet->getStyle('A2:' . $this->getExcelColumn($currentCol - 1) . $currentRow)->applyFromArray(array_merge($styleArray));
        $excel->setActiveSheetIndex(0);
        $extension = "xls";
        $filename = 'nezarezervirovannie_tovari';

        $response = new Response();
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', "attachment;filename=$filename.$extension");
        $response->headers->set('Cache-Control', 'max-age=0');

        $response->sendHeaders();
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    //Ищем буковку
    public function getExcelColumn($columnNumber)
    {
        $alphavit = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if (26 > $columnNumber) {
            return substr($alphavit, $columnNumber, 1);
        } else {
            $first = floor($columnNumber / 26);
            $second = fmod($columnNumber, 26);
            return substr($alphavit, $first - 1, 1) . substr($alphavit, $second, 1);
        }
    }

}

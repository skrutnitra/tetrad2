<?php

namespace Nitra\MainBundle\Controller\StockWarehouse;

use Admingenerated\NitraMainBundle\BaseStockWarehouseController\ListController as BaseListController;
use JMS\DiExtraBundle\Annotation as DI;

class ListController extends BaseListController
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /**
     * @var Pagerfanta\Pagerfanta объект постраничного ввода
     */
    private $paginator;

    public function indexAction()
    {
        $this->getQuery();
        $this->parseRequestForPager();

        $form = $this->getFilterForm();
        return $this->render('NitraMainBundle:StockWarehouseList:index.html.twig', $this->getAdditionalRenderParameters() + array(
                    'Stocks' => $this->getPager(),
                    'form' => $form->createView(),
                    'sortColumn' => $this->getSortColumn(),
                    'sortOrder' => $this->getSortOrder(),
                    'scopes' => $this->getScopes(),
        ));
    }

    /**
     * получить объект постраничного вывода
     * @return Pagerfanta\Pagerfanta
     */
    protected function getPager()
    {
        // проверить иницилиализацию постраничного вывода
        if (!is_null($this->paginator)) {
            // вернуть постраничный вывод
            return $this->paginator;
        }

        // инициализировать постраничный вывод родителем
        $this->paginator = parent::getPager();

        // вернуть 
        return $this->paginator;
    }

    /**
     * получить массив дополнительных параметров передаваемых в шаблон
     * @return array
     */
    public function getAdditionalRenderParameters()
    {

        // массив складов отображенных на странице
        $warehouseAddress = array();

        // массив названий продкутов
        $productNames = array();

        // массив цен по магазинам
        $pricesForStores = array();

        // масссив всех магазинов
        $stores = array();

        // получить массив всех магазинов
        $storesBase = $this->dm->getRepository('NitraMainBundle:Store')->findAll();
        foreach ($storesBase as $store) {
            $stores[$store->getId()] = $store->getName();
        }

        // наполнить массивы 
        foreach ($this->getPager()->getIterator() as $stock) {
            // массив ID отображенных складов
            $warehouseAddress[] = $stock->getWarehouseId();

            // массив ID отображенных продуктов
            $productNames[] = $stock->getProductId();

            // если у магазинов в стоке одинаковая цена, то выводить одну цену для всех магазинов, есил нет - сформировать массив магазин => выходная цена
            $storePrices = $stock->getStorePrice();
            $differentPrices = false;
            // запоминаем цену по первому магазину для дальнейшего сравнения 
            $firstStorePrice = current($storePrices)['price'];

            foreach ($storePrices as $storeId => $price) {
                if ($price['price'] != $firstStorePrice) {
                    $differentPrices = true;
                }
                $pricesForStores[$stock->getId()][$stores[$storeId]] = $price['price'];
            }

            if (!$differentPrices) {
                $pricesForStores[$stock->getId()] = $firstStorePrice;
            }
        }

        // проверить массив ID складов
        if ($warehouseAddress) {
            // получить склады отображенные на странице
            $warehouseDisplay = $this->em->getRepository('NitraMainBundle:Warehouse')->findBy(array('id' => $warehouseAddress));
            // массив складов array( ID склада => Адрес склада  )
            $warehouseAddress = array();
            foreach ($warehouseDisplay as $warehouse) {
                $warehouseAddress[$warehouse->getId()] = (string) $warehouse;
            }
        }

        // проверить массив ID продуктов
        if ($productNames) {
            // получить продукты отображенные на странице
            $productDisplay = $this->dm->getRepository('NitraMainBundle:Product')
                    ->createQueryBuilder()
                    ->field('id')->in($productNames)
                    ->getQuery()
                    ->execute();
            // массив продуктов array( ID продукта => название )
            $productNames = array();
            foreach ($productDisplay as $product) {
                $productNames[$product->getId()] = (string) $product;
            }
        }

        // вернуть массив параметров передаваемых в шаблон
        return array(
            // массив складов
            'warehouseAddress' => $warehouseAddress,
            // массив продуктов
            'productNames' => $productNames,
            // массив цен по магазину
            'pricesForStores' => $pricesForStores,
        );
    }

    /**
     * Store in the session service the current filters
     * @param array the filters
     */
    protected function setFilters($filters)
    {

        // установить фильтр по складу
        if (isset($filters['warehouse'])) {
            $filters['warehouse'] = array(
                'id' => $filters['warehouse']->getId(),
                'entityName' => get_class($filters['warehouse'])
            );
        }

        // установить фильтр кто подвязал
        if (isset($filters['bindedBy'])) {
            $filters['bindedBy'] = array(
                'id' => $filters['bindedBy']->getId(),
                'entityName' => get_class($filters['bindedBy'])
            );
        }

        // установить фильт дата подвязки 
        if (isset($filters['bindedAt'])) {

            // фильтр c
            if (isset($filters['bindedAt']['from']) && is_object($filters['bindedAt']['from'])) {
                $midnight = date('Y-m-d', $filters['bindedAt']['from']->getTimestamp()) . ' 00:00:00';
                $filters['bindedAt']['from'] = new \DateTime($midnight);
            }

            // фильтр по 
            if (isset($filters['bindedAt']['to']) && is_object($filters['bindedAt']['to'])) {
                $midnight = date('Y-m-d', $filters['bindedAt']['to']->getTimestamp()) . ' 23:59:59';
                $filters['bindedAt']['to'] = new \DateTime($midnight);
            }
        }

        // запомнить фильтр в сесси 
        $this->get('session')->set('Nitra\MainBundle\StockWarehouseList\Filters', $filters);
    }

    /**
     * Get filters from session
     */
    protected function getFilters()
    {
        // получить фильтр
        $filters = parent::getFilters();

        // фильтр по складу 
        if (isset($filters['warehouse'])) {
            $filters['warehouse'] = $this->em->getRepository($filters['warehouse']['entityName'])->find($filters['warehouse']['id']);
        }

        // фильтр кто подвязал
        if (isset($filters['bindedBy'])) {
            $filters['bindedBy'] = $this->getDoctrine()->getRepository($filters['bindedBy']['entityName'])->find($filters['bindedBy']['id']);
        }


        // вернуть фильтры
        return $filters;
    }

    /**
     * processFilters
     */
    protected function processFilters($query)
    {
        // получить фильтры
        $filterObject = $this->getFilters();

        // обхект запроса
        $queryFilter = $this->get('admingenerator.queryfilter.doctrine_odm');
        $queryFilter->setQuery($query);

        $sessionFilter = $this->get('session')->get('Nitra\MainBundle\StockWarehouseList\Filters');
        if (empty($sessionFilter)) {
            $filterObject['warehouseType'] = 'OWN';
            $this->setFilters($filterObject);
        }

//        // фильтр артикул товара
//        if (isset($filterObject['priceArticle']) && null !== $filterObject['priceArticle']) {
//            $query->field('priceArticle')->equals(new \MongoRegex("/.*".$filterObject['priceArticle'].".*/i"));
//        }
//        
//        // фильтр название товара из прайса
//        if (isset($filterObject['priceName']) && null !== $filterObject['priceName']) {
//            $query->field('priceName')->equals(new \MongoRegex("/.*".$filterObject['priceName'].".*/i"));
//        }
        // фильтр строка поиска
        if (isset($filterObject['priceName']) && null !== $filterObject['priceName']) {
            // поиск по артикулу товара
            $query->addOr($query->expr()->field('priceArticle')->equals(new \MongoRegex("/.*" . $filterObject['priceName'] . ".*/i")));
            // поиск по названию товара
            $query->addOr($query->expr()->field('priceName')->equals(new \MongoRegex("/.*" . $filterObject['priceName'] . ".*/i")));
        }

        // есть фильтр по складу и фильтр для типа складов(Свои, ТК, Поставщиков)
        if (isset($filterObject['warehouse']) && is_object($filterObject['warehouse']) && isset($filterObject['warehouseType'])) {

            // тип выбранного склада
            if ($filterObject['warehouse']->isSupplier()) {
                $warehouseType = 'SUP';
            } else if ($filterObject['warehouse']->isDelivery()) {
                $warehouseType = 'TK';
            } else {
                $warehouseType = 'OWN';
            }

            // выбранный склад совпадает с выбранным типом склада, отображаем его
            if ($warehouseType === $filterObject['warehouseType']) {
                $query->field('warehouseId')->equals($filterObject['warehouse']->getId());
            }
            // склад не совпал, отображаем пустой результат
            else {
                $query->field('_id')->equals(NULL);
            }
        }

        // есть только фильтр по складу
        else if (isset($filterObject['warehouse']) && is_object($filterObject['warehouse'])) {
            $query->field('warehouseId')->equals($filterObject['warehouse']->getId());
        }
        // есть только фильтр для типа складов(Свои, ТК, Поставщиков)
        else if (isset($filterObject['warehouseType'])) {

            $warehouses = array();
            $warRepo = $this->em->getRepository('NitraMainBundle:Warehouse');

            if ($filterObject['warehouseType'] === 'OWN') {
                $warehouses = $warRepo->buildQueryOwnWarehouses()
                        ->select('q.id')
                        ->getQuery()
                        ->getArrayResult();
            } elseif ($filterObject['warehouseType'] === 'TK') {
                $warehouses = $warRepo->buildQueryDeliveryWarehouses()
                        ->select('q.id')
                        ->getQuery()
                        ->getArrayResult();
            } elseif ($filterObject['warehouseType'] === 'SUP') {
                $warehouses = $warRepo->buildQuerySupplierWarehouses()
                        ->select('q.id')
                        ->getQuery()
                        ->getArrayResult();
            }

            $wareKeyes = array_map(function($a) {
                return $a['id'];
            }, $warehouses);

            $query->field('warehouseId')
                    ->in($wareKeyes);
        }

        // фильтр кто подвязал
        if (isset($filterObject['bindedBy']) && is_object($filterObject['bindedBy'])) {
            $query->field('bindedBy')->equals($filterObject['bindedBy']->getUsername());
        }

        // фильтр дата подвязки 
        if (isset($filterObject['bindedAt'])) {

            if ($filterObject['bindedAt']['from'] && !$filterObject['bindedAt']['to']) {
                $query->field('bindedAt')->gte($filterObject['bindedAt']['from']);
            }

            if ($filterObject['bindedAt']['to'] && !$filterObject['bindedAt']['from']) {
                $query->field('bindedAt')->lte($filterObject['bindedAt']['to']);
            }

            if ($filterObject['bindedAt']['to'] && $filterObject['bindedAt']['from']) {
                $query->field('bindedAt')->range($filterObject['bindedAt']['from'], $filterObject['bindedAt']['to']);
            }
        }
    }

    /**
     * запрос получения списка остатков
     * @return Doctrine\ORM\QueryBuilder
     */
    protected function buildQuery()
    {
        // получить запрос родителем
        $query = parent::buildQuery();
        // Товары которые есть на складах
        $query
                ->where("function() { return (this.quantity) > 0; }");
        // вернуть запрос получения списка
        return $query;
    }

}

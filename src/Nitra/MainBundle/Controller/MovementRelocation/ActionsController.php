<?php

namespace Nitra\MainBundle\Controller\MovementRelocation;

use Admingenerated\NitraMainBundle\BaseMovementRelocationController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nitra\MainBundle\Exception\NotValidForSaveException;

class ActionsController extends BaseActionsController
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    
    /**
    *  получить все movementEntry по movement
     * @Route("show-movement-entry", name="Nitra_MainBundle_MovementRelocation_showMovementEntry")
     */
    public function showMovementEntriesByMovement(Request $request){
        
        // Только аякс
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw $this->createNotFoundException('Только для AJAX');
        }
        $pk = $request->request->get('id');
        // если айди не сузществует
        if(!$pk) {
            return new JsonResponse('error');
        }
//        получаем Movement
        $Movement = $this->getObject($pk);
        
         return $this->render('NitraMainBundle:MovementRelocationList:movementEntryRow.html.twig', array(
                'Movement' => $Movement,
                'seeAll' => true
            ));
    }
    
    
    /**
     * поиск товаров 
     * @Route("/search-product-autocomplete-relocation", name="Nitra_MainBundle_MovementRelocation_Search_Product_Autocomplete")
     * @return mixed - если есть результаты для отображения 
     *                 то вернуть шаблон который будет отображен
     *                 иначе вернуть пустой контент
     */
    public function searchProductAutocompleteAction(Request $request)
    {
        // Получить массив данных для поиска 
        $postData = $request->request->get('filters_movement');
        
        // кол-во позиций за один результат поиска
        $countOnPage = 20;
        
        // страница поиска
        $numberPage = (isset($postData['numberPage']) && $postData['numberPage']) ? $postData['numberPage'] : 0;
        
        // получить объект склада отправителя //переделать на монго Id
        $warehouse = (isset($postData['fromWarehouse']) && $postData['fromWarehouse']) ? $this->em->getRepository('NitraMainBundle:Warehouse')->find($postData['fromWarehouse']) : false;

        // получить id категории продукта
        $category = (isset($postData['category']) && $postData['category']) ? $postData['category'] : false;
        
        // получить фразу поиска по продуктам 
        $searchVal = (isset($postData['name']) && $postData['name']) ? trim($postData['name']) : false;
        
        // поиск id продуктов если были указанны параметры поиска по продуктам (категории или поиск по названию)
        if($category || $searchVal) {
            $mongoProductQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
        }

        // добавить в запрос поиска id продуктов выбранную категорию
        if ($category) {
            $category = new \MongoId($category);
            $mongoProductQuery->field('category.$id')->equals($category);
        }

        // добавить в запрос поиска id продуктов указанную фразу
        if ($searchVal) {
            // Исключаем лишние пробелы
            $searchVal = preg_replace("/[[:blank:]]+/", ' ', $searchVal);

            // Если введено несколко фраз - разбиваем по пробелу
            if (strstr($searchVal, ' ')) {
                // Поиск с учетом регистра всех вхождений
                $searchValRegex = '/(?=.*' . str_replace(' ', ')(?=.*', $searchVal) . ')/i';
            } else {
                $searchValRegex = '/' . $searchVal . '/i';
            }
            // поиск по названию товара
            $mongoProductQuery->addAnd($mongoProductQuery->expr()->field('name')->equals(new \MongoRegex($searchValRegex)));
            
        }

        // делаем поиск по стоку 
        $mongoStockQuery = $this->dm->createQueryBuilder('NitraMainBundle:Stock');

        // добавить в звапрос поиска склад оправителя
        if ($warehouse) {
            $mongoStockQuery->field('warehouseId')->equals($warehouse->getId());
        }
        // если не указан склад отправитель то искать среди своих складов или складов ТК
        else {
            $ownAndDeliveryWarehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->getOwnAndDeliveryWarehouses();
            $warehouses = array(); 
            
            foreach($ownAndDeliveryWarehouses as $ownAndDeliveryWarehouse) {
                array_push($warehouses, $ownAndDeliveryWarehouse->getId());
            }
            
            $mongoStockQuery->field('warehouseId')->in($warehouses);
        }
        
        // если существуют параметры поиска по продуктам то получаем массив id полученных продуктов
        if(isset($mongoProductQuery)) {
            $productsLoggableCursor = $mongoProductQuery->getQuery()->execute();
            $products = array();
            
            while ($product = $productsLoggableCursor->getNext()) {
                $products[$product->getId()] = $product;
            }
            //если продуктов не найдено то возвращаем пустой ответ
            if(!count($products)) {
                return array(
                    'stocks' => array(),
                    'products' => array(),
                    'numberPage' => $numberPage,
                );
            }
            // добавляем полученные id продуктов в запрос (если был поиск по имени/категории)
            $mongoStockQuery->field('productId')->in(array_keys($products));
        }
        // получить результат запроса
        $stocksQuery = $mongoStockQuery
            ->skip($numberPage * $countOnPage) // Смещение
            ->limit($countOnPage) // Отображаем только установленное количество строк
            ->where("function() { return (this.quantity-this.reserved) > 0; }") // отображаем стоки свободные для заказа 
            ->getQuery()
            ->execute();
        
        $stocks = array();
        foreach($stocksQuery as $stock) {
            $stocks[] = $stock;
        }

        // если не было параметров поиска по продуктам то получаем продукты исходя из стоков
        if(!isset($products)) {
            // получаем все id продуктов из стока
            $productsIds = array();
            foreach($stocks as $stock) {
                array_push($productsIds, $stock->getProductId());
            }
            // получаем обьекты по id
            $productsLoggableCursor = $this->dm->createQueryBuilder('NitraMainBundle:Product')
                ->field('_id')
                ->in($productsIds)
                ->getQuery()
                ->execute();
            
            $products = array();
            
            while ($product = $productsLoggableCursor->getNext()) {
                $products[$product->getId()] = $product;
            }
        }
        
        // проверить
        // если есть результаты для отображения 
        if ($products || $stocks) {
            // вернуть шаблон отображения
            return $this->render('NitraMainBundle:MovementRelocationNew:searchProduct.html.twig', array(
                'stocks' => $stocks,
                'products' => $products,
                'numberPage' => $numberPage,
            ));
        }
        
        // иначе вернуть пустой контент
        return new Response();
    }
    
    /**
     * This is called when action throws an exception
     * Default behavior is redirecting to list with error message
     *
     * @param \Exception $e Exception
     * @param \Nitra\MainBundle\Entity\Movement $Movement Your \Nitra\MainBundle\Entity\Movement object
     * @return Response Must return a response!
     */
    protected function errorObjectDelete(\Exception $e, \Nitra\MainBundle\Entity\Movement $Movement = null)
    {
        if($e instanceof NotValidForSaveException) {
            $message = $e->getMessage();
        }
        else {
            $message = "action.object.delete.error";
        }
        
        $this->get('session')->getFlashBag()->add(
            'error',
            $this->get('translator')->trans($message, array('%name%' => 'delete'), 'Admingenerator'));

        return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementRelocation_list"));
    }
    
    /**
     * удалить перемещение
     * @param \Nitra\MainBundle\Entity\Movement $Movement - объект перемещения
     */
    protected function executeObjectDelete(\Nitra\MainBundle\Entity\Movement $Movement)
    {
        // удалить перемещение
        $this->em->getRepository('NitraMainBundle:Movement')->processDeleteMovementRelocation($Movement);
        
        // удалить перемещение
        parent::executeObjectDelete($Movement);
        
        try {
            // сохранить изменения по стоку (DocumentManager) 
            $this->dm->flush();        
        }
        
        catch (\Exception $e) {
            // изменение deletedAt на NULL если возникла ошибка при сохрание DocumentManager
            // отключение софт-делит фильтра
            $this->em->getFilters()->disable('softdeleteable');

            // изменение deletedAt для Movement
            $this->em->createQueryBuilder()
                ->update('NitraMainBundle:Movement m')
                ->set('m.deletedAt', 'NULL')
                ->where('m.id = :id')
                ->setParameter('id', $Movement->getId())
                ->getQuery()
                ->execute();
            
            // изменение deletedAt для MovementEntry
            $this->em->createQueryBuilder()
                ->update('NitraMainBundle:MovementEntry me')
                ->set('me.deletedAt', 'NULL')
                ->where('me.movement = :id')
                ->setParameter('id', $Movement->getId())
                ->getQuery()
                ->execute();
            
            $this->em->getFilters()->enable('softdeleteable');
            throw new \Exception($e->getMessage());
        }     
    }
}

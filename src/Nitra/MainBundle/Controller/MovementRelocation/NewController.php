<?php

namespace Nitra\MainBundle\Controller\MovementRelocation;

use Admingenerated\NitraMainBundle\BaseMovementRelocationController\NewController as BaseNewController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\MainBundle\Form\Type\MovementRelocation\SearchProductFiltersType;
use Nitra\MainBundle\Form\Type\MovementRelocation\NewType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\Form;
use Nitra\MainBundle\Exception\NotValidForSaveException;


class NewController extends BaseNewController
{
     /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * получить тип новой формы
     */
    protected function getNewType()
    {
        $type = new NewType($this->em, $this->dm);
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    
    public function createAction()
    {
        $Movement = $this->getNewObject();

        $this->preBindRequest($Movement);
        $form = $this->createForm($this->getNewType(), $Movement);
        $form->bind($this->get('request'));
        
        // дополнительная проверка вложеных форм
        $childrenIsValid = $this->isFormChildrenValid($form);
        
        if ($form->isValid() && $childrenIsValid) {
            try {
                $this->preSave($form, $Movement);
                $this->saveObject($Movement);
                $this->postSave($form, $Movement);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementRelocation_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementRelocation_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementRelocation_edit", array('pk' => $Movement->getId()) ));
            }
            catch(NotValidForSaveException $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans($e->getMessage(), array(), 'Admingenerator') );
            } 
            catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $Movement);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('NitraMainBundle:MovementRelocationNew:index.html.twig', $this->getAdditionalRenderParameters($Movement) + array(
            "Movement" => $Movement,
            "form" => $form->createView(),
        ));
    }
    
    /**
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param object \Nitra\MainBundle\Entity\Movement
     * @return array
     **/
    protected function getAdditionalRenderParameters(\Nitra\MainBundle\Entity\Movement $Movement)
    {
        // массив айди товаров
        $productIds = array();
        
        // массив имен продуктов
        $productNames = array();
        
        foreach($Movement->getMovementEntries() as $movementEntry) {
            
            $productIds[] = $movementEntry->getProductId();
            
            // получаем сток для отборажения цены в валюте 
            $stock = $this->dm->getRepository('NitraMainBundle:Stock')
                ->getStock($Movement->getFromWarehouse()->getId(), $movementEntry->getProductId(), $movementEntry->getStockParams());
            
            // записываем для позиции цену в валюте и валюту
            $movementEntry->setCurrencyPrice($stock->getCurrencyPrice());
            $currency = $this->em->getRepository('NitraMainBundle:Currency')->find($stock->getCurrency());
            $movementEntry->setCurrency($currency);
        }
        
        // дополнительная информация для отображения позиций при неуспешной валидации
        $productInfo = array();
        
        if($productIds) {
            // объект запроса список проудуктов
            $mongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
            $mongoQuery->field('id')->in($productIds);

            // получить продукты
            $products = $mongoQuery
                ->getQuery()
                ->execute();
            
            // наполнение массива информации при неуспешной валидации
            foreach ($products as $product) {
                $productNames[$product->getId()] = (string) $product;
            }
        }
        
        // объект формы поиска продуктов на складе отправителе
        $filterSearchType = new SearchProductFiltersType;
        $filterSearch = $this->createForm($filterSearchType);
        
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // форма фильтрации
            'filterSearch' => $filterSearch->createView(),
            'productNames' => $productNames,
        );
    }
    
    protected function saveObject(\Nitra\MainBundle\Entity\Movement $Movement)
    {
        // выполнить перемещение
        $this->em->getRepository('NitraMainBundle:Movement')->processMovementRelocation($Movement);

        // сохранить перемещение
        parent::saveObject($Movement);

        try {
            // сохранить изменения по стоку
            $this->dm->flush();        
        }
        catch (\Exception $e) {
            //удалить все сохранения по EntityManager
            $this->em->remove($Movement);
            $this->em->flush();
            // выкинуть ошибку сохранения DocumentManager
            throw new \Exception($e->getMessage());
        }
        
        return false;
    }
    
    /**
     * выполняет валидацию вложеных форм, добавляет для отображения ошибки в формы
     * @param object Symfony\Component\Form\Form $form
     * @return true | false 
    **/
    private function isFormChildrenValid(Form $form)
    {
        // статическое кол-во ошибок формы, так как функция выполняется рекурсивно
        static $form_errors = 0;
        $validator = $this->container->get('validator');
        
        // рекурсивный вызов функции если форма содержит свои вложенные формы
        foreach($form->all() as $child) {
            if($child->all()) {
                $this->isFormChildrenValid($child);
            }
        }
        
        // валидация обьекта из формы формы
        $errors = $validator->validate($form->getData());
        
        // добавление в форму ошибок для отображения (только по полям указаным в форме)
        foreach($errors as $error) {
            foreach($form->all() as $formField) {
                if($error->getPropertyPath() === (string) $formField->getPropertyPath()) {
                    $formField->addError(new \Symfony\Component\Form\FormError($error->getMessage()));
                    $form_errors += 1;
                }
            }
        }

        return ($form_errors > 0) ? false : true;
        
    }
}

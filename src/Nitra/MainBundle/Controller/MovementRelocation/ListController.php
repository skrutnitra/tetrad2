<?php

namespace Nitra\MainBundle\Controller\MovementRelocation;

use Admingenerated\NitraMainBundle\BaseMovementRelocationController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function buildQuery()
    {
        return $this->getDoctrine()
                    ->getManager()
                    ->getRepository('Nitra\MainBundle\Entity\Movement')
                    ->getRelocationMovements();
    }
}

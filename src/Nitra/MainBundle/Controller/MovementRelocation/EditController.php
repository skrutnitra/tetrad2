<?php

namespace Nitra\MainBundle\Controller\MovementRelocation;

use Admingenerated\NitraMainBundle\BaseMovementRelocationController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EditController extends BaseEditController
{
    public function indexAction($pk)
    {
        return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementRelocation_show", array('pk' => $pk)));
    }
            
    public function updateAction($pk)
    {
        throw new \Exception('Нельзя обновить перемещение.');
    }
}

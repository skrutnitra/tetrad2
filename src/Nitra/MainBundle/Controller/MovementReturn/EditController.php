<?php

namespace Nitra\MainBundle\Controller\MovementReturn;

use Admingenerated\NitraMainBundle\BaseMovementReturnController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EditController extends BaseEditController
{
    public function indexAction($pk)
    {
        return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementReturn_show", array('pk' => $pk)));
    }
            
    public function updateAction($pk)
    {
        throw new \Exception('Нельзя обновить перемещение.');
    }
}

<?php

namespace Nitra\MainBundle\Controller\MovementReturn;

use Admingenerated\NitraMainBundle\BaseMovementReturnController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function buildQuery()
    {
        return $this->getDoctrine()
                    ->getManager()
                    ->getRepository('Nitra\MainBundle\Entity\Movement')
                    ->getReturnMovements();
    }
}

<?php

namespace Nitra\MainBundle\Controller\Currency;

use Admingenerated\NitraMainBundle\BaseCurrencyController\ActionsController as BaseActionsController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ActionsController extends BaseActionsController
{
    /**
     * @return json все валюты для сайта
     * @Route("/get-all-currencies", name="get-all-currencies")
     */
    public function getCurrencies()
    {
        $em                 = $this->get('doctrine.orm.entity_manager');
        $objCurrencies      = $em->getRepository('NitraMainBundle:Currency')->findAll();
        $currencies         = array();
        $baseCurrencyCode   = $this->container->has('currency_code') ? $this->container->get('currency_code') : 'uah';
        
        foreach($objCurrencies as $currency) {
            $currencies[$currency->getCode()] = array(
                'name'      => $currency->getName(),
                'exchange'  => ($currency->getCode() == $baseCurrencyCode) ? 1.0 : $currency->getExchange(),
                'symbol'    => $currency->getSymbol(),
                'base'      => ($currency->getCode() == $baseCurrencyCode),
                'selected'  => false,
            );
        }
        
        return new JsonResponse($currencies);
    }
}

<?php
namespace Nitra\MainBundle\Controller\SupplierWarehouse;

use Admingenerated\NitraMainBundle\BaseSupplierWarehouseController\ListController as BaseListController;
use JMS\DiExtraBundle\Annotation as DI;

class ListController extends BaseListController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * buildQuery
     * @return Doctrine\ORM\QueryBuilder
     */
    protected function buildQuery()
    {
        return $this->em
            ->getRepository('NitraMainBundle:Warehouse')
            ->buildQuerySupplierWarehouses();
    }

}

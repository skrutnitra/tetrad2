<?php

namespace Nitra\MainBundle\Controller\MovementIncome;

use Admingenerated\NitraMainBundle\BaseMovementIncomeController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EditController extends BaseEditController
{
    public function indexAction($pk)
    {
        return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementIncome_show", array('pk' => $pk)));
    }
            
    public function updateAction($pk)
    {
        throw new \Exception('Нельзя обновить перемещение.');
    }
}

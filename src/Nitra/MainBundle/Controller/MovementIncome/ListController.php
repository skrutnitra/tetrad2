<?php

namespace Nitra\MainBundle\Controller\MovementIncome;

use Admingenerated\NitraMainBundle\BaseMovementIncomeController\ListController as BaseListController;
use Nitra\MainBundle\Form\Type\MovementIncome\MovementCommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;

class ListController extends BaseListController
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;

    /**
     * получить запрос 
     * на выбор всех закуок
     * @return Doctrine\ORM\QueryBuilder
     */
    protected function buildQuery()
    {
        return $this->getDoctrine()
                        ->getManager()
                        ->getRepository('Nitra\MainBundle\Entity\Movement')
                        ->getIncomeMovements();
    }

    /**
     * получить массив дополнительных параметров передаваемых в шаблон
     * @return array
     */
    public function getAdditionalRenderParameters()
    {

        // объект формы добавления комментариев
        $commentForm = $this->createForm(new MovementCommentType($this->em));

        // вернуть массив параметров передаваемых в шаблон
        return array(
            // форма добавления коментариев
            'commentForm' => $commentForm->createView(),
        );
    }
    /**
     * получить все позиции по закупке
     * @Route("/get-entries-by-movement-{movementId}", name="Nitra_MainBundle_Get_MovementEntries_By_MovementId", options={"expose"=true})
     * @Template("NitraMainBundle:MovementIncomeList:allMovementEntriesForMovement.html.twig")
     */
    public function getMovementEntriesByMovement($movementId)
    {
        $Movement = $this->em->getRepository('NitraMainBundle:Movement')->find($movementId);

        return array(
            'Movement' => $Movement,
        );
    }

    /**
     * processFilters
     * выполнить фильтр 
     * @param Doctrine\ORM\QueryBuilder $query объект запроса
     */
    protected function processFilters($query)
    {
        // выполнить фильтр родителя
        parent::processFilters($query);
        // получить объект фильтров
        $filterObject = $this->getFilters();
        //Фильтр по номеру заказа
        if (isset($filterObject['orderId']) && $filterObject['orderId']) {
            $query->andWhere('(EXISTS (
                                SELECT me.id
                                FROM NitraMainBundle:MovementEntry me
                                INNER JOIN me.orderEntry oe
                                WHERE me.movement = q.id
                                AND oe.order = :orderId
                    ))');
            $query->setParameter('orderId', $filterObject['orderId']);
        }
        
        if (isset($filterObject['productName']) && $filterObject['productName']) {
            
            // Исключаем лишние пробелы
            $productName = preg_replace("/[[:blank:]]+/", ' ', $filterObject['productName']);
            // Если введено несколко фраз - разбиваем по пробелу
            if (strstr($productName, ' ')) {
                // Поиск с учетом регистра всех вхождений
                $productName = '/(?=.*' . str_replace(' ', ')(?=.*', $productName) . ')/i';
            } else {
                $productName = '/' . $productName . '/i';
            }
            
            // поиск по названию товара
            $productsMongo = $this->dm->createQueryBuilder('NitraMainBundle:Product')
                    ->field('fullNameForSearch')->equals(new \MongoRegex($productName))
                    ->getQuery()
                    ->execute();
            
            $product = array();
            
            if(count($productsMongo) > 0) {
            // наполняем массив данных найденными продуктами
                foreach($productsMongo as $pr) {
                    $product[$pr->getId()] = $pr->getId();
                };
            }
            
            // поиск по названию товара в стоках
            $stockMongoQuery = $this->dm->createQueryBuilder('NitraMainBundle:Stock')
                    ->field('priceName')->equals(new \MongoRegex($productName));
                
            // исключаем найденые товары
            if($product) {
                $stockMongoQuery->field('productId')->notIn(array_keys($product));
            }
            $stockMongo = $stockMongoQuery->getQuery()
                    ->execute();
            
            // накидуем товары с стоков
            if (count($stockMongo) > 0) {
                foreach ($stockMongo as $stock) {
                    $product[$stock->getProductId()] = $stock->getProductId();
                };
            }
            
            $product = $product ? array_keys($product) : null;

            $query->andWhere('(EXISTS (
                                SELECT mep.id
                                FROM NitraMainBundle:MovementEntry mep
                                WHERE mep.movement = q.id
                                AND
                                mep.productId IN (:product)
                    ))');

            $query->setParameter('product', $product);
        }
        
        
    }
    
    
    
    

}

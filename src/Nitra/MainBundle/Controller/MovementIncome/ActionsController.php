<?php

namespace Nitra\MainBundle\Controller\MovementIncome;

use Admingenerated\NitraMainBundle\BaseMovementIncomeController\ActionsController as BaseActionsController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Nitra\MainBundle\Exception\NotValidForSaveException;
use Nitra\MainBundle\Form\Type\MovementIncome\MovementCommentType;

class ActionsController extends BaseActionsController
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * поиск товаров 
     * @Route("/search-product-autocomplete-income", name="Nitra_MainBundle_MovementIncome_Search_Product_Autocomplete")
     * @return mixed - если есть результаты для отображения 
     *                 то вернуть шаблон который будет отображен
     *                 иначе вернуть пустой контент
     */
    public function searchProductAutocompleteAction(Request $request)
    {
        // Получить массив данных для поиска 
        $postData = $request->request->get('filters_movement');
        
        $supplier = $request->request->get('select_supplier');

        // кол-во позиций за один результат поиска
        $countOnPage = 20;
        
        // страница поиска
        $numberPage = (isset($postData['numberPage']) && $postData['numberPage']) ? $postData['numberPage'] : 0;
        
        // получить id категории продукта
        $category = (isset($postData['category']) && $postData['category']) ? $postData['category'] : false;
        
        // получить фразу поиска по продуктам 
        $searchVal = (isset($postData['name']) && $postData['name']) ? trim($postData['name']) : false;
        
        // получить флаг что искать только по стокам  
        $searchOnlyInStocks = (isset($postData['search_only_in_stocks']) && $postData['search_only_in_stocks']) ? true : false;
        
        // получить поставщика для поиска
        $supplier = (isset($supplier) && $supplier) ? $supplier : false;
        
        // поиск id продуктов если были указанны параметры поиска по продуктам (категории или поиск по названию) (либо не указан флаг искать только по стокам)
        if($category || $searchVal || !$searchOnlyInStocks) {
            $mongoProductQuery = $this->dm->createQueryBuilder('NitraMainBundle:Product');
        }
        
        // добавить в запрос поиска id продуктов выбранную категорию
        if ($category) {
            $category = new \MongoId($category);
            $mongoProductQuery->field('category.$id')->equals($category);
        }
        
        // добавить в запрос поиска id продуктов указанную фразу
        if ($searchVal) {
            // Исключаем лишние пробелы
            $searchVal = preg_replace("/[[:blank:]]+/", ' ', $searchVal);

            // Если введено несколко фраз - разбиваем по пробелу
            if (strstr($searchVal, ' ')) {
                // Поиск с учетом регистра всех вхождений
                $searchValRegex = '/(?=.*' . str_replace(' ', ')(?=.*', $searchVal) . ')/i';
            } else {
                $searchValRegex = '/' . $searchVal . '/i';
            }
            // поиск по названию товара
            $mongoProductQuery->addAnd($mongoProductQuery->expr()->field('fullNameForSearch')->equals(new \MongoRegex($searchValRegex)));
            
        }

        // делаем поиск по стоку 
        $mongoStockQuery = $this->dm->createQueryBuilder('NitraMainBundle:Stock');
        
        
        // добавить в запрос поиск складов по поставщику
        if ($supplier) {
            // выбираем все склады по данному поставщику
            $supplierWarehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->findBySupplier($supplier);
            $warehouses = array(); 
            
                foreach($supplierWarehouses as $supplierWarehouse) {
                    array_push($warehouses, $supplierWarehouse->getId());
                }
            
            $mongoStockQuery->field('warehouseId')->in($warehouses);
        }
        // если не выбран конкретный поставщик то выбираем все склады поставщиков
        else {
            $supplierWarehouses = $this->em->getRepository('NitraMainBundle:Warehouse')->getSupplierWarehouses();
            $warehouses = array(); 
            
            foreach($supplierWarehouses as $supplierWarehouse) {
                array_push($warehouses, $supplierWarehouse->getId());
            }
            
            $mongoStockQuery->field('warehouseId')->in($warehouses);
        }
       
        // если существуют параметры поиска по продуктам  или существует фильтр поиска не только по стокам то получаем массив id полученных продуктов

        if(isset($mongoProductQuery) ) {
            $productsLoggableCursor = $mongoProductQuery->getQuery()->execute();
            $products = array();
            
            while ($product = $productsLoggableCursor->getNext()) {
                $products[$product->getId()] = $product;
            }
            //если продуктов не найдено то возвращаем пустой ответ
            if(!count($products)) {
                return new Response();
            }
            // добавляем полученные id продуктов в запрос (если был поиск по имени/категории)
            $mongoStockQuery->field('productId')->in(array_keys($products));
        }
        // получить результат запроса
        $stocksQuery = $mongoStockQuery
            ->skip($numberPage * $countOnPage) // Смещение
            ->limit($countOnPage) // Отображаем только установленное количество строк
            ->where("function() { return (this.quantity-this.reserved) > 0; }") // отображаем стоки свободные для заказа 
            ->getQuery()
            ->execute();
        
        $stocks = array();
        foreach($stocksQuery as $stock) {
            $stocks[$stock->getProductId()][] = $stock;
        }

        // если не было параметров поиска по продуктам или был указан поиск только по стокам то получаем продукты исходя из стоков
        if(!isset($products) || $searchOnlyInStocks) {
            // получаем все id продуктов из стока
            $productsIds = array();
            foreach($stocks as $productId => $value) {
                array_push($productsIds, $productId);
            }

            // получаем обьекты по id
            $productsLoggableCursor = $this->dm->createQueryBuilder('NitraMainBundle:Product')
                ->field('_id')
                ->in($productsIds)
                ->getQuery()
                ->execute();
            
            $products = array();
            
            while ($product = $productsLoggableCursor->getNext()) {
                $products[$product->getId()] = $product;
            }
            
        }
        
        // проверить
        // если есть результаты для отображения 
        if ($products || $stocks) {
            // вернуть шаблон отображения
            return $this->render('NitraMainBundle:MovementIncomeNew:searchProduct.html.twig', array(
                'stocks' => $stocks,
                'products' => $products,
                'numberPage' => $numberPage,
            ));
        }
        
        // иначе вернуть пустой контент
        return new Response();
    }
    
    /**
     * добавление комментария
     * @Route("/comment-add", name="Nitra_MainBundle_MovementIncome_Comment_Add")
     * @return JsonResponse
     */
    public function movementCommentAddAction()
    {
         
        // обработка формы
        if ($this->getRequest()->getMethod() == 'POST') {
            
            // объект формы 
            $form = $this->createForm(new MovementCommentType($this->em));

            // заполнить форму 
            $form->submit($this->getRequest());
            
            // проверить валидность формы
            if ($form->isValid()) {
                
                // получить обект данных форм
                $formData = $form->getData();
                // получить объект заказа
                $movement = $form['id']->getData();
                
                // сохранить 
                try {
                    
                    // автоинкримент комментария
                    $movement->addComment($formData['comment'], $this->getUser()->getUsername());
                    
                    // сохранить 
                    $this->em->flush();
                    
                } catch (\Exception $e) {
                    // вернуть ошибку
                    return new JsonResponse(array('type' => 'error', 'message' => $e->getMessage()));
                }
                
                // комментарий успешно добавлен, вернуть текст комментария
                return new JsonResponse(array('type' => 'success', 'movementId' => $movement->getId(), 'comment' => $movement->getComment()));
                
            } else {
                
                // Текст ошибки валидации
                $errorsText = '';
                
                // ошибка валидации по форме
                foreach($form->getErrors() as $error) {
                    $errorsText .= " ".$error->getMessage();
                }
                
                // ошибка валидации по каждому виджету
                foreach ($form->all() as $child) {
                    $errors = $child->getErrors();
                    if ($errors) {
                        $errorsText .= " ".$errors[0]->getMessage();
                    }
                }
                
                // отображение ошибки валидации формы
                return new JsonResponse(array(
                    'type' => 'error', 
                    'message' => $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator').$errorsText
                ));
            }
        }
        
        // вернуть пустой ответ
        return new JsonResponse();
    }    
    
    /**
     * This is called when action throws an exception
     * Default behavior is redirecting to list with error message
     *
     * @param \Exception $e Exception
     * @param \Nitra\MainBundle\Entity\Movement $Movement Your \Nitra\MainBundle\Entity\Movement object
     * @return Response Must return a response!
     */
    protected function errorObjectDelete(\Exception $e, \Nitra\MainBundle\Entity\Movement $Movement = null)
    {
        if($e instanceof NotValidForSaveException) {
            $message = $e->getMessage();
        }
        else {
            $message = "action.object.delete.error";
        }
        
        $this->get('session')->getFlashBag()->add(
            'error',
            $this->get('translator')->trans($message, array('%name%' => 'delete'), 'Admingenerator'));
        
        return new RedirectResponse($this->generateUrl("Nitra_MainBundle_MovementIncome_list"));
    }
    
    /**
     * удалить перемещение
     * @param \Nitra\MainBundle\Entity\Movement $Movement - объект перемещения
     */
    protected function executeObjectDelete(\Nitra\MainBundle\Entity\Movement $Movement)
    {
        // удалить перемещение
        $this->em->getRepository('NitraMainBundle:Movement')->processDeleteMovementIncome($Movement);
        
        parent::executeObjectDelete($Movement);
        
        try {
            // сохранить изменения по стоку (DocumentManager) 
            $this->dm->flush();        
        }
        
        catch (\Exception $e) {
            // изменение deletedAt на NULL если возникла ошибка при сохрание DocumentManager
            // отключение софт-делит фильтра
            $this->em->getFilters()->disable('softdeleteable');

            // изменение deletedAt для Movement
            $this->em->createQueryBuilder()
                ->update('NitraMainBundle:Movement m')
                ->set('m.deletedAt', 'NULL')
                ->where('m.id = :id')
                ->setParameter('id', $Movement->getId())
                ->getQuery()
                ->execute();
            
            // изменение deletedAt для MovementEntry
            $this->em->createQueryBuilder()
                ->update('NitraMainBundle:MovementEntry me')
                ->set('me.deletedAt', 'NULL')
                ->where('me.movement = :id')
                ->setParameter('id', $Movement->getId())
                ->getQuery()
                ->execute();
            
            $this->em->getFilters()->enable('softdeleteable');
            throw new \Exception($e->getMessage());
        }       
    }
}

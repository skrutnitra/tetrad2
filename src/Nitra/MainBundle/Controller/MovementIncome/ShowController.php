<?php

namespace Nitra\MainBundle\Controller\MovementIncome;
use JMS\DiExtraBundle\Annotation as DI;
use Admingenerated\NitraMainBundle\BaseMovementIncomeController\ShowController as BaseShowController;

class ShowController extends BaseShowController
{
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    public function indexAction($pk)
    {
        $Movement = $this->getObject($pk);

        if (!$Movement) {
            throw new NotFoundHttpException("The Nitra\MainBundle\Entity\Movement with id $pk can't be found");
        }
        
        $productIds = array();
        
        foreach($Movement->getMovementEntries() as $movementEntry) {
            $productIds[] = $movementEntry->getProductId();
        }
        
        $productsLoggableCursors = $this->dm
            ->createQueryBuilder('NitraMainBundle:Product')
            ->field('id')->in($productIds)
            ->getQuery()
            ->execute(); 
        
        $products = array();
        
        while ($product = $productsLoggableCursors->getNext()) {
            $products[$product->getId()] = $product;
        }

        return $this->render('NitraMainBundle:MovementIncomeShow:index.html.twig', $this->getAdditionalRenderParameters($Movement) + array(
            'Movement' => $Movement,
            'products' => $products,
        ));
    }
}

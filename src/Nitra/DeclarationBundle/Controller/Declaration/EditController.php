<?php
namespace Nitra\DeclarationBundle\Controller\Declaration;

use Admingenerated\NitraDeclarationBundle\BaseDeclarationController\EditController as BaseEditController;
use JMS\DiExtraBundle\Annotation as DI;

class EditController extends BaseEditController
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
        
    /**
     * массив объектов позиций деклараций для удаления 
     * @var array \Nitra\DeclarationBundle\Entity\DeclarationEntry
     */
    private $removeDeclarationEntries;
    
    /**
     * preBindRequest
     * @param \Nitra\DeclarationBundle\Entity\Declaration $Declaration your \Nitra\DeclarationBundle\Entity\Declaration object
     */
    public function preBindRequest(\Nitra\DeclarationBundle\Entity\Declaration $Declaration)
    {
        // наполнить массив позиций деклараций до сохранения 
        $this->removeDeclarationEntries = array();
        foreach ($Declaration->getDeclarationEntries() as $declarationEntry) {
            $this->removeDeclarationEntries[$declarationEntry->getId()] = $declarationEntry;
        }
    }
    
    /**
     * preSave
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\DeclarationBundle\Entity\Declaration $Declaration your \Nitra\DeclarationBundle\Entity\Declaration object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\DeclarationBundle\Entity\Declaration $Declaration)
    {
        // обойти все позиции деклараций которые пришли в форме
        foreach ($Declaration->getDeclarationEntries() as $declarationEntry) {
            
            // проверить если новая позиция для декларации 
            if (!$this->em->contains($declarationEntry)) {
                // persist $declarationEntry
                $this->em->persist($declarationEntry);
            }
            
            // Отсеиваем позиции, которые пришли в посте
            if (isset($this->removeDeclarationEntries[$declarationEntry->getId()])) {
                // позиция которая сохраниться в декларации удаляется из массива позиций на удаление
                unset($this->removeDeclarationEntries[$declarationEntry->getId()]);
            }
            
        }
        
        // Удаляем позиции из декларации
        foreach ($this->removeDeclarationEntries as $declarationEntry) {
            $this->em->remove($declarationEntry);
        }
    }
    
}

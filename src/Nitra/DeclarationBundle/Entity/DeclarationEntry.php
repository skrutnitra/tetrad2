<?php
namespace Nitra\DeclarationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class DeclarationEntry
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\DeclarationBundle\Entity\Declaration", inversedBy="declarationEntries")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $declaration;

    /**
     * позиция заказа
     * @ORM\ManyToOne(targetEntity="Nitra\OrderBundle\Entity\OrderEntry", inversedBy="declarationEntry")
     * @ORM\JoinColumn(name="order_entry_id", referencedColumnName="id")
     */
    private $orderEntry;    

    /**
     * @var string productId - MongoId товара
     * @ORM\Column(type="string", length=24)
     * @Assert\Length(min="24", max="24")
     * @Assert\NotBlank(message="Не указан продукт товарной позиции")
     */
    private $productId;
    
     /**
     * @var Document\Product
     */
    protected $product;
    
    /**
     * @var string $stockParams
     * @ORM\Column(name="stock_params", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $stockParams;
    
    /**
     * @var integer $quantity
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1, max=999999)
     * @Assert\NotBlank(message="Не указано количество товара")
     */
    private $quantity;
    
    /**
     * @var boolean $isActive
     * @ORM\Column(type="boolean")
     * @Assert\Type(type="bool")
     */
    private $isActive;
    
    /**
     * конструктор
     */
    public function __construct()
    {
        $this->quantity = 0;
        $this->isActive = true;
    }
    
    /**
     * object to string 
     * @return string
     */
    public function __toString()
    {
        
        // проверить кол-во продукта больше 1
        if ($this->getQuantity() > 1 ) {
            // вернуть навзание позиции с учетом кол-ва
            return $this->getQuantity() . " x " . $this->getEntryName();
        }
        
        // вернуть навзание позиции
        return $this->getEntryName();
    }
    
    /**
     * Получить навзание товарной позиции
     * @return string
     */
    public function getEntryName()
    {
        
        // возвразаемое имя 
        $entryName = '';
        
        // проверить связь с объектом продукта
        if ($this->getProduct()) {
            $entryName = (string)$this->getProduct();
        }
        
        // проверить параметры стока
        if ($this->getStockParams()) {
            $entryName .= " (".$this->getStockParams().")";
        }
        
        // вернуть string 
        return $entryName;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param string $productId
     * @return DeclarationEntry
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    
        return $this;
    }

    /**
     * Get productId
     *
     * @return string 
     */
    public function getProductId()
    {
        return $this->productId;
    }
    
    public function setProduct(\Nitra\MainBundle\Document\Product $product)
    {
        $this->productId = $product->getId();
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * Set stockParams
     * @param string $stockParams
     * @return OrderEntry
     */
    public function setStockParams($stockParams)
    {
        $this->stockParams = $stockParams;
    
        return $this;
    }

    /**
     * Get stockParams
     * @return string 
     */
    public function getStockParams()
    {
        return $this->stockParams;
    }    
    
    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return DeclarationEntry
     */
    public function setQuantity($quantity)
    {
        $this->quantity= $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set declaration
     *
     * @param \Nitra\DeclarationBundle\Entity\Declaration $declaration
     * @return DeclarationEntry
     */
    public function setDeclaration(\Nitra\DeclarationBundle\Entity\Declaration $declaration)
    {
        $this->declaration = $declaration;
        return $this;
    }

    /**
     * Get declaration
     *
     * @return \Nitra\DeclarationBundle\Entity\Declaration 
     */
    public function getDeclaration()
    {
        return $this->declaration;
    }

    /**
     * Set orderEntry
     *
     * @param \Nitra\OrderBundle\Entity\OrderEntry $orderEntry
     * @return DeclarationEntry
     */
    public function setOrderEntry(\Nitra\OrderBundle\Entity\OrderEntry $orderEntry = null)
    {
        $this->orderEntry = $orderEntry;
        return $this;
    }

    /**
     * Get orderEntry
     *
     * @return \Nitra\OrderBundle\Entity\OrderEntry 
     */
    public function getOrderEntry()
    {
        return $this->orderEntry;
    }
   
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return DeclarationEntry
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

}

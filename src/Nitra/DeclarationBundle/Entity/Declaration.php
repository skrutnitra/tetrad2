<?php
namespace Nitra\DeclarationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class Declaration
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан склад отправитель")
     */
    private $fromWarehouse;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан склад получатель")
     */
    private $toWarehouse;

    /**
     * @var string declarationNumber - Номер декларации
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max="100")
     */
    private $declarationNumber;

    /**
     * @var date - Дата прибытия
     * @ORM\Column(type="date")
     * @Assert\Date()
     * @Assert\NotBlank(message="Не указана дата прибытия")
     */
    private $date;
    
    /**
     * @var string $comment - Комментарий
     * @ORM\Column(type="string", nullable=true)
     */
    private $comment;
    
    /**
     * @ORM\OneToMany(targetEntity="Nitra\DeclarationBundle\Entity\DeclarationEntry", mappedBy="declaration", cascade={"remove"})
     */
    private $declarationEntries;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->declarationEntries = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @return string 
     */
    public function __toString()
    {
        return $this->getId();
    }
    
    /**
     * Валидатор
     * Проверить склад отправитель и скла получатель
     * @Assert\False(message = "Склад отправитель не должен быть равен складу получателя")
     */
    public function isWarehousesEqual()
    {
        return ($this->fromWarehouse && $this->toWarehouse && $this->fromWarehouse->getId() == $this->toWarehouse->getId());
    }

    /**
     * Валидатор
     * проверить наличие позиций для декларации
     * @Assert\True(message = "Нет ни одного продукта")
     */
    public function isDeclarationEntriesExists()
    {
        // проверить количество позиций
        if (count($this->declarationEntries)) {
            return true;
        }
        
        // нет позиций
        return false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set declarationNumber
     *
     * @param string $declarationNumber
     * @return Declaration
     */
    public function setDeclarationNumber($declarationNumber)
    {
        $this->declarationNumber = $declarationNumber;
    
        return $this;
    }

    /**
     * Get declarationNumber
     *
     * @return string 
     */
    public function getDeclarationNumber()
    {
        return $this->declarationNumber;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Declaration
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Declaration
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Add autoincrement comment in declaration
     * 
     * @param string $comment
     * @param string $username
     * @return Declaration
     */
    public function addComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);
        
        if ($comment && $username) {
            
            $formatter = \IntlDateFormatter::create(
                \Locale::getDefault(), 
                \IntlDateFormatter::MEDIUM, 
                \IntlDateFormatter::NONE
            );
            
            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setComment($this->getComment() . $commentText);
            
        }
        
        return $this;
    }    
    
    /**
     * Set fromWarehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $fromWarehouse
     * @return Declaration
     */
    public function setFromWarehouse(\Nitra\MainBundle\Entity\Warehouse $fromWarehouse)
    {
        $this->fromWarehouse = $fromWarehouse;
    
        return $this;
    }

    /**
     * Get fromWarehouse
     *
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getFromWarehouse()
    {
        return $this->fromWarehouse;
    }

    /**
     * Set toWarehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $toWarehouse
     * @return Declaration
     */
    public function setToWarehouse(\Nitra\MainBundle\Entity\Warehouse $toWarehouse)
    {
        $this->toWarehouse = $toWarehouse;
    
        return $this;
    }

    /**
     * Get toWarehouse
     *
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getToWarehouse()
    {
        return $this->toWarehouse;
    }

    /**
     * Add declarationEntry
     *
     * @param \Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntry
     * @return Declaration
     */
    public function addDeclarationEntry(\Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntry)
    {
        $declarationEntry->setDeclaration($this);
        $this->declarationEntries[] = $declarationEntry;
    
        return $this;
    }

    /**
     * Remove declarationEntries
     *
     * @param \Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntries
     */
    public function removeDeclarationEntry(\Nitra\DeclarationBundle\Entity\DeclarationEntry $declarationEntries)
    {
        $this->declarationEntries->removeElement($declarationEntries);
    }

    /**
     * Get declarationEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeclarationEntries()
    {
        return $this->declarationEntries;
    }
}

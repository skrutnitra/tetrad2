<?php
namespace Nitra\ExpenseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Nl\AccountBundle\Entity\Account;

/**
 * @ORM\Entity
 */
class Expense
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     * @Assert\Range(min=0.01, max=999999.99)
     * @Assert\NotBlank(message="Не указана сумма")
     */
    private $amount;
    
    /**
     * @ORM\ManyToOne(targetEntity="Nitra\AccountBundle\Entity\Account", inversedBy="expenses")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="Не указан счет")
     */
    protected $account;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     * @Assert\NotBlank(message="Не указана дата")
     */
    private $date;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указана категория")
     */
    private $category;
    
    /**
     * склад отправитель 
     * @ORM\ManyToOne(targetEntity="Nitra\ContractBundle\Entity\Contract")
     * @ORM\JoinColumn(name="contract_id", referencedColumnName="id", nullable=true)
     */
    private $contract;
    
    /**
     * object to string
     */
    public function __toString()
    {
        $formatter = \IntlDateFormatter::create(
                        \Locale::getDefault(), \IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE
        );
        $formatter->setPattern("d MMMM y");

        return 'Расход на сумму ' . $this->getAmount() . ' от ' . $formatter->format($this->getDate()). ($this->getComment() ? ' (' . $this->getComment() . ')': '');
    }
    
    /**
     * конструктор
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Expense
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Expense
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Expense
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

      /**
     * Set category
     *
     * @param \Nitra\ExpenseBundle\Entity\Category $category
     * @return Expense
     */
    public function setCategory(\Nitra\ExpenseBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Nitra\ExpenseBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
   

    /**
     * Set account
     *
     * @param \Nitra\AccountBundle\Entity\Account $account
     * @return Expense
     */
    public function setAccount(\Nitra\AccountBundle\Entity\Account $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Nitra\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set contract
     *
     * @param \Nitra\ContractBundle\Entity\Contract $contract
     * @return Expense
     */
    public function setContract(\Nitra\ContractBundle\Entity\Contract $contract = null)
    {
        $this->contract = $contract;
    
        return $this;
    }

    /**
     * Get contract
     *
     * @return \Nitra\ContractBundle\Entity\Contract 
     */
    public function getContract()
    {
        return $this->contract;
    }
    
}
<?php
namespace Nitra\ExpenseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
//use Nitra\ExpenseBundle\Entity\Category;

class LoadExpenseData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 12; // the order in which fixtures will be loaded
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // получить все категории
        $categories = $manager->getRepository('NitraExpenseBundle:Category')->findAll();
        
        // обойти все категории 
        // создать расход по каждой категории
        foreach($categories as $category) {
            
            // созать расходы по категории
            for ($index = 0; $index < 5; $index++) {

                // название переменной 
                $objName = 'Expense'.$index;

                // создать объект 
                $$objName = new \Nitra\ExpenseBundle\Entity\Expense();
                $$objName->setAmount(rand(100, 1000));
                $$objName->setAccount($this->getReference('account'));
                $$objName->setComment('Тестовый расход №'.$index);
                $$objName->setDate(new \DateTime());
                $$objName->setCategory($category);
                $$objName->setContract(null);
                $manager->persist($$objName);
            }
        }
        
        
        // получить все контракты
        $contracts = $manager->getRepository('NitraContractBundle:Contract')->findAll();
        
        // создать по каждому контракту расход
        foreach($contracts as $contract) {
            
            // созать расходы по контракту
            for ($index = 0; $index < 5; $index++) {
                // название переменной 
                $objName = 'Expense'.$index;

                // создать объект 
                $$objName = new \Nitra\ExpenseBundle\Entity\Expense();
                $$objName->setAmount(rand(100, 1000));
                $$objName->setAccount($this->getReference('account'));
                $$objName->setComment('Тестовый расход №'.$index.' по контракту ID: '.$contract->getId());
                $$objName->setDate(new \DateTime());
                $$objName->setCategory($categories[0]);
                $$objName->setContract($contract);
                $manager->persist($$objName);
            }
        }
        
        // сохранить 
        $manager->flush();
    }
    
}


<?php
namespace Nitra\ExpenseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\ExpenseBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
     
        // создать записи 
        for ($index = 0; $index < 10; $index++) {
            
            // название переменной 
            $objName = 'ExpenseCategory'.$index;
            
            // создать объект 
            $$objName = new Category();
            $$objName->setName('Категория расходов '.$index);
            $manager->persist($$objName);
            
            // запомнить
            $this->addReference($objName, $$objName);
        }
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
}


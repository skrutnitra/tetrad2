<?php

namespace Nitra\AnalyticsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;

/**
 *          Аналитика - прибыль
 */
class ProfitController extends Controller
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**               Прибыль 
     * @Route("/analytics-profit", name="Nitra_AnalyticsBundle_Profit")
     * @Template()
     */
    public function profitAction()
    {
        
        // Фильтры
        $filter = $this->createFormBuilder(null)
                ->add('dateFilter', 'date_range', array('label' => 'Дата', 'format' => 'd MMM y', 'required' => false))
                ->add('select', 'submit', array('label' => 'Фильтровать'))
                ->add('reset', 'submit', array('label' => 'Сбросить'));

        // Получаем значения фильтров
        $request = $this->getRequest();
        if ($request->isMethod("POST")) {
            $form = $filter->getForm()->submit($request);
            if ($form->isValid()) {
                /**
                 * Обработка фильтра
                 */
                if ($form->get('select')->isClicked()) {
                    $params = $form->getData();
                    if (!is_null($params['dateFilter']['from']) && !empty($params['dateFilter']['from'])) {
                        $midnight = date('Y-m-d', $params['dateFilter']['from']->getTimestamp()) . ' 00:00:00';
                        $from = new \DateTime($midnight);
                    }
                    if (!is_null($params['dateFilter']['to']) && !empty($params['dateFilter']['to'])) {
                        $midnight = date('Y-m-d', $params['dateFilter']['to']->getTimestamp()) . ' 23:59:59';
                        $to = new \DateTime($midnight);
                    }
                    $filter->setData($params);
                } elseif ($form->get('reset')->isClicked()) {
                    $filter->setData(null);
                }
            }
        }
        
        // статус завершен
        $orderStatusCompleted = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneByMethodName('completed');

        // статусы позиций не участвующие в статистике
        $oeStatusesNotIn = $this->em
            ->createQueryBuilder()
            ->select('st.id, st.methodName')
            ->from('NitraOrderBundle:OrderEntryStatus', 'st')
            ->where('st.methodName IN(:methodName)')->setParameter('methodName', array('canceled', 'returned_to_supplier'))
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        //прибыль по филиалам
        $profit = $this->em->createQueryBuilder()
                ->select('f.name as filial, f.id as filialId, SUM(oe.priceOut * oe.quantity) as moneyMov,
                          SUM((oe.priceOut - oe.priceIn) * oe.quantity) as margin,
                          ((SUM(oe.priceOut - oe.priceIn)/ SUM(oe.priceIn)) * 100) as percent')
                ->from('NitraOrderBundle:OrderEntry', 'oe')
                //только завершенные заказы 
                ->innerJoin('oe.order', 'o', 'WITH', 'o.orderStatus =:orderStatus')
                ->innerJoin('o.filial', 'f')
                //кроме позиций которые отменены и возвращены поставщику
                ->where('oe.orderEntryStatus NOT IN (:oeStatuses)')
                // todo: ID заменить на methodName
                ->setParameter('oeStatuses', array_keys($oeStatusesNotIn))
                ->setParameter('orderStatus', $orderStatusCompleted);
        
        //Если выбран фильтр
        if (isset($from) && isset($to)) {
            $profit
                    ->andWhere('o.completedAt BETWEEN :from AND :to') 
                    ->setParameter('from', $from->format('Y-m-d H:i:s'))
                    ->setParameter('to', $to->format('Y-m-d H:i:s'));
        }
        $profits = $profit
                ->groupBy('o.filial')
                ->getQuery()
                ->getArrayResult();
        /**
         * @var $sumMoneyMov -  сумма по оброту
         * @var $sumMoneyMov -  сумма по марже
         * @var $totalPercent - общий процент по прибыли
         */
        $sumMoneyMov = 0;
        $sumMargin = 0;
        $totalPercent = 0;
        $allSummQuery = $this->em->createQueryBuilder()
                ->select('SUM(oe.priceOut * oe.quantity) as sumMoneyMov ,
                          SUM((oe.priceOut - oe.priceIn) * oe.quantity) as sumMargin')
                ->from('NitraOrderBundle:OrderEntry', 'oe')
                //только завершенные заказы 
                ->innerJoin('oe.order', 'o', 'WITH', 'o.orderStatus =:orderStatus')->setParameter('orderStatus', $orderStatusCompleted)
                //кроме позиций которые отменены и возвращены поставщику
                ->where('oe.orderEntryStatus NOT IN (:oeStatuses)')
                ->setParameter('orderStatus', $orderStatusCompleted)
                ->setParameter('oeStatuses', array_keys($oeStatusesNotIn));
        
        //Если фильтруем по дате
        if (isset($from) && isset($to)) {
            $allSummQuery
                    ->andWhere('o.completedAt BETWEEN :from AND :to') 
                    ->setParameter('from', $from->format('Y-m-d H:i:s'))
                    ->setParameter('to', $to->format('Y-m-d H:i:s'));
        }
        $allSummQuery = $allSummQuery
                ->getQuery()
                ->getSingleResult();
        
        $sumMoneyMov += $allSummQuery['sumMoneyMov'];
        $sumMargin += $allSummQuery['sumMargin'];
        if($sumMoneyMov !== 0){
            $totalPercent += $sumMargin / ($sumMoneyMov - $sumMargin) * 100;
        }
        return array(
            'filter' => $filter->getForm()->createView(),
            'profits' => $profits,
            'sumMoneyMov' => $sumMoneyMov,
            'sumMargin' => $sumMargin,
            'totalPercent' => $totalPercent,
            'from' => (isset($from)) ? $from : '',
            'to' => (isset($to)) ? $to : ''
        );
    }

    /**         getDetailsProfits
     * Подробный отчет по филиалу (прибыли)
     * @Route("/details-profit", name="AnalyticsBundle_DetailsProfit")
     * @Template("NitraAnalyticsBundle:Profit:detailsProfits.html.twig")
     */
    public function getDetailsProfits()
    {
        //Только для AJAX`а
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw $this->createNotFoundException('Только для AJAX');
        }
        
        // статус завершен
        $orderStatusCompleted = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneByMethodName('completed');
        
        // статусы позиций не участвующие в статистике
        $oeStatusesNotIn = $this->em
            ->createQueryBuilder()
            ->select('st.id, st.methodName')
            ->from('NitraOrderBundle:OrderEntryStatus', 'st')
            ->where('st.methodName IN(:methodName)')->setParameter('methodName', array('canceled', 'returned_to_supplier'))
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        $from = $this->getRequest()->get('from');
        $to = $this->getRequest()->get('to');
        $filialId = $this->getRequest()->get('filialId');
        // Данные по выбранному филиалу
        $detailsProfits = $this->em->createQueryBuilder()
                ->select("CONCAT(m.firstname, CONCAT(' ', m.lastname)) as manager, 
                          SUM(oe.priceOut * oe.quantity) as moneyMov ,
                          SUM((oe.priceOut - oe.priceIn) * oe.quantity) as margin")
                ->from('NitraOrderBundle:OrderEntry', 'oe')
                //только завершенные заказы 
                ->innerJoin('oe.order', 'o', 'WITH', 'o.orderStatus =:orderStatus')->setParameter('orderStatus', $orderStatusCompleted)
                ->innerJoin('o.completedBy' , 'm')
                //кроме позиций которые отменены и возвращены поставщику
                ->where('oe.orderEntryStatus NOT IN (:oeStatuses)')
                ->setParameter('oeStatuses', array_keys($oeStatusesNotIn))
                ->andWhere('o.filial = :filialId')
                //Для выбранной категории
                ->setParameter('filialId', $filialId);
        if ($from !== '' && $to !== '') {
            $detailsProfits
                    ->andWhere('o.completedAt BETWEEN :from AND :to')
                    ->setParameter('from', $from)
                    ->setParameter('to', $to);
        }
        $detailsProfits = $detailsProfits
                ->groupBy('o.completedBy')
                //Менеджеры в алфавитном порядке
                ->orderBy('manager')
                ->getQuery()
                ->getArrayResult();
        return array(
            'detailsProfits' => $detailsProfits,
            'filialId' => $filialId
        );
    }

}

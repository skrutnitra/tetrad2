<?php
namespace Nitra\AnalyticsBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\AnalyticsBundle\Controller\ContractBallance;
use Nitra\AnalyticsBundle\Form\Type\ContractListFiltersType;
//use Nitra\ContractBundle\Entity\Contract;

/**
 * ContractListController
 * аналтика по контрактам
 */
class ContractListController extends ContractBallance
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * финансы по контрактам
     * @Route("/contract", name="Nitra_AnalyticsBundle_Contract_list")
     * @Template("NitraAnalyticsBundle:Contract:index.html.twig")
     */
    public function indexAction()
    {
        
        // получить форму фильтра
        $form = $this->getFilterForm();        
        $formData = $form->getData();
        
        // дата начала фильтра
        $sDate = null;
        if (isset($formData['date']) && isset($formData['date']['from']) && $formData['date']['from'] instanceof \DateTime ) {
            $sDate = $formData['date']['from']; 
        }
        
        // дата завершения фильтра
        $eDate = null;
        if (isset($formData['date']) && isset($formData['date']['to']) && $formData['date']['to'] instanceof \DateTime ) {
            $eDate = $formData['date']['to']; 
        }
        
        // запрос получения контрактов
        $contractQuery = $this->em->createQueryBuilder()
            ->select('c.id, c.name')
            ->addSelect('s.id AS supplierId, s.name AS supplierName')
            ->from('NitraContractBundle:Contract', 'c', 'c.id')
            ->innerJoin('c.supplier', 's')
            ;
        
        // фильтр по филиалу
        if (isset($formData['filial'])) {
            $contractQuery->andWhere('c.filial =:filial')->setParameter('filial', $formData['filial']);
        }
        
        // фильтр по поставщику
        if (isset($formData['supplier'])) {
            $contractQuery->andWhere('c.supplier =:supplier')->setParameter('supplier', $formData['supplier']);
        }
        
        // получить массив контрактов
        $contracts = $contractQuery
            ->getQuery()
            ->getArrayResult();
        
        // массив ID контарктов
        $contractIds = array();
        // сформировать результирующий бллансовый массив по контрактам
        foreach($contracts as $contractId => $contract) {
            $contractIds[] = $contractId;
        }
        
        // получить стартовые баллансы по контрактам
        // обнуление балланса по контракту
        $startBallances = $this->getStartBallances($contractIds);
        foreach($startBallances as $contractId => $amount) {
            $contracts[$contractId]['amount'] = $amount;
        }
        
        // получить все расходы по контрактам
        $expenses = $this->getExpenses($contractIds, $sDate, $eDate);
        foreach($expenses as $item) {
            $contractId = $item['contractId'];
            if (isset($contracts[$contractId])) {
                $contracts[$contractId]['amount'] += $item['amount'];
            }
        }
        
        // получить все закупки по контрактам
        $incomes = $this->getIncomes($contractIds, $sDate, $eDate);
        foreach($incomes as $item) {
            $contractId = $item['contractId'];
            if (isset($contracts[$contractId])) {
                $contracts[$contractId]['amount'] -= $item['amount'];
            }
        }
        
        // получить все возвраты по контрактам
        $returns = $this->getReturns($contractIds, $sDate, $eDate);
        foreach($returns as $item) {
            $contractId = $item['contractId'];
            if (isset($contracts[$contractId])) {
                $contracts[$contractId]['amount'] += $item['amount'];
            }
        }
        
        // Итого по фильтру
        $totalContracts = 0;
        foreach($contracts as $contractId => $contract) {
            $totalContracts += $contract['amount'];
        }
            
        
        // вернуть массив данных передаваемых в шаблон 
        return array(
            'form' => $form->createView(),
            'contracts' => $contracts,
            'totalContracts' => $totalContracts,
        );
    }
    
    /**
     * контроллер обработка сохранения фильтров
     * @Route("/contract-filters", name="Nitra_AnalyticsBundle_Contract_filters")
     * @return RedirectResponse
     */
    public function filtersAction()
    {
        // сброс фильтров
        if ($this->get('request')->get('reset')) {
            $this->setFilters(array());
            return new RedirectResponse($this->generateUrl('Nitra_AnalyticsBundle_Contract_list'));
        }
        
        // установить фильтры из POST
        if ($this->getRequest()->getMethod() == "POST") {
            $form = $this->getFilterForm();
            $form->bind($this->get('request'));
            if ($form->isValid()) {
                $filters = $form->getViewData();
            }
        }
        
        // установить фильтры из GET
        if ($this->getRequest()->getMethod() == "GET") {
            $filters = $this->getRequest()->query->all();
        }
        
        // установить фильтры
        if (isset($filters)) {
            $this->setFilters($filters);
        }
        
        // перенаправить подьзователя на список
        return new RedirectResponse($this->generateUrl('Nitra_AnalyticsBundle_Contract_list'));
    }

    /**
     * Store in the session service the current filters
     * @param array the filters
     */
    protected function setFilters($filters)
    {
        // поставщик преобразовать фильтр из объекта в массив сущности и ID 
        if (isset($filters['supplier'])) {
            $filters['supplier'] = array (
                'id' => $filters['supplier']->getId(),
                'entityName' => get_class($filters['supplier'])
            );
        }
        
        // филиал преобразовать фильтр из объекта в массив сущности и ID 
        if (isset($filters['filial'])) {
            $filters['filial'] = array (
                'id' => $filters['filial']->getId(),
                'entityName' => get_class($filters['filial'])
            );
        }
        
        // сохранить фильтры в сессии
        $this->get('session')->set('Nitra\AnalyticsBundle\Contract\Filters', $filters);
    }

    /**
     * Get filters from session
     */
    protected function getFilters()
    {
        // получить фильтры из сессии 
        $filters = $this->get('session')->get('Nitra\AnalyticsBundle\Contract\Filters', array());
        
        // поставщик преобразовать фильтр из массива в объект
        if (isset($filters['supplier'])) {
                $filters['supplier'] = $this->getDoctrine()
                ->getManagerForClass($filters['supplier']['entityName'])
                ->getRepository($filters['supplier']['entityName'])->find($filters['supplier']['id']);
        }
        
        // филиал преобразовать фильтр из массива в объект
        if (isset($filters['filial'])) {
                $filters['filial'] = $this->getDoctrine()
                ->getManagerForClass($filters['filial']['entityName'])
                ->getRepository($filters['filial']['entityName'])->find($filters['filial']['id']);
        }
        
        // вернуть фильтры
        return $filters;
    }    
    
    /**
     * получить форму фильтра
     * @return ContractsFiltersType
     */
    protected function getFiltersType()
    {
        $type = new ContractListFiltersType();
        return $type;
    }
    
    
    /**
     * получить форму фильтров
     * @return Symfony\Component\Form\Form
     */
    protected function getFilterForm()
    {
        // получить фильры
        $filters = $this->getFilters();
        
        // поставщик преобразовать фильтр в объект 
        if (isset($filters['supplier'])) {
            $this->getDoctrine()
                ->getManagerForClass(get_class($filters['supplier']))
                ->getUnitOfWork()
                ->registerManaged($filters['supplier'], array('id' => $filters['supplier']->getId()), array());
        }
        
        // филиал преобразовать фильтр в объект 
        if (isset($filters['filial'])) {
            $this->getDoctrine()
                ->getManagerForClass(get_class($filters['filial']))
                ->getUnitOfWork()
                ->registerManaged($filters['filial'], array('id' => $filters['filial']->getId()), array());
        }
        
        // вернуть форму
        return $this->createForm($this->getFiltersType(), $filters);
    }    
    
}

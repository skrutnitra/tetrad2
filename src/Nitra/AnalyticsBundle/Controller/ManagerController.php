<?php

namespace Nitra\AnalyticsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;

class ManagerController extends Controller
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**
     * @Route("/manager-status", name="Nitra_AnalyticsBundle_Manager_ManagerStatus")
     * @Template()
     */
    public function managerStatusAction()
    {
        /*
         * Подготавливаем переменные
         */
        $managersStatus = null;
        $managerFilter = null;
        $managers = null;
        $managerStatusHistory = null;
        $historyCriteria = array();
        /**
         * Подготавливаем репозитории
         */
        $orderEntryStatuses = $this->em->getRepository('NitraOrderBundle:OrderEntryStatus')->findBy(array(), array('id' => 'ASC'));
        $managersRepo = $this->em->getRepository('NitraManagerBundle:Manager');
        $statusHistoryRepo = $this->em->getRepository('NitraOrderBundle:OrderEntryStatusHistory');
        /**
         * Фильтр
         */
        $filters = $this->createFormBuilder(null)
                ->add("manager", "entity", array('class' => 'NitraManagerBundle:Manager',
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->orderBy('u.firstname', 'ASC');
            }, 'label' => 'Менеджер', 'required' => false, 'mapped' => true))
                ->add('dateFilter', 'date_range', array('label' => 'Дата', 'format' => 'd MMM y', 'required' => false))
                ->add('select', 'submit', array('label' => 'Фильтровать'))
                ->add('reset', 'submit', array('label' => 'Сбросить'));

        $request = $this->getRequest();
        /**
         * Получаем значения фильтров
         */
        if ($request->isMethod("POST")) {
            $form = $filters->getForm()->submit($request);
            if ($form->isValid()) {
                /**
                 * Смотрим на какую кнопку нажали фильтр/сброс
                 */
                if ($form->get('select')->isClicked()) {
                    $params = $form->getData();
                    $managerFilter = $params['manager'];
                    if (!is_null($params['dateFilter']['from']) && !empty($params['dateFilter']['from'])) {
                        $historyCriteria['fromDate'] = $params['dateFilter']['from'];
                    }
                    if (!is_null($params['dateFilter']['to']) && !empty($params['dateFilter']['to'])) {
                        $historyCriteria['toDate'] = $params['dateFilter']['to'];
                    }
                    $filters->setData($params);
                } elseif ($form->get('reset')->isClicked()) {
                    $historyCriteria = array();
                    $managerFilter = null;
                    $filters->setData(null);
                }
            }
        }
        /**
         * Применем фильты по менеджеру
         */
        if (!is_null($managerFilter) && !empty($managerFilter)) {
            $managers = $managersRepo->findBy(array('id' => $managerFilter->getId()), array('firstname' => 'ASC', 'lastname' => 'ASC'));
        } else {
            $managers = $managersRepo->findBy(array(), array('firstname' => 'ASC', 'lastname' => 'ASC'));
        }
        foreach ($managers as $manager) {
            foreach ($orderEntryStatuses as $toStatus) {
                
                // получить историю заказов для менеджера 
                // переводы позиций в статус
                $managerStatusHistory = $this->em->createQueryBuilder()
                        ->select('oesh')
                        ->from('NitraOrderBundle:OrderEntryStatusHistory', 'oesh')
                        ->innerJoin('oesh.orderEntry','oe')
                        ->where('oesh.toStatus = :toStatus')
                        ->andWhere('oesh.createdBy = :createdBy')
                        ->setParameter('toStatus', $toStatus)
                        ->setParameter('createdBy', $manager);
                
                // добавить в запрос 
                if (isset($historyCriteria['fromDate']) && $historyCriteria['fromDate']) {
                    $managerStatusHistory
                        ->andWhere('oesh.createdAt >= :from')
                        ->setParameter('from', $historyCriteria['fromDate']->format('Y-m-d 00:00:00'));
                }
                
                if (isset($historyCriteria['toDate']) && $historyCriteria['toDate']) {
                    $managerStatusHistory
                        ->andWhere('oesh.createdAt <= :to')
                        ->setParameter('to', $historyCriteria['toDate']->format('Y-m-d 23:59:59'));
                }
                
                // получить историю 
                $managerStatusHistory = $managerStatusHistory
                        ->getQuery()
                        ->execute();
                $sumTime = 0;
                $count = 0;
                foreach ($managerStatusHistory as $history) {
                    $count++;
                    $prevStatDate = $statusHistoryRepo->findPrevPositionDate($history);
                    $sumTime += $history->getCreatedAt()->getTimestamp() - $prevStatDate->getTimestamp();
                }
                $managersStatus[$manager->getId()][$toStatus->getId()] = $sumTime > 0 ? $this->secondToTime($sumTime / $count) : null;
            }
        }

        return array("managerStatus" => $managersStatus,
            'statuses' => $orderEntryStatuses,
            'managers' => $managers,
            'filter' => $filters->getForm()->createView());
    }

    public function secondToTime($second)
    {
        if (!is_null($second)) {
            $hours = floor($second / 3600);
            $minutes = ($second / 3600 - $hours) * 60;
            $seconds = ceil(($minutes - floor($minutes)) * 60);
            return "$hours час. " . floor($minutes) . " мин. $seconds сек.";
        }
    }

    /**
     * @Route("/efficiency-manager", name="efficiency_by_manager")
     */
    public function efficiencyByManagerAction()
    {
        // Значения по умолчанию
        $from = new \DateTime('first day of this month');
        $to = new \DateTime();
        $manager = null;

        // Фильтр
        $form = $this->createFormBuilder(null)
                ->add('period', 'date_range', array(
                    'label' => 'Период',
                    'format' => 'd MMM y'))
                ->add('manager', 'entity', array(
                    'required' => false,
                    'class' => 'Nitra\\ManagerBundle\\Entity\\Manager'))
                ->getForm();


        // сбросить фильтр
        if ($this->get('request')->get('reset')) {
            return new RedirectResponse($this->generateUrl("efficiency_by_manager"));
        }

        // Обработка фильтра
        if ($this->getRequest()->getMethod() == 'POST') {
            $form->bind($this->getRequest());
            $data = $form->getData();
            $from = $data['period']['from'];
            $to = $data['period']['to'];
            $manager = $data['manager'];
        } else {
            // значения по умолчанию
            $form->setData(array('period' => array('from' => $from, 'to' => $to)));
        }

        $result = $this->getEfficiencyManager($manager, $from->format('Y-m-d 00:00:00'), $to->format('Y-m-d 23:59:59'));

        return $this->render('NitraAnalyticsBundle:ReportByManager:index.html.twig', array(
                    'managers' => $result,
                    'form' => $form->createView()
        ));
    }

    /**
     * 
     * @param type $objectmanager - обьект менеджера
     * @param type $from - с ( дата создания заказа )
     * @param type $to  - по ( дата создания заказа )
     * @return type array
     */
    public function getEfficiencyManager($objectmanager, $from, $to)
    {

        // статус завершен
        $orderStatusCompleted = $this->em->getRepository('NitraOrderBundle:OrderStatus')->findOneByMethodName('completed');
        
        // статусы позиций не участвующие в статистике
        $oeStatusesNotIn = $this->em
            ->createQueryBuilder()
            ->select('st.id, st.methodName')
            ->from('NitraOrderBundle:OrderEntryStatus', 'st')
            ->where('st.methodName IN(:methodName)')->setParameter('methodName', array('canceled', 'returned_to_supplier'))
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // получить статистику
        $managerQuery = $this->em->createQueryBuilder()
                ->select('count(DISTINCT o.id) as orders,
                     sum(oe.quantity) as orderEntry,
                     sum(oe.priceOut * oe.quantity) as priceOut,
                     (SUM((oe.priceOut - oe.priceIn * cur.exchange) * oe.quantity)) as margin,
                     m.id as manager,
                     m.firstname as firstname,
                     m.lastname as lastname'
                )
                ->from('NitraOrderBundle:Order', 'o')
                ->innerJoin('o.orderEntries', 'oe')
                ->innerJoin('oe.currency', 'cur')
                ->innerJoin('o.createdBy', 'm')
                ->where('o.orderStatus =:orderStatus')->setParameter('orderStatus', $orderStatusCompleted)
                ->andWhere('oe.orderEntryStatus NOT IN (:oeMethodNameNotIn)')->setParameter('oeMethodNameNotIn', array_keys($oeStatusesNotIn))
                ->andWhere('o.createdAt BETWEEN :from AND :to')
                ->setParameter('from', $from)
                ->setParameter('to', $to)
                ->orderBy('margin', 'DESC')
                ->groupBy('m.id');

        //если существует фильтр по менедеру
        if ($objectmanager) {
            $managerQuery->andWhere('m.id =:manager')
                    ->setParameter('manager', $objectmanager);
        }

        $result = $managerQuery->getQuery()
                ->getArrayResult();

        return $result;
    }

}

<?php
namespace Nitra\AnalyticsBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\DiExtraBundle\Annotation as DI;

use Nitra\AnalyticsBundle\Controller\ContractBallance;
use Nitra\AnalyticsBundle\Form\Type\ContractDetailsFiltersType;
use Nitra\ContractBundle\Entity\Contract;
//use Nitra\ContractBundle\Entity\Contract;

/**
 * ContractDetailsController
 * аналтика по контракту 
 */
class ContractDetailsController extends ContractBallance
{
    
    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * финансы по контрактам
     * @Route("/contract-details-{pk}", name="Nitra_AnalyticsBundle_ContractDetails_list")
     * @ParamConverter("contract", class="NitraContractBundle:Contract", options={"id" = "pk"})
     * @Template("NitraAnalyticsBundle:ContractDetails:index.html.twig")
     */
    public function indexAction(Contract $contract)
    {
        
        // получить форму фильтра
        $form = $this->getFilterForm();        
        $formData = $form->getData();
        
        // получить балланс по контракту
        $ballances = $this->getBallances($contract->getId(), $formData['date']['from'], $formData['date']['to']);

        // вернуть массив данных передаваемых в шаблон 
        return array(
            'form' => $form->createView(),
            'contract' => $contract,
            'ballances' => $ballances,
        );        
    }
    
    /**
     * контроллер обработка сохранения фильтров
     * @Route("/contract-details-{pk}/filters", name="Nitra_AnalyticsBundle_ContractDetails_filters")
     * @ParamConverter("contract", class="NitraContractBundle:Contract", options={"id" = "pk"})
     * @return RedirectResponse
     */
    public function filtersAction(Contract $contract)
    {
        // сброс фильтров
        if ($this->get('request')->get('reset')) {
            $this->setFilters($this->getDefaultFilters());
            return new RedirectResponse($this->generateUrl('Nitra_AnalyticsBundle_ContractDetails_list', array('pk'=>$contract->getId())));
        }
        
        // установить фильтры из POST
        if ($this->getRequest()->getMethod() == "POST") {
            $form = $this->getFilterForm();
            $form->bind($this->get('request'));
            if ($form->isValid()) {
                $filters = $form->getViewData();
            }
        }
        
        // установить фильтры из GET
        if ($this->getRequest()->getMethod() == "GET") {
            $filters = $this->getRequest()->query->all();
        }
        
        // установить фильтры
        if (isset($filters)) {
            $this->setFilters($filters);
        }
        
        // перенаправить подьзователя на список
        return new RedirectResponse($this->generateUrl('Nitra_AnalyticsBundle_ContractDetails_list', array('pk'=>$contract->getId())));
    }

    /**
     * Store in the session service the current filters
     * @param array the filters
     */
    protected function setFilters($filters)
    {
        // сохранить фильтры в сессии
        $this->get('session')->set('Nitra\AnalyticsBundle\ContractDetails\Filters', $filters);
    }

    /**
     * Get filters from session
     */
    protected function getFilters()
    {
        // получить фильтры из сессии 
        $filters = $this->get('session')->get('Nitra\AnalyticsBundle\ContractDetails\Filters', array());
        
        // проверить фильтры по умолчанию
        if ($this->isApplyDefaultFilters($filters) === true) {
            $filters = $this->getDefaultFilters();
        }
        
        // вернуть фильтры
        return $filters;
    }    
    
    /**
     * проверка нужно ли применять к массиву фильтров фильтры по умочанию
     * @param array - массив фильтров
     * @return boolean - массив фильтров
     * @return true    - нужно устанавливать фильтры по умолчанию
     * @return false   - не нужно устанавливать фильтры по умолчанию
     */
    protected function isApplyDefaultFilters(array $filters=null)
    {
        // не определен массив фильтров
        if (!$filters || !is_array($filters)) {
            return true;
        }
        
        // проверить фильтр период дат
        if (!isset($filters['date'])) {
            return true;
        }
        
        // проверить фильтр даты from 
        if (!isset($filters['date']['from'])) {
            return true;
        }
        
        // проверить фильтр даты to
        if (!isset($filters['date']['to'])) {
            return true;
        }
        
        // фильтры по умолчанию не нужно устанавливать
        return false;
    }


    /**
     * получить фильтры по умолчанию
     * @return array - массив фильтров по умолчанию
     */
    protected function getDefaultFilters()
    {
        // массив фильтров
        $filters = array('date' => array(
            // начало суток
            'from' => new \DateTime(date('Y-m-d').' 00:00:00'),
            // конец суток
            'to' => new \DateTime(date('Y-m-d').' 00:00:00'),
        ));
        
        // вернуть фильтры
        return $filters;
    }    
    
    /**
     * получить форму фильтра
     * @return ContractsFiltersType
     */
    protected function getFiltersType()
    {
        $type = new ContractDetailsFiltersType();
        return $type;
    }
    
    
    /**
     * получить форму фильтров
     * @return Symfony\Component\Form\Form
     */
    protected function getFilterForm()
    {
        // получить фильры
        $filters = $this->getFilters();
        
        // вернуть форму
        return $this->createForm($this->getFiltersType(), $filters);
    }    
    
}

<?php
namespace Nitra\AnalyticsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Nitra\ContractBundle\Entity\Contract as EntityContract;

/**
 * ContractBallance
 * базовые мотоды аналитики по контрактам
 */
class ContractBallance extends Controller
{
    
    /**
     * получить расходы по контрактам
     * @param mixed $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return array[contractId] = $sum - массив расходов в разрезе контракта
     */
    public function getExpenses($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // преобразовать в массив ID контракта
        if ($contractIds && !is_array($contractIds)) {
            $contractIds = array($contractIds);
        }
        
        // запрос полуения расходов 
        $query = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('exp, cnt, cat, acc')
            ->from('NitraExpenseBundle:Expense', 'exp')
            ->innerJoin('exp.contract', 'cnt')
            ->innerJoin('exp.category', 'cat')
            ->innerJoin('exp.account', 'acc')
            ->where('exp.contract IS NOT NULL')
            ->orderBy('exp.id', 'ASC')
            ;
        
        // фильтр по дате начала
        if ($sDate) {
            $query->andWhere('exp.date >= :sDate')->setParameter('sDate', $sDate);
        }
        
        // фильтр по дате завершения
        if ($eDate) {
            $query->andWhere('exp.date <= :eDate')->setParameter('eDate', $eDate);
        }
        
        // фильтр по контрактам
        if ($contractIds) {
            $query->andWhere('cnt.id IN(:contractIds)')->setParameter('contractIds', $contractIds);
        }
        
        // получить расходы
        $expenses = $query
            ->getQuery()
            ->execute();
        
        // результирующий массив
        $results = array();
        foreach($expenses as $expense) {
            $expenseContract = $expense->getContract();
            $results[$expense->getId()] = array(
                'id' => $expense->getId(),
                'contractId' => $expense->getContract()->getId(),
                'date' => $expense->getDate(),
                'amount' => $expense->getAmount(),
                'comment' => '"'.(string)$expense->getCategory().'" со счета "'.(string)$expense->getAccount().'"',
            );
        }
        
        // вернуть результирующий массив
        return $results;
    }
    
    /**
     * получить ИТОГО расходы по по контрактам
     * @param array $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return $sum - сумма расходов
     */
    public function getSumExpense($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // получить все расходы
        $expenses = $this->getExpenses($contractIds, $sDate, $eDate);
        
        // всех расходов
        $sum = 0;
        foreach($expenses as $expense) {
            $sum += $expense['amount'];
        }
        
        // вернуть сумму
        return $sum;
    }
    
    /**
     * получить закупки по контрактам
     * @param mixed $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return array[contractId] = $sum - массив закупок в разрезе контракта
     */
    public function getIncomes($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // преобразовать в массив ID контракта
        if ($contractIds && !is_array($contractIds)) {
            $contractIds = array($contractIds);
        }
        
        // запрос полуения закупок 
        $query = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('me, m, c')
            ->from('NitraMainBundle:MovementEntry', 'me')
            ->innerJoin('me.movement', 'm')
            ->innerJoin('m.contract', 'c')
            ->where('m.contract IS NOT NULL AND m.toWarehouse IS NOT NULL')
            ->orderBy('me.id', 'ASC')
        ;
        
        // фильтр по дате начала
        if ($sDate) {
            $query->andWhere('m.date >= :sDate')->setParameter('sDate', $sDate);
        }
        
        // фильтр по дате завершения
        if ($eDate) {
            $query->andWhere('m.date <= :eDate')->setParameter('eDate', $eDate);
        }
        
        // фильтр по контрактам
        if ($contractIds) {
            $query->andWhere('c.id IN(:contractIds)')->setParameter('contractIds', $contractIds);
        }
        
        // получить закупки
        $incomes = $query
            ->getQuery()
            ->execute();
        
        // результирующий массив
        $results = array();
        foreach($incomes as $movementEntry) {
            $movement = $movementEntry->getMovement();
            $movementContract = $movement->getContract();
            $results[$movementEntry->getId()] = array(
                'id' => $movementEntry->getId(),
                'movementId' => $movement->getId(),
                'contractId' => $movementContract->getId(),
                'date' => $movement->getDate(),
                'amount' => $movementEntry->getTotalInBaseCurrency(),
                'comment' => (string)$movementEntry,
            );
        }
        
        // вернуть результирующий массив
        return $results;
    }
    
    /**
     * получить ИТОГО закупки по по контрактам
     * @param array $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return $sum - сумма закупок
     */
    public function getSumIncome($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // получить все закупки
        $incomes = $this->getIncomes($contractIds, $sDate, $eDate);
        
        // сымма всех закупок
        $sum = 0;
        foreach($incomes as $income) {
            $sum += $income['amount'];
        }
        
        // вернуть сумму
        return $sum;
    }
    
    /**
     * получить возвраты по контрактам
     * @param mixed $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return array[contractId] = $sum - массив возвратов в разрезе контракта
     */
    public function getReturns($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // преобразовать в массив ID контракта
        if ($contractIds && !is_array($contractIds)) {
            $contractIds = array($contractIds);
        }
        
        // запрос полуения возвратов 
        $query = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('me, m, c')
            ->from('NitraMainBundle:MovementEntry', 'me')
            ->innerJoin('me.movement', 'm')
            ->innerJoin('m.contract', 'c')
            ->where('m.fromWarehouse IS NOT NULL AND m.contract IS NOT NULL')
            ->orderBy('me.id', 'ASC')
        ;
        
        // фильтр по дате начала
        if ($sDate) {
            $query->andWhere('m.date >= :sDate')->setParameter('sDate', $sDate);
        }
        
        // фильтр по дате завершения
        if ($eDate) {
            $query->andWhere('m.date <= :eDate')->setParameter('eDate', $eDate);
        }
        
        // фильтр по контрактам
        if ($contractIds) {
            $query->andWhere('c.id IN(:contractIds)')->setParameter('contractIds', $contractIds);
        }
        
        // получить возвраты
        $returns = $query
            ->getQuery()
            ->execute();
        
        // результирующий массив
        $results = array();
        foreach($returns as $movementEntry) {
            $movement = $movementEntry->getMovement();
            $movementContract = $movement->getContract();
            $results[$movementEntry->getId()] = array(
                'id' => $movementEntry->getId(),
                'contractId' => $movementContract->getId(),
                'date' => $movement->getDate(),
                'amount' => $movementEntry->getTotalInBaseCurrency(),
                'comment' => (string)$movementEntry,
            );
        }
        
        // вернуть результирующий массив
        return $results;
    }
    
    /**
     * получить ИТОГО возвраты по по контрактам
     * @param array $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return $sum - сумма возвратов
     */
    public function getSumReturn($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // получить все возвраты
        $returns = $this->getReturns($contractIds, $sDate, $eDate);
        
        // сумма всех возвратов
        $sum = 0;
        foreach($returns as $return) {
            $sum += $return['amount'];
        }
        
        // вернуть сумму
        return $sum;
    }
    
    /**
     * получить начальные баллансы по контрактам
     * @param mixed $contractIds - массив ID контрактов
     * @return array[contractId] = $startBallanse - 
     */
    public function getStartBallances($contractIds=null)
    {
        // преобразовать в массив ID контракта
        if ($contractIds && !is_array($contractIds)) {
            $contractIds = array($contractIds);
        }
        
        // запрос стартовых баллансов
        $query = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('c.id, c.startBalance')
            ->from('NitraContractBundle:Contract', 'c');
        
        // фильтр по контрактам
        if ($contractIds) {
            $query->andWhere('c.id IN(:contractIds)')->setParameter('contractIds', $contractIds);
        }
        
        // получить стартовые баллансы
        $startBallances = $query
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // вернуть результирующий массив
        return $startBallances;
    }    
    
    /**
     * получить ИТОГО возвраты по по контрактам
     * @param array $contractIds - массив ID контрактов
     * @return $sum - сумма стартовых баллансов
     */
    public function getSumStartBallance($contractIds=null)
    {
        // получить все стартовые баллансы
        $startBallances = $this->getStartBallances($contractIds);
        
        // сумма всех стартовых баллансов
        $sum = 0;
        foreach($startBallances as $contractId => $startBallance) {
            $sum += $startBallance;
        }
        
        // вернуть сумму
        return $sum;
    }
    
    /**
     * получить балланс по контрактам в разрезе каждого контракта
     * @param mixed $contractIds - массив ID контрактов
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return array = результируюший массив
     */
    public function getBallances($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // преобразовать в массив ID контракта
        if ($contractIds && !is_array($contractIds)) {
            $contractIds = array($contractIds);
        }
        
        // результирующий массив
        $results = array(
            // массив дат баллансового периода
            'dates' => array(),
            // итого расходов за период
            'totalExpenses' => 0,
            // итого закупок за период
            'totalIncomes' => 0,
            // итого возвратов за период
            'totalReturns' => 0,
            
            // начальный балланс за период
            'startBallance' => 0,
            // коннечный балланс за период
            'endBallance' => 0,
            // итого балланс за период totlaBallance = (endBallance - startBallance)
            // конечный балланс за период минус (-) начальный балланс за период
            'totalBallance' => 0,
        );
        
        // получить расходы
        $expenses = $this->getExpenses($contractIds, $sDate, $eDate);
        foreach($expenses as $row) {
            $results['totalExpenses'] += $row['amount'];
            $timestamp = $row['date']->getTimestamp();
            if (!isset($results['dates'][$timestamp])) {
                $results['dates'][$timestamp] = array(
                    'date' => $row['date'], 
                    'items' => array()
                );
            }
            $row['type'] = 'expense';
            $row['abbr'] = 'Р';
            $row['name'] = 'Расход';
            $results['dates'][$timestamp]['items'][] = $row;
        }
        
        // получить закупки
        $incomes = $this->getIncomes($contractIds, $sDate, $eDate);
        foreach($incomes as $row) {
            $results['totalIncomes'] += $row['amount'];
            $timestamp = $row['date']->getTimestamp();
            if (!isset($results['dates'][$timestamp])) {
                $results['dates'][$timestamp] = array(
                    'date' => $row['date'], 
                    'items' => array()
                );
            }
            $row['type'] = 'income';
            $row['abbr'] = 'З';
            $row['name'] = 'Закупка';
            $results['dates'][$timestamp]['items'][] = $row;
        }
        
        // получить возвраты
        $returns = $this->getReturns($contractIds, $sDate, $eDate);
        foreach($returns as $row) {
            $results['totalReturns'] += $row['amount'];
            $timestamp = $row['date']->getTimestamp();
            if (!isset($results['dates'][$timestamp])) {
                $results['dates'][$timestamp] = array(
                    'date' => $row['date'], 
                    'items' => array()
                );
            }
            $row['type'] = 'return';
            $row['abbr'] = 'В';
            $row['name'] = 'Возврат';
            $results['dates'][$timestamp]['items'][] = $row;
        }
        
        // сортировать массив
        asort($results['dates']);
        
//        // расчет начального и конечного балансов на каждый день отчета
//        // с использованием объектных запросов
//        if ($results['dates']) {
//            // расчитать балансы на начало и на конец дня
//            foreach($results['dates'] as $timestamp => $atDate) { 
//                // балланс на начало дня (конец предыдущего дня)
//                $startDate = new \DateTime(date('Y-m-d H:i:s', strtotime(date('Y-m-d', $timestamp).' 23:59:59 -1 day')));;
//                $results['dates'][$timestamp]['startBallance'] = $this->getBallance($contractIds, null, $startDate);
//                
//                // балланс на конец дня 
//                $endDate = new \DateTime($atDate['date']->format('Y-m-d').' 23:59:59');
//                $results['dates'][$timestamp]['endBallance'] = $this->getBallance($contractIds, null, $endDate);
//            }
//        }
        
        // в указанном периоде не было операций
        // нет данных по баллансовым датам
        // получить стартовый балланс на сегодня
        if (!$results['dates']) {
            // сегодня
            $today = new \DateTime(date('Y-m-d').' 23:59:59');
            // балланс на конец дня (конец сегоднешнего дня), балланс на начало периода
            $todayStartBallance = $this->getBallance($contractIds, null, $today);
            $results['startBallance'] = $todayStartBallance; // балланс на начало периода
            $results['endBallance'] = $todayStartBallance; // балланс на конец периода
            
        } else {
            // расчет начального и конечного балансов на каждый день отчета
            // без использования объектных запросов
            
            
            // получить ключи $timestamp отчетов за каждый день
            $keys = array_keys($results['dates']);
            
            // ключ первой балансовой записи в массиве
            $timestamp = $keys[0];
            
            // балланс на начало дня (конец предыдущего дня), балланс на начало периода
            $startDate = new \DateTime(date('Y-m-d H:i:s', strtotime(date('Y-m-d', $timestamp).' 23:59:59 -1 day')));
            $startBallance = $this->getBallance($contractIds, null, $startDate);
            $results['dates'][$timestamp]['startBallance'] = $startBallance; // балланс на начало дня (конец предыдущего дня)
            $results['startBallance'] = $startBallance; // балланс на начало периода
            
            // расчитать балансы на начало и на конец дня
            // $timestamp - день на который расчитывается балланс
            // $atDate - массив операций за баллансовый день
            $iteration = 0;
            foreach($results['dates'] as $timestamp => $atDate) {
                
                // обновить счетчик итераций цикла
                $iteration ++;
                
                // балланс на начало дня равен баллансу на конец предидущего дня от расчтеного $timestamp
                // балланс для первого дня расчитывается отдельно вне цикла
                // для $iteration == 1 $endBallance еше не существует
                // появится в коце цикла расчета за день ($timestamp)
                if ($iteration > 1) {
                    $results['dates'][$timestamp]['startBallance'] = $endBallance;
                }
                
                // балланс на конец дня 
                $endBallance = $results['dates'][$timestamp]['startBallance'];
                
                // подсчитать балланс за денб
                foreach($atDate['items'] as $item) {
                    switch($item['type']) {
                        
                        // расход
                        case 'expense':
                            $endBallance += $item['amount'];
                            break;
                        
                        // закупка
                        case 'income':
                            $endBallance -= $item['amount'];
                            break;
                        
                        // возврат
                        case 'return':
                            $endBallance += $item['amount'];
                            break;
                        
                    }
                }
                
                // сохранить балланс на конец дня
                $results['dates'][$timestamp]['endBallance'] = $endBallance;
                
                // сохранить балланс на конец периода
                $results['endBallance'] += $endBallance;
            }
        }
        
        // сохранить балланс за период
        // конечный балланс за период минус (-) начальный балланс за период
        $results['totalBallance'] = ($results['endBallance'] - $results['startBallance']);
        
        // вернуть результируюший массив
        return $results;
    }
    
    /**
     * получить ИТОГО балланс по контрактам
     * @param \DateTime $sDate - дата начала на которую составляется балланс
     * @param \DateTime $eDate - дата конца на которую составляется балланс
     * @return $sum - сумма баллансов
     */
    public function getBallance($contractIds=null, \DateTime $sDate=null, \DateTime $eDate=null)
    {
        // сумма баллансов
        $sum = 0;
        
        // стартовые баллансы
        $sum += $this->getSumStartBallance($contractIds);
        
        // расходы
        $sum += $this->getSumExpense($contractIds, $sDate, $eDate);
        
        // закупки 
        $sum -= $this->getSumIncome($contractIds, $sDate, $eDate);
        
        // возвраты
        $sum += $this->getSumReturn($contractIds, $sDate, $eDate);
        
        // вернуть сумму
        return $sum;
    }
    
}

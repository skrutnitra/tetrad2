<?php

namespace Nitra\AnalyticsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;

/**
 *          Аналитика - Расходы
 */
class ExpenseController extends Controller
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**
     * @Route("/analytics-expense", name="AnalyticsBundle_Expense")
     * @Template()
     */
    public function analyticsExpenseAction()
    {
        /**
         * Фильтр
         */
        $filter = $this->createFormBuilder(null)
                ->add('dateFilter', 'date_range', array('label' => 'Дата', 'format' => 'd MMM y', 'required' => false))
                ->add('select', 'submit', array('label' => 'Фильтровать'))
                ->add('reset', 'submit', array('label' => 'Сбросить'));

        $request = $this->getRequest();
        /**
         * Получаем значения фильтров
         */
        if ($request->isMethod("POST")) {
            $form = $filter->getForm()->submit($request);
            if ($form->isValid()) {
                /**
                 * Обработка фильтра
                 */
                if ($form->get('select')->isClicked()) {
                    $params = $form->getData();
                    if (!is_null($params['dateFilter']['from']) && !empty($params['dateFilter']['from'])) {
                        $midnight = date('Y-m-d', $params['dateFilter']['from']->getTimestamp()) . ' 00:00:00';
                        $from = new \DateTime($midnight);
                    }
                    if (!is_null($params['dateFilter']['to']) && !empty($params['dateFilter']['to'])) {
                        $midnight = date('Y-m-d', $params['dateFilter']['to']->getTimestamp()) . ' 23:59:59';
                        $to = new \DateTime($midnight);
                    }
                    $filter->setData($params);
                } elseif ($form->get('reset')->isClicked()) {
                    $filter->setData(null);
                }
            }
        }
        /**
         * @var $expenses - массив с сгруппированым расходами
         */
        $expenses = $this->em->createQueryBuilder()
                ->select('expCat.id as categoryId, expCat.name as categoryName,  SUM(exp.amount) as summExp')
                ->from('NitraExpenseBundle:Expense', 'exp')
                ->innerJoin('exp.category', 'expCat');
        //Если фильтруем по дате
        if (isset($from) && isset($to)) {
            $expenses
                    ->andWhere('exp.date BETWEEN :from AND :to')
                    ->setParameters(
                            array('from' => $from->format('Y-m-d H:i:s'),
                                  'to' => $to->format('Y-m-d H:i:s'))
            );
        }
        $expenses = $expenses
                ->groupBy('exp.category')
                //Выводить в алфавитном порядке
                ->orderBy('categoryName')
                ->getQuery()
                ->getArrayResult();
        /**
         * @var $allSumm - общая сумма расходов с учетом фильтров
         */
        $allSumm = 0;
        $allSummQuery = $this->em->createQueryBuilder()
                ->select('SUM(exp.amount)')
                ->from('NitraExpenseBundle:Expense', 'exp');
        //Если фильтруем по дате
        if (isset($from) && isset($to)) {
            $allSummQuery
                    ->andWhere('exp.date BETWEEN :from AND :to')
                    ->setParameters(
                            array('from' => $from->format('Y-m-d'),
                                'to' => $to->format('Y-m-d'))
            );
        }
        $allSumm += $allSummQuery
                ->getQuery()
                ->getSingleScalarResult();

        return array(
            'filter' => $filter->getForm()->createView(),
            'expenses' => $expenses,
            'allSumm' => $allSumm,
            'from' => (isset($from)) ? $from : '',
            'to' => (isset($to)) ? $to : ''
        );
    }

    /**         getDetailsExpenses
     * Подробный отчет по каждой категории расходов
     * @Route("/details-expense", name="AnalyticsBundle_DetailsExpense")
     * @Template("NitraAnalyticsBundle:Expense:detailsExpenses.html.twig")
     */
    public function getDetailsExpenses()
    {
        //Только для AJAX`а
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw $this->createNotFoundException('Только для AJAX');
        }
        $from = $this->getRequest()->get('from');
        $to = $this->getRequest()->get('to');
        $categoryId = $this->getRequest()->get('categoryId');
        // Данные по выбранной категории
        $expenses = $this->em->createQueryBuilder()
                ->select('exp.date as date , acc.name as accName,  exp.amount as amount ')
                ->from('NitraExpenseBundle:Expense', 'exp')
                ->innerJoin('exp.account', 'acc')
                ->where('exp.category = :categoryId')
                //Для выбранной категории
                ->setParameter('categoryId', $categoryId);
        if ($from !== '' && $to !== '') {
            $expenses
                    ->andWhere('exp.date BETWEEN :from AND :to')
                    ->setParameter('from',$from)
                    ->setParameter('to',$to);
        }
        $expenses = $expenses
                //от старых к новым
                ->orderBy('exp.date')
                ->getQuery()
                ->getArrayResult();
        
        return array(
            'expenses' => $expenses ,
            'categoryId' => $categoryId
        );
    }

}

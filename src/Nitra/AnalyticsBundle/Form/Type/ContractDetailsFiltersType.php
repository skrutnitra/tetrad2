<?php
namespace Nitra\AnalyticsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContractDetailsFiltersType extends AbstractType
{
        
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // период дат
        $builder->add('date', 'date_range', array(
            'required' => false,  
            'format' => 'd MMM y',  
            'widget' => 'single_text',  
            'label' => 'Дата',
        ));
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'analytics_contract_details_filters';
    }
    
    /**
     * установить значения формы по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // установить данные по умолчанию 
        $resolver->setDefaults(array(
            // данные формы 
            'data' => array(
                // Дата
                'date' => array(
                    'from' => new \DateTime(),
                    'to' => new \DateTime(),
                ),
            ),
        ));
    }
    
}


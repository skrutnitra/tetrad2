<?php
namespace Nitra\AnalyticsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContractListFiltersType extends AbstractType
{
        
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
//        // период дат
//        $builder->add('date', 'date_range', array(
//            'required' => false,  
//            'format' => 'd MMM y',  
//            'widget' => 'single_text',  
//            'label' => 'Дата',
//        ));
        
        // филиал
        // todo: уточнить 
        // филиалы с учетом данных в сесси myFilialsJoined или нет ?
        $builder->add('filial', 'entity', array(
            'class' => 'NitraFilialBundle:Filial',
            'required' => false,
            'label' => 'Филиал',
        ));
        
        // поставщик
        $builder->add('supplier', 'entity', array(
            'class' => 'NitraMainBundle:Supplier',
            'required' => false,
            'label' => 'Поставщик',
        ));
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'analytics_contract_filters';
    }
    
}



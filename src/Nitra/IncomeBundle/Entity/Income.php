<?php
namespace Nitra\IncomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Nitra\AccountBundle\Entity\Account;

/**
 * Income
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Income
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var decimal $amount
     * @ORM\Column(name="amount", type="decimal", precision=8, scale=2)
     * @Assert\Range(min=0.01, max=999999.99)
     * @Assert\NotBlank(message="Не указана сумма")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\AccountBundle\Entity\Account", inversedBy="incomes")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(message="Не указан счет")
     */
    protected $account;

    /**
     * @var date $date
     * @ORM\Column(name="date", type="date")
     * @Assert\Date()
     * @Assert\NotBlank(message="Не указана дата")
     */
    private $date;

    /**
     * @var string $comment
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var Order заказ
     * @ORM\ManyToOne(targetEntity="\Nitra\OrderBundle\Entity\Order", inversedBy="incomes")
     */
    private $order;
    
    /**
     * @var Filial филиал
     * @ORM\ManyToOne(targetEntity="\Nitra\FilialBundle\Entity\Filial")
     * @Assert\NotBlank(message="Не указан филиал")
     */
    private $filial;
    
    /**
     * object to string
     */
    public function __toString()
    {
        $formatter = \IntlDateFormatter::create(
                        \Locale::getDefault(), \IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE
        );
        $formatter->setPattern("d MMMM y");

        return 'Поступление на сумму ' . $this->getAmount() . ' от ' . $formatter->format($this->getDate()). ($this->getComment() ? ' (' . $this->getComment() . ')': '');
    }
    
    /**
     * конструктор
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Income
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Income
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Income
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

  
    /**
     * Set account
     *
     * @param \Nitra\AccountBundle\Entity\Account $account
     * @return Income
     */
    public function setAccount(\Nitra\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Nitra\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set order
     *
     * @param \Nitra\OrderBundle\Entity\Order $order
     * @return Income
     */
    public function setOrder(\Nitra\OrderBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Nitra\OrderBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set filial
     *
     * @param \Nitra\FilialBundle\Entity\Filial $filial
     * @return Income
     */
    public function setFilial(\Nitra\FilialBundle\Entity\Filial $filial = null)
    {
        $this->filial = $filial;

        return $this;
    }

    /**
     * Get filial
     *
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getFilial()
    {
        return $this->filial;
    }
 
}

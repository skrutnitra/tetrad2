<?php
namespace Nitra\IncomeBundle\Controller\Income;

use Admingenerated\NitraIncomeBundle\BaseIncomeController\ListController as BaseListController;

class ListController extends BaseListController
{
    
    /**
     * processFilters
     * выполнить фильтр 
     * @param Doctrine\ORM\QueryBuilder $query объект запроса
     */
    protected function processFilters($query)
    {
        
        // фильтр родителя
        parent::processFilters($query);
        
        // получить объект сессий
        $session = $this->get('session');
        
        // проверить глобальный фильтр отображать филиалы пользователя
        if ($session->has('myFilialsDisplay') && $session->get('myFilialsDisplay')) {
            
            // получить объект фильтров
            $filterObject = $this->getFilters();
            
            // получить объект запроса
            $queryFilter = $this->get('admingenerator.queryfilter.doctrine');
            $queryFilter->setQuery($query);
            
            // добавить фильтр к запросу
            $queryFilter->addCollectionFilter('filial', $session->get('myFilialsDisplay'));
        }
        
    }
    
}

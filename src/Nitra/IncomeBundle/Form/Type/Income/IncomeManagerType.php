<?php
namespace Nitra\IncomeBundle\Form\Type\Income;

use Admingenerated\NitraIncomeBundle\Form\BaseIncomeType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class IncomeManagerType extends BaseNewType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // родитель создать форму
        parent::buildForm($builder, $options);
        
        $formOptions = $this->getFormOption('amount', array( 'precision' => 2,  'required' => true,  'label' => 'Сумма',  'translation_domain' => 'Admin',));
        $builder->add('amount', 'number', $formOptions);
        
        // для пользователя определен филиал по умолчанию 
        if ($options['session']->has('myFilialDefault') && null !== $options['session']->get('myFilialDefault')) {
            // получить филиал по умолчанию для пользователя
            $filialDefault = $options['em']->getRepository('NitraFilialBundle:Filial')->find($options['session']->get('myFilialDefault'));
            // установить приходу филиал пользователя по умолчанию 
            $options['data']->setFilial($filialDefault);
        }
        
        // для пользователя установлены филиалы 
        // переопределить виджет филиала
        // для оприходования денег по заказу доступны только те филиалы 
        // которые связаны с пользователейм
        if ($options['manager']->getFilials()->isEmpty() !== true) {
            
            // массив id филиалов пользователя
            $filialIds = array();
            foreach($options['manager']->getFilials() as $filial) {
                $filialIds[] = $filial->getId();
            }
            
           // виджет филиал
           $formOptions = $this->getFormOption('filial', array(
               'em' => 'default',
               'class' => 'Nitra\\FilialBundle\\Entity\\Filial',  
               'query_builder' => function(EntityRepository $er) use ($filialIds) {
                    $query = $er->createQueryBuilder('f')
                        ->where('f.id IN(:ids)')
                        ->setParameter('ids', $filialIds)
                        ;
                    // вернуть запрос 
                    return $query;
               },
               'multiple' => false,
               'required' => true,
               'property' => 'name',
               'label' => 'Филиал', 'translation_domain' => 'Admin',));
           $builder->add('filial', 'entity', $formOptions);
           
        }
        
    }
    
    /**
     * setDefaultOptions
     * установить значения формы по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\IncomeBundle\Entity\Income',
        ));
        
        $resolver->setRequired(array(
            'em',
            'manager',
            'session',
        ));
        
        $resolver->setAllowedTypes(array(
            'em' => 'Doctrine\ORM\EntityManager',
            'manager' => 'Nitra\ManagerBundle\Entity\Manager',
            'session' => 'Symfony\Component\HttpFoundation\Session\Session',
        ));        
        
    }
    
}

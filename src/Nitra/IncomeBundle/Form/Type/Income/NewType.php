<?php
namespace Nitra\IncomeBundle\Form\Type\Income;

use Nitra\IncomeBundle\Form\Type\Income\IncomeManagerType as IncomeManagerType;

class NewType extends IncomeManagerType
{
    
    public function getName()
    {
        return 'new_income';
    }
    
}

<?php
namespace Nitra\IncomeBundle\Form\Type\Income;

use Nitra\IncomeBundle\Form\Type\Income\NewType as NewType;

class EditType extends NewType
{
    
    public function getName()
    {
        return 'edit_income';
    }
    
}

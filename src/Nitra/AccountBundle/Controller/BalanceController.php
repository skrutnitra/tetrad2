<?php
namespace Nitra\AccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\AccountBundle\Entity\Account;

/**
 * Description of BalanceController
 *
 * @Route("/account-balance")
 */
class BalanceController extends Controller
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**
     * Балансы по счетам (отображения всех счетов)
     * @Route("/", name="suppliers_accounts")
     * @Template()
    */
    public function listAction()
    {
        $base_currency = $this->container->getParameter('currency_symbol');
        
        $accounts = $this->em->getRepository('NitraAccountBundle:Account')->findAll();
        $balances = array();
        $date = new \DateTime();

        // получение балансов на текущий день для каждого счета
        foreach($accounts as $account) {
            
            $total = 0;
            
            // Все расходы
            $expense = $this->em
                    ->createQueryBuilder()
                    ->select('SUM(e.amount)')
                    ->from('NitraExpenseBundle:Expense', 'e')
                    ->where('e.date <= :date')
                    ->andWhere('e.account = :account')
                    ->setParameters(array(
                        'account' => $account,
                        'date' => $date->format('Y-m-d'),
                    ))
                    ->getQuery()
                    ->getSingleScalarResult();

            // Все поступления
            $income = $this->em
                    ->createQueryBuilder()
                    ->select('SUM(i.amount)')
                    ->from('NitraIncomeBundle:Income', 'i')
                    ->where('i.date <= :date')
                    ->andWhere('i.account = :account')
                    ->setParameters(array(
                        'account' => $account,
                        'date' => $date->format('Y-m-d'),
                    ))
                    ->getQuery()
                    ->getSingleScalarResult();
            
            $total = $income - $expense;
            
            // Переводы средств
            $cashflowsQB = $this->em->createQueryBuilder();
            
            $cashflows = $cashflowsQB
                ->select('c, f, t')
                ->from('NitraAccountBundle:Cashflow', 'c')
                ->innerJoin('c.fromAccount', 'f')
                ->innerJoin('c.toAccount', 't')
                ->where('c.date <= :date')
                ->andWhere($cashflowsQB->expr()->orX('c.fromAccount = :fromAccount', 'c.toAccount = :toAccount'))
                ->setParameters(array(
                    'fromAccount' => $account,
                    'toAccount' => $account,
                    'date' => $date->format('Y-m-d'),
                ))
                ->getQuery()
                ->getResult();
            
            foreach ($cashflows as $cashflow) {
                // Если переводили со счета
                if ($account == $cashflow->getFromAccount()) {
                    $total -= $cashflow->getAmount();
                }

                // Если переводили на счет
                if ($account == $cashflow->getToAccount()) {
                    $total += $cashflow->getAmount();
                }
            }
            
            // Баланс на текущий день
            $balances[$account->getId()] = $total;
        
        }

        return array(
            'base_currency' => $base_currency,
            'accounts' => $accounts,
            'balances' => $balances,
        );
    }
    
    /**
     * Просмотр баланса по выбранному счету
     * @Route("/{accountId}", name="supplier_balance")
     * @ParamConverter("account", class="NitraAccountBundle:Account", options={"id" = "accountId"})
     * @Template()
    */
    public function showBalanceAction(Account $account) 
    {
        $base_currency = $this->container->getParameter('currency_symbol');
        // Значения по умолчанию
        $from = new \DateTime('first day of this month');
        $to = new \DateTime;
        $res = array();
        $totalExpenses = 0;
        $totalIncomes = 0;

        // Фильтр по дате
        $form = $this->createFormBuilder(null)
                ->add('period', 'date_range', array(
                    'label' => 'Период',
                    'format' => 'd MMM y'))
                ->getForm();

        // Обработка фильтра
        if ($this->getRequest()->getMethod() == 'POST') {
            $form->bind($this->getRequest());
            $data = $form->getData();
            $from = $data['period']['from'];
            $to = $data['period']['to'];
        } else {
            // значения по умолчанию
            $form->setData(array('period' => array('from' => $from, 'to' => $to)));
        }

        $qbi = $this->em->createQueryBuilder();
        $qbe = $this->em->createQueryBuilder();
        $qbeBefore = $this->em->createQueryBuilder();
        $qbiBefore = $this->em->createQueryBuilder();
        $qbCf = $this->em->createQueryBuilder();

        // Расходы за период
        $qbe->select('e, a')
                ->from('NitraExpenseBundle:Expense', 'e')
                ->innerJoin('e.account', 'a')
                ->where($qbe->expr()->between('e.date', ':from', ':to'))
                ->andWhere('e.account = :account')
                ->setParameters(array(
                    'account' => $account,
                    'from' => $from->format('Y-m-d'),
                    'to' => $to->format('Y-m-d'),
                ));

        // Поступления за период
        $qbi->select('i, a')
                ->from('NitraIncomeBundle:Income', 'i')
                ->innerJoin('i.account', 'a')
                ->where($qbi->expr()->between('i.date', ':from', ':to'))
                ->andWhere('i.account = :account')
                ->setParameters(array(
                    'account' => $account,
                    'from' => $from->format('Y-m-d'),
                    'to' => $to->format('Y-m-d'),
                ));

        // Переводы за период
        $qbCf->select('c, f, t')
                ->from('NitraAccountBundle:Cashflow', 'c')
                ->innerJoin('c.fromAccount', 'f')
                ->innerJoin('c.toAccount', 't')
                ->where($qbCf->expr()->between('c.date', ':from', ':to'))
                ->andWhere($qbCf->expr()->orX('c.fromAccount = :fromAccount', 'c.toAccount = :toAccount'))
                ->setParameters(array(
                    'fromAccount' => $account,
                    'toAccount' => $account,
                    'from' => $from->format('Y-m-d'),
                    'to' => $to->format('Y-m-d'),
                ));

        // Расчет баланса на начальную дату
        // Все расходы
        $qbeBefore->select('SUM(e.amount)')
                ->from('NitraExpenseBundle:Expense', 'e')
                ->where('e.date <= :from')
                ->andWhere('e.account = :account')
                ->setParameters(array(
                    'account' => $account,
                    'from' => $from->format('Y-m-d'),
                ));
        
        // Все поступления
        $qbiBefore->select('SUM(i.amount)')
                ->from('NitraIncomeBundle:Income', 'i')
                ->where('i.date <= :from')
                ->andWhere('i.account = :account')
                ->setParameters(array(
                    'account' => $account,
                    'from' => $from->format('Y-m-d'),
                ));

        // Баланс на начало периода
        $balanceBefore = $qbiBefore->getQuery()->getSingleScalarResult() - $qbeBefore->getQuery()->getSingleScalarResult();

        $expenses = $qbe->getQuery()->getResult();
        foreach ($expenses as $expense) {
            $res[] = array(
                'date' => $expense->getDate(),
                'account' => (string) $expense->getAccount(),
                'type' => 'expense',
                'description' => (string) $expense,
                'amount' => $expense->getAmount()
            );

            $totalExpenses += $expense->getAmount();
        }

        $cashflows = $qbCf->getQuery()->getResult();
        foreach ($cashflows as $cashflow) {
            $res[] = array(
                'date' => $cashflow->getDate(),
                'account' => 'C счета ' . $cashflow->getFromAccount() . ' на счет ' . $cashflow->getToAccount(),
                'type' => $account == $cashflow->getFromAccount()?'cashflow_exp':'cashflow_inc',
                'description' => (string) $cashflow,
                'amount' => $cashflow->getAmount()
            );

            // Если переводили со счета
            if ($account == $cashflow->getFromAccount()) {
                $totalExpenses += $cashflow->getAmount();
            }

            // Если переводили на счет
            if ($account == $cashflow->getToAccount()) {
                $totalIncomes += $cashflow->getAmount();
            }
        }

        $incomes = $qbi->getQuery()->getResult();
        foreach ($incomes as $income) {
            $res[] = array(
                'date' => $income->getDate(),
                'account' => (string) $income->getAccount(),
                'type' => 'income',
                'description' => (string) $income,
                'amount' => $income->getAmount(),
                'order' => $income->getOrder()
            );

            $totalIncomes += $income->getAmount();
        }

        uasort($res, array($this,"cmp"));

        // Сортировка по дате!!!
        return array(
            'form' => $form->createView(),
            'res' => $res,
            'balanceBefore' => $balanceBefore,
            'balanceAfter' => $balanceBefore + $totalIncomes - $totalExpenses,
            'totalIncomes' => $totalIncomes,
            'totalExpenses' => $totalExpenses,
            'base_currency'=>$base_currency,
            'accountName'=>$account->getName());
    }

    // Функция сравнения
    public function cmp($a, $b)
    {
        $a = date_format($a['date'], 'Y-m-d H:i:s');
        $b = date_format($b['date'], 'Y-m-d H:i:s');

        if ($a == $b) {
            return 0;
        }

        return ($a < $b) ? -1 : 1;
    }

}
?>
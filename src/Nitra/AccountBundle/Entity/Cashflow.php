<?php

namespace Nitra\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\AccountBundle\Entity\Cashflow
 *
 * @ORM\Table(name="account_cashflow")
 * @ORM\Entity
 */
class Cashflow
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\AccountBundle\Entity\Account", inversedBy="fromCashflows")
     * @ORM\JoinColumn(name="fromAccount_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Type("Nitra\AccountBundle\Entity\Account")
     */
    protected $fromAccount;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\AccountBundle\Entity\Account", inversedBy="toCashflows")
     * @ORM\JoinColumn(name="toAccount_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Type("Nitra\AccountBundle\Entity\Account")
     */
    protected $toAccount;

    /**
     * @var decimal $amount
     *
     * @ORM\Column(name="amount", type="decimal", precision=8, scale=2)
     *
     * @Assert\NotBlank
     * @Assert\Range(min=0.01, max=999999.99)
     */
    private $amount;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     *
     * @Assert\Length(max=255)
     */
    private $comment;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     *
     * @Assert\Date()
     */
    private $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function __toString()
    {
        $formatter = \IntlDateFormatter::create(
                        \Locale::getDefault(), \IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE
        );
        $formatter->setPattern("d MMMM y");

        return 'Перевод на сумму ' . $this->getAmount() . ' от ' . $formatter->format($this->getDate()) . ' с счета ' . $this->getFromAccount() . ' на счет ' . $this->getToAccount() . ($this->getComment() ? ' (' . $this->getComment() . ')' : '');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param  decimal  $amount
     * @return Cashflow
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return decimal
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set comment
     *
     * @param  string   $comment
     * @return Cashflow
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set fromAccount
     *
     * @param  Nitra\AccountBundle\Entity\Account $fromAccount
     * @return Cashflow
     */
    public function setFromAccount(\Nitra\AccountBundle\Entity\Account $fromAccount)
    {
        $this->fromAccount = $fromAccount;

        return $this;
    }

    /**
     * Get fromAccount
     *
     * @return Nitra\AccountBundle\Entity\Account
     */
    public function getFromAccount()
    {
        return $this->fromAccount;
    }

    /**
     * Set toAccount
     *
     * @param  Nitra\AccountBundle\Entity\Account $toAccount
     * @return Cashflow
     */
    public function setToAccount(\Nitra\AccountBundle\Entity\Account $toAccount)
    {
        $this->toAccount = $toAccount;

        return $this;
    }

    /**
     * Get toAccount
     *
     * @return Nitra\AccountBundle\Entity\Account
     */
    public function getToAccount()
    {
        return $this->toAccount;
    }

    /**
     * Set date
     *
     * @param  date     $date
     * @return Cashflow
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

 
}

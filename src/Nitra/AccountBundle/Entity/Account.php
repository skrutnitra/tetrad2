<?php

namespace Nitra\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="name", message="Счет с таким названием уже существует")
 */
class Account
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=64)
     * 
     * @Assert\Length(max="64")
     * @Assert\NotBlank
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * 
     * @Assert\Length(max="255") 
     */
    private $requisites;
    
    /**
     * @ORM\OneToMany(targetEntity="Nitra\IncomeBundle\Entity\Income", mappedBy="account")
     */
    protected $incomes;
    
    /**
     * @ORM\OneToMany(targetEntity="Nitra\AccountBundle\Entity\Cashflow", mappedBy="fromAccount")
     */
    protected $fromCashflows;

    /**
     * @ORM\OneToMany(targetEntity="Nitra\AccountBundle\Entity\Cashflow", mappedBy="toAccount")
     */
    protected $toCashflows;
    
    /**
     * @ORM\OneToMany(targetEntity="Nitra\ExpenseBundle\Entity\Expense", mappedBy="account")
     */
    protected $expenses;
    
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set requisites
     *
     * @param string $requisites
     * @return Account
     */
    public function setRequisites($requisites)
    {
        $this->requisites = $requisites;

        return $this;
    }

    /**
     * Get requisites
     *
     * @return string 
     */
    public function getRequisites()
    {
        return $this->requisites;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->incomes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add incomes
     *
     * @param \Nitra\IncomeBundle\Entity\Income $incomes
     * @return Account
     */
    public function addIncome(\Nitra\IncomeBundle\Entity\Income $incomes)
    {
        $this->incomes[] = $incomes;

        return $this;
    }

    /**
     * Remove incomes
     *
     * @param \Nitra\IncomeBundle\Entity\Income $incomes
     */
    public function removeIncome(\Nitra\IncomeBundle\Entity\Income $incomes)
    {
        $this->incomes->removeElement($incomes);
    }

    /**
     * Get incomes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIncomes()
    {
        return $this->incomes;
    }
  

    /**
     * Add fromCashflows
     *
     * @param \Nitra\AccountBundle\Entity\Cashflow $fromCashflows
     * @return Account
     */
    public function addFromCashflow(\Nitra\AccountBundle\Entity\Cashflow $fromCashflows)
    {
        $this->fromCashflows[] = $fromCashflows;

        return $this;
    }

    /**
     * Remove fromCashflows
     *
     * @param \Nitra\AccountBundle\Entity\Cashflow $fromCashflows
     */
    public function removeFromCashflow(\Nitra\AccountBundle\Entity\Cashflow $fromCashflows)
    {
        $this->fromCashflows->removeElement($fromCashflows);
    }

    /**
     * Get fromCashflows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFromCashflows()
    {
        return $this->fromCashflows;
    }

    /**
     * Add toCashflows
     *
     * @param \Nitra\AccountBundle\Entity\Cashflow $toCashflows
     * @return Account
     */
    public function addToCashflow(\Nitra\AccountBundle\Entity\Cashflow $toCashflows)
    {
        $this->toCashflows[] = $toCashflows;

        return $this;
    }

    /**
     * Remove toCashflows
     *
     * @param \Nitra\AccountBundle\Entity\Cashflow $toCashflows
     */
    public function removeToCashflow(\Nitra\AccountBundle\Entity\Cashflow $toCashflows)
    {
        $this->toCashflows->removeElement($toCashflows);
    }

    /**
     * Get toCashflows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getToCashflows()
    {
        return $this->toCashflows;
    }

    /**
     * Add expenses
     *
     * @param \Nitra\ExpenseBundle\Entity\Expense $expenses
     * @return Account
     */
    public function addExpense(\Nitra\ExpenseBundle\Entity\Expense $expenses)
    {
        $this->expenses[] = $expenses;

        return $this;
    }

    /**
     * Remove expenses
     *
     * @param \Nitra\ExpenseBundle\Entity\Expense $expenses
     */
    public function removeExpense(\Nitra\ExpenseBundle\Entity\Expense $expenses)
    {
        $this->expenses->removeElement($expenses);
    }

    /**
     * Get expenses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExpenses()
    {
        return $this->expenses;
    }
}

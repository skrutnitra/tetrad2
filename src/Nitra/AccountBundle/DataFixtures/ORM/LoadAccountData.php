<?php

namespace Nitra\AccountBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Nitra\AccountBundle\Entity\Account;
use Doctrine\Common\DataFixtures\AbstractFixture;

class LoadAccountData extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        // счет для оплат LiqPay
        $liqPayAccount = new Account();
        $liqPayAccount->setName("LiqPay");
        $liqPayAccount->setRequisites("Счет для оплат онлайн через сервис LiqPay");
        $manager->persist($liqPayAccount);

        $Account = new Account();
        $Account->setName("Инвестиции");
        $Account->setRequisites("Счет 7777 7777 7777");
        $manager->persist($Account);
        // запомнить
        $this->addReference('account', $Account);

        $Account1 = new Account();
        $Account1->setName("Счет1");
        $Account1->setRequisites("реквизиты счета 1");
        $manager->persist($Account1);

        $Account2 = new Account();
        $Account2->setName("Счет2");
        $Account2->setRequisites("реквизиты счета 2");
        $manager->persist($Account2);

        $Account3 = new Account();
        $Account3->setName("Счет3");
        $Account3->setRequisites("реквизиты счета 3");
        $manager->persist($Account3);

        $manager->flush();
    }
}
<?php

namespace Nitra\TetradkaGeoBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadRegionData
 */
class LoadRegionData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var Symfony\Component\DependencyInjection\Container
     */
    protected $container;
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // получить филиал
        $filial1 = $this->getReference('filial1');
        
        // сущность 
        $entityClass = $this->container->getParameter('nitra_geo.entity.region');
        
        // создаем регион
        $region1 = new $entityClass();
        $region1->setName('TetradkaRegion1');
        $region1->setFilial($filial1);
        $this->setReference('TetradkaRegion1', $region1);
        $manager->persist($region1);
        
        // создаем регион
        $region2 = new $entityClass();
        $region2->setName('TetradkaRegion2');
        $region2->setFilial($filial1);
        $this->setReference('TetradkaRegion2', $region2);
        $manager->persist($region2);
        
        // создаем регион
        $region3 = new $entityClass();
        $region3->setName('TetradkaRegion3');
        $region3->setFilial($filial1);
        $this->setReference('TetradkaRegion3', $region3);
        $manager->persist($region3);
        
        // сохранить
        $manager->flush();
    }
    
    
}


<?php

namespace Nitra\TetradkaGeoBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadCityData
 */
class LoadCityData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var Symfony\Component\DependencyInjection\Container
     */
    protected $container;
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // сущность 
        $entityClass = $this->container->getParameter('nitra_geo.entity.city');
        
        // создаем город
        $city1 = new $entityClass();
        $city1->setName('TetradkaCity1');
        $city1->setRegion($this->getReference('TetradkaRegion1'));
        $this->setReference('TetradkaCity1', $city1);
        $manager->persist($city1);
        
        // создаем город
        $city2 = new $entityClass();
        $city2->setName('TetradkaCity2');
        $city2->setRegion($this->getReference('TetradkaRegion2'));
        $this->setReference('TetradkaCity2', $city2);
        $manager->persist($city2);
        
        // создаем город
        $city3 = new $entityClass();
        $city3->setName('TetradkaCity3');
        $city3->setRegion($this->getReference('TetradkaRegion3'));
        $this->setReference('TetradkaCity3', $city3);
        $manager->persist($city3);
        
        // сохранить
        $manager->flush();
    }
    
}

<?php

namespace Nitra\TetradkaGeoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NitraTetradkaGeoBundle extends Bundle
{
    
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'NitraGeoBundle';
    }
    
}

<?php
namespace Nitra\TetradkaGeoBundle\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\GeoBundle\Command\DeliverySyncGeoCommand;

/**
 * DeliverySyncGeoCommand
 * Синхронизация географии 
 */
class TetradkaDeliverySyncGeoCommand extends DeliverySyncGeoCommand
{
    
    /**
     * @var \Nitra\FilialBundle\Entity\Filial
     * Филиал
     */
    protected $filial;
    
    /**
     * Получить филиала
     * @return \Nitra\FilialBundle\Entity\Filial
     */
    public function getFilial()
    {
        return $this->filial;
    }
    
    /**
     * Настройка команды
     */
    protected function configure()
    {
        // выполнить настройку родителем
        parent::configure();
        
        // установить параметры еоманды
        $this
            // обновить имя дочерней команды
            ->setName('nitra:tetradka:geo:sync')
            // добавить в команду обязательный параметр 
            ->addOption('filialId', null, InputOption::VALUE_REQUIRED, 'ID филиала по умолчанию для региона.')
            // обновить описание команды
            ->setDescription('Синхронизировать географию тетрадки.');
    }
    
    /**
     * {@inheritDoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // выполнить инициализацию родителем
        parent::initialize($input, $output);
        
        // Получить филиал
        $this->filial = $this->getEntityManager()
            ->getRepository('NitraFilialBundle:Filial')
            ->find($input->getOption('filialId'));
        if (!$this->filial) {
            throw new \Exception('Не найден филиал по указанному параметру filialId: '.$input->getOption('filialId'));
        }
    }
    
    /**
     * выполнить синхронизацию регионов
     * @param stdClass $dsRegions - массив регионов DeliverySync
     * @param OutputInterface $output
     */
    protected function processSyncRegions(\stdClass $dsRegions, OutputInterface $output)
    {
        
        // получить прогресс
        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, count((array)$dsRegions));
        
        // получить все регионы тетрадки
        $tetradkaRegions = $this->getTetradkaRegions();
        
        // обойти массив регионов
        foreach($dsRegions as $dsRegion) {
            
            // проверить существует ли регион в регионах тетрадки
            $businessKey = $dsRegion->id;
            if (in_array($businessKey, array_keys($tetradkaRegions))) {
                // удаляем из массива регионов 
                // оставшиеся регионы в массиве будут удалены
                unset($tetradkaRegions[$businessKey]);
                
            } else {
                // в тетрадке нет региона
                // добавить новый регион
                $entityClass = $this->getRegionClass();
                $region = new $entityClass;
                $region->setName($dsRegion->name);
                $region->setBusinessKey($businessKey);
                $region->setFilial($this->getFilial());
                
                // запомнить для сохранения
                $this->getEntityManager()->persist($region);
            }
            
            // обновить прогресс
            $progress->advance();
        }
        
        // регионы не пришли в синхронизации 
        if ($tetradkaRegions) {
            // получить удаляемые регионы
            // если удаляем через createQueryBuilder()->delete()
            // то не срабатывет SoftDeletable, запись удаялется физически из БД
            $regionsDelete = $this->getEntityManager()
                ->getRepository($this->getRegionClass())
                ->createQueryBuilder('region')
                ->where('region.businessKey IN(:ids)')->setParameter('ids', array_keys($tetradkaRegions))
                ->getQuery()
                ->execute();
            // удалить города которые не пришли в синхронизации 
            foreach($regionsDelete as $region) {
                $this->getEntityManager()->remove($region);
            }
        }
        
        // сохранить филиалы отдельно от городов, если таковые имеются
        $this->getEntityManager()->flush();
        
        // Синхронизация завершена
        $output->write(' ');
        $output->write('Синхронизация регионов завершена успешно.');
        // завершить прогресс
        $progress->finish();
    }
    
}
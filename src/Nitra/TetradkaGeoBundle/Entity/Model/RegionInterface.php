<?php

namespace Nitra\TetradkaGeoBundle\Entity\Model;

/**
 * RegionInterface
 */
interface RegionInterface extends \Nitra\GeoBundle\Entity\Model\RegionInterface
{
    
    /**
     * Get filial
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getFilial();
    
}

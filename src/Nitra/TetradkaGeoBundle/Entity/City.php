<?php

namespace Nitra\TetradkaGeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nitra\GeoBundle\Entity\City as BaseCity;

/**
 * Nitra\TetradkaGeoBundle\Entity\City
 * @ORM\Entity()
 */
class City extends BaseCity
{
    
}
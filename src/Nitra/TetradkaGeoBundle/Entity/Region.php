<?php

namespace Nitra\TetradkaGeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Nitra\GeoBundle\Entity\Region as BaseRegion;

/**
 * Nitra\TetradkaGeoBundle\Entity\Region
 * @ORM\Entity()
 */
class Region extends BaseRegion implements Model\RegionInterface
{
    
    /**
     * @ORM\ManyToOne(targetEntity="Nitra\FilialBundle\Entity\Filial")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotBlank(message="Не указан филиал")
     */
    protected $filial;
    
     /**
     * Set filial
     * @param \Nitra\FilialBundle\Entity\Filial $filial
     * @return Region
     */
    public function setFilial(\Nitra\FilialBundle\Entity\Filial $filial)
    {
        $this->filial = $filial;

        return $this;
    }

    /**
     * Get filial
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getFilial()
    {
        return $this->filial;
    }
    
}

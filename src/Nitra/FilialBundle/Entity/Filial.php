<?php

namespace Nitra\FilialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\FilialBundle\Entity\Filial
 * @ORM\Table(name="filial")
 * @ORM\Entity
 * @UniqueEntity(fields="name", message="Филиал с таким названием уже существует")
 */
class Filial
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer $id
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     * @Assert\Length(max="100")
     */
    private $name;

    /**
     * @var string $phone
     * @ORM\Column(type="string", nullable=true, length=45)
     * @Assert\Length(max="45")
     */
    private $phone;
    
    /**
     * @var string $address
     * @ORM\Column(type="string", nullable=true, length=120)
     * @Assert\Length(max="120")
     */
    private $address;

    /**
     * @var string $worktime
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Assert\Length(max="255")
     */
    private $worktime;
    
    /**
     * @var string $storeId
     * @ORM\Column(name="store_id", type="string", length=24)
     * @Assert\NotBlank(message="Не указан магазин для филиала")
     * @Assert\Length(max="24")
     */
    private $storeId;
    
    /**
     * склад филиала
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Warehouse")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Type(type="Nitra\MainBundle\Entity\Warehouse")
     */
    private $warehouse;
    
    /**
     * object to string
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Телефон 
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param text $juridicalAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Юридический адрес
     *
     * @return text 
     */
    public function getAddress()
    {
        return $this->address;
    }
   
    /**
     * Set worktime
     *
     * @param string $worktime
     */
    public function setWorktime($worktime)
    {
        $this->worktime = $worktime;
    }

    /**
     * Рабочее время
     *
     * @return string 
     */
    public function getWorktime()
    {
        return $this->worktime;
    }
    
    /**
     * Set storeId
     *
     * @param string $storeId
     * @return Filial
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
    
        return $this;
    }

    /**
     * Get storeId
     * @return string 
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * Set warehouse
     *
     * @param \Nitra\MainBundle\Entity\Warehouse $warehouse
     * @return Filial
     */
    public function setWarehouse(\Nitra\MainBundle\Entity\Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
    
        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \Nitra\MainBundle\Entity\Warehouse 
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    
}
<?php
namespace Nitra\FilialBundle\Form\Type\Filial;

use Admingenerated\NitraFilialBundle\Form\BaseFilialType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Doctrine\ORM\EntityRepository;

class NewType extends BaseNewType
{
    
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;
    
    /**
     * конструктор класса
     * @param \Doctrine\ODM\MongoDB\DocumentManager $documentManager
     */
    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // построить форму родителем
        parent::buildForm($builder, $options);
        
        // удалить склад 
        if ($builder->has('warehouse')) {
            $builder->remove('warehouse');
        }
        
        // виджет склад
        $builder->add('warehouse', 'select_warehouse', array(
            'required' => true,
            'label' => 'Склад',
            'constraints' => array(new Constraints\NotBlank(array('message' => 'Не указан склад.'))),
            'query_builder' => function(EntityRepository $er) {
            // получить запрос свои склады
            $query = $er->buildQueryOwnWarehouses();
            // вернуть запрос 
            return $query;
        },
        ));
        
        // получить магазины 
        $stores = $this->dm->getRepository('NitraMainBundle:Store')->findAll();
        $storesChoices = array();
        foreach($stores as $store) {
            $storesChoices[$store->getId()] = (string)$store;
        }
        // виджет магазин филиала 
        $formOptions = $this->getFormOption('storeId', array(
            'required' => true,
            'choices' => $storesChoices, 
            'label' => 'Магазин', 'translation_domain' => 'Admin',
        ));
        $builder->add('storeId', 'choice', $formOptions);        
        
    }
    
}

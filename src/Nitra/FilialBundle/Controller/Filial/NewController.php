<?php
namespace Nitra\FilialBundle\Controller\Filial;

use Admingenerated\NitraFilialBundle\BaseFilialController\NewController as BaseNewController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\FilialBundle\Form\Type\Filial\NewType;

class NewController extends BaseNewController
{
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * getNewType
     * переопределяем для передачи DocumentManager
     */
    protected function getNewType()
    {
        $type = new NewType($this->dm);
        $type->setSecurityContext($this->get('security.context'));
        return $type;
    }
    
}

<?php
namespace Nitra\FilialBundle\Controller\Filial;

use Admingenerated\NitraFilialBundle\BaseFilialController\EditController as BaseEditController;
use JMS\DiExtraBundle\Annotation as DI;
use Nitra\FilialBundle\Form\Type\Filial\EditType;

class EditController extends BaseEditController
{
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * getEditType
     * переопределяем для передачи DocumentManager
     */
    protected function getEditType()
    {
        $type = new EditType($this->dm);
        $type->setSecurityContext($this->get('security.context'));
        return $type;
    }
    
}

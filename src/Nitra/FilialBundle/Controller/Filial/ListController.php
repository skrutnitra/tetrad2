<?php
namespace Nitra\FilialBundle\Controller\Filial;

use Admingenerated\NitraFilialBundle\BaseFilialController\ListController as BaseListController;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;

class ListController extends BaseListController
{    
    
    /** @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * @var Pagerfanta\Pagerfanta 
     */
    private $paginator;
    
    /**
     * getPager
     * предотвращение повторного выполенения запросов
     */
    protected function getPager()
    {
        
        // проверить инициализацию $this->paginator
        if ($this->paginator !== null) {
            return $this->paginator;
        }
        
        // получить $paginator
        $paginator = new Pagerfanta(new PagerAdapter($this->getQuery()));
        $paginator->setMaxPerPage($this->getPerPage());
        $paginator->setCurrentPage($this->getPage(), false, true);
        
        // инициализировать $paginator для $this
        // для предотвращения выполнения повторныз запросов 
        // при вызове getAdditionalRenderParameters
        $this->paginator = $paginator;
        
        // вернуть $paginator
        return $this->paginator;
    }
    
    
    /**
     * Get additional parameters for rendering.
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * return array
     **/
    protected function getAdditionalRenderParameters()
    {
        
        // массив ID магазинов выборки
        $storeIds = array();
        foreach($this->getPager() as $filial) {
            $storeIds[] = $filial->getStoreId();
        }
        
        // получить массив названий магазинов по массиву ID 
        $storeNames = array();
        $storeIds = array_unique($storeIds);
        if ($storeIds) {
            
            // получить магазины
            $stores = $this->dm->createQueryBuilder('NitraMainBundle:Store')
                ->field('id')->in($storeIds)
                ->getQuery()
                ->execute();
            
            // наполнить массив имен продуктов
            if ($stores) {
                foreach($stores as $store) {
                    $storeNames[$store->getId()] = (string)$store;
                }
            }
        }
        
        // вернуть массив параметров передаваемых в шаблон отображения
        return array(
            // массив названий магазинов
            'storeNames' => $storeNames,
        );
    }
    
}

<?php
namespace Nitra\FilialBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Nitra\FilialBundle\Entity\Filial;

/**
 * LoadFilialData
 */
class LoadFilialData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface
     */
    protected $container;
    
    /**
     * @var array массив Id магазинов
     */
    protected $storeIds;
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }
    
    /**
     * получить случайный ID магазина
     * @return integer | null
     */
    public function getRandStoreId()
    {
        // получить массив ID магазинов
        if ($this->storeIds === null) {
            // получить DocumentManager
            $dm = $this->container->get('doctrine_mongodb')->getManager();
            // наполнить массив ID магазинов
            $stores = $dm->getRepository('NitraMainBundle:Store')->findAll();
            if ($stores) {
                foreach($stores as $store) {
                    $this->storeIds[] = $store->getId();
                }
            }
        }
        
        // нет магазинов
        if (!$this->storeIds) {
            return null;
        }
        
        // получить случайный ID магазина
        $storeKey = array_rand($this->storeIds);
        if ($storeKey !== null) {
            return $this->storeIds[$storeKey];
        }
        
        // нет магазинов
        return null;
    }
    
    
    /**
     * загрузить фикстуры
     */
    public function load(ObjectManager $manager)
    {
        
        $filial1 = new Filial();
        $filial1->setName("Филиал 1 Харьков");
        $filial1->setPhone("111-11-11");
        $filial1->setAddress("Сумская 1");
        $filial1->setWorktime("Пн-Вс 9:00-20:00");
        $filial1->setStoreId($this->getRandStoreId());
        $manager->persist($filial1);
        
        $filial2 = new Filial();
        $filial2->setName("Филиал 2");
        $filial2->setPhone("222-22-22");
        $filial2->setAddress("Название улицы 2");
        $filial2->setWorktime("Пн-Вс 9:00-20:00");
        $filial2->setStoreId($this->getRandStoreId());
        $manager->persist($filial2);
        
        $filial3 = new Filial();
        $filial3->setName("Филиал 3");
        $filial3->setPhone("333-33-33");
        $filial3->setAddress("Название улицы 3");
        $filial3->setWorktime("Пн-Вс 9:00-20:00");
        $filial3->setStoreId($this->getRandStoreId());
        $manager->persist($filial3);
        
        $filial4 = new Filial();
        $filial4->setName("Филиал 4");
        $filial4->setPhone("444-44-44");
        $filial4->setAddress("Название улицы 4");
        $filial4->setWorktime("Пн-Вс 9:00-20:00");
        $filial4->setStoreId($this->getRandStoreId());
        $manager->persist($filial4);
        
        // сохранить 
        $manager->flush();
        
        // запомнить
        $this->addReference('filial1', $filial1);
        $this->addReference('filial2', $filial2);
        $this->addReference('filial3', $filial3);
        $this->addReference('filial4', $filial4);

    }
    
    public function getOrder()
    {
        return 3;
    }
}
<?php
namespace Nitra\ContractBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nitra\MainBundle\Entity\Supplier;

/**
 * ContractRepository
 */
class ContractRepository extends EntityRepository
{
    
    /**
     * получить запрос 
     * контракты для поставщика
     * @param Supplier $supplier - поставщик для которого получаем контракты     
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQuerySupplierContracts(Supplier $supplier)
    {
        return $this->createQueryBuilder('q')
            ->where('q.supplier = :supplier')
            ->setParameter('supplier', $supplier)
            ;
    }
    
}

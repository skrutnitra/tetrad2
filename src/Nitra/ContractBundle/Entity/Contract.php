<?php
namespace Nitra\ContractBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="contract")
 * @ORM\Entity(repositoryClass="Nitra\ContractBundle\Repository\ContractRepository")
 * @UniqueEntity(fields={"supplier", "filial"}, message="Контракт уже существует")
 * @UniqueEntity(fields="name", message="Контракт с таким именем уже существует")
 */
class Contract
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

     use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", nullable=false, length=255 )
     * @Assert\NotBlank(message="Не указано название")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\MainBundle\Entity\Supplier")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан поставщик")
     */
    private $supplier;

    /**
     * @ORM\ManyToOne(targetEntity="Nitra\FilialBundle\Entity\Filial")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан филиал")
     */
    private $filial;

    /**
     * @var decimal - Стартовый баланс по договору
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\Length(min=0, max=999999.99)
     */
    private $startBalance;
    
    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->startBalance = 0;
    }
    
    /**
     * obj to string
     * @return string
     */
    public function __toString()
    {
        return "Контракт №".$this->id.": ".$this->name;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startBalance
     *
     * @param float $startBalance
     * @return Contract
     */
    public function setStartBalance($startBalance)
    {
        $this->startBalance = $startBalance;

        return $this;
    }

    /**
     * Get startBalance
     *
     * @return float 
     */
    public function getStartBalance()
    {
        return $this->startBalance;
    }

    /**
     * Set supplier
     *
     * @param \Nitra\MainBundle\Entity\Supplier $supplier
     * @return Contract
     */
    public function setSupplier(\Nitra\MainBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \Nitra\MainBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set filial
     *
     * @param \Nitra\FilialBundle\Entity\Filial $filial
     * @return Contract
     */
    public function setFilial(\Nitra\FilialBundle\Entity\Filial $filial)
    {
        $this->filial = $filial;

        return $this;
    }

    /**
     * Get filial
     *
     * @return \Nitra\FilialBundle\Entity\Filial 
     */
    public function getFilial()
    {
        return $this->filial;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Contract
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    
}

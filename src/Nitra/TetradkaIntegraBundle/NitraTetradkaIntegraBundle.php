<?php

namespace Nitra\TetradkaIntegraBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NitraTetradkaIntegraBundle extends Bundle
{
    
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'NitraIntegraBundle';
    }
    
}

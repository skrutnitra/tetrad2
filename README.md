### PHP-CI ###

[![Build Status](http://ci.nitra.ua/build-status/image/6?branch=master)](http://ci.nitra.ua/project/view/6) 

### Scrutinizer ###

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/nitra/tetradka/badges/quality-score.png?b=master&s=1eb341adba77fb381c2fd04e839d95ef2186e425)](https://scrutinizer-ci.com/b/nitra/tetradka/?branch=master)

[![Code Coverage](https://scrutinizer-ci.com/b/nitra/tetradka/badges/coverage.png?b=master&s=e6bbd42c01b6adbf03e5d440835b3a158b3e1f58)](https://scrutinizer-ci.com/b/nitra/tetradka/?branch=master)

[![Build Status](https://scrutinizer-ci.com/b/nitra/tetradka/badges/build.png?b=master&s=bdbfa6bd4aef7434e4e4fac37940cd98e42e6639)](https://scrutinizer-ci.com/b/nitra/tetradka/build-status/master)

### SensioLabsInsight ###

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/7001e78d-9625-4018-82fa-bf257716c909/big.png)](https://insight.sensiolabs.com/projects/7001e78d-9625-4018-82fa-bf257716c909)
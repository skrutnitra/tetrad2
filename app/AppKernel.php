<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new Admingenerator\GeneratorBundle\AdmingeneratorGeneratorBundle(),
            new Millwright\MenuBundle\MillwrightMenuBundle(),
            new Millwright\ConfigurationBundle\MillwrightConfigurationBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Pinano\Select2Bundle\PinanoSelect2Bundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new Nitra\ManagerBundle\NitraManagerBundle(),
            
            // NitraGeoBundle
            new Nitra\TetradkaGeoBundle\NitraTetradkaGeoBundle(),
            new Nitra\GeoBundle\NitraGeoBundle(),
            
            // NitraIntegraBundle
            new Nitra\TetradkaIntegraBundle\NitraTetradkaIntegraBundle(),
            new Nitra\IntegraBundle\NitraIntegraBundle(),            
            
            new Nitra\FilialBundle\NitraFilialBundle(),
            new Nitra\MainBundle\NitraMainBundle(),
            new Nitra\OrderBundle\NitraOrderBundle(),
            new Nitra\AccountBundle\NitraAccountBundle(),
            new Nitra\ExpenseBundle\NitraExpenseBundle(),
            new Nitra\ContractBundle\NitraContractBundle(),
            new Nitra\DeclarationBundle\NitraDeclarationBundle(),
            new Nitra\NlCollectionBundle\NitraNlCollectionBundle(),
            new Nitra\IncomeBundle\NitraIncomeBundle(),
            new Nitra\AnalyticsBundle\NitraAnalyticsBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
